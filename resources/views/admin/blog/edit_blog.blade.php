@extends('admin.master')
@section('title','Home')
@section('content')

   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Cập nhật Blog</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Cập nhật Blog</font>
                  </font>
               </li>
            </ol>
         </div>
         
         <div class="row row-cards">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">
                        <font style="vertical-align: inherit;">
                           <font style="vertical-align: inherit;">Cập nhật Blog</font>
                        </font>
                     </div>
                  </div>
                  {{-- thêm mới --}}
                  @foreach($edit_blog as $key => $edit_bl)
                    <form role="form" action="{{URL::to('/update-blog/'.$edit_bl->id)}}" method="post" 
                     enctype="multipart/form-data">
                    {{ csrf_field()}}
                     <div class="card-body">
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Tên blog</font>
                              </font>
                           </label> 
                           <input type="text" name="name" class="form-control w-100" placeholder="Nhập tên blog" value="{{$edit_bl->name}}">
                           <p class="help is-danger">{{ $errors->first('name') }}</p> 
                        </div>
                        {{-- user_id --}}
                        <div class="form-group">
                           <label name="exampleInputPassword1">User</label>
                           <select name="user_admin" class="form-control m-bot15">
                            @foreach($user as $key => $user_id)
                              @if($user_id->id == $edit_bl->user_id)
                                <option selected  value="{{$user_id->id}}">{{$user_id->firstname}}</option>
                              @else
                                <option value="{{$user_id->id}}">{{$user_id->firstname}}</option>
                              @endif 
                           @endforeach       
                           </select>
                        </div>
                        {{-- danh muc blog --}}
                        <div class="form-group">
                           <label name="exampleInputPassword1">danh mục sản phẩm</label>
                           <select name="category_bl" class="form-control m-bot15">
                            @foreach($category_blog as $key => $category_blog)
                              @if($category_blog->id == $edit_bl->category_blog_id)                   
                                <option selected value="{{$category_blog->id}}">{{$category_blog->name}}</option> 
                              @else
                                <option value="{{$category_blog->id}}">{{$category_blog->name}}</option>
                              @endif
                            @endforeach       
                           </select>
                        </div>
                        {{--tag_id  --}}
                        <div class="form-group">
                           <label name="exampleInputPassword1">tag</label>
                           <select name="tag_id" class="form-control m-bot15">
                              @foreach($tag as $key => $tag)
                                @if($tag->id)
                                  <option selected value="{{$tag->id}}">{{$tag->name}}</option>
                                @else
                                  <option value="{{$tag->id}}">{{$tag->name}}</option>
                                @endif  
                              @endforeach    
                           </select>
                        </div>
                        {{-- hình ảnh sản phẩm --}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Hình ảnh Blog</label>
                            <input type="file" name="img" class="form-control" id="img">
                            <img src="{{URL::to('/img_blog/'.$edit_bl->img)}}" height="90" width="120" style="margin-left: 5px">
                        </div>
                        {{-- mô tả Blog --}}
                        <div class="form-group">
                           <label class="form-label">mô tả Blog</label> 
                           <textarea name="description" id="description" style="resize: none;"rows="8"class="form-control">{{$edit_bl->description}}  
                           </textarea> 
                           <p class="help is-danger">{{ $errors->first('description') }}</p>
                        </div>
                        {{-- nội dung Blog --}}
                        <div class="form-group">
                           <label class="form-label">nội dung Blog</label> 
                           <textarea name="content" id="content" style="resize: none;"rows="8"class="form-control">{{$edit_bl->content}}  
                           </textarea> 
                           <p class="help is-danger">{{ $errors->first('content') }}</p>
                        </div>
                        {{-- hiển thị --}}
                         <div class="form-group">
                           <label name="exampleInputPassword1">Hiển thị</label>
                           <select name="status" class="form-control m-bot15">
                              <option value="1" {{$edit_bl->status == 1 ? "selected" : ""}}>Hiển Thị</option>
                              <option value="0" {{$edit_bl->status == 0 ? "selected" : ""}}>Ẩn</option>       
                           </select>
                        </div>
                        <button type="submit" name="update_blog" 
                        class="btn btn-primary waves-effect waves-light">
                           <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Cập nhật Blog</font>
                           </font>
                        </button> 
                     </div>            
                  </form>
                @endforeach
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--App-Content--> 
@endsection('content')

@section('js')
    <script>
        CKEDITOR.replace('description'); 
        CKEDITOR.replace('content');        
    </script>
@endsection