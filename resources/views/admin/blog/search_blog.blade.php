@extends('admin.master')
@section('title','Home')
@section('content')
   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Danh sách Blog</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Danh sách Blog</font>
                  </font>
               </li>
            </ol>
         </div>
         {{-- nếu cập nhật thành công --}}
         <?php 
             $message = Session::get('message');
             if ($message) {
                 echo '<div class="alert alert-success">'. $message .'</div>';
                 Session::put('message', null);
             }
         ?>
         <div class="row">
            <div class="col-lg-12">
               <div class="card">
                  <div class="card-body">
                     <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('all-blog')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('add-blog')}}" class="btn" aria-expanded="false"> Thêm Mới Blog </a> 
                     </div>
                     {{-- tìm kiếm --}}

                     <form method="post" class="form-inline mr-auto" style="float: right;" action="{{route('search-blog')}}">
                        @csrf
                        <div class="nav-search"> 
                           <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                           <button class="btn btn-primary" style="width: 60px;height: 39px;" type="submit">
                              <i class="fa fa-search"></i>
                           </button> 
                        </div>
                     </form>
                     {{-- bảng blog --}}
                     <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                           <div class="table-responsive border-top">
                              <table class="table card-table table-bordered table-hover table-vcenter text-nowrap">
                                 <tbody>
                                    <tr style="text-align: center;">
                                       {{-- tên blog --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Tên Blog
                                             </font>
                                          </font>
                                       </th>
                                       {{-- Ảnh blog --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Ảnh Blog</font>
                                          </font>
                                       </th>
                                       {{-- Tên danh mục blog --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Tên danh mục blog
                                             </font>
                                          </font>
                                       </th>
                                       {{-- tên user --}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Tên User
                                             </font>
                                          </font>
                                       </th>
                                       {{-- tag --}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Tag
                                             </font>
                                          </font>
                                       </th>
                                       {{-- trạng thái --}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Trạng thái</font>
                                          </font>
                                       </th>
                                       {{-- sửa xóa --}}
                                       <th style="text-align: center;">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;" >Sửa || xóa
                                             </font>
                                          </font>
                                       </th>
                                    </tr>

                                 {{-- phầm blog hiển thị --}}

                                    @foreach($all_blog as $key=> $show_blog)
                                    <tr style="text-align: center;">
                                       {{-- têm blog --}}
                                       <td>
                                          <a href="#" class="btn-link">
                                             <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">
                                                   {{$show_blog->name}}
                                                </font>
                                             </font>
                                          </a> 
                                       </td>
                                       {{-- Hình ảnh Blog --}}
                                       <td style="width: 160px;">
                                          <img src="../img_blog/{{$show_blog->img}}" 
                                       alt="hình ảnh"> 
                                       </td>
                                       {{--Tên danh mục blog  --}}
                                       <td>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                {{$show_blog->category_blog->name}}
                                             </font>
                                          </font>
                                       </td>
                                       {{-- tên user --}}
                                       <td>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                {{$show_blog->user->firstname}}
                                             </font>
                                          </font>
                                       </td>
                                       {{-- tag id --}}
                                       <td>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                @foreach($show_blog->tag as $tag)
                                                   {{$tag->name}}                                               
                                                @endforeach
                                             </font>
                                          </font>
                                       </td>
                                       {{-- trạng thái --}}
                                       <td>
                                          <span>
                                             <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">
                                                   <?php
                                                     if($show_blog->status == 0)
                                                     {
                                                     ?>
                                                     <a class="btn btn-xs btn-danger blog" 
                                                      style="font-size: 18px;width: 70px; height: 30px;padding: 4px;"
                                                      href="{{URL::to('unactive-blog/'.$show_blog->id)}}">
                                                        Ẩn
                                                      </a>
                                                     <?php
                                                     }else{
                                                     ?>
                                                     <a class="btn btn-xs btn-green blog" 
                                                      style="font-size: 18px;width: 120px;height: 30px;padding: 4px;"
                                                      href="{{URL::to('active-blog/'.$show_blog->id)}}">
                                                      Hiển Thị
                                                      </a>
                                                     <?php
                                                     }
                                                   ?> 
                                                </font>
                                             </font>
                                          </span>
                                       </td> 
                                       {{-- sửa xóa --}}
                                       <td class="r" style="text-align: center;width: 160px;">
                                          <a class="btn btn-xs btn-info blog" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          href="{{URL::to('/edit-blog/'.$show_blog->id)}}">
                                             <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
                                          </a>
                                          <a class="btn btn-xs btn-danger blog" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                                          href="{{URL::to('/delete-blog/'.$show_blog->id)}}">
                                             <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                          </a>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>  
                        </div> 
                     </div>
                  </div>
               </div>
               <ul class="pagination mb-5">
               </ul>
            </div>
         </div>
      </div>
   </div>
<!--App-Content--> 
@endsection('content')
