@extends('admin.master')
@section('title','Home')
@section('content')

   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Blog mới</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Blog mới</font>
                  </font>
               </li>
            </ol>
         </div>
         <?php 
              $message = Session::get('message');
              if ($message) {
                  echo '<div class="alert alert-success">'. $message .'</div>';
                  Session::put('message', null);
              }
            ?>
         <div class="row row-cards">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Tạo Blog Mới</font>
                          <a href="{{URL::to('all-blog')}}" class="btn" aria-expanded="false" style="vertical-align: inherit; font-size: 18px;color: #20BAED">Danh Sách Blog</a> 
                        </font>
                     </div>
                  </div>
                  {{-- thêm mới --}}
                  <form role="form" action="{{URL::to('/save-blog')}}" method="post" 
                     enctype="multipart/form-data">
                    {{ csrf_field()}}
                     <div class="card-body">
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Tên blog</font>
                              </font>
                           </label> 
                           <input type="text" name="name" class="form-control w-100" placeholder="Nhập tên blog">
                           <p class="help is-danger">{{ $errors->first('name') }}</p> 
                        </div>
                        {{-- user_id --}}
                        <div class="form-group">
                           <label name="exampleInputPassword1">Tài Khoản</label>
                           <select name="user_admin" class="form-control m-bot15">
                           @foreach($user as $key => $user_id)                   
                               <option value="{{$user_id->id}}">{{$user_id->firstname}}</option>  
                           @endforeach       
                           </select>
                        </div>
                        {{-- category-blog(id của danh mục blog) --}}
                        <div class="form-group">
                           <label name="exampleInputPassword1">Tên danh mục blog</label>
                           <select name="category_bl" class="form-control m-bot15">
                           @foreach($category_blog as $key => $category_blog)                   
                               <option value="{{$category_blog->id}}">{{$category_blog->name}}</option>  
                           @endforeach       
                           </select>
                        </div>
                        {{--tag  --}}

                         <div class="form-group">
                             <label name="exampleInputPassword1">Tag</label>
                             <select name="tag_id" class="form-control m-bot15">
                             @foreach($tag as $key => $tag_id)                   
                                 <option value="{{$tag_id->id}}">{{$tag_id->name}}</option>  
                             @endforeach       
                             </select>
                          </div>
                          
                        {{-- hình ảnh sản phẩm --}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Hình ảnh Blog</label>
                            <input type="file" name="img" class="form-control" id="img">
                            <p class="help is-danger">{{ $errors->first('img') }}</p>
                        </div>
                        {{-- mô tả Blog --}}
                        <div class="form-group">
                           <label class="form-label">mô tả Blog</label> 
                           <textarea name="description" id="description"
                            style="resize: none;"rows="8"class="form-control">
                           </textarea>
                          <p class="help is-danger">{{ $errors->first('description') }}</p>
                        </div>
                        {{-- nội dung Blog --}}
                        <div class="form-group">
                           <label class="form-label">Nội dung Blog</label> 
                           <textarea name="content" id="content"
                            style="resize: none;"rows="8"class="form-control">
                           </textarea>
                          <p class="help is-danger">{{ $errors->first('content') }}</p>
                        </div>
                        {{-- hiển thị --}}
                         <div class="form-group">
                           <label name="exampleInputPassword1">Hiển thị</label>
                           <select name="status" class="form-control m-bot15">
                               <option value="1">Hiển thị</option>
                               <option value="0">Ẩn</option>       
                           </select>
                        </div>
                        <button type="submit" name="add_blog" 
                        class="btn btn-primary waves-effect waves-light">
                           <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Thêm mới blog</font>
                           </font>
                        </button> 
                     </div>  
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--App-Content--> 
@endsection('content')

@section('js')
    <script>
        CKEDITOR.replace('description');     
        CKEDITOR.replace('content');  
    </script>
@endsection