@extends('admin.master')
@section('title','Edit Brand')
@section('content')
<div class="app-content  my-3 my-md-5">
<div class="side-app">
   <div class="page-header">
      <h4 class="page-title">Thương Hiệu</h4>
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="#">Admin</a></li>
         <li class="breadcrumb-item active" aria-current="page">Sửa Thương Hiệu</li>
      </ol>
   </div>
   <div class="row row-cards"> 
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <div class="card-title">Sửa Thương Hiệu</div>
            </div>
            <div class="card-body">
            	<form class="form-horizontal"  action="{{ route('brand.update', ['id' =>$brand->id]) }}" method="post" enctype="multipart/form-data">
                                    @csrf
               <div class="form-group"> 
                  <label class="form-label">Name</label> 
                  @error('name')
                  <small style="color:red">{{$message}}</small>
                  @enderror
               	<input type="text" class="form-control w-100" value="{{$brand->name}}" name="name">
               </div>
              
               <div class="form-group">
                  <label class="form-label"> Images</label> 
                  <div class="custom-file">
                  @error('img')
                  <small style="color:red">{{$message}}</small>
                  @enderror
                  <input type="file" class="custom-file-input" name="img">
                  <label class="custom-file-label">Upload Images</label> 
                 
               </div>
               <img width="20%"src="../img_thuong_hieu/{{$brand->img}}" />
               </div>
              
             
              
            <div class="card-footer"> <button type="submit" class="btn btn-primary waves-effect waves-light">Publish</button> </div>
         </div>
         </form>
      </div>
   </div>
</div>
</div>
@endsection('content')