@extends('admin.master')
@section('title','Thêm Thương Hiệu')
@section('content')
<div class="app-content  my-3 my-md-5">
<div class="side-app">
   <div class="page-header">
      <h4 class="page-title">Thương Hiệu</h4>
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="#">Admin</a></li>
         <li class="breadcrumb-item active" aria-current="page">Tạo thương hiệu</li>
      </ol>
   </div>
   <div class="row row-cards"> 
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <div class="card-title">Tạo Thương Hiệu</div>
            </div>
            <div class="card-body">
            	<form class="form-horizontal" action="{{ route('admin.brand.store') }}" method="post" enctype="multipart/form-data">
                                    @csrf
               <div class="form-group"> 
                  <label class="form-label">Tên thương hiệu</label> 
                  @error('name')
                  <small style="color:red">{{$message}}</small>
                  @enderror
               	<input type="text" class="form-control w-100" placeholder="Enter Title here" name="name" value="{{old('name')}}">
               </div>
              
             
               <div class="form-group">
                  <label class="form-label"> Images</label> 
                  @error('img')
                  <small style="color:red">{{$message}}</small>
                  @enderror
                  <div class="custom-file">
                   <input type="file" class="custom-file-input" name="img"  > 
                   <label class="custom-file-label">Upload Images</label> 
               </div>
               </div>
              
        
            
            <div class="card-footer"> <button type="submit" class="btn btn-primary waves-effect waves-light">Publish</button> </div>
         </div>
         </form>
      </div>
   </div>
</div>
</div>
@endsection