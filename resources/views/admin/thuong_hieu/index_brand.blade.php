@extends('admin.master')
@section('title','Thương Hiệu')
@section('content')
<div class="app-content  my-3 my-md-5">
<div class="side-app">
<div class="page-header">
      <h4 class="page-title">Thương Hiệu</h4>
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="#">Admin</a></li>
         <li class="breadcrumb-item active" aria-current="page">Danh Sách Thương Hiệu</li>
      </ol>
   </div>
  
<div class="row">

     <div class="col-md-12 col-lg-12">
          <div class="card">
 <div class="card-header">
      <h3 class="card-title">Danh Sách Thương Hiệu</h3> 
    </div> 
    <div class="table-responsive"> 
        <table class="table mb-0"> 
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tên thương hiệu</th>
                    <th>Hình ảnh</th>
                    <th>Trạng thái</th>
                </tr> 
            </thead> 
            <tbody>
            @foreach($brand as $brand)
                <tr>
                    <td scope="row">{{$brand->id}}</td>
                    <td>{{$brand->name}}</td>
                    <td><img width="100x" height="auto" src="../img_thuong_hieu/{{$brand->img}}"> </td>
                    <td>
                        <a href="{{ route('admin.brand.edit', ['id' =>$brand->id]) }}" class="btn btn-primary">Sửa</a>
                    </td>
                    <td>
                    <form action="{{ route('brand.destroy', ['brand' =>$brand->id])}}" method="POST">
                        @csrf 
                        <button onclick="return confirm('Xóa Thương hiệu này?')" class="btn btn-danger" type="submit">Xóa</button>
                    
                 </form>
             </td>
                </tr>
               
            </tbody>
            @endforeach
        </table> 
    </div> 
</div> 
</div>
</div>
</div>
@endsection('content')