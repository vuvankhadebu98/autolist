
@extends('admin.master')
@section('title','Danh sách tài khoản')
@section('content')
 <!--App-Content-->
 <div class="app-content  my-3 my-md-5">
    <div class="side-app">
        <div class="page-header">
            <h4 class="page-title">Tạo chức danh</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Cài đặt</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tạo chức danh</li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Chức danh mới</div>
                    </div>
                   
                    @if(session('success'))
                        <div class="alert alert-success">{{session('success')}}</div>
                    @endif
                    <div class="card-body">
                        {{-- Form thêm chức danh --}}
                        <form action="{{route('role.store')}}" method="post"class="form-horizontal">
                            @csrf
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3"> <label class="form-label mt-2"
                                            id="examplenameInputname2">Tên chức danh <i>(Không được để trống)</i></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="name" class="form-control"id="examplenameInputname3" placeholder="">
                                    </div>
                                </div>
                            </div>
                           
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label">Quyền Hạn</label>
                                    </div>
                                    <div class="col-md-9">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Chọn quyền</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-0 row justify-content-end">
                                <div class="col-md-3">
                                    <button type="submit"class="btn btn-primary waves-effect waves-light">
                                        Tạo chức danh
                                    </button>
                                </div>
                            </div>
                                {{-- Form chọn quyền --}}
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Chọn chức danh</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                        <form >
                                            <div class="col">
                                                    @foreach($permission as $per)
                                                        <div class="row">
                                                            <input type="checkbox" value="{{$per->name}}" name="role[]" value="">{{$per->name}}
                                                        </div>
                                                        <br>
                                                    @endforeach
                                            </div>
                                        </form>
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
                                       
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                {{-- Kết thúc form chọn quyền --}}
                        </form>
                        {{-- Kết thúc form chức vụ --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    // Bắt sự kiện hiển thị form chọn quyền
    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body input').val(recipient)
})
</script>
<!--App-Content-->
@endsection('content')