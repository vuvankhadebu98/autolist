<table id="example" class="table table-striped table-bordered dataTable no-footer table-center" role="grid" aria-describedby="example_info">
   <thead>
    <tr role="row">
     <th class="wd-15p sorting_asc" >Tên khách hàng</th>
     <th class="wd-15p sorting" >Trạng thái</th>
     <th class="wd-20p sorting" >Loại đơn hàng</th>
     <th class="wd-20p sorting" >Tổng thanh toán</th>
     <th class="wd-20p sorting" >Ngày đặt đơn</th>
     <th class="wd-25p sorting" >Thao tác</th>

   </tr>
 </thead>
 <tbody>
  @foreach($order_view as $key => $odv)
  <tr>
   <td scope="row">{{$odv->customer->firstname}} {{$odv->customer->lastname}}</td>
   <td>
    <?php 
    if($odv->status == 0){
      ?>
      <a  href="{{route('active-order',$odv->id)}}" class="btn btn-primary">Chờ xử lý</a>
      <?php 
    }else{
      ?>
      <a style="color: white" class="btn btn-success">Đã xử lý</a>
      <?php
    }
    ?>
   </td>
   <td>
     <?php 
    if($odv->check == 0){
      ?>
      <span>Đơn nhập</span>
      <?php 
    }else{
      ?>
      <span>Đơn bán</span>
      <?php
    }
    ?>
   </td>
   <td>{{number_format($odv->total_price)}} VNĐ</td>
   <td>{{date('H:i:s d-m-Y ', strtotime($odv->created_at))}}</td>
  

   
   
   <td style="text-align: center;">
    

  <!-- Model chi tiết -->
 <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg{{$odv->id}}">Xem chi tiết</button>

<div class="modal fade bd-example-modal-lg{{$odv->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <table id="example" class="table table-striped table-bordered dataTable no-footer table-center" role="grid" aria-describedby="example_info">
   <thead>
    <tr role="row">
     <th class="wd-15p sorting_asc" >Tên sản phẩm</th>
     <th class="wd-15p sorting" >Số lượng</th>
     <th class="wd-20p sorting" >Giá sản phẩm</th>
     <th class="wd-20p sorting" >Trạng thái</th>
     <th class="wd-15p sorting" >Ngày đặt</th>  
   </tr>
       </thead>
       <tbody>
       @foreach($odv->order_detail as $item)
        <tr>
         <td scope="row">{{$item->product->name}}</td>
         <td >{{$item->qty}}</td>

         <td>{{number_format($item->price)}} VNĐ</td>
         
         <td >
          <?php 
          if($item->status == 0){
            ?>
            <a href="" class="btn btn-danger">Chờ xử lý</a>
            <?php 
          }else{
            ?>
            <a href="" class="btn btn-success">Đã duyệt</a>
            <?php
          }
          ?>
         </td>
         <td >{{date('H:i:s d-m-Y ', strtotime($item->created_at))}}</td>   
      </tr>
        @endforeach
      </tbody>
    </table>
    </div>
  </div>
</div>
    
  </td>

  </tr>
@endforeach
</tbody>
</table>
