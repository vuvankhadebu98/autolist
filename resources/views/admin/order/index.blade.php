@extends('admin.master')
@section('title','Quản lý đơn hàng')
@section('content')
<div class="app-content">
 <div class="side-app">
  <div class="page-header">
   <a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a> 
   <a class=" btn btn-success" href="{{route('get.view.order')}}" style="color: white;">Danh sách hóa đơn</a>
   
   <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Tables</a></li>
    <li class="breadcrumb-item active" aria-current="page">Hóa đơn</li>
  </ol>
</div>
@if(session()->has('message'))
<div class="alert alert-success">
  {{ session()->get('message') }}
</div>
@elseif(session()->has('error'))
<div class="alert alert-danger">
  {{ session()->get('error') }}
</div>
@endif
<div class="row">
 <div class="col-md-12 col-lg-12">
  <div class="card">
   <div class="card-header">
    <div class="card-title">Danh sách hóa đơn</div>
  </div>
  <div class="card-body">
    <div class="table-responsive">
     <div class="dataTables_wrapper dt-bootstrap4 no-footer">
      <div class="row">
       <div class="col-sm-12 col-md-6">
        <div class="dataTables_length" id="example_length">
         {{-- <label>
          Show 
          <select name="example_length" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm" id="my-select">
           <option value="10">10</option>
           <option value="25">25</option>
           <option value="50">50</option>
           <option value="100">100</option>
         </select>

       </label> --}}
       <label>
       <p class="hover" id="active_product" onclick="return active_product(this);" style="margin-left: 15px;">Hóa đơn đã duyệt</p>
       </label>
       <label>
       <p class="hover" id="import_order" onclick="return import_order(this);" style="margin-left: 15px;">Hóa đơn nhập</p>
       </label>
       <label>
       <p class="hover" id="sale_order" onclick="return sale_order(this);" style="margin-left: 15px;">Hóa đơn bán</p>
       </label>
       
     </div>
     
   </div>
   <div class="col-sm-12 col-md-6" style="margin-top: -20px">
    <form action="{{route('search.order')}}" method="post">
      @csrf
    <div style="margin-top: 20px;" id="example_filter" class="dataTables_filter"><label>Search:<input type="search" name="search" class="form-control form-control-sm" placeholder="" aria-controls="example"></label></div>
  </form>
  </div>
</div>
<div class="row">
 <div id="example" class="col-sm-12">
  <table id="example" class="table table-striped table-bordered dataTable no-footer table-center" role="grid" aria-describedby="example_info">
   <thead>
    <tr role="row">
     <th class="wd-15p sorting_asc" >Tên khách hàng</th>
     <th class="wd-15p sorting" >Trạng thái</th>
     <th class="wd-20p sorting" >Loại đơn hàng</th>
     <th class="wd-20p sorting" >Tổng thanh toán</th>
     <th class="wd-20p sorting" >Ngày đặt đơn</th>
     <th class="wd-25p sorting" >Thao tác</th>

   </tr>
 </thead>
 <tbody>
  @foreach($order_view as $key => $odv)
  <tr>
   <td scope="row">{{$odv->customer->firstname}} {{$odv->customer->lastname}}</td>
   <td>
    <?php 
    if($odv->status == 0){
      ?>
      <a  href="{{route('active-order',$odv->id)}}" class="btn btn-primary">Chờ xử lý</a>
      <?php 
    }else{
      ?>
      <a style="color: white" class="btn btn-success">Đã xử lý</a>
      <?php
    }
    ?>
   </td>
   <td>
     <?php 
    if($odv->check == 0){
      ?>
      <span>Đơn nhập</span>
      <?php 
    }else{
      ?>
      <span>Đơn bán</span>
      <?php
    }
    ?>
   </td>
   <td>{{number_format($odv->total_price)}} VNĐ</td>
   <td>{{date('H:i:s d-m-Y ', strtotime($odv->created_at))}}</td>
  

   
   
   <td style="text-align: center;">
    

  <!-- Model chi tiết -->
 <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg{{$odv->id}}">Xem chi tiết</button>

<div class="modal fade bd-example-modal-lg{{$odv->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <table id="example" class="table table-striped table-bordered dataTable no-footer table-center" role="grid" aria-describedby="example_info">
   <thead>
    <tr role="row">
     <th class="wd-15p sorting_asc" >Tên sản phẩm</th>
     <th class="wd-15p sorting" >Số lượng</th>
     <th class="wd-20p sorting" >Giá sản phẩm</th>
     <th class="wd-20p sorting" >Trạng thái</th>
     <th class="wd-15p sorting" >Ngày đặt</th>
     
   </tr>
       </thead>
       <tbody>
       @foreach($odv->order_detail as $item)
        <tr>
         <td scope="row">{{$item->product_name}}</td>

         <td >{{$item->qty}}</td>

         <td>{{number_format($item->price)}} VNĐ</td>
         
         <td >
          <?php 
          if($item->status == 0){
            ?>
            <a href="" class="btn btn-danger">Chờ xử lý</a>
            <?php 
          }else{
            ?>
            <a href="" class="btn btn-success">Đã duyệt</a>
            <?php
          }
          ?>
         </td>
         <td >{{date('H:i:s d-m-Y ', strtotime($item->created_at))}}</td>   
      </tr>
        @endforeach
      </tbody>
    </table>
    </div>
  </div>
</div>
    
  </td>

</tr>
@endforeach
</tbody>
</table>
</div>
</div>
<div class="row">
  <div class="col-sm-12 col-md-12  ">
   <div class="dataTables_paginate paging_simple_numbers align-content-center" id="example_paginate">
    <ul class="pagination">
     {{$order_view->links()}}
   </ul>
 </div>
</div>
</div>
</div>
</div>
</div>
<!-- table-wrapper --> 
</div>
<!-- section-wrapper --> 
</div>

</div>
</div>
</div>
@include('sweetalert::alert')
<script type="text/javascript">
 document.getElementById('my-select').addEventListener('change', function() {
  var value = $("#my-select option:selected").val();
      // alert(value);
  // console.log('You selected: ', this.value);
  $.ajax({
    url: '{{route('change.load.list.order')}}',
    method: 'GET',
    data:{
     value:value,
   },
   success: function(data){
     $("div#example table").empty();
     $("div#example table").html(data);
     console.log(data);
   }
 })
});
 
 function active_product(e) {
    $.ajax({
    url: '{{route('load.listactive.order')}}',
    method: 'GET',
   success: function(data){
     $("div#example table").empty();
     $("div#example table").html(data);
     console.log(data);
   }
 });
 }
 
 function import_order(e) {
    $.ajax({
    url: '{{route('load.import.order')}}',
    method: 'GET',
   success: function(data){
     $("div#example table").empty();
     $("div#example table").html(data);
     console.log(data);
   }
 });
 }

 function sale_order(e) {
    $.ajax({
    url: '{{route('load.sale.order')}}',
    method: 'GET',
   success: function(data){
     $("div#example table").empty();
     $("div#example table").html(data);
     console.log(data);
   }
 });
 }

 $('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body input').val(recipient)
})
</script>

@endsection
