{{-- phầm Thông Tin hiển thị --}}
  @foreach($all_contact as $key=> $show_contact)
    <tr style="text-align: center;">
       {{-- têm người liên hệ --}}
       <td>
          <font style="vertical-align: inherit;">
             <font style="vertical-align: inherit;">
                 <span style="color: #CC0000;">
                   {{$show_contact->name}}
                </span>
             </font>
          </font>
       </td>
       {{-- email --}}
       <td>
          <font style="vertical-align: inherit;">
             <font style="vertical-align: inherit;">
                <span style="color: #059D56;">
                   {{$show_contact->email}}
                </span>
             </font>
          </font>
       </td>
       {{-- nội dung người liên hệ --}}
       <td>
          <font style="vertical-align: inherit;">
             <font style="vertical-align: inherit;">
                <span>
                   {!!$show_contact->content!!}
                </span>
             </font>
          </font>
       </td>
       {{-- trạng thái --}}
       <td>
          <span>
             <font style="vertical-align: inherit;">
                <font style="vertical-align: inherit;">
                   <?php
                     if($show_contact->status == 0)
                     {
                     ?>
                     <a class="btn btn-xs btn-danger blog" 
                      style="font-size: 18px;width: 120px; height: 30px;padding: 4px;"
                      href="{{URL::to('unactive-contact/'.$show_contact->id)}}">
                        Chưa Duyệt
                      </a>
                     <?php
                     }else{
                     ?>
                     <a class="btn btn-xs btn-green blog" 
                      style="font-size: 18px;width: 120px;height: 30px;padding: 4px;"
                      href="{{URL::to('active-contact/'.$show_contact->id)}}">
                        Đã Duyệt
                      </a>
                     <?php
                     }
                   ?>  
                </font>
             </font>
          </span>
       </td> 
  @endforeach