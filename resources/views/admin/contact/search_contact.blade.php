@extends('admin.master')
@section('title','Home')
@section('content')
   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Danh sách Liên Hệ</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Danh sách Liên Hệ</font>
                  </font>
               </li>
            </ol>
         </div>
         {{-- nếu cập nhật thành công --}}
         <?php 
             $message = Session::get('message');
             if ($message) {
                 echo '<div class="alert alert-success">'. $message .'</div>';
                 Session::put('message', null);
             }
         ?>
         <div class="row">
            <div class="col-lg-12">
               <div class="card">
                  <div class="card-body">
                     
                     <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('list-contact')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     {{-- lọc tìm kiếm theo liên hệ đã đuyệt --}}
                    <label class=" search-tag">
                        <p class="btn btn-primary" id="contact_active" onclick="return contact_active(this);" style="margin-left: 5px; margin-top: 17px; ">Liên Hệ Đã Duyệt</p>
                     </label>
                     <label class="c search-tag">
                        <p class="btn btn-primary" id="contact_unactive" onclick="return contact_unactive(this);" style="margin-left: 5px; margin-top: 17px; ">Liên Hệ Chưa Được Duyệt</p>
                     </label>
                     {{-- tìm kiếm --}}
                     <form method="post" class="form-inline mr-auto" style="float: right;" action="{{route('search-contact')}}">
                        @csrf
                        <div class="nav-search"> 
                           <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                           <button class="btn btn-primary" style="width: 60px;height: 39px;" type="submit">
                              <i class="fa fa-search"></i>
                           </button> 
                        </div>
                     </form>
                     {{-- bảng Thông Tin --}}
                     <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                           <div class="table-responsive border-top">
                              <table id="example" class="table card-table table-bordered table-hover table-vcenter text-nowrap">
                                 <tbody>
                                    <tr style="text-align: center;">
                                       </th>
                                       {{-- tên blog --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Tên Người Liên Hệ
                                             </font>
                                          </font>
                                       </th>
                                       {{-- email --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Email Người liên Hệ 
                                             </font>
                                          </font>
                                       </th>
                                       {{-- nội dung người liên hệ --}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Nội Dung Người Liên Hệ
                                             </font>
                                          </font>
                                       </th>
                                       {{-- trạng thái --}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Trạng thái</font>
                                          </font>
                                       </th>
                                    </tr>

                                 {{-- phầm Thông Tin hiển thị --}}
                                 @foreach($all_contact as $key=> $show_contact)
                                    <tr style="text-align: center;">
                                       {{-- têm người liên hệ --}}
                                       <td>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                 <span style="color: #CC0000;">
                                                   {{$show_contact->name}}
                                                </span>
                                             </font>
                                          </font>
                                       </td>
                                       {{-- email --}}
                                       <td>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                <span style="color: #059D56;">
                                                   {{$show_contact->email}}
                                                </span>
                                             </font>
                                          </font>
                                       </td>
                                       {{-- nội dung người liên hệ --}}
                                       <td>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                <span>
                                                   {!!$show_contact->content!!}
                                                </span>
                                             </font>
                                          </font>
                                       </td>
                                       {{-- trạng thái --}}
                                       <td>
                                          <span>
                                             <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">
                                                   <?php
                                                     if($show_contact->status == 0)
                                                     {
                                                     ?>
                                                     <a class="btn btn-xs btn-danger blog" 
                                                      style="font-size: 18px;width: 120px; height: 30px;padding: 4px;"
                                                      href="{{URL::to('unactive-contact/'.$show_contact->id)}}">
                                                        Chưa Duyệt
                                                      </a>
                                                     <?php
                                                     }else{
                                                     ?>
                                                     <a class="btn btn-xs btn-green blog" 
                                                      style="font-size: 18px;width: 120px;height: 30px;padding: 4px;"
                                                      href="{{URL::to('active-contact/'.$show_contact->id)}}">
                                                        Đã Duyệt
                                                      </a>
                                                     <?php
                                                     }
                                                   ?>  
                                                </font>
                                             </font>
                                          </span>
                                       </td> 
                                 @endforeach
                                 </tbody>
                              </table>
                           </div>  
                        </div> 
                     </div>
                  </div>
               </div>
               <ul class="pagination mb-5">
               </ul>
            </div>
         </div>
      </div>
   </div>
<!--App-Content--> 
<script type="text/javascript">
  {{-- lọc ajax contact active --}}
    function contact_active(e) {
      $.ajax({
      url: '{{route('load.contact.active')}}',
      method: 'GET',
      success: function(data){
         $("table#example tbody").empty();
         $("table#example tbody").html(data);
         console.log(data);
         }
      });
   }
   {{-- lọc ajax contact unactive --}}
    function contact_unactive(e) {
      $.ajax({
      url: '{{route('load.contact.unactive')}}',
      method: 'GET',
      success: function(data){
         $("table#example tbody").empty();
         $("table#example tbody").html(data);
         console.log(data);
         }
      });
   }
</script>
@endsection('content')
