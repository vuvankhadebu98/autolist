@extends('admin.master')
@section('title','Home')
@section('content')

   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Cập nhật Quảng Cáo</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Cập nhật Quảng Cáo</font>
                  </font>
               </li>
            </ol>
         </div>

         <div class="row row-cards">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">
                        <font style="vertical-align: inherit;">
                           <font style="vertical-align: inherit;">Cập nhật Quảng Cáo</font>
                           <a href="{{URL::to('all-poster')}}" class="btn" aria-expanded="false" style="vertical-align: inherit; font-size: 18px;color: #20BAED">Danh Sách Quảng Cáo</a>
                        </font>
                     </div>
                  </div>
                  {{-- thêm mới --}}
                  @foreach($edit_poster as $key => $edit_pt)
                    <form role="form" action="{{URL::to('/update-poster/'.$edit_pt->id)}}" method="post" 
                     enctype="multipart/form-data">
                    {{ csrf_field()}}
                     <div class="card-body">
                        {{-- user_id --}}
                        <div class="form-group">
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                               <font style="vertical-align: inherit;">Người Đăng</font>
                             </font>
                            </label>
                           <select name="user_admin" class="form-control m-bot15">
                            @foreach($user as $key => $user_id)
                              @if($user_id->id == $edit_pt->id_user)
                                <option selected  value="{{$user_id->id}}">{{$user_id->firstname}}</option>
                              @else
                                <option value="{{$user_id->id}}">{{$user_id->firstname}}</option>
                              @endif 
                           @endforeach       
                           </select>
                        </div>
                        {{-- brand_id thương hiệu sản phẩm --}}
                         <div class="form-group">
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                               <font style="vertical-align: inherit;">Thương hiệu sản phẩm</font>
                             </font>
                            </label>
                           <select name="brand_sp" class="form-control m-bot15">
                            @foreach($brand as $key => $brand_id)
                              @if($brand_id->id == $edit_pt->brand_id)                   
                                <option selected value="{{$brand_id->id}}">{{$brand_id->name}}</option> 
                              @else
                                <option value="{{$brand_id->id}}">{{$brand_id->name}}</option>
                              @endif
                            @endforeach       
                           </select>
                        </div>
                        {{-- danh muc sản phẩm --}}
                        <div class="form-group">
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                               <font style="vertical-align: inherit;">Danh Mục Sản Phẩm</font>
                             </font>
                            </label>
                           <select name="category_sp" class="form-control m-bot15">
                            @foreach($category as $key => $category_id)
                              @if($category_id->id == $edit_pt->category_id)                   
                                <option selected value="{{$category_id->id}}">{{$category_id->name}}</option> 
                              @else
                                <option value="{{$category_id->id}}">{{$category_id->name}}</option>
                              @endif
                            @endforeach       
                           </select>
                        </div>
                        {{-- hình ảnh Quảng Cáo --}}
                        <div class="form-group">
                            <label class="form-label">
                              <font style="vertical-align: inherit;">
                               <font style="vertical-align: inherit;">Hình ảnh Quảng Cáo</font>
                             </font>
                            </label> 
                            <input type="file" name="img" class="form-control"/>
                            <img src="{{URL::to('/img_poster/'.$edit_pt->img)}}" 
                            height="90" width="120" style="margin-left: 5px"/> 
                        </div>
                        {{-- Tên Quảng Cáo(title) --}}
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Tên Quảng Cáo</font>
                              </font>
                           </label> 
                           <input type="text" name="title" class="form-control w-100" 
                           placeholder="Tên quảng cáo..."value="{{$edit_pt->title}}"> 
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-6" style="float: left;">
                            {{-- price --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Giá</font>
                                  </font>
                               </label> 
                               <input type="number" name="price" class="form-control w-100" 
                               placeholder="giá quảng cáo..." value="{{$edit_pt->price}}">
                               <p class="help is-danger">{{ $errors->first('price') }}</p> 
                            </div>
                            {{-- seats --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Số Ghế</font>
                                  </font>
                               </label> 
                               <input type="number" name="seats" class="form-control w-100" 
                               placeholder="Nhập số ghế" value="{{$edit_pt->seats}}">
                               <p class="help is-danger">{{ $errors->first('seats') }}</p> 
                            </div>
                            {{-- km_went --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">km Đã Đi</font>
                                  </font>
                               </label> 
                               <input type="number" name="km_went" class="form-control w-100" 
                               placeholder="Số Km xe đã đi" value="{{$edit_pt->km_went}}">
                               <p class="help is-danger">{{ $errors->first('km_went') }}</p> 
                            </div>
                          </div>
                          <div class="col-md-6" style="float: right;">
                            {{-- promotion --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Vận Tốc Tối Đa(km/h)</font>
                                  </font>
                               </label> 
                               <input type="number" name="promotion" class="form-control w-100" 
                               placeholder="nhập vận tốc tối đa(km/h)" value="{{$edit_pt->promotion}}">
                               <p class="help is-danger">{{ $errors->first('promotion') }}</p> 
                            </div>
                            {{-- loại nhiên liệu --}}
                            <div class="form-group">
                              <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Loại Nhiên Liệu</font>
                                  </font>
                               </label> 
                               <select name="engine" class="form-control m-bot15">
                                  <option value="0" {{$edit_pt->engine == 0 ? "selected" : ""}}>Diesel sinh học</option>
                                  <option value="1" {{$edit_pt->engine == 1 ? "selected" : ""}}>Xăng</option>
                                  <option value="2" {{$edit_pt->engine == 2 ? "selected" : ""}}>Năng lượng mặt trời</option>
                                  <option value="3" {{$edit_pt->engine == 3 ? "selected" : ""}}>Nitơ</option>
                                  <option value="4" {{$edit_pt->engine == 4 ? "selected" : ""}}>Amoniac</option>
                                  <option value="5" {{$edit_pt->engine == 5 ? "selected" : ""}}>Cồn</option> 
                                  <option value="6" {{$edit_pt->engine == 6 ? "selected" : ""}}>LPG</option> 
                                  <option value="7" {{$edit_pt->engine == 7 ? "selected" : ""}}>CNG</option>       
                               </select>
                            </div>
                            {{-- transmission_type --}}
                            <div class="form-group">
                              <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Kiểu Hộp Số</font>
                                  </font>
                               </label> 
                               <select name="transmission_type" class="form-control m-bot15">
                                  <option value="0" {{$edit_pt->transmission_type == 0 ? "selected" : ""}}>Hộp số sàn</option>
                                  <option value="1" {{$edit_pt->transmission_type == 1 ? "selected" : ""}}>Hộp số tự động</option>
                                  <option value="2" {{$edit_pt->transmission_type == 2 ? "selected" : ""}}>Hộp số tự động vô cấp CVT</option>
                                  <option value="3" {{$edit_pt->transmission_type == 3 ? "selected" : ""}}>Hộp số ly hợp kép DCT</option>
                                  <option value="4" {{$edit_pt->transmission_type == 4 ? "selected" : ""}}>Hộp số sang số trực tiếp DSG</option>    
                               </select>
                            </div>
                          </div>
                        </div> 
                        {{-- description --}}
                        <div class="form-group col-md-12" style="float: left;">
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Mô Tả Quảng Cáo</font>
                              </font>
                           </label> 
                           <textarea name="description" id="description" style="resize: none;"rows="8"class="form-control">
                           {{$edit_pt->description}}   
                           </textarea>
                           <p class="help is-danger">{{ $errors->first('description') }}</p> 
                        </div>
                        {{-- content --}}
                        <div class="form-group col-md-12" style="float: left;">
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Nội Dung Quảng Cáo</font>
                              </font>
                           </label> 
                           <textarea name="content" id="content" style="resize: none;"rows="8"class="form-control">
                           {{$edit_pt->content}}   
                           </textarea>
                           <p class="help is-danger">{{ $errors->first('content') }}</p> 
                        </div>
                      {{-- cdddddddddddddddddddddddddddddddddddddddd --}}
                        <div class="col-md-12">
                          {{-- 1 --}}
                          <div class="col-lg-4" style="float: left;">
                            {{-- engine--}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Động Cơ</font>
                                  </font>
                               </label> 
                               <input type="text" name="engine" class="form-control w-100" 
                               placeholder="nhập động cơ.." value="{{$edit_pt->engine}}">
                               <p class="help is-danger">{{ $errors->first('engine') }}</p> 
                            </div>
                            {{-- enegine_displacement --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Hệ Thống Động Cơ</font>
                                  </font>
                               </label> 
                               <input type="text" name="enegine_displacement" class="form-control w-100" 
                               placeholder="hệ thống động cơ" value="{{$edit_pt->enegine_displacement}}">
                               <p class="help is-danger">{{ $errors->first('enegine_displacement') }}</p> 
                            </div>
                            {{-- fuel_tank_capacity --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Dung Tích Bình Xăng(lit)</font>
                                  </font>
                               </label> 
                               <input type="number" name="fuel_tank_capacity" class="form-control w-100" 
                               placeholder="dung tích bình xăng" value="{{$edit_pt->fuel_tank_capacity}}">
                               <p class="help is-danger">{{ $errors->first('fuel_tank_capacity') }}</p> 
                            </div>
                            {{-- break --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Tính Năng Nổi Bật</font>
                                  </font>
                               </label> 
                               <input type="text" name="break" class="form-control w-100" 
                               placeholder="nhập tính năng nổi bật" value="{{$edit_pt->break}}">
                               <p class="help is-danger">{{ $errors->first('break') }}</p> 
                            </div>
                            {{-- bootspace --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Không Gian Khởi Động(lít)</font>
                                  </font>
                               </label> 
                               <input type="text" name="bootspace" class="form-control w-100" 
                               placeholder="nhập không gian khỏi động" value="{{$edit_pt->bootspace}}">
                               <p class="help is-danger">{{ $errors->first('bootspace') }}</p> 
                            </div>
                          </div>
                          {{-- 2 --}}
                          <div class="col-lg-4" style="float: left;">
                            {{-- arai_milage --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Vận tốc(bốc phá m/s)</font>
                                  </font>
                               </label> 
                               <input type="text" name="arai_milage" class="form-control w-100" 
                               placeholder="vận tốt bốc phá km/s" value="{{$edit_pt->arai_milage}}">
                               <p class="help is-danger">{{ $errors->first('arai_milage') }}</p> 
                            </div>
                            {{-- max_torque --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Momen xoắn cực đại(nm@v/p)</font>
                                  </font>
                               </label> 
                               <input type="text" name="max_torque" class="form-control w-100" 
                               placeholder="nhập momen" value="{{$edit_pt->max_torque}}">
                               <p class="help is-danger">{{ $errors->first('max_torque') }}</p> 
                            </div>
                            {{-- max_power --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Cộng Xuất Tối Đa(bhp@rpm)</font>
                                  </font>
                               </label> 
                               <input type="text" name="max_power" class="form-control w-100" 
                               placeholder="nhập công xuất tối đa" value="{{$edit_pt->max_power}}">
                               <p class="help is-danger">{{ $errors->first('max_power') }}</p> 
                            </div>
                            {{-- like --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Yêu Thích</font>
                                  </font>
                               </label> 
                               <input type="number" name="like" class="form-control w-100" 
                               placeholder="yêu thích" value="{{$edit_pt->like}}">
                               <p class="help is-danger">{{ $errors->first('like') }}</p> 
                            </div>
                            {{-- rate --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Tỉ Lệ(5/5)</font>
                                  </font>
                               </label> 
                               <input type="text" name="rate" class="form-control w-100" 
                               placeholder="đánh giá" value="{{$edit_pt->rate}}">
                               <p class="help is-danger">{{ $errors->first('rate') }}</p> 
                            </div>
                          </div>
                          {{-- 3 --}}
                          <div class="col-lg-4" style="float: left;">
                            {{-- view --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Lượt Xem</font>
                                  </font>
                               </label> 
                               <input type="text" name="view" class="form-control w-100" 
                               placeholder="Lượt Xem" value="{{$edit_pt->view}}">
                               <p class="help is-danger">{{ $errors->first('view') }}</p> 
                            </div>
                            {{-- video --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Video</font>
                                  </font>
                               </label> 
                               <input type="text" name="video" class="form-control w-100" 
                               placeholder="video" value="{{$edit_pt->video}}">
                               <p class="help is-danger">{{ $errors->first('video') }}</p> 
                            </div>
                            {{-- package --}}
                            <div class="form-group">
                                <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                   <font style="vertical-align: inherit;">Lựa Chọn</font>
                                 </font>
                                </label> 
                               <select name="package" class="form-control m-bot15">
                                <option value="0" {{$edit_pt->package == 0 ? "selected" : ""}}>30$->60$</option>
                                <option value="1" {{$edit_pt->package == 1 ? "selected" : ""}}>60$->90$</option>
                                <option value="2" {{$edit_pt->package == 2 ? "selected" : ""}}>90$->120$</option>
                                <option value="3" {{$edit_pt->package == 3 ? "selected" : ""}}>120$->150$</option>
                               </select>
                            </div>
                           {{-- Kiểm Tra --}}
                            <div class="form-group">
                              <label class="form-label">
                                <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Kiểm Tra</font>
                               </font>
                              </label> 
                             <select name="check" class="form-control m-bot15">
                              <option value="0" {{$edit_pt->check == 0 ? "selected" : ""}}>Hết Hàng</option>
                              <option value="1" {{$edit_pt->check == 1 ? "selected" : ""}}>Còn Hàng</option> 
                             </select>
                           </div>
                           {{-- Kiểm Chứng --}}
                           <div class="form-group">
                              <label class="form-label">
                                <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Kiểm Chứng</font>
                               </font>
                              </label> 
                             <select name="check_new" class="form-control m-bot15">
                              <option value="0" {{$edit_pt->check_new == 0 ? "selected" : ""}}>Mới</option>
                              <option value="1" {{$edit_pt->check_new == 1 ? "selected" : ""}}>Thuê</option>
                              <option value="2" {{$edit_pt->check_new == 2 ? "selected" : ""}}>Đã Sử Dụng</option> 
                             </select>
                           </div>
                          {{-- Trạng Thái --}}
                           <div class="form-group">
                              <label class="form-label">
                                <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Trạng Thái</font>
                               </font>
                              </label> 
                             <select name="status" class="form-control m-bot15">
                                <option value="0" {{$edit_pt->status == 0 ? "selected" : ""}}>Ẩn </option>
                                <option value="1" {{$edit_pt->status == 1 ? "selected" : ""}}>Hiển Thị</option>
                             </select>
                           </div>
                         </div>
                        </div> 
                        <button type="submit" name="update_poster" 
                        class="btn btn-primary waves-effect waves-light">
                           <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Cập nhật quảng cáo</font>
                           </font>
                        </button> 
                     </div>            
                  </form>
                @endforeach
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--App-Content--> 
@endsection('content')

@section('js')
    <script>
        CKEDITOR.replace('description'); 
        CKEDITOR.replace('content');      
    </script>
@endsection