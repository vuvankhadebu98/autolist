{{-- bảng poster --}}
 <div class="tab-content" style="margin-top: -5px">
    <div class="tab-pane active" id="tab1">
       <div class="table-responsive border-top">
          <table id="example" class="table card-table table-bordered table-hover table-vcenter text-nowrap">
             <tbody>
                <tr style="text-align: center;">
                  {{-- id --}}
                  <th class="w-1">
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Mã Quảng Cáo</font>
                      </font>
                   </th>
                   {{-- Tên poster(quảng cáo) --}}
                   <th class="w-1">
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Tên Quảng Cáo</font>
                      </font>
                   </th>
                   {{-- Ảnh poster --}}
                   <th class="w-1">
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Ảnh Quảng Cáo</font>
                      </font>
                   </th>
                   {{-- Tên thương hiệu brand --}}
                   <th class="w-1">
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Tên Thương Hiệu Sản Phẩm
                         </font>
                      </font>
                   </th>
                   {{-- Tên danh mục category --}}
                   <th class="w-1">
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Tên danh mục Sản Phẩm
                         </font>
                      </font>
                   </th>
                   {{-- tên user --}}
                   <th>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Tên Người Đăng
                         </font>
                      </font>
                   </th>
                   {{-- price(giá) --}}
                   <th>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Giá
                         </font>
                      </font>
                   </th>
                    {{-- vận tốc tối đa(promotion) --}}
                   <th>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Vận Tốc Tối Đa
                         </font>
                      </font>
                   </th>
                   {{-- package(chọn gói) --}}
                   <th>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Gói Dùng
                         </font>
                      </font>
                   </th>
                   {{-- kiểm tra sản phẩm --}}
                   <th>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Kiểm Tra
                         </font>
                      </font>
                   </th>
                   {{-- kiểm tra xem mới hay là đã sử dụng hay thuê--}}
                   <th>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Kiểm Chứng
                         </font>
                      </font>
                   </th>
                   {{-- trạng thái --}}
                   <th>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Trạng thái</font>
                      </font>
                   </th>
                   {{-- sửa xóa --}}
                   <th style="text-align: center;">
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;" >Lựa Chọn
                         </font>
                      </font>
                   </th>
                </tr>

             {{-- phầm blog hiển thị --}}

                @foreach($all_poster as $key=> $show_poster)
                <tr style="text-align: center;">
                  {{-- mã --}}
                    <td>
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">
                           {{$show_poster->id}}
                        </font>
                     </font> 
                   </td>
                   {{-- têm Quảng Cáo --}}
                   <td>
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">
                          <span style="color: #CC0000;">
                           {{$show_poster->title}}
                          </span>
                        </font>
                     </font> 
                   </td>
                   {{-- Hình ảnh quảng cáo --}}
                    <td style="width: 160px;">
                      <img src="../img_poster/{{$show_poster->img}}" 
                      > 
                    </td>
                    {{-- tên thương hiệu--}}
                   <td>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">
                            <span style="color: #CC0000;">
                              {{$show_poster->brand->name}}
                            </span>
                         </font>
                      </font>
                   </td> 
                    {{-- tên danh mục sản phẩm --}}
                   <td>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">
                            <span style="color: #CC0000;">
                              {{$show_poster->category->name}}
                            </span>
                         </font>
                      </font>
                   </td>
                   {{-- Tên Người Đăng --}}
                   <td>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">
                          <span style="">
                            {{$show_poster->user->firstname}}
                          </span>
                         </font>
                      </font>
                   </td>
                   {{-- giá --}}
                   <td>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">
                          <span style="color: #ff9327;">
                            {{number_format($show_poster->price,0,',','.')}} vnđ
                          </span>
                         </font>
                      </font>
                   </td>
                   {{-- vận tốc tối đa --}}
                   <td>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">
                          <span style="color: #ff9327;">
                            {{$show_poster->promotion}}.km/h
                          </span>
                         </font>
                      </font>
                   </td>
                   {{-- Chọn Gói Dùng --}}
                   <td>
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">
                           <?php
                             if($show_poster->package == 0)
                             {
                             ?>
                              <span class="hienthi" id="0" style="color: #0DD902;">30->60$</span>
                             <?php
                             }elseif($show_poster->package == 1){
                             ?>
                              <span class="hienthi" id="1" style="color: #09B5C8;">60->90$</span>
                             <?php
                             }
                             elseif($show_poster->package == 2){
                             ?>
                              <span class="hienthi" id="1" style="color: #00EC99;">90->120$</span>
                             <?php
                             }
                             elseif($show_poster->package == 3){
                             ?>
                              <span class="hienthi" id="1" style="color: #0C00EF;">120->150$</span>
                             <?php
                             }
                           ?> 
                        </font>
                     </font>
                   </td>
                   {{-- Kiểm Tra --}}
                   <td>
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">
                           <?php
                             if($show_poster->check == 0)
                             {
                             ?>
                              <span class="hienthi" id="0"style="color: #E60404;">Hết Hàng</span>
                             <?php
                             }elseif($show_poster->check == 1){
                             ?>
                              <span class="hienthi" id="1" style="color: #0DD902;">Còn Hàng</span>
                             <?php
                             }
                           ?>
                        </font>
                     </font>
                   </td>
                   {{-- Kiểm Chứng --}}
                   <td>
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">
                           <?php
                             if($show_poster->check_new == 0)
                             {
                             ?>
                              <span class="hienthi" id="0" style="color: #05a01f;">Mới</span>
                             <?php
                             }elseif($show_poster->check_new == 1){
                             ?>
                              <span class="hienthi" id="1" style="color: #ff382b;">Cho Thuê</span>
                             <?php
                             }
                             elseif($show_poster->check_new == 2){
                             ?>
                              <span class="hienthi" id="2" style="color: #868e96;">Đã Sử Dụng</span>
                             <?php
                             }
                           ?>
                        </font>
                     </font>
                   </td>
                   {{-- trạng thái --}}
                   <td>
                      <span>
                         <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">
                               <?php
                                 if($show_poster->status == 0)
                                 {
                                 ?>
                                 <a class="btn btn-xs btn-danger blog" 
                                  style="font-size: 18px;width: 70px; height: 30px;padding: 4px;"
                                  href="{{URL::to('unactive-poster/'.$show_poster->id)}}">
                                    Ẩn
                                  </a>
                                 <?php
                                 }else{
                                 ?>
                                 <a class="btn btn-xs btn-green blog" 
                                  style="font-size: 18px;width: 120px;height: 30px;padding: 4px;"
                                  href="{{URL::to('active-poster/'.$show_poster->id)}}">
                                  Hiển Thị
                                  </a>
                                 <?php
                                 }
                               ?>
                            </font>
                         </font>
                      </span>
                   </td> 
                   {{-- sửa xóa --}}
                   <td class="r" style="text-align: center;width: 160px;">
                      <a class="btn btn-xs btn-info blog" 
                      style="font-size: 18px;width: 70px; height: 30px;"
                      href="{{URL::to('/edit-poster/'.$show_poster->id)}}">
                         <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
                      </a>
                      <a class="btn btn-xs btn-danger blog" 
                      style="font-size: 18px;width: 70px; height: 30px;"
                      onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                      href="{{URL::to('/delete-poster/'.$show_poster->id)}}">
                         <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                      </a>
                   </td>
                </tr>
                @endforeach
             </tbody>
          </table>
       </div>  
    </div> 
 </div>