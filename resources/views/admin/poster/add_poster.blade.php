@extends('admin.master')
@section('title','Home')
@section('content')

   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Quảng Cáo Mới</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài Đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Quảng Cáo Mới</font>
                  </font>
               </li>
            </ol>
         </div>
         <?php 
              $message = Session::get('message');
              if ($message) {
                  echo '<div class="alert alert-success">'. $message .'</div>';
                  Session::put('message', null);
              }
            ?>
         <div class="row row-cards">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Tạo Quảng Cáo Mới</font>
                          <a href="{{URL::to('all-poster')}}" class="btn" aria-expanded="false" style="vertical-align: inherit; font-size: 18px;color: #20BAED">Danh Sách Quảng Cáo</a> 
                        </font>
                     </div>
                  </div>
                  {{-- thêm mới --}}
                  <form role="form" action="{{URL::to('/save-poster')}}" method="post" 
                     enctype="multipart/form-data">
                    {{ csrf_field()}}
                     <div class="card-body">
                        {{-- user_id --}}
                        <div class="form-group">
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Người Đăng</font>
                              </font>
                           </label> 
                           <select name="user_admin" class="form-control m-bot15">
                           @foreach($user as $key => $user_id)                   
                              <option value="{{$user_id->id}}">{{$user_id->firstname}}</option>  
                           @endforeach       
                           </select>
                        </div>
                        {{--brand_id của sản phẩm) --}}
                        <div class="form-group">
                          <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Tên Thương Hiệu Sản Phẩm</font>
                              </font>
                           </label> 
                           <select name="brand_sp" class="form-control m-bot15">
                           @foreach($brand as $key => $brand_id)                   
                               <option value="{{$brand_id->id}}">{{$brand_id->name}}</option>  
                           @endforeach       
                           </select>
                        </div> 
                        {{--category_id của sản phẩm) --}}
                        <div class="form-group">
                          <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;"> Danh Mục Sản Phẩm</font>
                              </font>
                           </label> 
                           <select name="category_sp" class="form-control m-bot15">
                           @foreach($category as $key => $category_id)                   
                               <option value="{{$category_id->id}}">{{$category_id->name}}</option>  
                           @endforeach       
                           </select>
                        </div> 
                        {{-- hình ảnh quảng cáo --}}
                        <div class="form-group">
                            <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Ảnh Quảng Cáo</font>
                              </font>
                           </label> 
                            <input type="file" name="img" class="form-control" id="img">
                            <p class="help is-danger">{{ $errors->first('img') }}</p>
                        </div>
                        {{-- title --}}
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Tên Quảng Cáo</font>
                              </font>
                           </label> 
                           <input type="text" name="title" class="form-control w-100" placeholder="Nhập tên quảng cáo">
                           <p class="help is-danger">{{ $errors->first('title') }}</p> 
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-6" style="float: left;">
                            {{-- price --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Giá</font>
                                  </font>
                               </label> 
                               <input type="number" name="price" class="form-control w-100" 
                               placeholder="Nhập giá quảng cáo">
                               <p class="help is-danger">{{ $errors->first('price') }}</p> 
                            </div>
                            {{-- seats --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Số Ghế</font>
                                  </font>
                               </label> 
                               <input type="number" name="seats" class="form-control w-100" 
                               placeholder="Nhập số ghế">
                               <p class="help is-danger">{{ $errors->first('seats') }}</p> 
                            </div>
                            {{-- km_went --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">km Đã Đi</font>
                                  </font>
                               </label> 
                               <input type="number" name="km_went" class="form-control w-100" 
                               placeholder="Số Km xe đã đi">
                               <p class="help is-danger">{{ $errors->first('km_went') }}</p> 
                            </div>
                          </div>
                          <div class="col-md-6" style="float: right;">
                            {{-- promotion --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Vận Tốc Tối Đa(km/h)</font>
                                  </font>
                               </label> 
                               <input type="number" name="promotion" class="form-control w-100" 
                               placeholder="Nhập vận tốc của xe">
                               <p class="help is-danger">{{ $errors->first('promotion') }}</p> 
                            </div>
                            {{--fuel_type--}}
                            <div class="form-group">
                              <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Loại Nhiên Liệu</font>
                                  </font>
                               </label> 
                               <select name="fuel_type" class="form-control m-bot15">
                                  <option value="0" selected="">Diesel sinh học</option>
                                  <option value="1">Xăng</option>
                                  <option value="2">Năng lượng mặt trời</option>
                                  <option value="3">Nitơ</option>
                                  <option value="4">Amoniac</option>
                                  <option value="5">Cồn</option> 
                                  <option value="6">LPG</option> 
                                  <option value="7">CNG</option>       
                               </select>
                            </div>
                            {{-- transmission_type --}}
                            <div class="form-group">
                              <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Kiểu Hộp Số</font>
                                  </font>
                               </label> 
                               <select name="transmission_type" class="form-control m-bot15">
                                  <option value="0" selected=""> Hộp số sàn</option>
                                  <option value="1">Hộp số tự động</option>
                                  <option value="2">Hộp số tự động vô cấp CVT</option>
                                  <option value="3">Hộp số ly hợp kép DCT</option>
                                  <option value="4">Hộp số sang số trực tiếp DSG</option>    
                               </select>
                            </div>
                          </div>
                        </div> 
                        {{-- description --}}
                        <div class="form-group col-md-12" style="float: left;">
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Mô Tả Quảng Cáo</font>
                              </font>
                           </label> 
                           <textarea name="description" id="description" style="resize: none;"rows="8"class="form-control">   
                           </textarea>
                           <p class="help is-danger">{{ $errors->first('description') }}</p> 
                        </div>
                        {{-- content --}}
                        <div class="form-group col-md-12" style="float: left;">
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Nội Dung Quảng Cáo</font>
                              </font>
                           </label> 
                           <textarea name="content" id="content" style="resize: none;"rows="8"class="form-control">   
                           </textarea>
                           <p class="help is-danger">{{ $errors->first('content') }}</p> 
                        </div>
                      {{-- cdddddddddddddddddddddddddddddddddddddddd --}}
                        <div class="col-md-12">
                          {{-- 1 --}}
                          <div class="col-lg-4" style="float: left;">
                            {{-- engine--}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Động Cơ</font>
                                  </font>
                               </label> 
                               <input type="text" name="engine" class="form-control w-100" 
                               placeholder="nhập động cơ">
                               <p class="help is-danger">{{ $errors->first('engine') }}</p> 
                            </div>
                            {{-- enegine_displacement --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Hệ Thống Động Cơ</font>
                                  </font>
                               </label> 
                               <input type="text" name="enegine_displacement" class="form-control w-100" 
                               placeholder="nhập hệ thống động cơ">
                               <p class="help is-danger">{{ $errors->first('enegine_displacement') }}</p> 
                            </div>
                            {{-- fuel_tank_capacity --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Dung Tích Bình Xăng(lit)</font>
                                  </font>
                               </label> 
                               <input type="number" name="fuel_tank_capacity" class="form-control w-100" 
                               placeholder=" nhập dung tích bình xăng">
                               <p class="help is-danger">{{ $errors->first('fuel_tank_capacity') }}</p> 
                            </div>
                            {{-- break --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Tính Năng Nổi Bật</font>
                                  </font>
                               </label> 
                               <input type="text" name="break" class="form-control w-100" 
                               placeholder="nhập tính tăng nổi bật">
                               <p class="help is-danger">{{ $errors->first('break') }}</p> 
                            </div>
                            {{-- bootspace --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Không Gian Khởi Động(lít)</font>
                                  </font>
                               </label> 
                               <input type="text" name="bootspace" class="form-control w-100" 
                               placeholder="không gian khởi động">
                               <p class="help is-danger">{{ $errors->first('bootspace') }}</p> 
                            </div>
                          </div>
                          {{-- 2 --}}
                          <div class="col-lg-4" style="float: left;">
                            {{-- arai_milage --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">vận tốc(bốc phá m/s)</font>
                                  </font>
                               </label> 
                               <input type="text" name="arai_milage" class="form-control w-100" 
                               placeholder="vận tốc bốc phá km/s">
                               <p class="help is-danger">{{ $errors->first('arai_milage') }}</p> 
                            </div>
                            {{-- max_torque --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Momen xoắn cực đại(nm@v/p)</font>
                                  </font>
                               </label> 
                               <input type="text" name="max_torque" class="form-control w-100" 
                               placeholder="nhập momen">
                               <p class="help is-danger">{{ $errors->first('max_torque') }}</p> 
                            </div>
                            {{-- max_power --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Cộng Xuất Tối Đa(bhp@rpm)</font>
                                  </font>
                               </label> 
                               <input type="text" name="max_power" class="form-control w-100" 
                               placeholder="nhập công xuất tối đa">
                               <p class="help is-danger">{{ $errors->first('max_power') }}</p> 
                            </div>
                            {{-- like --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Yêu Thích</font>
                                  </font>
                               </label> 
                               <input type="number" name="like" class="form-control w-100" 
                               placeholder="yêu thích">
                               <p class="help is-danger">{{ $errors->first('like') }}</p> 
                            </div>
                            {{-- rate --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Tỉ Lệ(5/5)</font>
                                  </font>
                               </label> 
                               <input type="text" name="rate" class="form-control w-100" 
                               placeholder="đánh giá">
                               <p class="help is-danger">{{ $errors->first('rate') }}</p> 
                            </div>
                          </div>
                          {{-- 3 --}}
                          <div class="col-lg-4" style="float: left;">
                            {{-- view --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Lượt Xem</font>
                                  </font>
                               </label> 
                               <input type="text" name="view" class="form-control w-100" 
                               placeholder="Lượt Xem">
                               <p class="help is-danger">{{ $errors->first('view') }}</p> 
                            </div>
                            {{-- video --}}
                            <div class="form-group "> 
                               <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Video</font>
                                  </font>
                               </label> 
                               <input type="text" name="video" class="form-control w-100" 
                               placeholder="video">
                               <p class="help is-danger">{{ $errors->first('video') }}</p> 
                            </div>
                            {{-- package--}}
                            <div class="form-group">
                              <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Chọn Gói</font>
                                  </font>
                               </label> 
                               <select name="package" class="form-control m-bot15">
                                  <option value="0" selected="">30$-60$</option>
                                  <option value="1">60$-90$</option>
                                  <option value="2">90$-120$</option>
                                  <option value="3">120$-150$</option>       
                               </select>
                            </div>
                            {{-- check --}}
                            <div class="form-group">
                              <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Kiểm Tra</font>
                                  </font>
                               </label> 
                               <select name="check" class="form-control m-bot15">
                                   <option value="0">Hết Hàng</option>
                                   <option value="1">Còn Hàng</option>   
                               </select>
                            </div>
                            {{--check_new --}}
                             <div class="form-group">
                              <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Kiểm Chứng</font>
                                  </font>
                               </label> 
                               <select name="check_new" class="form-control m-bot15">
                                   <option value="0">Mới</option>
                                   <option value="1">Cho Thuê</option>
                                   <option value="2">Đã Sử Dụng</option>   
                               </select>
                            </div>
                            {{-- status --}}
                            <div class="form-group">
                              <label class="form-label">
                                  <font style="vertical-align: inherit;">
                                     <font style="vertical-align: inherit;">Trạng Thái</font>
                                  </font>
                               </label> 
                               <select name="status" class="form-control m-bot15">
                                   <option value="0">Ẩn</option>
                                   <option value="1">Hiển Thị</option>   
                               </select>
                            </div>
                          </div>
                        </div> 
                        {{-- thêm mới --}}
                        <button type="submit" name="add_poster" 
                        class="btn btn-primary waves-effect waves-light">
                           <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Thêm Mới Quảng Cáo</font>
                           </font>
                        </button> 
                     </div>            
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--App-Content--> 
@endsection('content')

@section('js')
    <script>
        CKEDITOR.replace('description');
        CKEDITOR.replace('content');       
    </script>
@endsection