@extends('admin.master')
@section('title','Home')
@section('content')
   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Danh sách Quảng Cáo</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Danh sách Quảng Cáo</font>
                  </font>
               </li>
            </ol>
         </div>
         {{-- nếu cập nhật thành công --}}
         <?php 
             $message = Session::get('message');
             if ($message) {
                 echo '<div class="alert alert-success">'. $message .'</div>';
                 Session::put('message', null);
             }
         ?>
         <div class="row">
            <div class="col-lg-12">
               <div class="card">
                  <div class="card-body">
                     
                     <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('all-poster')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     
                      <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('add-poster')}}" class="btn" aria-expanded="false"> Thêm Mới Quảng Cáo </a> 
                     </div>
                     {{-- tìm kiếm --}}
                     <form method="post" class="form-inline mr-auto" style="float: right;" action="{{route('search-poster')}}">
                        @csrf
                        <div class="nav-search"> 
                           <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                           <button class="btn btn-primary" style="width: 60px;height: 39px;" type="submit">
                              <i class="fa fa-search"></i>
                           </button> 
                        </div>
                     </form>
                     {{-- bảng poster --}}
                     <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                           <div class="table-responsive border-top">
                              <table class="table card-table table-bordered table-hover table-vcenter text-nowrap">
                                 <tbody>
                                    <tr style="text-align: center;">
                                       {{-- Tên poster(quảng cáo) --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Tên Quảng Cáo</font>
                                          </font>
                                       </th>
                                       {{-- Ảnh poster --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Ảnh Quảng Cáo</font>
                                          </font>
                                       </th>
                                       {{-- Tên danh mục category --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Tên danh mục Sản Phẩm
                                             </font>
                                          </font>
                                       </th>
                                       {{-- tên user --}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Tên Người Đăng
                                             </font>
                                          </font>
                                       </th>
                                       {{-- package(chọn gói) --}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Chọn Gói Dùng
                                             </font>
                                          </font>
                                       </th>
                                       {{-- kiểm tra sản phẩm --}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Kiểm Tra
                                             </font>
                                          </font>
                                       </th>
                                       {{-- kiểm tra xem mới hay là đã sử dụng hay thuê--}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Kiểm Chứng
                                             </font>
                                          </font>
                                       </th>
                                       {{-- trạng thái --}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Trạng thái</font>
                                          </font>
                                       </th>
                                       {{-- sửa xóa --}}
                                       <th style="text-align: center;">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;" >Lựa Chọn
                                             </font>
                                          </font>
                                       </th>
                                    </tr>

                                 {{-- phầm blog hiển thị --}}

                                    @foreach($all_poster as $key=> $show_poster)
                                    <tr style="text-align: center;">
                                       {{-- têm Quảng Cáo --}}
                                       <td>
                                          <a href="#" class="btn-link">
                                             <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">
                                                   {{$show_poster->title}}
                                                </font>
                                             </font>
                                          </a> 
                                       </td>
                                       {{-- Hình ảnh quảng cáo --}}
                                       <td style="width: 160px;">
                                          <img src="../img_poster/{{$show_poster->img}}" 
                                       alt="hình ảnh"> 
                                       </td>
                                       {{--Tên danh mục sản phẩm  --}}
                                       <td>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                {{$show_poster->category->name}}
                                             </font>
                                          </font>
                                       </td>
                                       {{-- Tên Người Đăng --}}
                                       <td>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                {{$show_poster->user->firstname}}
                                             </font>
                                          </font>
                                       </td>
                                       {{-- Chọn Gói Dùng --}}
                                       <td>
                                          <a href="#" class="btn-link">
                                             <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">
                                                   <?php
                                                     if($show_poster->package == 0)
                                                     {
                                                     ?>
                                                      <span class="hienthi" id="0" style="color: #0DD902;">300->600$</span>
                                                     <?php
                                                     }elseif($show_poster->package == 1){
                                                     ?>
                                                      <span class="hienthi" id="1" style="color: #0DD902;">600->900$</span>
                                                     <?php
                                                     }
                                                     elseif($show_poster->package == 2){
                                                     ?>
                                                      <span class="hienthi" id="1" style="color: #0DD902;">900->1200$</span>
                                                     <?php
                                                     }
                                                     elseif($show_poster->package == 3){
                                                     ?>
                                                      <span class="hienthi" id="1" style="color: #0DD902;">1200->1500$</span>
                                                     <?php
                                                     }
                                                   ?> 
                                                </font>
                                             </font>
                                          </a> 
                                       </td>
                                       {{-- Kiểm Tra --}}
                                       <td>
                                          <a href="#" class="btn-link">
                                             <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">
                                                   <?php
                                                     if($show_poster->check == 0)
                                                     {
                                                     ?>
                                                      <span class="hienthi" id="0"style="color: #E60404;">Hết Hàng</span>
                                                     <?php
                                                     }elseif($show_poster->check == 1){
                                                     ?>
                                                      <span class="hienthi" id="1" style="color: #0DD902;">Còn Hàng</span>
                                                     <?php
                                                     }
                                                   ?>
                                                </font>
                                             </font>
                                          </a> 
                                       </td>
                                       {{-- Kiểm Chứng --}}
                                       <td>
                                          <a href="#" class="btn-link">
                                             <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">
                                                   <?php
                                                     if($show_poster->check_new == 0)
                                                     {
                                                     ?>
                                                      <span class="hienthi" id="0" style="color: #05a01f;">Mới</span>
                                                     <?php
                                                     }elseif($show_poster->check_new == 1){
                                                     ?>
                                                      <span class="hienthi" id="1" style="color: #ff382b;">Cho Thuê</span>
                                                     <?php
                                                     }
                                                     elseif($show_poster->check_new == 2){
                                                     ?>
                                                      <span class="hienthi" id="2" style="color: #868e96;">Đã Sử Dụng</span>
                                                     <?php
                                                     }
                                                   ?>
                                                </font>
                                             </font>
                                          </a> 
                                       </td>
                                       {{-- trạng thái --}}
                                       <td>
                                          <span>
                                             <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">
                                                   <?php
                                                     if($show_poster->status == 0)
                                                     {
                                                     ?>
                                                     <a class="btn btn-xs btn-danger blog" 
                                                      style="font-size: 18px;width: 70px; height: 30px;padding: 4px;"
                                                      href="{{URL::to('unactive-poster/'.$show_poster->id)}}">
                                                        Ẩn
                                                      </a>
                                                     <?php
                                                     }else{
                                                     ?>
                                                     <a class="btn btn-xs btn-green blog" 
                                                      style="font-size: 18px;width: 120px;height: 30px;padding: 4px;"
                                                      href="{{URL::to('active-poster/'.$show_poster->id)}}">
                                                      Hiển Thị
                                                      </a>
                                                     <?php
                                                     }
                                                   ?>  
                                                </font>
                                             </font>
                                          </span>
                                       </td> 
                                       {{-- sửa xóa --}}
                                       <td class="r" style="text-align: center;width: 160px;">
                                          <a class="btn btn-xs btn-info blog" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          href="{{URL::to('/edit-poster/'.$show_poster->id)}}">
                                             <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
                                          </a>
                                          <a class="btn btn-xs btn-danger blog" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                                          href="{{URL::to('/delete-poster/'.$show_poster->id)}}">
                                             <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                          </a>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>  
                        </div> 
                     </div>
                  </div>
               </div>
               <ul class="pagination mb-5">
               </ul>
            </div>
         </div>
      </div>
   </div>
<!--App-Content--> 
@endsection('content')
