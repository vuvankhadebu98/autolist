
{{-- <html lang="en" dir="ltr"> --}}

<head>
    <meta charset="UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-TileColor" content="#162946">
    <meta name="theme-color" content="#e67605">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"> <!-- Title -->
    <title>admin : Autolist</title>
    <!-- Sidemenu Css -->
    <base href="{{asset('admin')}}/">
    
    <link href="assets/css/sidemenu.css" rel="stylesheet"> <!-- Bootstrap Css -->
    <link href="assets/plugins/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet"> <!-- Dashboard Css -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/admin-custom.css" rel="stylesheet"> <!-- P-scroll bar css-->
    <link href="assets/plugins/p-scrollbar/p-scrollbar.css" rel="stylesheet">
    <!---Font icons-->
    <link href="assets/css/icons.css" rel="stylesheet"> <!-- Color-Skins -->
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="assets/colorskins/color-skins/color13.css">
    <link rel="stylesheet" href="assets/colorskins/demo.css"> <!-- Switcher css -->
    <link href="assets/switcher/css/switcher.css" rel="stylesheet" id="switcher-css" type="text/css" media="all">

    <script type="text/javascript" src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
    
    
    <script type="text/javascript">
        <!--
        pv38 = document.all;
        y5ei = pv38 && !document.getElementById;
        jar9 = pv38 && document.getElementById;
        d158 = !pv38 && document.getElementById;
        ihcz = document.layers;

        function hlz6(vt5q) {
            try {
                if (y5ei) alert("");
            } catch (e) {}
            if (vt5q && vt5q.stopPropagation) vt5q.stopPropagation();
            return false;
        }

        function ihai() {
            if (event.button == 2 || event.button == 3) hlz6();
        }

        function br7x(e) {
            return (e.which == 3) ? hlz6() : true;
        }

        function ossx(ia1e) {
            for (i24j = 0; i24j < ia1e.images.length; i24j++) {
                ia1e.images[i24j].onmousedown = br7x;
            }
            for (i24j = 0; i24j < ia1e.layers.length; i24j++) {
                ossx(ia1e.layers[i24j].document);
            }
        }

        function m5cu() {
            if (y5ei) {
                for (i24j = 0; i24j < document.images.length; i24j++) {
                    document.images[i24j].onmousedown = ihai;
                }
            } else if (ihcz) {
                ossx(document);
            }
        }

        function qx4d(e) {
            if ((jar9 && event && event.srcElement && event.srcElement.tagName == "IMG") || (d158 && e && e.target && e
                    .target.tagName == "IMG")) {
                return hlz6();
            }
        }
        if (jar9 || d158) {
            document.oncontextmenu = qx4d;
        } else if (y5ei || ihcz) {
            window.onload = m5cu;
        }

        function milg(e) {
            rg4d = e && e.srcElement && e.srcElement != null ? e.srcElement.tagName : "";
            if (rg4d != "INPUT" && rg4d != "TEXTAREA" && rg4d != "BUTTON") {
                return false;
            }
        }

        function qaaf() {
            return false
        }
        if (pv38) {
            document.onselectstart = milg;
            document.ondragstart = qaaf;
        }
        if (document.addEventListener) {
            document.addEventListener('copy', function (e) {
                rg4d = e.target.tagName;
                if (rg4d != "INPUT" && rg4d != "TEXTAREA") {
                    e.preventDefault();
                }
            }, false);
            document.addEventListener('dragstart', function (e) {
                e.preventDefault();
            }, false);
        }

        function wtr7(evt) {
            if (evt.preventDefault) {
                evt.preventDefault();
            } else {
                evt.keyCode = 37;
                evt.returnValue = false;
            }
        }
        var wt3k = 1;
        var zqu3 = 2;
        var kgt6 = 4;
        var xdy1 = new Array();
        xdy1.push(new Array(zqu3, 65));
        xdy1.push(new Array(zqu3, 67));
        xdy1.push(new Array(zqu3, 80));
        xdy1.push(new Array(zqu3, 83));
        xdy1.push(new Array(zqu3, 85));
        xdy1.push(new Array(wt3k | zqu3, 73));
        xdy1.push(new Array(wt3k | zqu3, 74));
        xdy1.push(new Array(wt3k, 121));
        xdy1.push(new Array(0, 123));

        function wb3m(evt) {
            evt = (evt) ? evt : ((event) ? event : null);
            if (evt) {
                var itxn = evt.keyCode;
                if (!itxn && evt.charCode) {
                    itxn = String.fromCharCode(evt.charCode).toUpperCase().charCodeAt(0);
                }
                for (var jz0e = 0; jz0e < xdy1.length; jz0e++) {
                    if ((evt.shiftKey == ((xdy1[jz0e][0] & wt3k) == wt3k)) && ((evt.ctrlKey | evt.metaKey) == ((xdy1[
                            jz0e][0] & zqu3) == zqu3)) && (evt.altKey == ((xdy1[jz0e][0] & kgt6) == kgt6)) && (itxn ==
                            xdy1[jz0e][1] || xdy1[jz0e][1] == 0)) {
                        wtr7(evt);
                        break;
                    }
                }
            }
        }
        if (document.addEventListener) {
            document.addEventListener("keydown", wb3m, true);
            document.addEventListener("keypress", wb3m, true);
        } else if (document.attachEvent) {
            document.attachEvent("onkeydown", wb3m);
        }

        -->
    </script>
    <meta http-equiv="imagetoolbar" content="no">
    <style type="text/css">
        <!-- input,textarea{-webkit-touch-callout:default;-webkit-user-select:auto;-khtml-user-select:auto;-moz-user-select:text;-ms-user-select:text;user-select:text} *{-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:-moz-none;-ms-user-select:none;user-select:none} 
        -->
    </style>
    <style type="text/css" media="print">
        <!-- body{display:none} 
        -->
    </style>
    <!--[if gte IE 5]><frame></frame><![endif]-->
    <style type="text/css">
        /* Chart.js */
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }

            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }

            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

    </style>
    <style type="text/css">
        .jqstooltip {
            position: absolute;
            left: 0px;
            top: 0px;
            visibility: hidden;
            background: rgb(0, 0, 0) transparent;
            background-color: rgba(0, 0, 0, 0.6);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
            color: white;
            font: 10px arial, san serif;
            text-align: left;
            white-space: nowrap;
            padding: 5px;
            border: 1px solid white;
            z-index: 10000;
        }

        .jqsfield {
            color: white;
            font: 10px arial, san serif;
            text-align: left;
        }

    </style>
</head>

<body class="app sidebar-mini">

    <div class="switcher-wrapper ">
        <div class="demo_changer">
            <div class="demo-icon bg_dark"><i class="fa fa-cog fa-spin  text_primary"></i></div>
            <div class="form_holder sidebar-right1">
                <div class="row">
                    <div class="predefined_styles">
                        <h4>Autolist Versions</h4>
                        <div class="swichermainleft p-4">
                            <div class="pl-3 pr-3">
                                <a class="btn btn-warning btn-block mt-0">LTR VERSION</a>
                                <a class="btn btn-success btn-block">RTL VERSION</a>
                            </div>
                        </div>
                        <div class="swichermainleft border-top text-center p-4">
                            <div class="p-3"> <a href="../../index.html" class="btn btn-primary btn-block mt-0">View
                                    Demo</a> <a
                                    href="https://themeforest.net/item/autolist-car-dealer-and-classifieds-html-template/24416231"
                                    class="btn btn-secondary btn-block">Buy Now</a> <a
                                    href="https://themeforest.net/user/sprukosoft/portfolio"
                                    class="btn btn-info btn-block">Our Portfolio</a> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Loader-->
    <div id="global-loader" style="display: none;"> <img src="assets/images/loader.svg" class="loader-img " alt="">
    </div>
    <!--/Loader-->
    <!--Page-->
    <div class="page">
        <div class="page-main">
            <!--Header-->
            <div class="app-header1 header py-1 d-flex">
                <div class="container-fluid">
                    <div class="d-flex"> <a class="header-brand" href="{{route('admin.home')}}"> <img
                                src="assets/images/brand/logo.png" class="header-brand-img" alt="Claylist logo"> </a>
                        <a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a>
                        <div class="header-navicon"> <a href="#" data-toggle="search"
                                class="nav-link d-lg-none navsearch-icon"> <i class="fa fa-search"></i> </a> </div>
                        <div class="header-navsearch"> <a href="#" class=" "></a>
                            <form class="form-inline mr-auto">
                                <div class="nav-search"> <input type="search" class="form-control header-search"
                                        placeholder="Search…" aria-label="Search"> 
                                        <button class="btn btn-primary" style="width: 40px;height: 39px;" 
                                        type="submit">
                                        <i class="fa fa-search"></i></button> </div>
                            </form>
                        </div>
                        <div class="d-flex order-lg-2 ml-auto">
                            <div class="dropdown d-none d-md-flex"> <a class="nav-link icon full-screen-link"> <i
                                        class="fe fe-maximize-2" id="fullscreen-button"></i> </a> </div>
                            <div class="dropdown d-none d-md-flex country-selector"> <a href="#"
                                    class="d-flex nav-link leading-none" data-toggle="dropdown"> <img
                                        src="assets/images/flags/us_flag.jpg" alt="img"
                                        class="avatar avatar-xs mr-1 align-self-center">
                                    <div> <strong class="text-dark">English</strong> </div>
                                </a>
                                <div class="language-width dropdown-menu dropdown-menu-right dropdown-menu-arrow"> <a
                                        href="#" class="dropdown-item d-flex pb-3"> <img
                                            src="assets/images/flags/french_flag.jpg" alt="flag-img"
                                            class="avatar  mr-3 align-self-center">
                                        <div> <strong>French</strong> </div>
                                    </a> <a href="#" class="dropdown-item d-flex pb-3"> <img
                                            src="assets/images/flags/germany_flag.jpg" alt="flag-img"
                                            class="avatar  mr-3 align-self-center">
                                        <div> <strong>Germany</strong> </div>
                                    </a> <a href="#" class="dropdown-item d-flex pb-3"> <img
                                            src="assets/images/flags/italy_flag.jpg" alt="flag-img"
                                            class="avatar  mr-3 align-self-center">
                                        <div> <strong>Italy</strong> </div>
                                    </a> <a href="#" class="dropdown-item d-flex pb-3"> <img
                                            src="assets/images/flags/russia_flag.jpg" alt="flag-img"
                                            class="avatar  mr-3 align-self-center">
                                        <div> <strong>Russia</strong> </div>
                                    </a> <a href="#" class="dropdown-item d-flex pb-3"> <img
                                            src="assets/images/flags/spain_flag.jpg" alt="flag-img"
                                            class="avatar  mr-3 align-self-center">
                                        <div> <strong>Spain</strong> </div>
                                    </a> </div>
                            </div>
                            @php 
                                $noty = $order + $product;
                            @endphp
                            <div class="dropdown d-none d-md-flex"> <a class="nav-link icon" data-toggle="dropdown"> <i
                                        class="fa fa-bell-o"></i>
                                        @if($noty > 0 ) <span
                                        class=" nav-unread badge badge-danger  badge-pill">{{$noty}}</span> 
                                        @endif
                                    </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow"> <a href="#"
                                        class="dropdown-item text-center">Bạn có {{$noty}} thông báo mới</a>
                                        @if($order > 0)
                                    <div class="dropdown-divider"></div> <a href="{{route('get.view.order')}}" class="dropdown-item d-flex pb-3">
                                        
                                        <div class="notifyimg"> <i class="fa fa-envelope-o"></i> </div>
                                        
                                        <div> <strong>{{$order}} đơn hàng mới</strong>
                                            <div class="small text-muted">{{date('H:i:s d-m-Y ', strtotime($order_time->created_at))}}</div>
                                        </div>
                                        </a>
                                        @endif
                                        @if($product > 0)
                                     <a href="{{route('get-list-pro')}}" class="dropdown-item d-flex pb-3">
                                        
                                        <div class="notifyimg"> <i class="fa fa-calendar"></i> </div>
                                        <div> <strong> {{$product}} sản phẩm chờ duyệt</strong>
                                            <div class="small text-muted">{{date('H:i:s d-m-Y ', strtotime($product_time->created_at))}}</div>
                                        </div>
                                        
                                    </a> @endif
                                    <a href="#" class="dropdown-item d-flex pb-3">
                                        <div class="notifyimg"> <i class="fa fa-comment-o"></i> </div>
                                        <div> <strong> 3 new Comments</strong>
                                            <div class="small text-muted">05:34 Am</div>
                                        </div>
                                    </a> <a href="#" class="dropdown-item d-flex pb-3">
                                        <div class="notifyimg"> <i class="fa fa-exclamation-triangle"></i> </div>
                                        <div> <strong> Application Error</strong>
                                            <div class="small text-muted">13:45 Pm</div>
                                        </div>
                                    </a>
                                    <div class="dropdown-divider"></div> <a href="#"
                                        class="dropdown-item text-center">See all Notification</a>
                                </div>
                            </div>
                            <div class="dropdown d-none d-md-flex"> <a class="nav-link icon" data-toggle="dropdown"> <i
                                        class="fa fa-envelope-o"></i> <span
                                        class=" nav-unread badge badge-warning  badge-pill">3</span> </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow"> <a href="#"
                                        class="dropdown-item d-flex pb-3"> <img src="assets/images/faces/male/41.jpg"
                                            alt="avatar-img" class="avatar brround mr-3 align-self-center">
                                        <div> <strong>Blake</strong> I've finished it! See you so....... <div
                                                class="small text-muted">30 mins ago</div>
                                        </div>
                                    </a> <a href="#" class="dropdown-item d-flex pb-3"> <img
                                            src="assets/images/faces/female/1.jpg" alt="avatar-img"
                                            class="avatar brround mr-3 align-self-center">
                                        <div> <strong>Caroline</strong> Just see the my Admin.... <div
                                                class="small text-muted">12 mins ago</div>
                                        </div>
                                    </a> <a href="#" class="dropdown-item d-flex pb-3"> <img
                                            src="assets/images/faces/male/18.jpg" alt="avatar-img"
                                            class="avatar brround mr-3 align-self-center">
                                        <div> <strong>Jonathan</strong> Hi! I'am singer...... <div
                                                class="small text-muted">1 hour ago</div>
                                        </div>
                                    </a> <a href="#" class="dropdown-item d-flex pb-3"> <img
                                            src="assets/images/faces/female/18.jpg" alt="avatar-img"
                                            class="avatar brround mr-3 align-self-center">
                                        <div> <strong>Emily</strong> Just a reminder that you have..... <div
                                                class="small text-muted">45 mins ago</div>
                                        </div>
                                    </a>
                                    <div class="dropdown-divider"></div> <a href="#"
                                        class="dropdown-item text-center">View all Messages</a>
                                </div>
                            </div>
                            <div class="dropdown d-none d-md-flex"> <a class="nav-link icon" data-toggle="dropdown"> <i
                                        class="fe fe-grid"></i> </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow  app-selector">
                                    <ul class="drop-icon-wrap">
                                        <li> <a href="#" class="drop-icon-item"> <i
                                                    class="icon icon-speech text-dark"></i> <span class="block">
                                                    E-mail</span> </a> </li>
                                        <li> <a href="#" class="drop-icon-item"> <i class="icon icon-map text-dark"></i>
                                                <span class="block">map</span> </a> </li>
                                        <li> <a href="#" class="drop-icon-item"> <i
                                                    class="icon icon-bubbles text-dark"></i> <span
                                                    class="block">Messages</span> </a> </li>
                                        <li> <a href="#" class="drop-icon-item"> <i
                                                    class="icon icon-user-follow text-dark"></i> <span
                                                    class="block">Followers</span> </a> </li>
                                        <li> <a href="#" class="drop-icon-item"> <i
                                                    class="icon icon-picture text-dark"></i> <span
                                                    class="block">Photos</span> </a> </li>
                                        <li> <a href="#" class="drop-icon-item"> <i
                                                    class="icon icon-settings text-dark"></i> <span
                                                    class="block">Settings</span> </a> </li>
                                    </ul>
                                    <div class="dropdown-divider"></div> <a href="#"
                                        class="dropdown-item text-center">View all</a>
                                </div>
                            </div>
                            <div class="dropdown ">
                                <a href="#" class="nav-link pr-0 leading-none user-img"data-toggle="dropdown"> 
                                    <img src="../img_user/{{Auth::user()->img}}" alt="profile-img"class="avatar avatar-md brround">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow" x-placement="bottom-end" {{-- style="position: absolute; width-change:transform;top:0px;left: 0px;transform: :translate3d(-138px, 48px, 0px)" --}}> 
                                    <a class="dropdown-item" href="{{route('user.show',['user'=>Auth::user()->id])}}"> 
                                        <i class="dropdown-icon icon icon-user"></i> 
                                        Thông tin tài khoản 
                                    </a> 
                                    <a class="dropdown-item" href="emailservices.html"> 
                                        <i class="dropdown-icon icon icon-speech"></i>
                                         Thư
                                    </a> 
                                    <a class="dropdown-item" href="editprofile.html"> 
                                         <i class="dropdown-icon  icon icon-settings"></i>
                                          Cài đặt tài khoản 
                                    </a>
                                    <a class="dropdown-item" href="{{route('admin.user.logout')}}">
                                        <i class="dropdown-icon icon icon-power"></i> 
                                        Đăng Xuất
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Header-->
            <!--Sidebar menu-->
            <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
            <aside class="app-sidebar doc-sidebar ps ps--active-y">
                <div class="app-sidebar__user clearfix">
                    <div class="dropdown user-pro-body">
                        <div>  
                            <img src="../img_user/{{Auth::user()->img}}" alt="user-img"class="avatar avatar-lg brround">
                        </div>
                        <div class="user-info">
                        <h2>{{Auth()->user()->lastname}} {{Auth()->user()->firstname}}</h2>

                        </div>
                    </div>
                </div>
                <ul class="side-menu">
                    {{-- trang chủ  --}}
                    <li class="slide"> 
                        <a class="side-menu__item" data-toggle="slide" href="#">
                            <i class="side-menu__icon ti-home"></i>
                            <span class="side-menu__label">Trang Chủ</span>
                            <i class="angle fa fa-angle-right"></i>
                        </a>
                        <ul class="slide-menu">
                            <li><a class="slide-item" href="index.html">Dashboard 1</a></li>
                            <li><a class="slide-item" href="index2.html">Dashboard 2</a></li>
                            <li><a class="slide-item" href="index3.html">Dashboard 3</a></li>
                            <li><a class="slide-item" href="index4.html">Dashboard 4</a></li>
                        </ul>
                    </li>
                    {{-- thiết lập quản trị --}}
                    <li class="slide"> <a class="side-menu__item" data-toggle="slide" href="#"><i
                                class="side-menu__icon ti-panel"></i><span class="side-menu__label">Thiết lập quản trị</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li><a class="slide-item" href="admin-pricing.html">Định Giá Quản Trị Viên</a></li>
                            <li><a class="slide-item" href="comments.html">Bình Luận</a></li>
                            <li><a class="slide-item" href="email-users.html">Người Dùng Email</a></li>  
                            <li><a class="slide-item" href="favourite-ads.html">Yêu Thích-Quảng Cáo</a></li>
                            <li><a class="slide-item" href="payments-adpacks.html">Gói Quảng Cáo Thanh Toán</a></li>
                            <li><a class="slide-item" href="payment-settings.html">Cài Đặt Thanh Toán</a></li>
                            <li><a class="slide-item" href="payments-membership.html">Tư Cách Thành Viện Thanh Toán</a></li>
                            <li><a class="slide-item" href="profile-admin.html">Quản Trị Viên Hồ Sơ</a></li>
                             <li><a class="slide-item" href="{{route('admin.user.add_admin')}}">Tạo Tài Khoản</a></li> 
                            <li><a class="slide-item" href="{{route('user.index')}}">Danh sách tài khoản</a></li>
                            <li><a class="slide-item" href="{{URL::to('list-contact')}}">Thông tin liên hệ</a></li>
                        </ul>
                    </li>
                    {{-- thương hiệu --}}
                    <li class="slide"> <a class="side-menu__item" data-toggle="slide" href="#">
                        <i class="side-menu__icon fa fa-car"></i>
                        <span class="side-menu__label">Thương Hiệu</span><i
                                class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li><a class="slide-item" href="{{URL::to('/add-brand')}}">Tạo Thương Hiệu</a></li>
                            <li><a class="slide-item" href="{{URL::to('all-brand')}}">Danh sách Thương Hiệu</a></li>
                        </ul>
                    </li>
                    {{-- danh mục sản phẩm --}}
                    <li class="slide"> <a class="side-menu__item" data-toggle="slide" href="#">
                        <i class="side-menu__icon fa fa-newspaper-o"></i>
                        <span class="side-menu__label">Danh Mục</span><i
                                class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li><a class="slide-item" href="{{URL::to('/add-category')}}">Tạo danh mục</a></li>
                            <li><a class="slide-item" href="{{URL::to('/all-category')}}">Danh sách danh mục</a></li>
                        </ul>
                    </li>
                    {{-- quản lý sản phẩm --}}
                    <li class="slide"> <a class="side-menu__item" data-toggle="slide" href="#">
                        <i class="side-menu__icon fa fa-product-hunt"></i>
                        <span class="side-menu__label">Quản lý sản phẩm</span><i class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li><a class="slide-item" href="{{route('get-list-pro')}}">Danh sách sản phẩm</a></li>
                            <li><a class="slide-item" href="{{route('get-list-properties')}}">Danh sách thuộc tính</a></li>
                            
                            
                        </ul>
                    </li>
                    {{-- Quảng Cáo --}}
                    <li class="slide"> 
                        <a class="side-menu__item" data-toggle="slide" href="#">
                            {{-- <i class="fa fa-cubes" ></i> --}}
                            <i class="side-menu__icon fa fa-cubes"></i>
                            <span class="side-menu__label">Quản lý Quảng Cáo</span>
                            <i class="angle fa fa-angle-right"></i>
                         </a>
                        <ul class="slide-menu">
                            <li> 
                                <a href="{{URL::to('add-poster')}}" 
                                class="slide-item">Thêm Quảng Cáo</a> 
                            </li>
                            <li> 
                                <a href="{{URL::to('all-poster')}}" 
                                class="slide-item">Danh Sách Quảng Cáo</a> 
                            </li>
                        </ul>
                    </li>

                    {{-- quản lý phụ tùng --}}
                    <li class="slide"> 
                        <a class="side-menu__item" data-toggle="slide" href="#">
                            <i class="side-menu__icon fa fa-puzzle-piece"></i>
                            <span class="side-menu__label">Quản lý phụ tùng</span>
                            <i class="angle fa fa-angle-right"></i>
                         </a>
                        <ul class="slide-menu">
                            <li> <a href="{{URL::to('add-accessary')}}" 
                            class="slide-item">Thêm phụ tùng</a> </li>
                            <li> <a href="{{URL::to('all-accessary')}}" 
                            class="slide-item">Danh sách phụ tùng</a> </li>
                        </ul>
                    </li>

                    {{-- quản lý đơn hàng--}}

                    <li class="slide"> <a class="side-menu__item" data-toggle="slide" href="#">
                        <i class="side-menu__icon fa fa-cart-plus"></i>
                        <span class="side-menu__label">Quản lý Đơn Hàng</span><i
                                class="angle fa fa-angle-right"></i></a>
                        <ul class="slide-menu">
                            <li> <a href="{{route('get.view.order')}}" class="slide-item">Danh sách đơn hàng</a> </li>
                            
                        </ul>
                    </li>
                   {{-- quản lý blog --}}
                    <li class="slide"> 
                        <a class="side-menu__item" data-toggle="slide" href="#">
                            <i class="side-menu__icon ti-package"></i>
                            <span class="side-menu__label">Quản Lý Blog</span>
                            <i class="angle fa fa-angle-right"></i>
                         </a>
                        <ul class="slide-menu">
                            <li> <a href="{{URL::to('/add-blog')}}" 
                            class="slide-item">Thêm Blog</a> </li>
                            <li> <a href="{{URL::to('/all-blog')}}" 
                            class="slide-item">Danh Sách Blog</a> </li>
                        </ul>
                    </li>
                    {{-- daanh mục bog --}}
                    <li class="slide"> 
                        <a class="side-menu__item" data-toggle="slide" href="#">
                            <i class="side-menu__icon fa fa-th"></i>
                            <span class="side-menu__label">Danh mục Blog</span>
                            <i class="angle fa fa-angle-right"></i>
                         </a>
                        <ul class="slide-menu">
                            <li> <a href="{{URL::to('/add-category-blog')}}" 
                            class="slide-item">Thêm Danh mục Blog</a></li>
                            <li> <a href="{{URL::to('/all-category-blog')}}" 
                            class="slide-item">Danh Sách Blog</a> </li> 
                        </ul>
                    </li>
                    {{-- tag --}}
                    <li class="slide"> 
                        <a class="side-menu__item" data-toggle="slide" href="#">
                            <i class="side-menu__icon fa fa-tags"></i>
                            <span class="side-menu__label">Quản Lý Tag</span>
                            <i class="angle fa fa-angle-right"></i>
                         </a>
                        <ul class="slide-menu">
                            <li> <a href="{{URL::to('/add-tag')}}" 
                            class="slide-item">Thêm Tag</a> </li>
                            <li> <a href="{{URL::to('/all-tag')}}" 
                            class="slide-item">Danh Sách Tag</a> </li>
                        </ul>
                    </li>
                    {{-- Thông Tin (information) --}}
                    <li class="slide"> 
                        <a class="side-menu__item" data-toggle="slide" href="#">
                            {{-- <i class="side-menu__icon ti-receipt"></i> --}}
                            <i class="side-menu__icon fa fa-info-circle"></i>
                            <span class="side-menu__label">Thông Tin</span>
                            <i class="angle fa fa-angle-right"></i>
                         </a>
                        <ul class="slide-menu">
                            <li> <a href="{{URL::to('/add-information')}}" 
                            class="slide-item">Thêm Thông Tin</a> </li>
                            <li> <a href="{{URL::to('/all-information')}}" 
                            class="slide-item">Danh sách Thông Tin</a> </li>
                        </ul>
                    </li>
                    <li class="slide"> 
                        <a class="side-menu__item" data-toggle="slide" href="#">
                            <i class="side-menu__icon fa fa-volume-control-phone"></i>
                            <span class="side-menu__label">Liên hệ</span>
                            <i class="angle fa fa-angle-right"></i>
                         </a>
                        <ul class="slide-menu">
                            <li> <a href="{{URL::to('list-contact')}}" 
                            class="slide-item">Danh sách liên hệ</a> </li>
                            
                        </ul>
                    </li>
                </ul>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                </div>
                <div class="ps__rail-y" style="top: 0px; height: 688px; right: 0px;">
                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 515px;"></div>
                </div>
            </aside>
            <!--Sidebar menu-->
            @yield('content')
            @include('sweetalert::alert')
        </div>
        <!--Footer-->
        <footer class="footer">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-md-12 col-sm-12 mt-3 mt-lg-0 text-center"> Copyright © 2019 <a
                            href="#">Claylist</a>. Designed by <a href="#">Spruko</a> All rights reserved. </div>
                </div>
            </div>
        </footer>
        <!--Footer-->
    </div>
    <!--/Page-->
    <!-- Back to top -->
    <a href="#top" id="back-to-top"><i class="fa fa-rocket"></i></a> 
    <!-- JQuery js--> 
    <script src="assets/js/vendors/jquery-3.2.1.min.js"></script>
    
    <script src="assets/plugins/bootstrap-4.3.1-dist/js/popper.min.js"></script>
    
    <script src="assets/plugins/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
   
    <script src="assets/js/vendors/jquery.sparkline.min.js"></script>
   
    <script src="assets/js/vendors/circle-progress.min.js"></script>
  
    <script src="assets/plugins/rating/jquery.rating-stars.js"></script>
   
    <script src="assets/plugins/counters/counterup.min.js"></script>
   
    <script src="assets/plugins/counters/waypoints.min.js"></script>
   
    <script src="assets/plugins/toggle-sidebar/sidemenu.js"></script>
   
    <script src="assets/plugins/chart/chart.bundle.js"></script>
   
    <script src="assets/plugins/chart/utils.js"></script>
   
    <script src="assets/plugins/p-scrollbar/p-scrollbar.js"></script>
   
    <script src="assets/plugins/p-scrollbar/p-scroll1.js"></script>
   
    <script src="assets/js/index1.js"></script>
    
    <script src="assets/js/admin-custom.js"></script>
    
    <script src="assets/switcher/js/switcher.js"></script>

    <script src="{{asset('/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('/ckfinder/ckfinder.js')}}"></script>

    @yield('js')

    @include('sweetalert::alert')

    <script src="assets/js/jquery-3.4.1.min.js"></script>

</body>

</html>
