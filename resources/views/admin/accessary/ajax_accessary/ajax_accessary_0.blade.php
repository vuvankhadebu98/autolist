<div class="tab-content" style="margin-top: -7px">
    <div class="tab-pane active" id="tab1">
       <div class="table-responsive border-top">
          <table id="example" class="table card-table table-bordered table-hover table-vcenter text-nowrap">
             <tbody>
                <tr style="text-align: center;">
                  {{-- mã phụ tùng--}}
                   <th class="w-1">
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Mã Phụ Tùng
                         </font>
                      </font>
                   </th>
                   {{-- tên accessary --}}
                   <th class="w-1">
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Tên Phụ Tùng
                         </font>
                      </font>
                   </th>
                   {{-- Ảnh accessary --}}
                   <th class="w-1">
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Ảnh Phụ Tùng</font>
                      </font>
                   </th>
                   {{-- tên user --}}
                   <th>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Tên Tài Khoản 
                         </font>
                      </font>
                   </th>
                   {{-- giá phụ tùng --}}
                    <th>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Giá Phụ Tùng
                         </font>
                      </font>
                   </th>
                   {{-- gói quảng cáo(package) --}}
                    <th>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Gói Quảng cáo
                         </font>
                      </font>
                   </th>
                    {{-- Kiểu Phụ Tùng(check) --}}
                    <th>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Kiểu Phụ Tùng
                         </font>
                      </font>
                   </th>
                   {{-- trạng thái --}}
                   <th>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">Trạng thái</font>
                      </font>
                   </th>
                   {{-- sửa xóa --}}
                   <th style="text-align: center;">
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;" >Lựa Chọn
                         </font>
                      </font>
                   </th>
                </tr>

             {{-- phầm blog hiển thị --}}

                @foreach($all_accessary as $key=> $show_accessary)
                <tr style="text-align: center;">
                  {{-- mã phụ tùng --}}
                   <td>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">
                            {{$show_accessary->code}}
                         </font>
                      </font> 
                   </td>
                   {{-- têm phụ tùng --}}
                   <td>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">
                            {{$show_accessary->name}}
                         </font>
                      </font> 
                   </td>
                   {{-- Hình ảnh phụ tùng --}}
                   <td style="width: 180px;">
                      <img src="../img_accessary/{{$show_accessary->img}}" 
                   alt="hình ảnh"width="180px" height="70px"> 
                   </td>
                   {{-- tên user --}}
                   <td>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">
                            {{$show_accessary->user->firstname}}
                         </font>
                      </font>
                   </td>
                   {{-- giá Phụ tùng --}}
                   <td>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">
                            {{number_format($show_accessary->price,0,',','.')}} vnđ
                         </font>
                      </font>
                   </td>
                   {{-- gói quảng cáo --}}
                   <td>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">
                            <?php
                             if($show_accessary->package == 0)
                             {
                             ?>
                              <span class="hienthi" id="0" style="color: #0DD902;">30->60$</span>
                             <?php
                             }elseif($show_accessary->package == 1){
                             ?>
                              <span class="hienthi" id="1" style="color: #09B5C8;">60->90$</span>
                             <?php
                             }
                             elseif($show_accessary->package == 2){
                             ?>
                              <span class="hienthi" id="1" style="color: #00EC99;">90->120$</span>
                             <?php
                             }
                             elseif($show_accessary->package == 3){
                             ?>
                              <span class="hienthi" id="1" style="color: #0C00EF;">120->150$</span>
                             <?php
                             }
                             elseif($show_accessary->package == 4){
                             ?>
                              <span class="hienthi" id="1" style="color: #0C00FF;">150->180$</span>
                             <?php
                             }
                           ?> 
                         </font>
                      </font>
                   </td>
                   {{-- chọn kiểu phụ tùng --}}
                   {{-- gói quảng cáo --}}
                   <td>
                      <font style="vertical-align: inherit;">
                         <font style="vertical-align: inherit;">
                            <?php
                             if($show_accessary->check == 0)
                             {
                             ?>
                              <span class="hienthi" id="0" style="color: #0DD902;">Phụ Tùng Bán</span>
                             <?php
                             }elseif($show_accessary->check == 1){
                             ?>
                              <span class="hienthi" id="1" style="color: #09B5C8;">Phụ Tùng Quảng Cáo</span>
                             <?php
                             }
                           ?> 
                         </font>
                      </font>
                   </td>
                   {{-- trạng thái --}}
                   <td>
                      <span>
                         <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">
                               <?php
                                 if($show_accessary->status == 0)
                                 {
                                 ?>
                                 <a class="btn btn-xs btn-danger blog" 
                                  style="font-size: 18px;width: 70px; height: 30px;padding: 4px;"
                                  href="{{URL::to('unactive-accessary/'.$show_accessary->id)}}">
                                    Ẩn
                                  </a>
                                 <?php
                                 }else{
                                 ?>
                                 <a class="btn btn-xs btn-green blog" 
                                  style="font-size: 18px;width: 120px;height: 30px;padding: 4px;"
                                  href="{{URL::to('active-accessary/'.$show_accessary->id)}}">
                                  Hiển Thị
                                  </a>
                                 <?php
                                 }
                               ?>  
                            </font>
                         </font>
                      </span>
                   </td> 
                   {{-- sửa xóa --}}
                   <td class="r" style="text-align: center;width: 160px;">
                      <a class="btn btn-xs btn-info blog" 
                      style="font-size: 18px;width: 70px; height: 30px;"
                      href="{{URL::to('/edit-accessary/'.$show_accessary->id)}}">
                         <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
                      </a>
                      <a class="btn btn-xs btn-danger blog" 
                      style="font-size: 18px;width: 70px; height: 30px;"
                      onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                      href="{{URL::to('/delete-accessary/'.$show_accessary->id)}}">
                         <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                      </a>
                   </td>
                </tr>
                @endforeach
             </tbody>
          </table>
       </div>  
    </div> 
 </div>