@extends('admin.master')
@section('title','Home')
@section('content')

   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Phụ Tùng mới</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Phụ Tùng mới</font>
                  </font>
               </li>
            </ol>
         </div>
         <?php 
              $message = Session::get('message');
              if ($message) {
                  echo '<div class="alert alert-success">'. $message .'</div>';
                  Session::put('message', null);
              }
            ?>
         <div class="row row-cards">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Tạo Phụ Tùng Mới</font>
                          <a href="{{URL::to('all-accessary')}}" class="btn" aria-expanded="false" style="vertical-align: inherit; font-size: 18px;color: #20BAED">Danh Sách Phụ Tùng</a> 
                        </font>
                     </div>
                  </div>
                  {{-- thêm mới --}}
                  <form role="form" action="{{URL::to('/save-accessary')}}" method="post" 
                     enctype="multipart/form-data">
                    {{ csrf_field()}}
                     <div class="card-body">
                      {{-- user_id --}}
                        <div class="form-group">
                           <label name="exampleInputPassword1">người đăng</label>
                           <select name="user_admin" class="form-control m-bot15">
                           @foreach($user as $key => $user_id)                   
                               <option value="{{$user_id->id}}">{{$user_id->firstname}}</option>  
                           @endforeach       
                           </select>
                        </div>
                        {{-- mã phụ tùng --}}
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Mã Phụ Tùng</font>
                              </font>
                           </label> 
                           <input type="number" name="code" class="form-control w-100" placeholder="Nhập mã phụ tùng">
                           <p class="help is-danger">{{ $errors->first('code') }}</p> 
                        </div>
                      {{-- tên phụ tùng --}}
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Tên Phụ Tùng</font>
                              </font>
                           </label> 
                           <input type="text" name="name" class="form-control w-100" placeholder="Nhập tên phụ tùng">
                           <p class="help is-danger">{{ $errors->first('name') }}</p> 
                        </div>
                        {{-- hình ảnh phụ tùng --}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Hình ảnh phụ tùng</label>
                            <input type="file" name="img" class="form-control" id="img">
                            <p class="help is-danger">{{ $errors->first('img') }}</p>
                        </div>
                        {{-- giá phụ tùng --}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá Phụ Tùng</label>
                            <input type="number" name="price" class="form-control">
                            <p class="help is-danger">{{ $errors->first('price') }}</p>
                        </div>
                        {{-- rate --}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Đánh giá(1->5)</label>
                            <input type="number" name="rate" class="form-control" id="rate">
                            <p class="help is-danger">{{ $errors->first('rate') }}</p>
                        </div>
                        {{-- view --}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Lượt Xem</label>
                            <input type="number" name="view" class="form-control" id="view">
                            <p class="help is-danger">{{ $errors->first('view') }}</p>
                        </div>
                        {{-- mô tả phụ tùng --}}
                        <div class="form-group">
                           <label class="form-label">Mô Tả Phụ Tùng</label> 
                           <textarea name="description" id="description"
                            style="resize: none;"rows="8"class="form-control">
                           </textarea>
                          <p class="help is-danger">{{ $errors->first('description') }}</p>
                        </div>
                        {{-- nội dung phụ tùng --}}
                        <div class="form-group">
                           <label class="form-label">Nội Dung Phụ Tùng</label> 
                           <textarea name="content" id="content"
                            style="resize: none;"rows="8"class="form-control">
                           </textarea>
                          <p class="help is-danger">{{ $errors->first('content') }}</p>
                        </div>
                        {{-- package--}}
                         <div class="form-group">
                           <label name="exampleInputPassword1">Gói Quảng Cáo</label>
                           <select name="package" class="form-control m-bot15">
                               <option value="0">30->60$</option>
                               <option value="1">60->90$</option>
                               <option value="2">90->120$</option>
                               <option value="3">120->150$</option>
                               <option value="4">150->180$</option>       
                           </select>
                        </div>
                        {{-- check--}}
                         <div class="form-group">
                           <label name="exampleInputPassword1">Chọn kiểu phụ tùng</label>
                           <select name="check" class="form-control m-bot15">
                               <option value="0">Phụ Tùng Bán</option>
                               <option value="1">Phụ Tùng Quảng Cáo</option>   
                           </select>
                        </div>
                        {{-- hiển thị --}}
                         <div class="form-group">
                           <label name="exampleInputPassword1">Hiển thị</label>
                           <select name="status" class="form-control m-bot15">
                               <option value="1">Hiển thị</option>
                               <option value="0">Ẩn</option>       
                           </select>
                        </div>
                        <button type="submit" name="add_blog" 
                        class="btn btn-primary waves-effect waves-light">
                           <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Thêm mới phụ tùng</font>
                           </font>
                        </button> 
                     </div>  
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--App-Content--> 
@endsection('content')

@section('js')
    <script>
        CKEDITOR.replace('description'); 
        CKEDITOR.replace('content');     
    </script>
@endsection