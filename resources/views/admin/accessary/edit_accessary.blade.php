@extends('admin.master')
@section('title','Home')
@section('content')

   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Cập nhật Phụ Tùng</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Cập nhật Phụ Tùng</font>
                  </font>
               </li>
            </ol>
         </div>
         
         <div class="row row-cards">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">
                        <font style="vertical-align: inherit;">
                           <font style="vertical-align: inherit;">Cập nhật Phụ Tùng</font>
                        </font>
                     </div>
                  </div>
                  {{-- thêm mới --}}
                  @foreach($edit_accessary as $key => $edit_acc)
                    <form role="form" action="{{URL::to('/update-accessary/'.$edit_acc->id)}}" method="post" 
                     enctype="multipart/form-data">
                    {{ csrf_field()}}
                     <div class="card-body">
                     	{{-- user_id --}}
                        <div class="form-group">
                           <label name="exampleInputPassword1">User</label>
                           <select name="user_admin" class="form-control m-bot15">
                            @foreach($user as $key => $user_id)
                              @if($user_id->id == $edit_acc->user_id)
                                <option selected  value="{{$user_id->id}}">{{$user_id->firstname}}</option>
                              @else
                                <option value="{{$user_id->id}}">{{$user_id->firstname}}</option>
                              @endif 
                           @endforeach       
                           </select>
                        </div>
                        {{-- mã phụ tùng --}}
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Mã Phụ Tùng</font>
                              </font>
                           </label> 
                           <input type="number" name="code" class="form-control w-100" placeholder="Nhập mã phụ tùng" 
                           value="{{$edit_acc->code}}">
                           <p class="help is-danger">{{ $errors->first('code') }}</p> 
                        </div>
                     	{{-- tên phụ tùng --}}
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Tên phụ tùng</font>
                              </font>
                           </label> 
                           <input type="text" name="name" class="form-control w-100" placeholder="Nhập tên blog" value="{{$edit_acc->name}}"> 
                        </div>
                        {{-- hình ảnh phụ tùng --}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Hình ảnh phụ tùng</label>
                            <input type="file" name="img" class="form-control" id="img">
                            <img src="{{URL::to('/img_accessary/'.$edit_acc->img)}}" height="90" width="120" style="margin-left: 5px"> 
                        </div>
                        {{-- giá phụ tùng --}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá Phụ Tùng</label>
                            <input type="number" name="price" class="form-control" value="{{$edit_acc->price}}">
                            <p class="help is-danger">{{ $errors->first('price') }}</p>
                        </div>
                        {{-- rate --}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Đánh giá(1->5)</label>
                            <input type="number" name="rate" class="form-control" value="{{$edit_acc->rate}}">
                            <p class="help is-danger">{{ $errors->first('rate') }}</p>
                        </div>
                        {{-- view --}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Lượt Xem</label>
                            <input type="number" name="view" class="form-control" value="{{$edit_acc->view}}">
                            <p class="help is-danger">{{ $errors->first('view') }}</p>
                        </div>
                        {{-- mô tả phụ tùng --}}
                        <div class="form-group">
                           <label class="form-label">Mô Tả Phụ Tùng</label> 
                           <textarea name="description" id="description" style="resize: none;"rows="8"
                           class="form-control">{{$edit_acc->description}}  
                           </textarea> 
                           <p class="help is-danger">{{ $errors->first('description') }}</p>
                        </div>
                        {{-- nội dung phụ tùng --}}
                        <div class="form-group">
                           <label class="form-label">Nội dung Phụ Tùng</label> 
                           <textarea name="content" id="content" style="resize: none;"rows="8"
                           class="form-control">{{$edit_acc->content}}  
                           </textarea> 
                           <p class="help is-danger">{{ $errors->first('content') }}</p>
                        </div>
                        {{-- package--}}
                         <div class="form-group">
                           <label name="exampleInputPassword1">Gói Quảng Cáo</label>
                           <select name="package" class="form-control m-bot15">
                               <option value="0"{{$edit_acc->package == 0 ? "selected" : ""}}>30->60$</option>
                               <option value="1"{{$edit_acc->package == 1 ? "selected" : ""}}>60->90$</option>
                               <option value="2"{{$edit_acc->package == 2 ? "selected" : ""}}>90->120$</option>
                               <option value="3"{{$edit_acc->package == 3 ? "selected" : ""}}>120->150$</option>
                               <option value="4"{{$edit_acc->package == 4 ? "selected" : ""}}>150->180$</option>       
                           </select>
                        </div>
                        {{-- check--}}
                         <div class="form-group">
                           <label name="exampleInputPassword1">Chọn kiểu phụ tùng</label>
                           <select name="check" class="form-control m-bot15">
                               <option value="0"{{$edit_acc->check == 0 ? "selected" : ""}}>Phụ Tùng Bán</option>
                               <option value="1"{{$edit_acc->check == 1 ? "selected" : ""}}>Phụ Tùng Quảng Cáo</option>   
                           </select>
                        </div>
                        {{-- status --}}
                         <div class="form-group">
                           <label name="exampleInputPassword1">Hiển thị</label>
                           <select name="status" class="form-control m-bot15">
                              <option value="1" {{$edit_acc->status == 1 ? "selected" : ""}}>Hiển Thị</option>
                              <option value="0" {{$edit_acc->status == 0 ? "selected" : ""}}>Ẩn</option>       
                           </select>
                        </div>
                        <button type="submit" name="update_blog" 
                        class="btn btn-primary waves-effect waves-light">
                           <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Cập nhật phụ tùng</font>
                           </font>
                        </button> 
                     </div>            
                  </form>
                @endforeach
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--App-Content--> 
@endsection('content')

@section('js')
    <script>
        CKEDITOR.replace('description'); 
        CKEDITOR.replace('content');      
    </script>
@endsection