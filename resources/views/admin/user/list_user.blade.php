@extends('admin.master')
@section('title','Danh sách tài khoản')
@section('content')
<!--App-Content-->
<div class="app-content  my-3 my-md-5">
    <div class="side-app">
        <div class="page-header">
            <h1 class="page-title">Danh sách tài khoản</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Thiết lập</a></li>
                <li class="breadcrumb-item active" aria-current="page">Danh sách tài khoản</li>
            </ol>
        </div>
        <div class="row ">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class=" ">
                        <div class="user-tabs mb-4">
                            <!-- Tabs -->
                            <ul class="nav panel-tabs">
                                <li class=""><a href="#tab1" class="active" data-toggle="tab">Tất cả
                                        ({{count($user)}})</a></li>
                                <li><a href="#tab2" data-toggle="tab">Cộng tác viên ({{count($user_admin)}})</a></li>
                                <li><a href="#tab3" data-toggle="tab">Khách hàng ({{count($user_customer)}})</a></li>
                                {{-- tìm kiếm --}}
                                <form method="post" class="form-inline mr-auto-2" style="float: right;" 
                                action="{{route('search-user')}}">
                                    <div class="nav-search-3"> 
                                       <input type="search" class="form-control header-search-2" placeholder="Search…" aria-label="Search" name="key"> 
                                       <button class="btn btn-primary" style="width: 45px;height: 39px;" type="submit">
                                          <i class="fa fa-search"></i>
                                       </button> 
                                    </div>
                                </form>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="tab-content">
                            {{-- Tất cả tài khoản --}}
                            <div class="tab-pane active " id="tab1">
                                <div class="mail-option">
                                    <div class="chk-all">
                                        <div class="btn-group"> <a data-toggle="dropdown" href="#"
                                                class="btn mini all" aria-expanded="false"> Thực hiện nhiều 
                                                <i class="fa fa-angle-down "></i> </a>
                                            <ul class="dropdown-menu">
                                                <li id="lock_User"><a>Khóa</a></li>
                                                <li id="unlock_User"><a>Kích hoạt</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Apply </a> </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Kiểm tra hàng loạt </a> </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Báo cáo hàng loạt </a> </div>
                                    <div class="btn-group hidden-phone"> 
                                        <select onchange="convert_User(this)">
                                            <option value="">Chuyển đổi chức vụ</option>
                                            @foreach($user_role as $u)
                                            <option  >{{$u->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="btn-group hidden-phone"> 
                                        <a href="#" class="btn mini blue">
                                            Chuyển đổi </a> </div>
                                    {{-- <ul class="unstyled inbox-pagination">
                                        <li><span>1-20 of 1,737 items</span></li>
                                        <li> <a class="np-btn" href="#"><i
                                                    class="fa fa-angle-right pagination-right"></i></a>
                                        </li>
                                    </ul> --}}
                                </div>
                                <div class="table-responsive border-top">
                                    <table
                                        class="table card-table table-bordered table-hover table-vcenter mb-0 text-nowrap">
                                        <tbody>
                                            <tr>
                                                <th class="w-1"></th>
                                                <th class="w-1">Mã</th>
                                                <th class="w-1">Họ và Tên</th>
                                                <th class="w-1">Chức vụ</th>
                                                <th class="w-1">Email</th>
                                                <th class="w-1">Posts</th>
                                                <th class="w-1">Trạng thái</th>
                                                <th class="w-1">Ads</th>
                                                <th class="w-1">Đăng nhập gần nhất</th>
                                                <th class="w-1">Thời gian đăng kí</th>
                                            </tr>
                                            @foreach($user_admin as $u)
                                            <tr >
                                                <th> 
                                                    <label class="custom-control custom-checkbox"> 
                                                        <input id-index="{{$u->id}}" type="checkbox" class="custom-control-input" name="checkbox" value="checkbox">
                                                        <span class="custom-control-label"></span>
                                                    </label>
                                                </th>
                                                <td>{{$u->id}}</td>
                                                <td>
                                                    <div class="d-flex">
                                                        <span class="avatar avatar-md  d-block brround cover-image mr-3"
                                                            data-image-src="../img_user/{{$u->img}}"
                                                            style="background: url(&quot;../assets/images/faces/male/25.jpg&quot;) center center;"></span>
                                                        <span class="mt-2">{{$u->lastname}} {{$u->firstname}}</span>
                                                    </div>
                                                </td>
                                                <td>{{$u->role_name}}</td>
                                                <td>{{$u->email}}</td>
                                                <td>12</td>
                                                <td class="text-success" id="{{$u->id}}" >{{$u->status}}</td>
                                                    <td>85</td>
                                                <td class="w-100">{{$u->updated_at}}</td>
                                                    <td>{{$u->created_at}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="mail-option mb-0 mt-4">
                                    <div class="chk-all">
                                        <div class="btn-group"> <a data-toggle="dropdown" href="#"
                                                class="btn mini all" aria-expanded="false"> Thực hiện nhiều <i
                                                    class="fa fa-angle-down "></i> </a>
                                            <ul class="dropdown-menu">
                                                <li id="lock_User"><a >Khóa</a></li>
                                                <li  id="unlock_User"><a >Kích Hoạt</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Áp dụng </a> </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Kiểm tra hàng loạt </a> </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Báo cáo hàng loạt </a> </div>
                                    <div class="btn-group hidden-phone"> <a data-toggle="dropdown" href="#"
                                            class="btn mini blue" aria-expanded="false"> Chuyển đổi chức danh <i
                                                class="fa fa-angle-down "></i> </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Cộng Tác Viên</a></li>
                                            <li><a href="#">Khách Hàng</a></li>
                                        </ul>
                                    </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn mini blue">
                                            Chuyển đổi </a> </div>
                                    {{-- <ul class="unstyled inbox-pagination mb-0">
                                        <li><span>1-20 of 1,737 items</span></li>
                                        <li> <a class="np-btn" href="#"><i
                                                    class="fa fa-angle-right pagination-right"></i></a>
                                        </li>
                                    </ul> --}}
                                </div>
                            </div>
                            {{-- Tài khoản quản trị --}}
                            {{-- <div class="tab-pane " id="tab2">
                                <div class="mail-option">
                                    <div class="chk-all">
                                        <div class="btn-group"> <a data-toggle="dropdown" href="#"
                                                class="btn mini all" aria-expanded="false"> Bulk Actions <i
                                                    class="fa fa-angle-down "></i> </a>
                                            <ul class="dropdown-menu">
                                                <li ><a>Khóa </a></li>
                                                <li id=""><a>Mở Khóa</a></li>                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Apply </a> </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Bulk Check Sploggers </a> </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Bulk Report Sploggers </a> </div>
                                    <div class="btn-group hidden-phone"> <a data-toggle="dropdown" href="#"
                                            class="btn mini blue" aria-expanded="false"> Change role to <i
                                                class="fa fa-angle-down "></i> </a>
                                        <ul class="dropdown-menu">
                                            <li ><a href="#">Contributor</a></li>
                                            <li><a href="#">Register</a></li>
                                        </ul>
                                    </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn mini blue">
                                            Change </a> </div>
                                    <ul class="unstyled inbox-pagination">
                                        <li><span>1-20 of 1,734 items</span></li>
                                        <li> <a class="np-btn" href="#"><i
                                                    class="fa fa-angle-right pagination-right"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="table-responsive border-top">
                                    <table
                                        class="table card-table table-bordered table-hover table-vcenter mb-0 text-nowrap">
                                        <tbody>
                                            <tr>
                                                <th class="w-1"></th>
                                                <th class="w-1">Id</th>
                                                <th class="w-1">Image</th>
                                                <th>Designation</th>
                                                <th>Email</th>
                                                <th>Posts</th>
                                                <th>Status</th>
                                                <th>Ads</th>
                                                <th>Last Login</th>
                                                <th>Registered</th>
                                            </tr>
                                            @foreach($user_admin as $u)
                                            <tr >
                                                <th> 
                                                    <label class="custom-control custom-checkbox"> 
                                                            <input type="checkbox" class="custom-control-input" name="checkbox" value="checkbox">
                                                             <span class="custom-control-label"></span>
                                                     </label>
                                                </th>
                                                <td>{{$u->id}}</td>
                                                    <td>
                                                        <div class="d-flex"> <span
                                                                class="avatar avatar-md  d-block brround cover-image mr-3"
                                                                data-image-src="../img_user/{{$u->img}}"
                                                                style="background: url(&quot;../assets/images/faces/male/25.jpg&quot;) center center;"></span>
                                                        <span class="mt-2">{{$u->lastname}} {{$u->firstname}}</span> </div>
                                                    </td>
                                                    <td>{{$u->roles->name}}</td>
                                                    <td>{{$u->email}}</td>
                                                    <td>12</td>
                                                <td class="text-success" id="{{$u->id}}" >{{$u->status}}</td>
                                                    <td>85</td>
                                                <td class="w-100">{{$u->updated_at}}</td>
                                                    <td>{{$u->created_at}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="mail-option mb-0 mt-4">
                                    <div class="chk-all">
                                        <div class="btn-group"> <a data-toggle="dropdown" href="#"
                                                class="btn mini all" aria-expanded="false"> Bulk Actions <i
                                                    class="fa fa-angle-down "></i> </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Bulk Actions</a></li>
                                                <li><a href="#">Delete</a></li>
                                                <li><a href="#">Activate</a></li>
                                                <li><a href="#">Deactivate</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Apply </a> </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Bulk Check Sploggers </a> </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Bulk Report Sploggers </a> </div>
                                    <div class="btn-group hidden-phone"> <a data-toggle="dropdown" href="#"
                                            class="btn mini blue" aria-expanded="false"> Change role to <i
                                                class="fa fa-angle-down "></i> </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Contributor</a></li>
                                            <li><a href="#">Register</a></li>
                                        </ul>
                                    </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn mini blue">
                                            Change </a> </div>
                                    <ul class="unstyled inbox-pagination mb-0">
                                        <li><span>1-20 of 1,734 items</span></li>
                                        <li> <a class="np-btn" href="#"><i
                                                    class="fa fa-angle-right pagination-right"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div> --}}
                            {{-- Tài khoản khách hàng --}}
                            {{-- <div class="tab-pane " id="tab3">
                                <div class="mail-option">
                                    <div class="chk-all">
                                        <div class="btn-group"> <a data-toggle="dropdown" href="#"
                                                class="btn mini all" aria-expanded="false"> Bulk Actions <i
                                                    class="fa fa-angle-down "></i> </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Bulk Actions</a></li>
                                                <li><a href="#">Delete</a></li>
                                                <li><a href="#">Activate</a></li>
                                                <li><a href="#">Deactivate</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Apply </a> </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Bulk Check Sploggers </a> </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Bulk Report Sploggers </a> </div>
                                    <div class="btn-group hidden-phone"> <a data-toggle="dropdown" href="#"
                                            class="btn mini blue" aria-expanded="false"> Change role to <i
                                                class="fa fa-angle-down "></i> </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Contributor</a></li>
                                            <li><a href="#">Register</a></li>
                                        </ul>
                                    </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn mini blue">
                                            Change </a> </div>
                                    <ul class="unstyled inbox-pagination">
                                        <li><span>1-3 of 3 items</span></li>
                                        <li> <a class="np-btn" href="#"><i
                                                    class="fa fa-angle-right pagination-right"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="table-responsive border-top">
                                    <table
                                        class="table card-table table-bordered table-hover table-vcenter text-nowrap">
                                        <tbody>
                                            <tr>
                                                <th class="w-1"></th>
                                                <th class="w-1">Id</th>
                                                <th class="w-1">Image</th>
                                                <th>Designation</th>
                                                <th>Email</th>
                                                <th>Posts</th>
                                                <th>Status</th>
                                                <th>Ads</th>
                                                <th>Last Login</th>
                                                <th>Registered</th>
                                            </tr>
                                            @foreach($user_customer as $u)
                                            <tr >
                                                <th> 
                                                    <label class="custom-control custom-checkbox"> 
                                                            <input id-index="{{$u->id}}" type="checkbox" class="custom-control-input" name="checkbox" value="checkbox">
                                                             <span class="custom-control-label"></span>
                                                     </label>
                                                </th>
                                                <td>{{$u->id}}</td>
                                                    <td>
                                                        <div class="d-flex"> <span
                                                                class="avatar avatar-md  d-block brround cover-image mr-3"
                                                                data-image-src="../img_user/{{$u->img}}"
                                                                style="background: url(&quot;../assets/images/faces/male/25.jpg&quot;) center center;"></span>
                                                        <span class="mt-2">{{$u->lastname}} {{$u->firstname}}</span> </div>
                                                    </td>
                                                    <td>{{$u->roles->name}}</td>
                                                    <td>{{$u->email}}</td>
                                                    <td>12</td>
                                                <td class="text-success" id="{{$u->id}}" >{{$u->status}}</td>
                                                    <td>85</td>
                                                <td class="w-100">{{$u->updated_at}}</td>
                                                    <td>{{$u->created_at}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="mail-option mb-0 mt-4">
                                    <div class="chk-all">
                                        <div class="btn-group"> <a data-toggle="dropdown" href="#"
                                                class="btn mini all" aria-expanded="false"> Bulk Actions <i
                                                    class="fa fa-angle-down "></i> </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Bulk Actions</a></li>
                                                <li><a href="#">Delete</a></li>
                                                <li><a href="#">Activate</a></li>
                                                <li><a href="#">Deactivate</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Apply </a> </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Bulk Check Sploggers </a> </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn"
                                            aria-expanded="false"> Bulk Report Sploggers </a> </div>
                                    <div class="btn-group hidden-phone"> <a data-toggle="dropdown" href="#"
                                            class="btn mini blue" aria-expanded="false"> Change role to <i
                                                class="fa fa-angle-down "></i> </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Contributor</a></li>
                                            <li><a href="#">Register</a></li>
                                        </ul>
                                    </div>
                                    <div class="btn-group hidden-phone"> <a href="#" class="btn mini blue">
                                            Change </a> </div>
                                    <ul class="unstyled inbox-pagination mb-0">
                                        <li><span>1-3 of 3 items</span></li>
                                        <li> <a class="np-btn" href="#"><i
                                                    class="fa fa-angle-right pagination-right"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <ul class="pagination mb-5">
                    <li class="page-item page-prev disabled"> <a class="page-link" href="#"
                            tabindex="-1">Quay lại</a> </li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                    <li class="page-item page-next"> <a class="page-link" href="#">Tiếp</a> </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--/App-Content-->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<script> 
    // Hàm bắt sự kiên checked Khóa tài khoản
    $( "#lock_User" ).click(function() {
        var check = $("input[type='checkbox']").is(":checked");
        var someObj={};
        someObj.fruitsGranted=[];
        someObj.fruitsDenied=[];

        
        $("input[type='checkbox']").each(function(){
                var $this = $(this);
                if($this.is(":checked")){
                    someObj.fruitsGranted.push($this.attr("id-index"));
                }else{
                    someObj.fruitsDenied.push($this.attr("id-index"));
                }
        });
        
        $.ajax({ 
            url: '{{route('admin.user.lock')}}',
            method: 'GET',
            data: {
                id:someObj.fruitsGranted,
            },
            success: function (data) {
                for (let index = 0; index < data.length; index++) {
                    var i = data[index];
                    console.log(i);
                    $('#'+i).html('Bị Khóa');   
                }
                alertify.success('Khóa thành công');
            }
        });
       
    });

   // Hàm bắt sự khoản mở Khóa
    $( "#unlock_User" ).click(function() {
        var check = $("input[type='checkbox']").is(":checked");
        var someObj_unlock={};
        someObj_unlock.fruitsGranted=[];
        someObj_unlock.fruitsDenied=[];

        
        $("input[type='checkbox']").each(function(){
                var $this = $(this);
                if($this.is(":checked")){
                    someObj_unlock.fruitsGranted.push($this.attr("id-index"));
                }else{
                    someObj_unlock.fruitsDenied.push($this.attr("id-index"));
                }
        });
        
        $.ajax({ 
            url: '{{route('admin.user.unlock')}}',
            method: 'GET',
            data: {
                id:someObj_unlock.fruitsGranted,
            },
            success: function (data) {
                for (let index = 0; index < data.length; index++) {
                    var i = data[index];
                    console.log(i);
                    $('#'+i).html('Đang Hoạt Động');   
                }
                alertify.success('Mở khóa thành công');
            }
        });
       
    });   

    
</script>
<script>
    function genderChanged(obj)
    {
                     
    }
     //Hàm bắt sự đổi chức vụ 
     $( "#convert_User").onchange(function() {
        alert('cuong');



        // var check = $("input[type='checkbox']").is(":checked");
        // var someObj_unlock={};
        // someObj_unlock.fruitsGranted=[];
        // someObj_unlock.fruitsDenied=[];

        
        // $("input[type='checkbox']").each(function(){
        //         var $this = $(this);
        //         if($this.is(":checked")){
        //             someObj_unlock.fruitsGranted.push($this.attr("id-index"));
        //         }else{
        //             someObj_unlock.fruitsDenied.push($this.attr("id-index"));
        //         }
        // });
        
        // $.ajax({ 
        //     url: '{{route('admin.user.unlock')}}',
        //     method: 'GET',
        //     data: {
        //         id:someObj_unlock.fruitsGranted,
        //     },
        //     success: function (data) {
        //         for (let index = 0; index < data.length; index++) {
        //             var i = data[index];
        //             console.log(i);
        //             $('#'+i).html('Đang Hoạt Động');   
        //         }
        //         alertify.success('Mở khóa thành công');
        //     }
        });
</script>
@endsection('content')