@extends('admin.master')
@section('title','Thêm tài khoản')
@section('content')
<!--App-Content-->
            <!--App-Content-->
            <div class="app-content  my-3 my-md-5">
                <div class="side-app">
                    <div class="page-header">
                        <h4 class="page-title">Tạo tài khoản</h4>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Cài đặt</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Tạo tài khoản mới</li>
                        </ol>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">Tạo tài khoản mới</div>
                                </div>
                                @if(count($errors)>0)
                                <div class="alert alert-danger">
                                    @foreach($errors->all() as $error)
                                        {{$error}}<br>
                                    @endforeach
                                </div>
                                @endif
                                @if(session('thongbao'))
                                    <div class="alert alert-success">{{session('thongbao')}}</div>
                                @endif
                                @if(session('error_pass'))
                                    <div class="alert alert-danger">{{session('error_pass')}}</div>
                                @endif
                                <div class="card-body">
                                <form class="form-horizontal" action="{{route('user.update',['user'=>Auth()->user()->id])}}" method="post">
                                    @method('put')
                                    @csrf
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-md-3"> <label class="form-label mt-2"
                                                        id="examplenameInputname2">Tên tài khoản <i>(bắt buộc)</i></label>
                                                </div>
                                                <div class="col-md-9"> <input type="text" class="form-control"
                                                        id="examplenameInputname3" placeholder="" name="username"> </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-md-3"> <label class="form-label mt-2" id="inputEmail3">
                                                        Email <i>(bắt buộc)</i></label> </div>
                                                <div class="col-md-9"> <input type="email" class="form-control"
                                                        id="inputEmail4" placeholder="" name="email"> </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-md-3"> <label class="form-label mt-2" id="inputEmail3">
                                                        Số điện thoại <i>(bắt buộc)</i></label> </div>
                                                <div class="col-md-9"> <input type="text" class="form-control"
                                                        id="inputEmail4" placeholder="" name="phone"> </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-md-3"> <label class="form-label mt-2">Tên</label>
                                                </div>
                                                <div class="col-md-9"> <input type="text" class="form-control"
                                                        placeholder="" name="firstname"> </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-md-3"> <label class="form-label mt-2">Họ</label>
                                                </div>
                                                <div class="col-md-9"> <input type="text" class="form-control"
                                                        placeholder="" name="lastname"> </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-md-3"> <label class="form-label mt-2">Giới tính</label>
                                                </div>
                                               
                                                    Nam
                                                    <div class="col-md-3"> <input style="width:20px;height:20px" type="radio" class="form-control"
                                                        placeholder="" name="sex" value="1"></div>
                                                    Nữ
                                                    <div class="col-md-1"><input style="width:20px;height:20px" type="radio" class="form-control"
                                                        placeholder="" name="sex" value="0" ></div>
                                          
                                                
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-md-3"> <label class="form-label mt-2">Mật khẩu</label>
                                                </div>
                                                <div class="col-md-9"> <input type="password" class="form-control"
                                                        placeholder="" name="password"> </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-md-3"> <label class="form-label mt-2">Nhập lại mật khẩu</label> </div>
                                                <div class="col-md-9"> <input type="password" class="form-control"
                                                        placeholder="" name="repassword"> </div>
                                            </div>
                                        </div>
                                        {{-- <div class="form-group justify-content-end">
                                            <div class="row">
                                                <div class="col-md-3"> <label class="form-label mt-2">Send User
                                                        Notification</label> </div>
                                                <div class="col-md-9"> <label
                                                        class="custom-control custom-checkbox mb-0"> <input
                                                            type="checkbox" class="custom-control-input"> <span
                                                            class="custom-control-label text-dark">Send The New User an
                                                            Email About thier Account</span> </label> </div>
                                            </div>
                                        </div> --}}
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-md-3"> <label class="form-label">Chức vụ</label> </div>
                                                <div class="col-md-9"> 
                                                    <select name="role" class="form-control">
                                                        @foreach($role as $r)
                                                        <option  value="{{$r->id}}">{{$r->name}}</option>
                                                        @endforeach
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group mb-0 row justify-content-end">
                                            <div class="col-md-3"> <button type="submit"
                                                    class="btn btn-primary waves-effect waves-light">Tạo tài khoản</button> </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--App-Content-->

@endsection('content')
