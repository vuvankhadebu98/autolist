@extends('admin.master')
@section('title','Danh sách tài khoản')
@section('content')
    <!--App-Content-->
    <div class="app-content">
        <div class="side-app">
            <div class="page-header"> <a aria-label="Hide Sidebar" class="app-sidebar__toggle"
                    data-toggle="sidebar" href="#"></a>
                <h4 class="page-title">Sửa thông tin</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Trang</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Sửa thông tin</li>
                </ol>
            </div>
            @if(count($errors)>0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                    {{$error}}<br>
                @endforeach
            </div>
            @endif
            @if(session('success'))
                <div class="alert alert-success">{{session('success')}}</div>
            @endif
            @if(session('thatbai'))
            <div class="alert alert-danger">{{session('thatbai')}}</div>
            @endif
            @foreach ($user_detail as $user)
            <div class="row ">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Thông tin của tôi</h3>
                        </div>
                        <div class="card-body">
                            <form action="{{route('admin.user.edit_pass')}}" method="post">
                                @csrf
                                <div class="row mb-2">
                                    <div class="col-auto">
                                        <img class="avatar brround avatar-xl"src="../img_user/{{$user->img}}" alt="Avatar-img">
                                    </div>
                                    <div class="col">
                                        <h3 class="mb-1 ">{{$user->firstname}} {{$user->lastname}} </h3>
                                        
                                        <input type='file' id="imgInp" name="img">
                                        <input type="hidden" value="{{$user->img}}" name="old_img">
                                    </div>
                                </div>
                               
                                <div class="form-group"> 
                                    <label class="form-label">Email</label> 
                                    <input class="form-control" name="email" readonly placeholder="{{$user->email}}">
                                </div>
                                
                                
                                <div class="form-footer">
                                    
                                </div>
                                <div class="form-footer">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Đổi mật khẩu</button>
                                    

                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Đổi mật khẩu</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{ route('admin.user.edit_pass')}}" method="post">
                                                @csrf
                                                <div class="form-group">
                                                    <label class="form-label">Mật khẩu cũ</label>
                                                    <input type="password" name="password" class="form-control" value="">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Mật khẩu mới</label>
                                                    <input type="password" name="new_password" class="form-control" value="">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                                    <input type="submit" value="Đổi mật khẩu" class="btn btn-primary"></input>
                                                </div>
                                            </form>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <form class="card" action="{{route('admin.user.update_user')}}" method="post">
                        @csrf
                        <div class="card-header">
                            <h3 class="card-title">Cập nhật Thông tin</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">Tên</label>
                                        <input type="text" name="firstname" class="form-control" placeholder="Tên" value="{{ $user->firstname}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">Họ</label>
                                        <input type="text" name="lastname" class="form-control" placeholder="Họ" value="{{ $user->lastname}}">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group"> <label class="form-label">Số điện thoại</label>
                                        <input type="text" name="phone" class="form-control" placeholder="Số điện thoại" value="{{ $user->phone}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group v1"> 
                                        <label class="form-label">Giới Tính</label>
                                        <select name="sex" class="form-control custom-select" >
                                            <option value="1" {{$user->sex == 1 ? "selected" : ""}}>Nam</option>
                                            <option value="0" {{$user->sex == 0 ? "selected" : ""}}>Nữ</option>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Địa chỉ Facebook</label>
                                        <input type="text" name="url_face" class="form-control" placeholder="FaceBook" value="{{$user->url_face}}">
                                    </div>
                                </div>
                               
                               
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Tỉnh/Thành phố</label>
                                        <select class="form-control custom-select" name="city" >
                                            @if(count($province)==1)
                                            <option value="{{Auth::user()->citys->id}}">{{Auth::user()->citys->_name}}</option>
                                            @else
                                            @foreach($province as $city)
                                                <option value="{{$city->id}}">{{$city->_name}}</option>
                                            @endforeach
                                            @endif
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">Địa chỉ</label>
                                        <input type="text" name="address"  class="form-control" value="{{$user->address}}" placeholder="Địa chỉ cụ thể" >
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <script src="../ckeditor/ckeditor.js"></script>
                                    
                                    <div class="form-group mb-0">
                                        <label class="form-label">Thông tin chi tiết</label>
                                        <textarea name="detail_aboutme" id="ckeditor1" class="ckeditor" >{{$user->detail_aboutme}}</textarea>
                                        <script>
                                            CKEDITOR.replace(ckeditor1);
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <input type="submit" class="btn btn-primary" value="Cập nhật thông tin"></input> </div>
                    </form>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Add projects And Upload</h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table card-table table-vcenter text-nowrap">
                                <thead>
                                    <tr>
                                        <th>Project Name</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Price</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><a href="store.html" class="text-inherit">Untrammelled prevents </a>
                                        </td>
                                        <td>28 May 2019</td>
                                        <td><span class="status-icon bg-success"></span> Completed</td>
                                        <td>$56,908</td>
                                        <td class="text-right"> <a class="icon" href="javascript:void(0)"></a>
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i
                                                    class="fa fa-pencil"></i> Edit</a> <a class="icon"
                                                href="javascript:void(0)"></a> <a href="javascript:void(0)"
                                                class="btn btn-green btn-sm"><i class="fa fa-link"></i>
                                                Update</a> <a class="icon" href="javascript:void(0)"></a> <a
                                                href="javascript:void(0)" class="btn btn-danger btn-sm"><i
                                                    class="fa fa-trash"></i> Delete</a> </td>
                                    </tr>
                                    <tr>
                                        <td><a href="store.html" class="text-inherit">Untrammelled prevents</a>
                                        </td>
                                        <td>12 June 2019</td>
                                        <td><span class="status-icon bg-danger"></span> On going</td>
                                        <td>$45,087</td>
                                        <td class="text-right"> <a class="icon" href="javascript:void(0)"></a>
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i
                                                    class="fa fa-pencil"></i> Edit</a> <a class="icon"
                                                href="javascript:void(0)"></a> <a href="javascript:void(0)"
                                                class="btn btn-green btn-sm"><i class="fa fa-link"></i>
                                                Update</a> <a class="icon" href="javascript:void(0)"></a> <a
                                                href="javascript:void(0)" class="btn btn-danger btn-sm"><i
                                                    class="fa fa-trash"></i> Delete</a> </td>
                                    </tr>
                                    <tr>
                                        <td><a href="store.html" class="text-inherit">Untrammelled prevents</a>
                                        </td>
                                        <td>12 July 2019</td>
                                        <td><span class="status-icon bg-warning"></span> Pending</td>
                                        <td>$60,123</td>
                                        <td class="text-right"> <a class="icon" href="javascript:void(0)"></a>
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i
                                                    class="fa fa-pencil"></i> Edit</a> <a class="icon"
                                                href="javascript:void(0)"></a> <a href="javascript:void(0)"
                                                class="btn btn-green btn-sm"><i class="fa fa-link"></i>
                                                Update</a> <a class="icon" href="javascript:void(0)"></a> <a
                                                href="javascript:void(0)" class="btn btn-danger btn-sm"><i
                                                    class="fa fa-trash"></i> Delete</a> </td>
                                    </tr>
                                    <tr>
                                        <td><a href="store.html" class="text-inherit">Untrammelled prevents</a>
                                        </td>
                                        <td>14 June 2019</td>
                                        <td><span class="status-icon bg-warning"></span> Pending</td>
                                        <td>$70,435</td>
                                        <td class="text-right"> <a class="icon" href="javascript:void(0)"></a>
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i
                                                    class="fa fa-pencil"></i> Edit</a> <a class="icon"
                                                href="javascript:void(0)"></a> <a href="javascript:void(0)"
                                                class="btn btn-green btn-sm"><i class="fa fa-link"></i>
                                                Update</a> <a class="icon" href="javascript:void(0)"></a> <a
                                                href="javascript:void(0)" class="btn btn-danger btn-sm"><i
                                                    class="fa fa-trash"></i> Delete</a> </td>
                                    </tr>
                                    <tr>
                                        <td><a href="store.html" class="text-inherit">Untrammelled prevents</a>
                                        </td>
                                        <td>25 June 2019</td>
                                        <td><span class="status-icon bg-success"></span> Completed</td>
                                        <td>$15,987</td>
                                        <td class="text-right"> <a class="icon" href="javascript:void(0)"></a>
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i
                                                    class="fa fa-pencil"></i> Edit</a> <a class="icon"
                                                href="javascript:void(0)"></a> <a href="javascript:void(0)"
                                                class="btn btn-green btn-sm"><i class="fa fa-link"></i>
                                                Update</a> <a class="icon" href="javascript:void(0)"></a> <a
                                                href="javascript:void(0)" class="btn btn-danger btn-sm"><i
                                                    class="fa fa-trash"></i> Delete</a> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
<!--/App-Content-->

@endsection('content')