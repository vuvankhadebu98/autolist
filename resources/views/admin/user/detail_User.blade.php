@extends('admin.master')
@section('title','Danh sách tài khoản')
@section('content')

<!--App-Content-->
<div class="app-content">
    <div class="side-app">
        <div class="page-header"> <a aria-label="Hide Sidebar" class="app-sidebar__toggle"
                data-toggle="sidebar" href="#"></a>
            <h4 class="page-title">Thông tin</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Trang</a></li>
                <li class="breadcrumb-item active" aria-current="page">Thông tin</li>
            </ol>
        </div>
        @foreach($detail_User as $detail)
        <div class="row">
            <div class="col-lg-5 col-xl-4">
                <div class="card card-profile cover-image "data-image-src="../admin/assets/images/photos/gradient1.jpg" style="background: url(&quot;../assets/images/photos/gradient1.jpg&quot;) center center;">
                    <div class="card-body text-center">
                        <img class="card-profile-img" src="../img_user/{{$detail->img}}" alt="img">
                        <h3 class="mb-1 text-white">{{$detail->lastname}} {{$detail->firstname}}</h3>
                        
                        <a href="{{route('admin.user.get_edit_admin',['id' => Auth::user()->id])}}" class="btn btn-success btn-sm mt-2">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                             Sửa thông tin
                        </a>
                    </div>
                </div>
                <div class="card p-5 ">
                    <div class="card-title"> Liên hệ &amp; Thông tin cá nhân </div>
                    <div class="media-list">
                        <div class="media mt-1 pb-2">
                            <div class="mediaicon"> <i class="fa fa-link" aria-hidden="true"></i> </div>
                            <div class="card-body ml-5 p-1">
                                <h6 class="mediafont text-dark">Facebook</h6>
                                <a class="d-block" href="">{{$detail->url_face}}</a> 
                            </div> <!-- media-body -->
                        </div> <!-- media -->
                        <!-- media -->
                        <div class="media mt-1 pb-2">
                            <div class="mediaicon"> <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            </div>
                            <div class="card-body p-1 ml-5">
                                <h6 class="mediafont text-dark">Email </h6>
                                <span class="d-block">{{$detail->url_google}}</span>
                            </div> <!-- media-body -->
                        </div> <!-- media -->
                        <div class="media mt-1">
                            <div class="mediaicon"> <i class="fa fa-twitter" aria-hidden="true"></i> </div>
                            <div class="card-body p-1 ml-5">
                                <h6 class="mediafont text-dark">Twitter</h6>
                                <a class="d-block" href="#">{{$detail->url_twitter}}</a>
                            </div> <!-- media-body -->
                        </div> <!-- media -->
                    </div> <!-- media-list -->
                </div>
               
            </div>
            <div class="col-lg-7 col-xl-8">
                <div class="card">
                    <div class="card-body">
                        <div id="profile-log-switch">
                            <div class="fade show active ">
                                <div class="table-responsive border ">
                                    <table class="table row table-borderless w-100 m-0 ">
                                        <tbody class="col-lg-12 col-xl-6 p-0">
                                            <tr>
                                                <td><strong>Tên :</strong> {{$detail->lastname}} {{$detail->firstname}} </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Địa chỉ :</strong> {{$detail->address}}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Ngôn ngữ :</strong> {{$detail->country}} 
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tbody class="col-lg-12 col-xl-6 p-0">
                                            <tr>
                                                <td><strong>Website :</strong>Autolist.com</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Email :</strong> {{$detail->url_google}} 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Số điện thoại :</strong>{{$detail->phone}}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Thành phố :</strong>{{$detail->city}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row mt-5 profie-img">
                                    <div class="col-md-12">
                                        <div class="media-heading">
                                            <h5><strong>Thông tin chi tiết</strong></h5>
                                        </div>
                                        <p>{!!$detail->detail_aboutme!!}</p>
                                        
                                    </div> 
                                        {{-- <img class="img-fluid rounded w-25 h-25 m-2"src="../assets/images/photos/8.jpg" alt="banner image">
                                        <img class="img-fluid rounded w-25 h-25 m-2"src="../assets/images/photos/10.jpg" alt="banner image ">
                                        <img class="img-fluid rounded w-25 h-25 m-2"src="../assets/images/photos/11.jpg" alt="banner image "> --}}
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card profile-info">
                    <form> <textarea class="form-control input-lg p-text-area  border-0" rows="2"
                            placeholder="Whats in your mind today?"></textarea> </form>
                    <div class="card-footer"> <button type="button"
                            class="btn btn-sm btn-info pull-right">Post</button>
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-map-marker mr-3"></i></a></li>
                            <li><a href="#"><i class="fa fa-camera mr-3"></i></a></li>
                            <li><a href="#"><i class=" fa fa-film mr-3"></i></a></li>
                            <li><a href="#"><i class="fa fa-microphone"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="fb-user-thumb"> <img src="../assets/images/faces/male/25.jpg" alt=""
                                    class="avatar brround avatar-md"> </div>
                            <div class="ml-2">
                                <h5 class="mb-1 font-weight-semibold"><a href="#" class="#">Margarita
                                        Elina</a></h5>
                                <p>7 minutes ago near Alaska, USA</p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="fb-user-status">John is world famous professional photographer. with
                            forward thinking clients to create beautiful, honest and amazing things that
                            bring positive results. John is world famous professional photographer. with
                            forward thinking clients to create beautiful, honest and amazing things that
                            bring positive results. </p>
                        <div class="fb-status-container fb-border">
                            <div class="fb-time-action"> <a href="#" title="Like this">Like</a>
                                <span>-</span> <a href="#" title="Leave a comment">Comments</a>
                                <span>-</span> <a href="#"
                                    title="Send this to friend or post it on your time line">Share</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Dự án gần đây</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table card-table table-vcenter border text-nowrap">
                                <thead>
                                    <tr>
                                        <th>Project Name</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><a href="store.html" class="text-inherit">Untrammelled prevents
                                            </a></td>
                                        <td>28 May 2019</td>
                                        <td><span class="status-icon bg-success"></span> Completed</td>
                                        <td>$56,908</td>
                                    </tr>
                                    <tr>
                                        <td><a href="store.html" class="text-inherit">Untrammelled
                                                prevents</a></td>
                                        <td>12 June 2019</td>
                                        <td><span class="status-icon bg-danger"></span> On going</td>
                                        <td>$45,087</td>
                                    </tr>
                                    <tr>
                                        <td><a href="store.html" class="text-inherit">Untrammelled
                                                prevents</a></td>
                                        <td>12 July 2019</td>
                                        <td><span class="status-icon bg-warning"></span> Pending</td>
                                        <td>$60,123</td>
                                    </tr>
                                    <tr>
                                        <td><a href="store.html" class="text-inherit">Untrammelled
                                                prevents</a></td>
                                        <td>14 June 2019</td>
                                        <td><span class="status-icon bg-warning"></span> Pending</td>
                                        <td>$70,435</td>
                                    </tr>
                                    <tr>
                                        <td><a href="store.html" class="text-inherit">Untrammelled
                                                prevents</a></td>
                                        <td>25 June 2019</td>
                                        <td><span class="status-icon bg-success"></span> Completed</td>
                                        <td>$15,987</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
<!--/App-Content-->
@endsection('content')