<div class="tab-content" style="margin-top: -5px">
   <div class="tab-pane active" id="tab1">
      <div class="table-responsive border-top">
         <table id="example" class="table card-table table-bordered table-hover table-vcenter text-nowrap">
            <tbody>
               <tr style="text-align: center;">
                  {{-- mã category --}}
                  <th class="w-1">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Mã
                        </font>
                     </font>
                  </th>
                  {{-- tên category --}}
                  <th class="w-1">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Tên Danh Mục
                        </font>
                     </font>
                  </th>
                  {{-- Ảnh category --}}
                  <th class="w-1">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Ảnh Danh Mục</font>
                     </font>
                  </th>
                  {{-- trạng thái --}}
                  <th>
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Loại Danh Mục</font>
                     </font>
                  </th>
                  {{-- sửa xóa --}}
                  <th style="text-align: center;">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;" >Lựa Chọn
                        </font>
                     </font>
                  </th>
               </tr>
            {{-- phầm category hiển thị --}}

               @foreach($all_category as $key=> $show_category)
               <tr style="text-align: center;">
                  {{-- mã danh mục --}}
                  <td style="width: 20px;color: red">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">
                           {{$show_category->id}}
                        </font>
                     </font> 
                  </td>
                  {{-- têm category --}}
                  <td style="width: 260px">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">
                           {{$show_category->name}}
                        </font>
                     </font> 
                  </td>
                  {{-- Hình ảnh Blog --}}
                  <td style="width: 180px;">
                     <img style="width:160px; height: 100px" src="../img_category/{{$show_category->img}}" 
                  alt="hình ảnh"> 
                  </td>
                  {{-- trạng thái --}}
                  <td style="width: 200px">
                     <span>
                        <font style="vertical-align: inherit;">
                           <font style="vertical-align: inherit;">
                             <?php
                              if($show_category->check == 0)
                              {
                              ?>
                               <span class="hienthi" id="0" style="color: #0DD902;">Sản Phẩm</span>
                              <?php
                              }elseif($show_category->check == 1){
                              ?>
                               <span class="hienthi" id="1" style="color: #09B5C8;">Phụ Tùng</span>
                              <?php
                              }
                             ?>
                           </font>
                        </font>
                     </span>
                  </td> 
                  {{-- sửa xóa --}}
                  <td class="r" style="text-align: center;width: 160px;">
                     <a class="btn btn-xs btn-info blog" 
                     style="font-size: 18px;width: 70px; height: 30px;"
                     href="{{URL::to('/edit-category/'.$show_category->id)}}">
                        <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
                     </a>
                     <a class="btn btn-xs btn-danger blog" 
                     style="font-size: 18px;width: 70px; height: 30px;"
                     onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                     href="{{URL::to('/delete-category/'.$show_category->id)}}">
                        <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                     </a>
                  </td>
               </tr>
               @endforeach
            </tbody>
         </table>
      </div>  
   </div>
</div>