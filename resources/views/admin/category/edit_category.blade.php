@extends('admin.master')
@section('title','Home')
@section('content')

   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Cập Nhật Danh Mục</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Cập Nhật Danh Mục</font>
                  </font>
               </li>
            </ol>
         </div>
         <?php 
              $message = Session::get('message');
              if ($message) {
                  echo '<div class="alert alert-success">'. $message .'</div>';
                  Session::put('message', null);
              }
            ?>
         <div class="row row-cards">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Cập Nhật Danh Mục</font>
                          <a href="{{URL::to('all-category')}}" class="btn" aria-expanded="false" style="vertical-align: inherit; font-size: 18px;color: #20BAED">Danh Sách Danh Mục</a> 
                        </font>
                     </div>
                  </div>
                  {{-- thêm mới --}}
                  @foreach($edit_category as $key => $edit_cate)
                  <form role="form" action="{{URL::to('/update-category/'.$edit_cate->id)}}" method="post" 
                     enctype="multipart/form-data">
                    {{ csrf_field()}}
                     <div class="card-body">
                      {{-- tên danh mục --}}
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Tên Thương Hiệu</font>
                              </font>
                           </label> 
                           <input type="text" name="name" class="form-control w-100" 
                           placeholder="Nhập tên thương hiệu" value="{{$edit_cate->name}}">
                           <p class="help is-danger">{{ $errors->first('name') }}</p> 
                        </div>
                        {{-- hình ảnh danh mục--}}
                        <div class="form-group">
                          <label for="exampleInputEmail1">Hình ảnh Thương Hiệu</label>
                          <input type="file" name="img" class="form-control" id="img">
                          <img src="{{URL::to('/img_category/'.$edit_cate->img)}}" height="90" width="140" style="margin-left: 5px">
                          <p class="help is-danger">{{ $errors->first('img') }}</p>
                        </div>
                        {{-- hiển thị --}}
                         <div class="form-group">
                           <label name="exampleInputPassword1">Tình Trạng</label>
                           <select name="check" class="form-control m-bot15">
                              <option value="0" {{$edit_cate->check == 0 ? "selected" : ""}}>Sản Phẩm</option>
                              <option value="1" {{$edit_cate->check == 1 ? "selected" : ""}}>Phụ Tùng</option>
                           </select>
                        </div>
                        <button type="submit" name="update_category" 
                        class="btn btn-primary waves-effect waves-light">
                           <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Cập Nhật Thương Hiệu</font>
                           </font>
                        </button> 
                     </div>  
                  </form>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--App-Content--> 
@endsection('content')