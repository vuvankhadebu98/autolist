@extends('admin.master')
@section('title','Home')
@section('content')

   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Danh sách Danh Mục</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Danh Sách Danh Mục</font>
                  </font>
               </li>
            </ol>
         </div>
         {{-- nếu cập nhật thành công --}}
         <?php 
             $message = Session::get('message');
             if ($message) {
                 echo '<div class="alert alert-success">'. $message .'</div>';
                 Session::put('message', null);
             }
         ?>
         <div class="row">
            <div class="col-lg-12">
               <div class="card">
                  <div class="card-body">
                     
                     <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('all-category')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     
                      <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('add-category')}}" class="btn" aria-expanded="false"> Thêm Mới Danh Mục </a> 
                     </div>
                     <label class="search-cateogry">
                        <p class="btn btn-primary" id="active_category" onclick="return active_category(this);">Danh Mục Sản phẩm</p>
                     </label>
                     <label class="search-cateogry">
                        <p class="btn btn-primary" id="active_category_1" onclick="return active_category_1(this);">Danh Mục Phụ Tùng</p>
                     </label>
                     {{-- tìm kiếm --}}
                     <form method="post" class="form-inline mr-auto" style="float: right;" action="{{route('search-category')}}">
                        @csrf
                        <div class="nav-search"> 
                           <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                           <button class="btn btn-primary" style="width: 60px;height: 39px;" type="submit">
                              <i class="fa fa-search"></i>
                           </button> 
                        </div>
                     </form>
                     {{-- bảng blog --}}
                     <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                           <div class="table-responsive border-top">
                              <table id="example" class="table card-table table-bordered table-hover table-vcenter text-nowrap">
                                 <tbody>
                                    <tr style="text-align: center;">
                                       {{-- mã category --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Mã
                                             </font>
                                          </font>
                                       </th>
                                       {{-- tên category --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Tên Danh Mục
                                             </font>
                                          </font>
                                       </th>
                                       {{-- Ảnh category --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Ảnh Danh Mục</font>
                                          </font>
                                       </th>
                                       {{-- trạng thái --}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Loại Danh Mục</font>
                                          </font>
                                       </th>
                                       {{-- sửa xóa --}}
                                       <th style="text-align: center;">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;" >Lựa Chọn
                                             </font>
                                          </font>
                                       </th>
                                    </tr>
                                 {{-- phầm category hiển thị --}}

                                    @foreach($all_category as $key=> $show_category)
                                    <tr style="text-align: center;">
                                       {{-- mã danh mục --}}
                                       <td style="width: 20px;color: red">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                {{$show_category->id}}
                                             </font>
                                          </font> 
                                       </td>
                                       {{-- têm category --}}
                                       <td style="width: 260px">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                {{$show_category->name}}
                                             </font>
                                          </font> 
                                       </td>
                                       {{-- Hình ảnh Blog --}}
                                       <td style="width: 180px;">
                                          <img style="width:160px; height: 100px" src="../img_category/{{$show_category->img}}" 
                                       alt="hình ảnh"> 
                                       </td>
                                       {{-- trạng thái --}}
                                       <td style="width: 200px">
                                          <span>
                                             <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">
                                                  <?php
                                                   if($show_category->check == 0)
                                                   {
                                                   ?>
                                                    <span class="hienthi" id="0" style="color: #0DD902;">Sản Phẩm</span>
                                                   <?php
                                                   }elseif($show_category->check == 1){
                                                   ?>
                                                    <span class="hienthi" id="1" style="color: #09B5C8;">Phụ Tùng</span>
                                                   <?php
                                                   }
                                                  ?>
                                                </font>
                                             </font>
                                          </span>
                                       </td> 
                                       {{-- sửa xóa --}}
                                       <td class="r" style="text-align: center;width: 160px;">
                                          <a class="btn btn-xs btn-info blog" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          href="{{URL::to('/edit-category/'.$show_category->id)}}">
                                             <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
                                          </a>
                                          <a class="btn btn-xs btn-danger blog" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                                          href="{{URL::to('/delete-category/'.$show_category->id)}}">
                                             <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                          </a>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>  
                        </div> 
                     </div>
                  </div>
               </div>
               <ul class="pagination mb-5">
               </ul>
            </div>
         </div>
      </div>
   </div>
<!--App-Content--> 
<script type="text/javascript">
   function active_category(e) {
      $.ajax({
      url: '{{route('load.active')}}',
      method: 'GET',
      success: function(data){
         $("table#example tbody").empty();
         $("table#example tbody").html(data);
         console.log(data);
         }
      });
   }
   function active_category_1(e) {
      $.ajax({
      url: '{{route('load.active.1')}}',
      method: 'GET',
      success: function(data){
         $("table#example tbody").empty();
         $("table#example tbody").html(data);
         console.log(data);
         }
      });
   }
</script>
@endsection('content')
