@extends('admin.master')
@section('title','Home')
@section('content')
   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Danh sách Blog</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Danh sách Blog</font>
                  </font>
               </li>
            </ol>
         </div>
         {{-- nếu cập nhật thành công --}}
         <?php 
             $message = Session::get('message');
             if ($message) {
                 echo '<div class="alert alert-success">'. $message .'</div>';
                 Session::put('message', null);
             }
         ?>
         <div class="row">
            <div class="col-lg-12">
               <div class="card">
                  <div class="card-body">
                     
                     <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('all-information')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     
                      <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('add-information')}}" class="btn" aria-expanded="false"> Thêm Mới Thông Tin </a> 
                     </div>
                     {{-- tìm kiếm --}}
                     <form method="post" class="form-inline mr-auto" style="float: right;" action="{{route('search-information')}}">
                        @csrf
                        <div class="nav-search"> 
                           <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                           <button class="btn btn-primary" style="width: 60px;height: 39px;" type="submit">
                              <i class="fa fa-search"></i>
                           </button> 
                        </div>
                     </form>
                     {{-- bảng Thông Tin --}}
                     <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                           <div class="table-responsive border-top">
                              <table class="table card-table table-bordered table-hover table-vcenter text-nowrap">
                                 <tbody>
                                    <tr style="text-align: center;">
                                       {{-- mã --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Mã Thông Tin
                                             </font>
                                          </font>
                                       </th>
                                       {{-- tên blog --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Tên Thông Tin
                                             </font>
                                          </font>
                                       </th>
                                       {{-- đường dẫn --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Đường Dẫn 
                                             </font>
                                          </font>
                                       </th>
                                       {{-- Số Điện Thoại --}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Số Điện Thoại
                                             </font>
                                          </font>
                                       </th>
                                       {{-- tag --}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Địa Chỉ
                                             </font>
                                          </font>
                                       </th>
                                       {{-- trạng thái --}}
                                       <th>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Trạng thái</font>
                                          </font>
                                       </th>
                                       {{-- sửa xóa --}}
                                       <th style="text-align: center;">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;" >Lựa Chọn
                                             </font>
                                          </font>
                                       </th>
                                    </tr>

                                 {{-- phầm Thông Tin hiển thị --}}
                                 @foreach($all_information as $key=> $show_information)
                                    <tr style="text-align: center;">
                                       {{-- mã thông tin --}}
                                       <td>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                {{$show_information->id}}
                                             </font>
                                          </font>
                                       </td>
                                       {{-- têm thông tin --}}
                                       <td>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                 <span style="color: #CC0000;">
                                                   {{$show_information->name}}
                                                </span>
                                             </font>
                                          </font>
                                       </td>
                                       {{-- url --}}
                                       <td>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                <span style="color: #15A5E2;">
                                                   {{$show_information->url}}
                                                </span>
                                             </font>
                                          </font>
                                       </td>
                                       {{-- phone --}}
                                       <td>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                <span style="color: #059D56;">
                                                   {{$show_information->phone}}
                                                </span>
                                             </font>
                                          </font>
                                       </td>
                                       {{-- address --}}
                                       <td>
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                <span>
                                                   {!!$show_information->address!!}
                                                </span>
                                             </font>
                                          </font>
                                       </td>
                                       {{-- trạng thái --}}
                                       <td>
                                          <span>
                                             <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">
                                                   <?php
                                                     if($show_information->status == 0)
                                                     {
                                                     ?>
                                                     <a class="btn btn-xs btn-danger blog" 
                                                      style="font-size: 18px;width: 70px; height: 30px;padding: 4px;"
                                                      href="{{URL::to('unactive-information/'.$show_information->id)}}">
                                                        Ẩn
                                                      </a>
                                                     <?php
                                                     }else{
                                                     ?>
                                                     <a class="btn btn-xs btn-green blog" 
                                                      style="font-size: 18px;width: 120px;height: 30px;padding: 4px;"
                                                      href="{{URL::to('active-information/'.$show_information->id)}}">
                                                      Hiển Thị
                                                      </a>
                                                     <?php
                                                     }
                                                   ?>  
                                                </font>
                                             </font>
                                          </span>
                                       </td> 
                                       {{-- sửa xóa --}}
                                       <td class="r" style="text-align: center;width: 160px;">
                                          <a class="btn btn-xs btn-info blog" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          href="{{URL::to('/edit-information/'.$show_information->id)}}">
                                             <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
                                          </a>
                                          <a class="btn btn-xs btn-danger blog" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                                          href="{{URL::to('/delete-information/'.$show_information->id)}}">
                                             <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                          </a>
                                       </td>
                                    </tr>
                                 @endforeach
                                 </tbody>
                              </table>
                           </div>  
                        </div> 
                     </div>
                  </div>
               </div>
               <ul class="pagination mb-5">
                  {{$all_information->links()}}
               </ul>
            </div>
         </div>
      </div>
   </div>
<!--App-Content--> 
@endsection('content')
