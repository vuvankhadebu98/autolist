@extends('admin.master')
@section('title','Home')
@section('content')

   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Cập Nhật Thông Tin</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Cập Nhật Thông Tin</font>
                  </font>
               </li>
            </ol>
         </div>
         <?php 
              $message = Session::get('message');
              if ($message) {
                  echo '<div class="alert alert-success">'. $message .'</div>';
                  Session::put('message', null);
              }
            ?>
         <div class="row row-cards">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Cập Nhật Thông Tin</font>
                        </font>
                     </div>
                  </div>
                  {{-- thêm mới --}}
                  @foreach($edit_information as $key => $edit_ifm)
                  <form role="form" action="{{URL::to('/update-information/'.$edit_ifm->id)}}" method="post" 
                     enctype="multipart/form-data">
                    {{ csrf_field()}}
                     <div class="card-body">
                      {{-- tên thông tin --}}
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Tên Thông Tin</font>
                              </font>
                           </label> 
                           <input type="text" name="name" class="form-control w-100" 
                           placeholder="Nhập tên thông tin" value="{{$edit_ifm->name}}">
                           <p class="help is-danger">{{ $errors->first('name') }}</p>
                        </div>
                        {{-- url --}}
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Đường Dẫn</font>
                              </font>
                           </label> 
                           <input type="text" name="url" class="form-control w-100" 
                           placeholder="Nhập đường dẫn" value="{{$edit_ifm->url}}">
                           <p class="help is-danger">{{ $errors->first('url') }}</p> 
                        </div>
                        {{-- phone --}}
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Số Điện Thoại</font>
                              </font>
                           </label> 
                           <input type="number" name="phone" class="form-control w-100" 
                           placeholder="Nhập sđt" value="{{$edit_ifm->phone}}">
                           <p class="help is-danger">{{ $errors->first('phone') }}</p> 
                        </div>
                        {{-- địa chỉ --}}
                        <div class="form-group">
                           <label class="form-label">Địa Chỉ</label> 
                           <textarea name="address" id="address"
                            style="resize: none;"rows="8"class="form-control">
                            {{$edit_ifm->address}}
                           </textarea>
                          <p class="help is-danger">{{ $errors->first('address') }}</p>
                        </div>
                        {{-- hiển thị --}}
                         <div class="form-group">
                           <label name="exampleInputPassword1">Trạng Thái</label>
                           <select name="status" class="form-control m-bot15">
                               <option value="1" {{$edit_ifm->status == 1 ? "selected" : ""}}>Hiển thị</option>
                               <option value="0" {{$edit_ifm->status == 0 ? "selected" : ""}}>Ẩn</option>       
                           </select>
                        </div>
                        {{-- thêm mới --}}
                        <button type="submit" name="update_information" 
                        class="btn btn-primary waves-effect waves-light">
                           <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Cập Nhật Thông Tin</font>
                           </font>
                        </button> 
                     </div>  
                  </form>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--App-Content--> 
@endsection

@section('js')
  <script>
      CKEDITOR.replace('address');      
  </script>
@endsection
