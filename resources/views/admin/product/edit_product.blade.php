@extends('admin.master')
@section('title','Danh mục sản phẩm')
@section('content')
<div class="app-content  my-3 my-md-5">
   <div class="side-app">
      <div class="page-header">
         <h4 class="page-title">Sản phẩm</h4>
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Settings</a></li>
            <li class="breadcrumb-item active" aria-current="page">Cập nhật sản phẩm</li>
         </ol>
      </div>
      @if(session()->has('message'))
      <div class="alert alert-success">
         {{ session()->get('message') }}
      </div>
      @elseif(session()->has('error'))
      <div class="alert alert-danger">
         {{ session()->get('error') }}
      </div>
      @endif
      <div class="row row-cards">
         <div class="col-md-12">
            <div class="card">
               <div class="card-header">
                  <div class="card-title">Cập nhật {{$product->name}}</div>
               </div>
               <form action="{{route('post-edit',$product->id)}}" method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="card-body">
                     <div class="form-group "> 
                        <label class="form-label">Tên sản phẩm</label>
                        <input type="text" name="product_name" class="form-control w-100" value="{{$product->name}}">
                        <p class="help is-danger">{{ $errors->first('product_name') }}</p>
                     </div>
                     <div class="row information">
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Số ghế</label> 
                              <input type="number" name="seats" class="form-control" value="{{$product->seats}}">
                              <p class="help is-danger">{{ $errors->first('seats') }}</p> 
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Không gian khởi động</label> 
                              <input type="number" name="boot_space" class="form-control" value="{{$product->bootspace}}">
                              <p class="help is-danger">{{ $errors->first('boot_space') }}</p> 
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Số dặm đi được</label> 
                              <input type="number" name="arai_milege" class="form-control" value="{{$product->arai_milage}}">
                              <p class="help is-danger">{{ $errors->first('arai_milege') }}</p> 
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Momen xoắn cực đại</label> 
                              <input type="text" name="max_torque" class="form-control" value="{{$product->max_torque}}">
                              <p class="help is-danger">{{ $errors->first('max_torque') }}</p> 
                           </div>
                        </div>
                     </div>
                     <div class="row information">
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Giá niêm yết</label> 
                              <input type="number" min='1' name="price" class="form-control" value="{{$product->price}}">
                              <p class="help is-danger">{{ $errors->first('price') }}</p> 
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Ưu đãi (%)</label> 
                              <input type="number" name="promotion" class="form-control" value="{{$product->promotion}}">
                              <p class="help is-danger">{{ $errors->first('promotion') }}</p> 
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Cộng cụ chuyển</label> 
                              <input type="number" name="egn_displace" class="form-control" value="{{$product->enegine_displacement}}"> 
                              <p class="help is-danger">{{ $errors->first('egn_displace') }}</p>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Cộng Xuất Tối Đa</label> 
                              <input type="text" name="max_power" class="form-control" value="{{$product->max_power}}">
                              <p class="help is-danger">{{ $errors->first('max_power') }}</p> 
                           </div>
                        </div>
                     </div>
                     <div class="row information">
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Loại hộp số</label> 
                              <select name="transmission" class="form-control select2 select2-accessible" data-placeholder="Choose Browser" data-select2-id="1" tabindex="-1" aria-hidden="true">
                              <option value="1" {{$product->transmission_type == 1 ? "selected" : ""}}>Hộp số sàn</option>
                              <option value="2" {{$product->transmission_type == 2 ? "selected" : ""}}>Hộp số tự động</option>
                              <option value="3" {{$product->transmission_type == 3 ? "selected" : ""}}>Hộp số tự động vô cấp CVT</option>
                              <option value="4" {{$product->transmission_type == 4 ? "selected" : ""}}>Hộp số ly hợp kép DCT</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Động cơ</label> 
                              <input type="text" name="engine" class="form-control" value="{{$product->engine}}"> 
                              <p class="help is-danger">{{ $errors->first('engine') }}</p>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Loại nhiên liệu</label> 
                              <select name="fuel_type" class="form-control select2 select2-accessible" >
                              <option value="1" {{$product->fuel_type == 1 ? "selected" : ""}}>Diesel</option>
                              <option value="2" {{$product->fuel_type == 2 ? "selected" : ""}}>Xăng</option>
                              <option value="3" {{$product->fuel_type == 3 ? "selected" : ""}}>CNG</option>
                              <option value="4" {{$product->fuel_type == 4 ? "selected" : ""}}>Điện</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Dung tích bình chứa (liter)</label> 
                              <input type="number" name="fuel_tank" class="form-control" value="{{$product->fuel_tank_capacity}}">
                              <p class="help is-danger">{{ $errors->first('fuel_tank') }}</p>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-3 form-group">
                           <div class="form-group ">
                              <label class="form-label">Bộ hãm phanh</label> 
                              <input type="text" name="break" class="form-control" value="{{$product->break}}">
                              <p class="help is-danger">{{ $errors->first('break') }}</p> 
                           </div>
                        </div>
                        <div class="form-group col-md-4">
                           <label class="form-label">Kiểu dáng</label> 
                           <select name="category_name" class="form-control select2 select2-accessible">
                              @foreach($category as $key => $cate)
                              @if($product->category_id == $cate->id)
                              <option selected value="{{ $cate->id }}">{{ $cate->name }}</option>
                              @else
                              <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                              @endif
                              @endforeach
                           </select>
                        </div>
                        <div class="form-group col-md-4">
                           <label class="form-label">Thương hiệu</label> 
                           <select name="brand_name" class="form-control select2 select2-accessible" data-placeholder="Choose Browser" data-select2-id="1" tabindex="-1" aria-hidden="true">
                              @foreach($brand as $key => $brand_name)
                              @if($product->brand_id == $brand_name->id)
                              <option selected value="{{ $brand_name->id }}">{{ $brand_name->name }}</option>
                              @else
                              <option value="{{ $brand_name->id }}">{{ $brand_name->name }}</option>
                              @endif
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="form-group "> 
                        <label class="form-label">Link video</label>
                        <input type="text" name="video_url" class="form-control w-100" value="{{str_replace('embed/','watch?v=',$product->video)}}">
                        <p class="help is-danger">{{ $errors->first('video_url') }}</p>
                     </div>
                     <iframe width="560" height="315" src="{{$product->video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                     <div class="form-group">
                        <label class="form-label">Mô tả sản phẩm</label> 
                        <textarea name="product_description" id="ckeditor1" cols="50" rows="10" class="form-control" style="resize: none">{{$product->description}}</textarea>
                        <p class="help is-danger">{{ $errors->first('product_description') }}</p>
                     </div>
                     {{-- nội dung --}}
                     <div class="form-group">
                        <label class="form-label">Nội dung sản phẩm</label> 
                        <textarea name="content" id="content" cols="50" rows="10" class="form-control" style="resize: none">{{$product->content}}</textarea>
                        <p class="help is-danger">{{ $errors->first('content') }}</p>
                     </div>
                     {{-- tính năng --}}
                     <div class="form-group">
                        <div class="form-group m-0">
                           <div class="form-label">Tính năng</div>
                           @foreach($attribute as $key => $att)
                           <div class="form-check form-check-inline">
                              <input class="form-check-input"  name="checkbox[]" type="checkbox" value="{{$att->id}}">
                              <label class="form-check-label" for="inlineCheckbox1">{{ $att->attribute_name }}</label>
                           </div>
                           @endforeach
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="form-group m-0">
                           <div class="form-label">Tags</div>
                           @foreach($tags as $key => $tag)
                           <div class="form-check form-check-inline">
                              <input class="form-check-input" name="tags[]" type="checkbox" id="inlineCheckbox1" value="{{$tag->id}}">
                              <label class="form-check-label" for="inlineCheckbox1">{{ $tag->name }}</label>
                           </div>
                           @endforeach
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="form-upload">
                           <div class="form-upload__preview"></div>
                           <div class="form-upload__field">
                              <label class="form-upload__title" for="upload">Chọn hình ảnh
                              <input class="form-upload__control js-form-upload-control" 
                              name="images[]" id="upload" type="file" multiple="true" />
                              <p class="help is-danger">{{ $errors->first('images') }}</p>
                              </label>
                              <img src="{{URL::to('/img_product/'.$product->image_product)}}" height="90" width="120" style="margin-left: 5px"> 
                              <button class="btn btn-dark btn-clear ml-3">Xóa hết</button>
                           </div>
                        </div>
                     </div>
                     {{-- check(xem còn hàng hay hết hàng) --}}
                     <div class="col-md-3">
                         <div class="form-group ">
                            <label class="form-label">kiểm tra</label> 
                            <select name="check_sp" class="form-control " >
                               <option value="0"{{$product->check_sp == 0 ? "selected" : ""}}>Còn Hàng</option>
                               <option value="1"{{$product->check_sp == 1 ? "selected" : ""}}>Hết Hàng</option>
                            </select>
                         </div>
                      </div>
                      {{-- check_new --}}
                      <div class="col-md-3">
                         <div class="form-group ">
                            <label class="form-label">Kiểm chứng</label> 
                            <select name="check_new" class="form-control " >
                               <option value="1"{{$product->check_new == 1 ? "selected" : ""}}>Mới</option>
                               <option value="2"{{$product->check_new == 2 ? "selected" : ""}}>Cho thuê</option>
                               <option value="3"{{$product->check_new == 3 ? "selected" : ""}}>Đã Sử dụng</option>
                            </select>
                         </div>
                      </div>
                  </div>
                  <div class="card-footer"> <button type="submit" class="btn btn-primary waves-effect waves-light">Cập nhật</button> </div>
            </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   (function ($) {
    $.fn.attachmentUploader = function () {
      const uploadControl = $(this).find(".js-form-upload-control");
      const btnClear = $(this).find(".btn-clear");
      $(uploadControl).on("change", function (e) {
        const preview = $(this)
        .closest(".form-upload")
        .children(".form-upload__preview");
        const files = e.target.files;
   
        function previewUpload(file) {
          if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
            var reader = new FileReader();
            reader.addEventListener(
              "load",
              function () {
                const html =
                '<div class="form-upload__item">' +
                '<div class="form-upload__item-thumbnail" style="background-image: url(' +
                this.result +
                ')"></div>' +
                '<p class="form-upload__item-name">' +
                file.name +
                "</p>" +
                "</div>";
                preview.append(html);
                btnClear.show();
              },
              false
              );
            reader.readAsDataURL(file);
          } else {
            alert("Please upload image only");
            uploadControl.val("");
          }
        }
   
        [].forEach.call(files, previewUpload);
   
        btnClear.on("click", function () {
          $(".form-upload__item").remove();
          uploadControl.val("");
          $(this).hide();
        });
      });
    };
   })(jQuery);
   
   $(".form-upload").attachmentUploader();
   
</script>
@endsection
@section('js')
    <script>
        CKEDITOR.replace('product_description'); 
        CKEDITOR.replace('content');     
    </script>
@endsection