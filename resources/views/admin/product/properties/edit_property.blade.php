@extends('admin.master')
@section('title','Danh mục sản phẩm')
@section('content')
<div class="app-content  my-3 my-md-5">
   <div class="side-app">
      <div class="page-header">
         <a class=" btn btn-success" href="{{route('get-list-properties')}}" style="color: white;">Danh sách thuộc tính</a>
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Settings</a></li>
            <li class="breadcrumb-item active" aria-current="page">New Ad</li>
         </ol>
      </div>
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @elseif(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
            @endif
      <div class="row row-cards">
         <div class="col-md-12">
            <div class="card">
               <div class="card-header">
                  <div class="card-title">Cập nhật thuộc tính: {{$property->attribute_name}}</div>
               </div>
               <form action="{{route('post-edit-property',$property->id)}}" method="POST">
                  @csrf
               <div class="card-body">
                  <div class="form-group "> 
                     <label class="form-label">Tên thuộc tính</label>
                     <input type="text" name="attribute_name" class="form-control w-100" value="{{$property->attribute_name}}">
                  </div>
                  <button type="submit" class="btn btn-primary waves-effect waves-light">Cập nhật</button>     
               </div>
            </form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection