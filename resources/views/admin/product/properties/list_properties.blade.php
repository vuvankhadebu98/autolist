@extends('admin.master')
@section('title','Danh mục sản phẩm')
@section('content')
<div class="app-content">
   <div class="side-app">
      <div class="page-header">
         <a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a> 
         <a class=" btn btn-success" href="{{route('get-add-properties')}}" style="color: white;">Thêm thuộc tính</a>
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Tables</a></li>
            <li class="breadcrumb-item active" aria-current="page">Thuộc tính</li>
         </ol>
      </div>
      @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @elseif(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
            @endif
      <div class="row">
         <div class="col-md-12 col-lg-12">
            <div class="card">
               <div class="card-header">
                  <div class="card-title">Danh sách thuộc tính</div>
               </div>
               <div class="card-body">
                  <div class="table-responsive">
                     <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                        <div class="row">
                           
                           <div class="col-sm-12 col-md-6">
                              <div id="example_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example"></label></div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <table id="example" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="example_info">
                                 <thead>
                                    <tr role="row">
                                        <th class="wd-15p sorting_asc">#</th>
                                       <th class="wd-15p sorting_asc" >Tên sản phẩm</th>
                                       
                                       <th class="wd-25p sorting" >Thao tác</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($properties as $key => $proper)
                                    <tr>
                                     <td scope="row">{{ $key+1 }}</td>

                                     <td >{{ $proper->attribute_name}}</td>
                                    
                                     <td style="text-align: center;">
                                       <a href="{{route('get-edit-property',$proper->id)}}" class="btn btn-danger">Edit</a>


                                       <a href="{{route('delete-property',$proper->id)}}" onclick="return confirm('Xóa thuộc tính {{$proper->attribute_name}}?')" class="btn btn-danger" >Delete</a>
                                    </td>
                                 </tr>
                                   @endforeach
                              </tbody>
                           </table>
                        </div>
                     </div>
                     
                     <div class="row">
                        <div class="col-sm-12 col-md-12  ">
                           <div class="dataTables_paginate paging_simple_numbers align-content-center" id="example_paginate">
                              <ul class="pagination">
                                 
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- table-wrapper --> 
         </div>
         <!-- section-wrapper --> 
      </div>

   </div>
</div>
</div>
<script type="text/javascript">
   document.getElementById('my-select').addEventListener('change', function() {
      var value = $("#my-select option:selected").val();
      // alert(value);
  // console.log('You selected: ', this.value);
   $.ajax({
      url: '{{route('change.load.listpro')}}',
      method: 'GET',
      data:{
         value:value,
      },
      success: function(data){
         $("table#example tbody").empty();
          $("table#example tbody").html(data);
         console.log(data);
      }
   })
});
</script>
@endsection