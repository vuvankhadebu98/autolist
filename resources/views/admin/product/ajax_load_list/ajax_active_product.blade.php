@foreach($products as $key => $product)
  <tr>
     <td scope="row">
        <p>{{$product->name}}</p>
     </td>
     <td >
        <p>{{$product->category->name}}</p>
     </td>
     <td>
        <p>{{$product->brand->name}}</p>
     </td>
     @php 
     $imageImplode = $product->image_product;
     $imageExplode = explode(',',$imageImplode);
     @endphp
     <td style="width: 160px;">
        @foreach(array_slice($imageExplode,0,1) as $items)
          <img src="{{asset('img_product/'.$items)}}"> 
        @endforeach
     </td>

     <td >
        <p>{{number_format($product->price)}} VNĐ</p>
     </td>
     <td>
        <p>{{$product->promotion}} %</p>
     </td>
     {{-- check_sp --}}
     <td>
        <?php
         if($product->check_sp == 0)
         {
         ?>
          <span class="hienthi" id="0"style="color: #E60404;">Hết Hàng</span>
         <?php
         }elseif($product->check_sp == 1){
         ?>
          <span class="hienthi" id="1" style="color: #0DD902;">Còn Hàng</span>
         <?php
         }
       ?>
     </td>
     <td>
       <font style="vertical-align: inherit;">
          <font style="vertical-align: inherit;">
             <?php
               if($product->check_new == 0)
               {
               ?>
                <span class="hienthi" id="0" style="color: #05a01f;">Mới</span>
               <?php
               }elseif($product->check_new == 1){
               ?>
                <span class="hienthi" id="1" style="color: #ff382b;">Cho Thuê</span>
               <?php
               }
               elseif($product->check_new == 2){
               ?>
                <span class="hienthi" id="2" style="color: #868e96;">Đã Sử Dụng</span>
               <?php
               }
             ?>
          </font>
       </font>
     </td>
     {{-- status --}}
     <td style="text-align: center;">
        <?php 
           if($product->status == 0){
             ?>
        <a style=" margin-top: 25px;" href="{{route('active-pro',$product->id)}}" class="btn btn-danger">Chờ xử lý</a>
        <?php 
           }else{
             ?>
        <a style=" margin-top: 25px;" href="{{route('unactive-pro',$product->id)}}" class="btn btn-success">Đã duyệt</a>
        <?php
           }
        ?>
      </td>
      <td style="text-align: center;">
        <a  style=" margin-top: 25px;" href="{{route('get-edit',$product->id)}}" class="btn btn-danger">Cập Nhật</a>
        <a style=" margin-top: 25px;" href="{{route('delete-product',$product->id)}}" onclick="return confirm('Xóa sản phẩm {{$product->name}}?')" class="btn btn-danger" >Xóa</a>
      </td>
  </tr>
  @endforeach