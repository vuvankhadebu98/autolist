@extends('admin.master')
@section('title','Danh mục sản phẩm')
@section('content')
<div class="app-content">
   <div class="side-app">
      <div class="page-header">
         <a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a> 
         <a class=" btn btn-success" href="{{route('get-add-pro')}}" style="color: white;">Thêm sản phẩm</a>
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Tables</a></li>
            <li class="breadcrumb-item active" aria-current="page">Sản phẩm</li>
         </ol>
      </div>
      @if(session()->has('message'))
      <div class="alert alert-success">
         {{ session()->get('message') }}
      </div>
      @elseif(session()->has('error'))
      <div class="alert alert-danger">
         {{ session()->get('error') }}
      </div>
      @endif
      <div class="row">
         <div class="col-md-12">
            <div class="card">
               <div class="card-header">
                  <div class="card-title"><a href="{{route('get-list-pro')}}" style="text-decoration: none">Danh sách sản phẩm</a></div>
               </div>
               <div class="card-body">
                  <div class="tab-content">
                     <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                        <div class="row">
                           <div class="col-sm-12 col-md-8">
                              <div class="row">
                                 <label class="col-sm-4">
                                    <p class="btn btn-primary" id="active_product" onclick="return active_product(this);" style="margin-left: 15px; margin-top: 15px;  ">Sản phẩm đã duyệt</p>
                                 </label>
                                 <label class="col-sm-4" style="margin-top: 20px">
                                    <select name="example_length" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm" id="select-category">
                                       <option>----Chọn kiểu xe----</option>
                                       @foreach($category_product as $key => $cate)
                                       <option value="{{$cate->id}}">{{$cate->name}}</option>
                                       @endforeach
                                    </select>
                                 </label>
                                 <label class="col-sm-4" style="margin-top: 20px">
                                    <select name="example_length" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm" id="select-brand">
                                       <option>----Chọn thương hiệu----</option>
                                       @foreach($brand_product as $key => $brand)
                                       <option value="{{$brand->id}}">{{$brand->name}}</option>
                                       @endforeach
                                    </select>
                                 </label>
                              </div>
                           </div>
                           <div class="col-sm-12 col-md-4">
                              <form action="{{route('search.product')}}" method="post">
                                 @csrf
                                 <div style="margin-top: 20px" id="example_filter" class="pull-right dataTables_filter"><label>Search:<input type="search" name="search" class="form-control form-control-sm" placeholder="Nhập tên sản phẩm" aria-controls="example"></label></div>
                              </form>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12 table-responsive border-top">
                              <table id="example" class="table card-table table-bordered table-hover table-vcenter text-nowrap" role="grid" aria-describedby="example_info">
                                 <thead>
                                    <tr role="row">
                                       <th class="wd-15p sorting_asc" >Tên sản phẩm</th>
                                       <th class="wd-15p sorting" >Kiểu dáng</th>
                                       <th class="wd-20p sorting" >Thương hiệu</th>
                                       <th class="wd-20p sorting" >Hình ảnh</th>
                                       <th class="wd-15p sorting" >Giá niêm yết</th>
                                       <th class="wd-10p sorting" >Giá giảm</th>
                                       <th class="wd-10p sorting" >Kiểm tra</th>
                                       <th class="wd-10p sorting" >Kiểm chứng</th>
                                       <th class="wd-15p sorting" >Trạng Thái</th>
                                       <th class="wd-20p sorting" >Thao Tác</th>

                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($products as $key => $product)
                                    <tr>
                                       <td scope="row">
                                          <p>{{$product->name}}</p>
                                       </td>
                                       <td >
                                          <p>{{$product->category->name}}</p>
                                       </td>
                                       <td>
                                          <p>{{$product->brand->name}}</p>
                                       </td>
                                       @php 
                                       $imageImplode = $product->image_product;
                                       $imageExplode = explode(',',$imageImplode);
                                       @endphp
                                       <td style="width: 160px;">
                                          @foreach(array_slice($imageExplode,0,1) as $items)
                                            <img src="{{asset('img_product/'.$items)}}"> 
                                          @endforeach
                                       </td>

                                       <td >
                                          <p>{{number_format($product->price)}} VNĐ</p>
                                       </td>
                                       <td>
                                          <p>{{$product->promotion}} %</p>
                                       </td>
                                       {{-- check_sp --}}
                                       <td>
                                          <?php
                                           if($product->check_sp == 1)
                                           {
                                           ?>
                                            <span class="hienthi" id="0"style="color: #E60404;">Hết Hàng</span>
                                           <?php
                                           }elseif($product->check_sp == 0){
                                           ?>
                                            <span class="hienthi" id="1" style="color: #0DD902;">Còn Hàng</span>
                                           <?php
                                           }
                                         ?>
                                       </td>
                                       <td>
                                         <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">
                                               <?php
                                                 if($product->check_new == 1)
                                                 {
                                                 ?>
                                                  <span class="hienthi" id="0" style="color: #05a01f;">Mới</span>
                                                 <?php
                                                 }elseif($product->check_new == 2){
                                                 ?>
                                                  <span class="hienthi" id="1" style="color: #ff382b;">Cho Thuê</span>
                                                 <?php
                                                 }
                                                 elseif($product->check_new == 3){
                                                 ?>
                                                  <span class="hienthi" id="2" style="color: #868e96;">Đã Sử Dụng</span>
                                                 <?php
                                                 }
                                               ?>
                                            </font>
                                         </font>
                                       </td>
                                       {{-- status --}}
                                       <td style="text-align: center;">
                                          <?php 
                                             if($product->status == 0){
                                               ?>
                                          <a style=" margin-top: 25px;" href="{{route('active-pro',$product->id)}}" class="btn btn-danger">Chờ xử lý</a>
                                          <?php 
                                             }else{
                                               ?>
                                          <a style=" margin-top: 25px;" href="{{route('unactive-pro',$product->id)}}" class="btn btn-success">Đã duyệt</a>
                                          <?php
                                             }
                                          ?>
                                        </td>
                                        <td style="text-align: center;">
                                          <a  style=" margin-top: 25px;" href="{{route('get-edit',$product->id)}}" class="btn btn-danger">Cập Nhật</a>
                                          <a style=" margin-top: 25px;" href="{{route('delete-product',$product->id)}}" onclick="return confirm('Xóa sản phẩm {{$product->name}}?')" class="btn btn-danger" >Xóa</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12 col-md-12  ">
                              <div class="dataTables_paginate paging_simple_numbers align-content-center" id="example_paginate">
                                 <ul class="pagination">
                                    {{$products->links()}}
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- table-wrapper --> 
            </div>
            <!-- section-wrapper --> 
         </div>
      </div>
   </div>
</div>
@include('sweetalert::alert')
<script type="text/javascript">
   function active_product(e) {
      $.ajax({
      url: '{{route('load.listactive')}}',
      method: 'GET',
     success: function(data){
       $("table#example tbody").empty();
       $("table#example tbody").html(data);
       console.log(data);
     }
   });
   }
   //lọc theo danh mục
   document.getElementById('select-category').addEventListener('change', function() {
    var value = $("#select-category option:selected").val();
    //     alert(value);
    // console.log('You selected: ', this.value);
    $.ajax({
      url: '{{route('change.cate.listpro')}}',
      method: 'GET',
      data:{
       value:value,
     },
     success: function(data){
       $("table#example tbody").empty();
       $("table#example tbody").html(data);
       console.log(data);
     }
   })
   });
   document.getElementById('select-brand').addEventListener('change', function() {
    var value = $("#select-brand option:selected").val();
    //     alert(value);
    // console.log('You selected: ', this.value);
    $.ajax({
      url: '{{route('change.brand.listpro')}}',
      method: 'GET',
      data:{
       value:value,
     },
     success: function(data){
       $("table#example tbody").empty();
       $("table#example tbody").html(data);
       console.log(data);
     }
   })
   });
</script>


@endsection