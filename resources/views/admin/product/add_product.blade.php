@extends('admin.master')
@section('title','Danh mục sản phẩm')
@section('content')
<div class="app-content  my-3 my-md-5">
   <div class="side-app">
      <div class="page-header">
         <a class=" btn btn-success" href="{{route('get-list-pro')}}" style="color: white;">Danh sách sản phẩm</a>
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Settings</a></li>
            <li class="breadcrumb-item active" aria-current="page">Thêm sản phẩm</li>
         </ol>
      </div>
      @if(session()->has('message'))
      <div class="alert alert-success">
         {{ session()->get('message') }}
      </div>
      @elseif(session()->has('error'))
      <div class="alert alert-danger">
         {{ session()->get('error') }}
      </div>
      @endif
      <div class="row row-cards">
         <div class="col-md-12">
            <div class="card">
               <div class="card-header">
                  <div class="card-title">Thêm sản phẩm mới</div>
               </div>
               <form action="{{route('post-add-pro')}}" method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="card-body">
                     <div class="form-group ">
                        <label class="form-label">Tên sản phẩm</label>
                        <input type="text" name="product_name" class="form-control w-100" placeholder="Nhập tên sản phẩm">
                        <p class="help is-danger">{{ $errors->first('product_name') }}</p>
                     </div>
                     <div class="row information">
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Số ghế</label> 
                              <input type="number" name="seats" class="form-control" placeholder="Nhập số lượng ghế"> 
                              <p class="help is-danger">{{ $errors->first('seats') }}</p>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Không gian khởi động</label> 
                              <input type="text" name="boot_space" class="form-control" placeholder="Nhập thông số">
                              <p class="help is-danger">{{ $errors->first('boot_space') }}</p>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Số dặm đi được</label> 
                              <input type="text" name="arai_milege" class="form-control" placeholder="Nhập thông số"> 
                              <p class="help is-danger">{{ $errors->first('arai_milege') }}</p>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Momen xoắn cực đại</label> 
                              <input type="text" name="max_torque" class="form-control" placeholder="Nhập thông số"> 
                              <p class="help is-danger">{{ $errors->first('max_torque') }}</p>
                           </div>
                        </div>
                     </div>
                     <div class="row information">
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Giá niêm yết</label> 
                              <input type="number" min='1' name="price" class="form-control" placeholder="Nhập vào giá"> 
                              <p class="help is-danger">{{ $errors->first('price') }}</p>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Ưu đãi (%)</label> 
                              <input type="number" name="promotion" class="form-control" placeholder="Enter Sub title"> 
                              <p class="help is-danger">{{ $errors->first('promotion') }}</p>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Công cụ chuyển</label> 
                              <input type="text" name="egn_displace" class="form-control" placeholder="Nhập thông số"> 
                              <p class="help is-danger">{{ $errors->first('egn_displace') }}</p>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Công xuất Tối Đa</label> 
                              <input type="number" name="max_power" class="form-control" placeholder="Nhập thông số"> 
                              <p class="help is-danger">{{ $errors->first('max_power') }}</p>
                           </div>
                        </div>
                     </div>
                     <div class="row information">
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Loại hộp số</label> 
                              <select name="transmission" class="form-control " data-placeholder="Choose Browser" 
                                 data-select2-id="1" tabindex="-1" aria-hidden="true">
                                 <option value="1" selected="">Hộp số sàn</option>
                                 <option value="2">Hộp số tự động</option>
                                 <option value="3">Hộp số tự động vô cấp CVT</option>
                                 <option value="4">Hộp số ly hợp kép DCT</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Động Cơ</label> 
                              <input type="text" name="engine" class="form-control" placeholder="Nhập thông số">
                              <p class="help is-danger">{{ $errors->first('engine') }}</p>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Loại nhiên liệu</label> 
                              <select name="fuel_type" class="form-control " >
                                 <option value="1" selected="">Diesel</option>
                                 <option value="2">Xăng</option>
                                 <option value="3">CNG</option>
                                 <option value="4">Điện</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group ">
                              <label class="form-label">Dung tích bình chứa (liter)</label> 
                              <input type="number" name="fuel_tank" class="form-control" placeholder="Enter Sub title">
                              <p class="help is-danger">{{ $errors->first('fuel_tank') }}</p>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-3 form-group">
                           <div class="form-group ">
                              <label class="form-label">Bộ hãm phanh</label> 
                              <input type="text" name="break" class="form-control" placeholder="Enter Sub title">
                              <p class="help is-danger">{{ $errors->first('break') }}</p>
                           </div>
                        </div>
                        <div class="form-group col-md-4">
                           <label class="form-label">Kiểu dáng</label> 
                           <select name="category_name" class="form-control ">
                              @foreach($category as $key => $cate)
                              <option value="{{$cate->id}}">{{$cate->name}}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="form-group col-md-4">
                           <label class="form-label">Thương hiệu</label> 
                           <select name="brand_name" class="form-control " data-placeholder="Choose Browser" data-select2-id="1" tabindex="-1" aria-hidden="true">
                              @foreach($brand as $key => $brand_name)
                              <option value="{{$brand_name->id}}">{{$brand_name->name}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="form-group ">
                        <label class="form-label">Link video</label>
                        <input type="text" name="video_url" class="form-control w-100" placeholder="Chèn vào link video youtube">
                        <p class="help is-danger">{{ $errors->first('video_url') }}</p>
                     </div>
                     {{-- mô tả --}}
                     <div class="form-group">
                        <label class="form-label">Mô tả sản phẩm</label> 
                        <textarea name="product_description" id="ckeditor1" cols="50" rows="10" class="form-control" style="resize: none"></textarea>
                        <p class="help is-danger">{{ $errors->first('product_description') }}</p>
                     </div>
                     {{-- nội dung --}}
                     <div class="form-group">
                        <label class="form-label">Nội Dung sản phẩm</label> 
                        <textarea name="content" id="content" cols="50" rows="10" class="form-control" style="resize: none"></textarea>
                        <p class="help is-danger">{{ $errors->first('content') }}</p>
                     </div>
                     <div class="form-group">
                        <div class="form-group m-0">
                           <div class="form-label">Tính năng</div>
                           @foreach($attribute as $key => $att)
                           <div class="form-check form-check-inline">
                              <input class="form-check-input" name="checkbox[]" type="checkbox" id="inlineCheckbox1" value="{{$att->id}}">
                              <label class="form-check-label" for="inlineCheckbox1">{{ $att->attribute_name }}</label>
                           </div>
                           <p class="help is-danger">{{ $errors->first('checkbox') }}</p>
                           @endforeach
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="form-group m-0">
                           <div class="form-label">Tags</div>
                           @foreach($tags as $key => $tag)
                           <div class="form-check form-check-inline">
                              <input class="form-check-input" name="tags[]" type="checkbox" id="inlineCheckbox1" value="{{$tag->id}}">
                              <label class="form-check-label" for="inlineCheckbox1">{{ $tag->name }}</label>
                           </div>
                           <p class="help is-danger">{{ $errors->first('tags') }}</p>
                           @endforeach
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="form-upload">
                           <div class="form-upload__preview"></div>
                           <div class="form-upload__field">
                              <label class="form-upload__title" for="upload">
                                 Chọn hình ảnh
                                 <input class="form-upload__control js-form-upload-control" name="images[]" id="upload" type="file" multiple="true"/>
                                 <p class="help is-danger">{{ $errors->first('images') }}</p>
                              </label>
                              <button class="btn btn-dark btn-clear ml-3">Xóa hết</button>
                           </div>
                        </div>
                     </div>
                     {{-- check(xem còn hàng hay hết hàng) --}}
                      <div class="col-md-3">
                         <div class="form-group ">
                            <label class="form-label">kiểm tra</label> 
                            <select name="check_sp" class="form-control " >
                               <option value="0" selected="">Còn Hàng</option>
                               <option value="1">Hết Hàng</option>
                            </select>
                         </div>
                      </div>
                      {{-- check_new --}}
                      <div class="col-md-3">
                         <div class="form-group ">
                            <label class="form-label">Kiểm chứng</label> 
                            <select name="check_new" class="form-control " >
                               <option value="1" selected="">Mới</option>
                               <option value="2">Cho thuê</option>
                               <option value="3">Đã Sử dụng</option>
                            </select>
                         </div>
                      </div>
                     <div>
                      <button type="submit" class="btn btn-primary waves-effect waves-light">Thêm sản phẩm</button> 
                    </div>
                  </div>
                </form>
            </div> 
         </div>
      </div>
   </div>
</div>
@include('sweetalert::alert')
<script type="text/javascript">
   (function ($) {
    $.fn.attachmentUploader = function () {
      const uploadControl = $(this).find(".js-form-upload-control");
      const btnClear = $(this).find(".btn-clear");
      $(uploadControl).on("change", function (e) {
        const preview = $(this)
        .closest(".form-upload")
        .children(".form-upload__preview");
        const files = e.target.files;
   
        function previewUpload(file) {
          if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
            var reader = new FileReader();
            reader.addEventListener(
              "load",
              function () {
                const html =
                '<div class="form-upload__item">' +
                '<div class="form-upload__item-thumbnail" style="background-image: url(' +
                this.result +
                ')"></div>' +
                '<p class="form-upload__item-name">' +
                file.name +
                "</p>" +
                "</div>";
                preview.append(html);
                btnClear.show();
              },
              false
              );
            reader.readAsDataURL(file);
          } else {
            alert("Please upload image only");
            uploadControl.val("");
          }
        }
   
        [].forEach.call(files, previewUpload);
   
        btnClear.on("click", function () {
          $(".form-upload__item").remove();
          uploadControl.val("");
          $(this).hide();
        });
      });
    };
   })(jQuery);
   
   $(".form-upload").attachmentUploader();
   
</script>
@endsection
@section('js')
<script>
   CKEDITOR.replace('product_description');
   CKEDITOR.replace('content');       
</script>
@endsection