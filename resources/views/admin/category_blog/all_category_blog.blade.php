@extends('admin.master')
@section('title','Home')
@section('content')
   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Danh sách danh mục blog</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Danh sách danh mục blog</font>
                  </font>
               </li>
            </ol>
         </div>
         {{-- nếu cập nhật thành công --}}
         <?php 
             $message = Session::get('message');
             if ($message) {
                 echo '<div class="alert alert-success">'. $message .'</div>';
                 Session::put('message', null);
             }
         ?>
         <div class="row">
            <div class="col-lg-12">
               <div class="card">
                  <div class="card-body">
                      <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('all-category-blog')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     
                      <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('add-category-blog')}}" class="btn" aria-expanded="false"> Thêm Mới Danh Mục Blog </a> 
                     </div>
                     {{-- tìm kiếm --}}
                     <form method="post" class="form-inline mr-auto" style="float: right;" action="{{route('search-category-blog')}}">
                        @csrf
                        <div class="nav-search"> 
                           <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                           <button class="btn btn-primary" style="width: 60px;height: 39px;" type="submit">
                              <i class="fa fa-search"></i>
                           </button> 
                        </div>
                     </form>
                     <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                           <div class="table-responsive border-top">
                              <table class="table card-table table-bordered table-hover table-vcenter text-nowrap">
                                 <tbody>
                                 {{-- hiển thị trang danh mục blog  --}}
                                    <tr style="text-align: center;">
                                       {{-- id category_blog --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Mã DM_BL
                                             </font>
                                          </font>
                                       </th> 
                                       {{-- tên blog --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Tên danh mục blog
                                             </font>
                                          </font>
                                       </th> 
                                       {{-- sửa xóa --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;" >Lựa Chọn
                                             </font>
                                          </font>
                                       </th>
                                    </tr>

                                 {{-- phầm danh mục blog hiển thị --}}
                                    @foreach($all_category_blog as $show_category_blog)
                                    <tr style="text-align: center;">
                                       {{-- mã danh mục blog --}}
                                       <td style="width: 15px;">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                {{$show_category_blog->id}}
                                             </font>
                                          </font>
                                       </td>
                                       {{-- têm danh mục blog --}}
                                       <td style="width: 500px;">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">
                                                <span style="color: #CC0000;">
                                                   {{$show_category_blog->name}}
                                                </span>
                                             </font>
                                          </font>
                                       </td>
                                       {{-- sửa xóa --}}
                                       <td class="r" style="text-align: center;width: 200px;">
                                          <a class="btn btn-xs btn-info" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          href="{{URL::to('/edit-category-blog/'.$show_category_blog->id)}}">
                                             <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
                                          </a>
                                          <a class="btn btn-xs btn-danger" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                                          href="{{URL::to('/delete-category-blog/'.$show_category_blog->id)}}">
                                             <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                          </a>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>  
                        </div> 
                     </div>
                  </div>
               </div>
               <ul class="pagination mb-5">
                  {{$all_category_blog->links()}}
               </ul>
            </div>
         </div>
      </div>
   </div>
<!--App-Content--> 
@endsection('content')