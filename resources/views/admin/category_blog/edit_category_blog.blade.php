@extends('admin.master')
@section('title','Home')
@section('content')

   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Cập nhật danh muc blog</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Cập nhật danh mục blog</font>
                  </font>
               </li>
            </ol>
         </div>
         
         <div class="row row-cards">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">
                        <font style="vertical-align: inherit;">
                           <font style="vertical-align: inherit;">Cập nhật danh mục blog</font>
                        </font>
                     </div>
                  </div>
                  {{-- thêm mới --}}
                  @foreach($edit_category_blog as $key => $edit_category_bl)
                    <form role="form" action="{{URL::to('/update-category-blog/'.$edit_category_bl->id)}}" method="post" 
                     enctype="multipart/form-data">
                    {{ csrf_field()}}
                     <div class="card-body">
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Tên danh mục blog</font>
                              </font>
                           </label> 
                           <input type="text" name="name" class="form-control w-100" placeholder="Nhập tên danh mục blog" value="{{$edit_category_bl->name}}"> 
                        </div>
                        <button type="submit" name="update_category_blog" 
                        class="btn btn-primary waves-effect waves-light">
                           <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Cập nhật danh mục blog</font>
                           </font>
                        </button> 
                     </div>            
                  </form>
                @endforeach
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--App-Content--> 
@endsection('content')

@section('js')
    <script>
        CKEDITOR.replace('description');      
    </script>
@endsection