@extends('admin.master')
@section('title','Home')
@section('content')

   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Danh mục Blog Mới</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Danh mục Blog mới</font>
                  </font>
               </li>
            </ol>
         </div>
         <?php 
              $message = Session::get('message');
              if ($message) {
                  echo '<div class="alert alert-success">'. $message .'</div>';
                  Session::put('message', null);
              }
            ?>
         <div class="row row-cards">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">
                        <font style="vertical-align: inherit;">
                           <font style="vertical-align: inherit;">Tạo Danh Mục Blog Mới</font>
                            <a href="{{URL::to('all-category-blog')}}" class="btn" aria-expanded="false" style="vertical-align: inherit; font-size: 18px;color: #20BAED">Danh Sách Danh Mục Blog</a> 
                        </font>
                     </div>
                  </div>
                  {{-- thêm mới --}}
                  <form role="form" action="{{URL::to('/save-category-blog')}}" method="post" 
                     enctype="multipart/form-data">
                    {{ csrf_field()}}
                     <div class="card-body">
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Tên danh mục blog</font>
                              </font>
                           </label> 
                           <input type="text" name="name" class="form-control w-100" placeholder="Nhập tên blog">
                           <p class="help is-danger">{{ $errors->first('name') }}</p> 
                        </div>

                        <button type="submit" name="add_category_blog" 
                        class="btn btn-primary waves-effect waves-light">
                           <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Thêm mới danh mục blog</font>
                           </font>
                        </button> 
                     </div>            
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--App-Content--> 
@endsection('content')

@section('js')
    <script>
        CKEDITOR.replace('description');      
    </script>
@endsection