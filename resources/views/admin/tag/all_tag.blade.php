@extends('admin.master')
@section('title','Home')
@section('content')
   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Danh Sách Tag</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài Đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Danh Sách Tag</font>
                  </font>
               </li>
            </ol>
         </div>
         {{-- nếu cập nhật thành công --}}
         <?php 
             $message = Session::get('message');
             if ($message) {
                 echo '<div class="alert alert-success">'. $message .'</div>';
                 Session::put('message', null);
             }
         ?>
         <div class="row">
            <div class="col-lg-12">
               <div class="card">
                  <div class="card-body">
                     <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('all-tag')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     
                      <div class="btn-group hidden-phone" style="background-color:#05B811";> 
                        <a href="{{URL::to('add-tag')}}" class="btn" aria-expanded="false" style="color: #fff"> Thêm Mới Tag </a> 
                     </div>
                     <label class=" search-tag">
                        <p class="btn btn-primary" id="active_tag_product" onclick="return active_tag_product(this);" style="margin-left: 5px; margin-top: 17px; ">Tag Sản Phẩm</p>
                     </label>
                     <label class="c search-tag">
                        <p class="btn btn-primary" id="active_tag_blog" onclick="return active_tag_blog(this);" style="margin-left: 5px; margin-top: 17px; ">Tag Blog</p>
                     </label>
                     <label class=" search-tag">
                        <p class="btn btn-primary" id="active_tag_poster" onclick="return active_tag_poster(this);" style="margin-left: 5px; margin-top: 17px; ">Tag Quảng Cáo</p>
                     </label>
                     <label class=" search-tag">
                        <p class="btn btn-primary" id="active_tag_accessaries" onclick="return active_tag_accessaries(this);" style="margin-left: 5px; margin-top: 17px; ">Tag Phụ Tùng</p>
                     </label>
                     {{-- tìm kiếm --}}
                     <form method="post" class="form-inline mr-auto" style="float: right;" action="{{route('search-tag')}}">
                        @csrf
                        <div class="nav-search"> 
                           <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                           <button class="btn btn-primary" style="width: 60px;height: 39px;" type="submit">
                              <i class="fa fa-search"></i>
                           </button> 
                        </div>
                     </form>
                     <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                           <div class="table-responsive border-top">
                              <table id="example" class="table card-table table-bordered table-hover table-vcenter text-nowrap">
                                 <tbody>
                                    <tr style="text-align: center;">
                                      {{--  --}}
                                      <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Mã Tag
                                             </font>
                                          </font>
                                       </th>
                                       {{-- tên tag --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Tên Tag
                                             </font>
                                          </font>
                                       </th>
                                       {{-- Lựa chọn --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Loại Tag
                                             </font>
                                          </font>
                                       </th>
                                       {{-- sửa xóa --}}
                                       <th style="text-align: center;">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;" >Lựa Chọn
                                             </font>
                                          </font>
                                       </th>
                                    </tr>

                                    {{-- phầm blog hiển thị --}}

                                    @foreach($all_tag as $key=> $show_tag)
                                    <tr style="text-align: center;">
                                      {{-- id --}}
                                      <td>
                                         <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">
                                              <span>
                                               {{$show_tag->id}}
                                              </span>
                                            </font>
                                         </font>
                                       </td> 
                                       {{-- têm tag --}}
                                       <td style="width: 400px">
                                         <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">
                                              <span style="color: #CC0000;">
                                               {{$show_tag->name}}
                                              </span>
                                            </font>
                                         </font>
                                       </td> 
                                   {{-- check --}}
                                      <td style="width: 300px">
                                         <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">
                                               <?php
                                                 if($show_tag->check == 1)
                                                 {
                                                 ?>
                                                 <span class="hienthi" >Sản Phẩm</span>
                                                 <?php
                                                 }elseif($show_tag->check == 2){
                                                 ?>
                                                <span class="hienthi" >Blog</span>
                                                 <?php
                                                 }
                                                 elseif($show_tag->check == 3){
                                                 ?>
                                                <span class="hienthi" >Quảng Cáo</span>
                                                 <?php
                                                 }
                                                 elseif($show_tag->check == 4){
                                                 ?>
                                                <span class="hienthi" >Phụ Tùng</span>
                                                 <?php
                                                 }
                                               ?> 
                                            </font>
                                         </font>
                                      </td> 
                                       
                                       {{-- sửa xóa --}}
                                       <td class="r" style="text-align: center;width: 250px;">
                                          <a class="btn btn-xs btn-info" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          href="{{URL::to('/edit-tag/'.$show_tag->id)}}">
                                             <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
                                          </a>
                                          <a class="btn btn-xs btn-danger" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                                          href="{{URL::to('/delete-tag/'.$show_tag->id)}}">
                                             <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                          </a>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>  
                        </div> 
                     </div>
                  </div>
               </div>
               <ul class="pagination mb-5">
                {{ $all_tag->links() }}
               </ul>
            </div>
         </div>
      </div>
   </div>
<!--App-Content--> 
<script type="text/javascript">
   function active_tag_product(e) {
      $.ajax({
      url: '{{route('load.product')}}',
      method: 'GET',
      success: function(data){
         $("table#example tbody").empty();
         $("table#example tbody").html(data);
         console.log(data);
         }
      });
   }
   function active_tag_blog(e) {
      $.ajax({
      url: '{{route('load.blog')}}',
      method: 'GET',
      success: function(data){
         $("table#example tbody").empty();
         $("table#example tbody").html(data);
         console.log(data);
         }
      });
   }
   function active_tag_poster(e) {
      $.ajax({
      url: '{{route('load.poster')}}',
      method: 'GET',
      success: function(data){
         $("table#example tbody").empty();
         $("table#example tbody").html(data);
         console.log(data);
         }
      });
   }
   function active_tag_accessaries(e) {
      $.ajax({
      url: '{{route('load.accessaries')}}',
      method: 'GET',
      success: function(data){
         $("table#example tbody").empty();
         $("table#example tbody").html(data);
         console.log(data);
         }
      });
   }
</script>
@endsection('content')