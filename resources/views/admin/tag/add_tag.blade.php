@extends('admin.master')
@section('title','Home')
@section('content')

<!--/Sidebar menu--> <!--App-Content--> 
<div class="app-content  my-3 my-md-5">
   <div class="side-app">
      <div class="page-header">
         <h4 class="page-title">
            <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">Tag Mới</font>
            </font>
         </h4>
         <ol class="breadcrumb">
            <li class="breadcrumb-item">
               <a href="#">
               <font style="vertical-align: inherit;">
               <font style="vertical-align: inherit;">Cài Đặt</font>
               </font>
               </a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">
               <font style="vertical-align: inherit;">
               <font style="vertical-align: inherit;">Tag Mới</font> 
               </font>
            </li>
         </ol>
      </div>
      <?php 
         $message = Session::get('message');
         if ($message) {
           echo '<div class="alert alert-success">'. $message .'</div>';
           Session::put('message', null);
         }
         ?>
      <div class="row row-cards">
         <div class="col-md-12">
            <div class="card">
               <div class="card-header">
                  <div class="card-title">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Tạo Tag Mới</font>
                        <a href="{{URL::to('all-tag')}}" class="btn" aria-expanded="false" style="vertical-align: inherit; font-size: 18px;color: #20BAED">Danh Sách Tag</a>
                     </font>
                  </div>
               </div>
               {{-- thêm mới --}}
               <form role="form" action="{{URL::to('/save-tag')}}" method="post" id="formDemo"
                  enctype="multipart/form-data">
                  {{ csrf_field()}}
                  <div class="card-body">
                     <div class="form-group">
                        <label class="form-label">
                        <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Lựa Chọn</font>
                        </font>
                        </label> 
                        <select name="check" class="form-control m-bot15">
                           <option value="1">Sản Phẩm</option>
                           <option value="2">Blog</option>
                           <option value="3">Quảng Cáo</option>
                           <option value="4">Phụ Tùng</option>
                        </select>
                     </div>
                     <div class="form-group "> 
                        <label class="form-label">
                        <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Tên Tag</font>
                        </font>
                        </label> 
                        <input type="text" name="name" class="form-control w-100" placeholder="Nhập tên tag">
                        <p class="help is-danger">{{ $errors->first('name') }}</p> 
                     </div>
                     <button type="submit" name="add_tag" 
                        class="btn btn-primary waves-effect waves-light">
                     <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Thêm mới tag</font>
                     </font>
                     </button> 
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!--App-Content--> 
@endsection('content')
