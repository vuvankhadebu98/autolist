@extends('admin.master')
@section('title','Home')
@section('content')
   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Danh Sách Tag</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài Đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Danh Sách Tag</font>
                  </font>
               </li>
            </ol>
         </div>
         {{-- nếu cập nhật thành công --}}
         <?php 
             $message = Session::get('message');
             if ($message) {
                 echo '<div class="alert alert-success">'. $message .'</div>';
                 Session::put('message', null);
             }
         ?>
         <div class="row">
            <div class="col-lg-12">
               <div class="card">
                  <div class="card-body">
                     <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('all-tag')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     
                      <div class="btn-group hidden-phone"> 
                        <a href="{{URL::to('add-tag')}}" class="btn" aria-expanded="false"> Thêm Mới Tag </a> 
                     </div>
                     {{-- tìm kiếm --}}
                     <form method="post" class="form-inline mr-auto" style="float: right;" action="{{route('search-tag')}}">
                        @csrf
                        <div class="nav-search"> 
                           <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                           <button class="btn btn-primary" style="width: 60px;height: 39px;" type="submit">
                              <i class="fa fa-search"></i>
                           </button> 
                        </div>
                     </form>
                     <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                           <div class="table-responsive border-top">
                              <table class="table card-table table-bordered table-hover table-vcenter text-nowrap">
                                 <tbody>
                                    <tr style="text-align: center;">
                                       {{-- tên tag --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Tên Tag
                                             </font>
                                          </font>
                                       </th>
                                       {{-- Lựa chọn --}}
                                       <th class="w-1">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;">Loại Tag
                                             </font>
                                          </font>
                                       </th>
                                       {{-- sửa xóa --}}
                                       <th style="text-align: center;">
                                          <font style="vertical-align: inherit;">
                                             <font style="vertical-align: inherit;" >Sửa || xóa
                                             </font>
                                          </font>
                                       </th>
                                    </tr>

                                    {{-- phầm blog hiển thị --}}

                                    @foreach($all_tag as $key=> $show_tag)
                                    <tr style="text-align: center;">
                                       {{-- têm tag --}}
                                       <td>
                                          <a href="#" class="btn-link">
                                             <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">
                                                   {{$show_tag->name}}
                                                </font>
                                             </font>
                                          </a> 
                                       </td> 
                                   {{-- check --}}
                                      <td>
                                         <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">
                                               <?php
                                                 if($show_tag->check == 1)
                                                 {
                                                 ?>
                                                 <span class="hienthi" >Sản Phẩm</span>
                                                 <?php
                                                 }elseif($show_tag->check == 2){
                                                 ?>
                                                <span class="hienthi" >Blog</span>
                                                 <?php
                                                 }
                                                 elseif($show_tag->check == 3){
                                                 ?>
                                                <span class="hienthi" >Quảng Cáo</span>
                                                 <?php
                                                 }
                                               ?> 
                                            </font>
                                         </font>
                                      </td> 
                                       
                                       {{-- sửa xóa --}}
                                       <td class="r" style="text-align: center;width: 160px;">
                                          <a class="btn btn-xs btn-info" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          href="{{URL::to('/edit-tag/'.$show_tag->id)}}">
                                             <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
                                          </a>
                                          <a class="btn btn-xs btn-danger" 
                                          style="font-size: 18px;width: 70px; height: 30px;"
                                          onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                                          href="{{URL::to('/delete-tag/'.$show_tag->id)}}">
                                             <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                          </a>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>  
                        </div> 
                     </div>
                  </div>
               </div>
               <ul class="pagination mb-5">
               </ul>
            </div>
         </div>
      </div>
   </div>
<!--App-Content--> 
@endsection('content')