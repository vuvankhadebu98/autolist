@foreach($all_tag as $key=> $show_tag)
<tr style="text-align: center;">
  {{-- id --}}
  <td>
     <font style="vertical-align: inherit;">
        <font style="vertical-align: inherit;">
          <span>
           {{$show_tag->id}}
          </span>
        </font>
     </font>
   </td> 
   {{-- têm tag --}}
   <td style="width: 400px">
     <font style="vertical-align: inherit;">
        <font style="vertical-align: inherit;">
          <span style="color: #CC0000;">
           {{$show_tag->name}}
          </span>
        </font>
     </font>
   </td> 
{{-- check --}}
  <td style="width: 300px">
     <font style="vertical-align: inherit;">
        <font style="vertical-align: inherit;">
           <?php
             if($show_tag->check == 1)
             {
             ?>
             <span class="hienthi" >Sản Phẩm</span>
             <?php
             }elseif($show_tag->check == 2){
             ?>
            <span class="hienthi" >Blog</span>
             <?php
             }
             elseif($show_tag->check == 3){
             ?>
            <span class="hienthi" >Quảng Cáo</span>
             <?php
             }
             elseif($show_tag->check == 4){
             ?>
            <span class="hienthi" >Phụ Tùng</span>
             <?php
             }
           ?> 
        </font>
     </font>
  </td> 
   
   {{-- sửa xóa --}}
   <td class="r" style="text-align: center;width: 250px;">
      <a class="btn btn-xs btn-info" 
      style="font-size: 18px;width: 70px; height: 30px;"
      href="{{URL::to('/edit-tag/'.$show_tag->id)}}">
         <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
      </a>
      <a class="btn btn-xs btn-danger" 
      style="font-size: 18px;width: 70px; height: 30px;"
      onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
      href="{{URL::to('/delete-tag/'.$show_tag->id)}}">
         <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
      </a>
   </td>
</tr>
@endforeach