@extends('admin.master')
@section('title','Home')
@section('content')

   <!--/Sidebar menu--> <!--App-Content--> 
   <div class="app-content  my-3 my-md-5">
      <div class="side-app">
         <div class="page-header">
            <h4 class="page-title">
               <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Cập nhật Tag</font>
               </font>
            </h4>
            <ol class="breadcrumb">
               <li class="breadcrumb-item">
                  <a href="#">
                     <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Cài Đặt</font>
                     </font>
                  </a>
               </li>
               <li class="breadcrumb-item active" aria-current="page">
                  <font style="vertical-align: inherit;">
                     <font style="vertical-align: inherit;">Cập Nhật Tag</font>
                  </font>
               </li>
            </ol>
         </div>   
         <div class="row row-cards">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">
                        <font style="vertical-align: inherit;">
                           <font style="vertical-align: inherit;">Cập Nhật Tag</font>
                        </font>
                     </div>
                  </div>
                  {{-- thêm mới --}}
                  @foreach($edit_tag as $key => $edit_tg)
                    <form role="form" action="{{URL::to('/update-tag/'.$edit_tg->id)}}" method="post" 
                     enctype="multipart/form-data">
                    {{ csrf_field()}}
                     <div class="card-body">
                      {{-- Check  --}}
                        <div class="form-group">
                            <label class="form-label">
                              <font style="vertical-align: inherit;">
                               <font style="vertical-align: inherit;">Lựa Chọn</font>
                             </font>
                            </label> 
                           <select name="check" class="form-control m-bot15">
                              <option value="1" {{$edit_tg->check == 1 ? "selected" : ""}}>Sản Phẩm</option>
                              <option value="2" {{$edit_tg->check == 2 ? "selected" : ""}}>Blog</option>
                              <option value="3" {{$edit_tg->check == 3 ? "selected" : ""}}>Quảng Cáo</option>
                              <option value="4" {{$edit_tg->check == 4 ? "selected" : ""}}>Phụ Tùng</option>
                           </select>
                         </div>
                      {{-- tên Tag --}}
                        <div class="form-group "> 
                           <label class="form-label">
                              <font style="vertical-align: inherit;">
                                 <font style="vertical-align: inherit;">Tên Tag</font>
                              </font>
                           </label> 
                           <input type="text" name="name" class="form-control w-100" placeholder="Nhập tên Tag" value="{{$edit_tg->name}}"> 
                        </div>
                        {{-- cập nhật --}}
                        <button type="submit" name="update_tag" 
                        class="btn btn-primary waves-effect waves-light">
                           <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Cập nhật Tag</font>
                           </font>
                        </button> 
                     </div>            
                  </form>
                @endforeach
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--App-Content--> 
@endsection('content')
