@extends('client.master')
@section('content')
        </div>
        <!--/Topbar-->
        <!--Breadcrumb-->
        <section>
            <div class="bannerimg cover-image bg-background3" data-image-src="assets/images/banners/banner2.jpg"
                style="background: url(&quot;assets/images/banners/banner2.jpg&quot;) center center;">
                <div class="header-text mb-0">
                    <div class="container">
                        <div class="text-center text-white">
                            <h1 class="">Register</h1>

                            <ol class="breadcrumb text-center">
                                <li class="breadcrumb-item"><a href="{{route('getHome')}}">Home</a></li>
                                <li class="breadcrumb-item active text-white" aria-current="page"><a href="{{route('getRegister')}}">Register</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/Breadcrumb-->
        <!--Section-->
        <section class="sptb">
            <div class="container customerpage">
                <div class="row">
                    <div class="single-page">
                        <div class="col-lg-5 col-xl-4 col-md-6 d-block mx-auto">
                            <div class="wrapper wrapper2">
                                <form method="post" action="{{route('postRegister')}}">
                                    @csrf
                                    <br>
                                    <h3>Register</h3>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p style="color: black">Showroom</p>
                                            <input type="radio"  name="role_id" value="11" style="width: 15%">
                                        </div>
                                        <div class="col-sm-6">
                                            <p style="color: black">User</p>
                                            <input style="width: 15%" type="radio"  name="role_id" value="12" >
                                        </div>
                                    </div>


                                @if(Session::has('messages'))
                                    <strong style="color: green">{{Session::get('messages')}}</strong>
                                    @endif
                                    <input value="{{old('firstname')}}" type="text" name="firstname" style="width: 90%; margin: 10px 0" placeholder="Firstname">
                                    @if($errors->has('firstname'))<p style="color: red" style="color: red"><strong>{{$errors->first('firstname')}}</strong></p>@endif

                                    <input value="{{old('lastname')}}" type="text" name="lastname" style="width: 90%; margin: 10px 0" placeholder="Lastname">
                                    @if($errors->has('lastname'))<p style="color: red"><strong>{{$errors->first('lastname')}}</strong></p>@endif

                                    <input value="{{old('username')}}" type="text" name="username" style="width: 90%; margin: 10px 0" placeholder="Username">
                                    @if($errors->has('username'))<p style="color: red"><strong>{{$errors->first('username')}}</strong></p>@endif


                                    <input value="{{old('email')}}" type="gmail" name="email" style="width: 90%; margin: 10px 0" placeholder="Email">
                                    @if($errors->has('email'))<p style="color: red"><strong>{{$errors->first('email')}}</strong></p>
                                    @endif

                                    <input  type="password" name="password" style="width: 90%; margin: 10px 0" placeholder="Password">
                                    @if($errors->has('password'))<p style="color: red"><strong>{{$errors->first('password')}}</strong></p>@endif

                                    <input  type="password" name="confirm_password" style="width: 90%; margin: 10px 0" placeholder="Password">
                                    @if($errors->has('confirm_password'))<p style="color: red"><strong>{{$errors->first('confirm_password')}}</strong></p>@endif

                                    <input type="submit" name="register" value="LOGIN" style="width: 90%; margin: 10px 0; background-color: red; color: white; font-weight: 900">
                                </form>
                                <hr class="divider">
                                <div class="card-body social-images">
                                    <p class="text-body text-left">Sign In to Social Accounts</p>
                                    <div class="row">
                                        <div class="col-6"> <a href="https://www.facebook.com/"
                                                class="btn btn-white mr-2 border px-2 btn-lg btn-block mb-0 text-left">
                                                <img src="assets/images/svgs/svg/facebook.svg" class="h-5 w-5"
                                                    alt=""><span class="ml-3 d-inline-block font-weight-bold">
                                                    Facebook</span> </a> </div>
                                        <div class="col-6"> <a href="https://www.google.com/gmail/"
                                                class="btn btn-white mr-2 px-2 border btn-lg btn-block mb-0 text-left">
                                                <img src="assets/images/svgs/svg/search.svg" class="h-5 w-5"
                                                    alt=""><span class="ml-3 d-inline-block font-weight-bold">
                                                    Google</span> </a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Section-->
        @endsection
