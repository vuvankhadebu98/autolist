@extends('client.master')
@section('content')
</div>
<!--Topbar-->
<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg"
        style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white ">
                    <h1 class="">Personal Blog</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page"> Personal Blog</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Breadcrumb-->
<!--User Profile-->
<section class="sptb">
    <div class="container p-blog">
        <div class="row">
            <div class="col-lg-12">
                @foreach($User as $value)
                    <div class="card ">
                    <div class="card-body pattern-1">
                        <div class="wideget-user">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="wideget-user-desc text-center">
                                        <div class="wideget-user-img"> <img class="brround"
                                                src="../../public/img_user/{{$value->img}}" alt="img"> </div>
                                        <div class="user-wrap wideget-user-info"> <a href="#" class="text-white">
                                                <h4 class="font-weight-semibold">{{$value->firstname}} {{$value->lastname}}</h4>
                                            </a>
                                            <div class="wideget-user-rating"> <a href="#"><i
                                                        class="fa fa-star text-warning"></i></a> <a href="#"><i
                                                        class="fa fa-star text-warning"></i></a> <a href="#"><i
                                                        class="fa fa-star text-warning"></i></a> <a href="#"><i
                                                        class="fa fa-star text-warning"></i></a> <a href="#"><i
                                                        class="fa fa-star-o text-warning mr-1"></i></a> <span
                                                    class="text-white">5 (3876 Reviews)</span> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 text-center">
                                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                    <div class="addthis_inline_share_toolbox"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer py-0">
                        <div class="wideget-user-tab d-lg-flex mt-4 mt-xl-0">
                            <div class="tab-menu-heading">
                                <div class="tabs-menu1">
                                    <ul class="nav">
                                        <li><a href="#tab-5" class="active d-block" data-toggle="tab">Profile</a></li>
                                        <li><a href="#tab-6" data-toggle="tab" class="d-block">Ads Listing</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="d-sm-flex ml-auto mb-4 mb-xl-0 mt-3"> <label class="mr-2 mt-2 mb-sm-1">Sort
                                    By:</label>
                                <div class="selectgroup"> <label class="selectgroup-item mb-md-0"> <input type="radio"
                                            name="value" value="Price" class="selectgroup-input" checked=""> <span
                                            class="selectgroup-button">Price <i class="fa fa-sort ml-1"></i></span>
                                    </label> <label class="selectgroup-item mb-md-0"> <input type="radio" name="value"
                                            value="Popularity" class="selectgroup-input"> <span
                                            class="selectgroup-button">Popularity</span> </label> <label
                                        class="selectgroup-item mb-0"> <input type="radio" name="value" value="Latest"
                                            class="selectgroup-input"> <span class="selectgroup-button">Latest</span>
                                    </label> <label class="selectgroup-item mb-0"> <input type="radio" name="value"
                                            value="Rating" class="selectgroup-input"> <span
                                            class="selectgroup-button">Rating</span> </label> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb-0">
                    <div class="card-body p-0">
                        <div class="border-0">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-5">
                                    <div class="profile-log-switch p-5 card mb-0">
                                        <div class="media-heading">
                                            <h3 class="card-title mb-3 font-weight-bold">Personal Details</h3>
                                        </div>
                                        <ul class="usertab-list mb-0">
                                            <li><a href="#" class="text-dark"><span class="font-weight-semibold">Full
                                                        Name :</span> {{$value->firstname}} {{$value->lastname}} </a></li>
                                            <li><a href="#" class="text-dark"><span
                                                        class="font-weight-semibold">Location :</span>{{$Province}}</a>
                                            </li>
                                            <li><a href="#" class="text-dark"><span
                                                        class="font-weight-semibold">Languages :</span> English,
                                                    German,Vehiclenish.</a></li>
                                            <li><a href="#" class="text-dark"><span class="font-weight-semibold">Email
                                                        :</span>
                                                    {{$value->email}}</a></li>
                                            <li><a href="#" class="text-dark"><span class="font-weight-semibold">Phone
                                                        :</span> {{$value->phone}}</a></li>
                                        </ul>
                                        <div class="row profie-img">
                                            <div class="col-md-12">
                                                <div class="media-heading">
                                                    <h3 class="card-title mb-3 font-weight-bold">Biography</h3>
                                                </div>
                                                {{$value->detail_aboutme}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-6">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card overflow-hidden">
                                                @foreach($Poster as $value_poster)

                                                <div class="d-md-flex">
                                                    <div class="item-card9-img">
                                                        <div class="arrow-ribbon bg-primary">Sale</div>
                                                        <div class="item-card9-imgs"> <a class="link"
                                                                href="{{route('getDetail',['id'=>$value_poster->id])}}"></a> <img
                                                                src="../../public/img_user/{{$value->img}}" style="height: 280px;width: 600px;" alt="img"
                                                                class="cover-image"> </div>
                                                        <div class="item-card9-icons"> <a href="#"
                                                                class="item-card9-icons1 wishlist"> <i
                                                                    class="fa fa fa-heart-o"></i></a> </div>
                                                        <div class="item-overly-trans">
                                                            <div class="rating-stars"> <input type="number"
                                                                    readonly="readonly" class="rating-value star"
                                                                    name="rating-stars-value" value="3">
                                                                <div class="rating-stars-container">
                                                                    <div class="rating-star sm is--active"> <i
                                                                            class="fa fa-star"></i> </div>
                                                                    <div class="rating-star sm is--active"> <i
                                                                            class="fa fa-star"></i> </div>
                                                                    <div class="rating-star sm is--active"> <i
                                                                            class="fa fa-star"></i> </div>
                                                                    <div class="rating-star sm"> <i
                                                                            class="fa fa-star"></i> </div>
                                                                    <div class="rating-star sm"> <i
                                                                            class="fa fa-star"></i> </div>
                                                                </div>
                                                            </div> <span><a href="{{route('getDetail',['id'=>$value_poster->id])}}"
                                                                    class="bg-gray">Used</a></span>
                                                        </div>
                                                    </div>
                                                    <div class="card border-0 mb-0">
                                                        <div class="card-body ">
                                                            <div class="item-card9"> <a href="{{route('getDetail',['id'=>$value_poster->id])}}"
                                                                    class="text-dark">
                                                                    <h4 class="font-weight-semibold mt-1">
                                                                        {{$value_poster->title}}</h4>
                                                                </a>
                                                                <div class="item-card9-desc mb-2"> <a href="#"
                                                                        class="mr-4"><span class=""><i
                                                                                class="fa fa-map-marker text-muted mr-1"></i>
                                                                            {{$Province}}</span></a> <a href="#"
                                                                        class="mr-4"><span class=""><i
                                                                                class="fa fa-calendar-o text-muted mr-1"></i>
                                                                            {{$value_poster->created_at}}</span></a> </div>
                                                                <p class="mb-0 leading-tight">{{$value_poster->content}}</p>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer pt-4 pb-4">
                                                            <div class="item-card9-footer d-sm-flex">
                                                                <div class="item-card9-cost">
                                                                    <h4 class="text-dark font-weight-bold mb-0 mt-0">
                                                                        $748.00</h4>
                                                                </div>
                                                                <div class="ml-auto"> <a href="#" class="mr-4"
                                                                        title="Car type"><i
                                                                            class="fa fa-car  mr-1 text-muted"></i>
                                                                        Automatic</a> <a href="#" class="mr-4"
                                                                        title="Kilometrs"><i
                                                                            class="fa fa-road text-muted mr-1 "></i>4000Kms</a>
                                                                    <a href="#" class="" title="FuealType"><i
                                                                            class="fa fa-tachometer text-muted mr-1"></i>Electric</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection
