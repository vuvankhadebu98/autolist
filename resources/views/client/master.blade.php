<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <base href="{{asset('client')}}/">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="msapplication-TileColor" content="#162946">
    <meta name="theme-color" content="#e72a1a">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="Duplex VehiclesOptimized" content="320">
    {{-- seo --}}
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="robots" content="INDEX,FOLLOW"/>
    <link  rel="canonical" href="" />
    <meta name="author" content="">
    <link  rel="icon" type="image/x-icon" href="" />
    <title></title>

      {{-- <meta property="og:image" content="{{$image_og}}" /> --}}
      <meta property="og:site_name" content="http://localhost/autolist_2/public" />
      <meta property="og:description" content="" />
      <meta property="og:title" content="" />
      <meta property="og:url" content="" />
      <meta property="og:type" content="website" />
    {{-- seo --}}

    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"> <!-- Title -->
    <title>Danh sách ô tô, đại lý, cho thuê xe ô tô rao vặt Bootstrap Mẫu HTML sạch đáp ứng hiện đại</title>
    <!-- Bootstrap Css -->
    <link href="assets/plugins/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet"> <!-- Dashboard Css -->
    <link href="assets/css/style.css" rel="stylesheet"> <!-- Font-awesome  Css -->
    <link href="assets/css/icons.css" rel="stylesheet">
    <!--Select2 Plugin -->
    <link href="assets/plugins/select2/select2.min.css" rel="stylesheet"> <!-- Owl Theme css-->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet"> <!-- Custom scroll bar css-->
    <link href="assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet"> <!-- Switcher css -->
    <link href="assets/switcher/css/switcher.css" rel="stylesheet" id="switcher-css" type="text/css" media="all">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- COLOR-SKINS -->
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="assets/colorskins/color-skins/color13.css">
    <link rel="stylesheet" href="assets/colorskins/demo.css">

    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <!-- page login -->
    <link rel="stylesheet" type="text/css" href="assets/css/login.css">
    <!-- page forgot_password -->
    <link rel="stylesheet" type="text/css" href="assets/css/forgot_password.css">
    <!-- page profile -->
    <link rel="stylesheet" type="text/css" href="assets/css/profile.css">
    <!-- page edit_ads -->
    <link rel="stylesheet" type="text/css" href="assets/css/edit_ads.css">
    <!-- page ad_detail -->
    <link rel="stylesheet" type="text/css" href="assets/css/ad_detail.css">
    <!-- page master -->
    <link rel="stylesheet" type="text/css" href="assets/css/_master.css">
    <!-- page add_ads -->
    <link rel="stylesheet" type="text/css" href="assets/css/add_ads.css">
    <!-- page orders -->
    <link rel="stylesheet" type="text/css" href="assets/css/orders.css">

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- ckeditor -->
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v8.0" nonce="W1I8DWqZ"></script>

     <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    <script type="text/javascript">

        rt5u = document.all;
        oe8b = rt5u && !document.getElementById;
        izua = rt5u && document.getElementById;
        awd6 = !rt5u && document.getElementById;
        jaov = document.layers;

        function qn87(tpp8) {
            try {
                if (oe8b) alert("");
            } catch (e) {}
            if (tpp8 && tpp8.stopPropagation) tpp8.stopPropagation();
            return false;
        }

        function n870() {
            if (event.button == 2 || event.button == 3) qn87();
        }

        function ok6f(e) {
            return (e.which == 3) ? qn87() : true;
        }

        function l91e(l994) {
            for (a7vo = 0; a7vo < l994.images.length; a7vo++) {
                l994.images[a7vo].onmousedown = ok6f;
            }
            for (a7vo = 0; a7vo < l994.layers.length; a7vo++) {
                l91e(l994.layers[a7vo].document);
            }
        }

        function bowi() {
            if (oe8b) {
                for (a7vo = 0; a7vo < document.images.length; a7vo++) {
                    document.images[a7vo].onmousedown = n870;
                }
            } else if (jaov) {
                l91e(document);
            }
        }

        function qbof(e) {
            if ((izua && event && event.srcElement && event.srcElement.tagName == "IMG") || (awd6 && e && e.target && e
                    .target.tagName == "IMG")) {
                return qn87();
            }
        }
        if (izua || awd6) {
            document.oncontextmenu = qbof;
        } else if (oe8b || jaov) {
            window.onload = bowi;
        }

        function dw5i(e) {
            fjyz = e && e.srcElement && e.srcElement != null ? e.srcElement.tagName : "";
            if (fjyz != "INPUT" && fjyz != "TEXTAREA" && fjyz != "BUTTON") {
                return false;
            }
        }

        function k0rr() {
            return false
        }
        if (rt5u) {
            document.onselectstart = dw5i;
            document.ondragstart = k0rr;
        }
        if (document.addEventListener) {
            document.addEventListener('copy', function (e) {
                fjyz = e.target.tagName;
                if (fjyz != "INPUT" && fjyz != "TEXTAREA") {
                    e.preventDefault();
                }
            }, false);
            document.addEventListener('dragstart', function (e) {
                e.preventDefault();
            }, false);
        }

        function xil9(evt) {
            if (evt.preventDefault) {
                evt.preventDefault();
            } else {
                evt.keyCode = 37;
                evt.returnValue = false;
            }
        }
        var ko16 = 1;
        var ki8d = 2;
        var z84u = 4;
        var gs0m = new Array();
        gs0m.push(new Array(ki8d, 65));
        gs0m.push(new Array(ki8d, 67));
        gs0m.push(new Array(ki8d, 80));
        gs0m.push(new Array(ki8d, 83));
        gs0m.push(new Array(ki8d, 85));
        gs0m.push(new Array(ko16 | ki8d, 73));
        gs0m.push(new Array(ko16 | ki8d, 74));
        gs0m.push(new Array(ko16, 121));
        gs0m.push(new Array(0, 123));

        function tdes(evt) {
            evt = (evt) ? evt : ((event) ? event : null);
            if (evt) {
                var vn98 = evt.keyCode;
                if (!vn98 && evt.charCode) {
                    vn98 = String.fromCharCode(evt.charCode).toUpperCase().charCodeAt(0);
                }
                for (var g9fy = 0; g9fy < gs0m.length; g9fy++) {
                    if ((evt.shiftKey == ((gs0m[g9fy][0] & ko16) == ko16)) && ((evt.ctrlKey | evt.metaKey) == ((gs0m[
                            g9fy][0] & ki8d) == ki8d)) && (evt.altKey == ((gs0m[g9fy][0] & z84u) == z84u)) && (vn98 ==
                            gs0m[g9fy][1] || gs0m[g9fy][1] == 0)) {
                        xil9(evt);
                        break;
                    }
                }
            }
        }
        if (document.addEventListener) {
            document.addEventListener("keydown", tdes, true);
            document.addEventListener("keypress", tdes, true);
        } else if (document.attachEvent) {
            document.attachEvent("onkeydown", tdes);
        }
    </script>
    <meta http-equiv="imagetoolbar" content="no">
    <style type="text/css">
        input,textarea{-webkit-touch-callout:default;-webkit-user-select:auto;-khtml-user-select:auto;-moz-user-select:text;-ms-user-select:text;user-select:text} *{-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:-moz-none;-ms-user-select:none;user-select:none}
    </style>
    <style type="text/css" media="print">
        body{display:none}
    </style>
    <!--[if gte IE 5]><frame></frame><![endif]-->
    <style type="text/css">
        .jqstooltip {
            position: absolute;
            left: 0px;
            top: 0px;
            visibility: hidden;
            background: rgb(0, 0, 0) transparent;
            background-color: rgba(0, 0, 0, 0.6);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
            color: white;
            font: 10px arial, san serif;
            text-align: left;
            white-space: nowrap;
            padding: 5px;
            border: 1px solid white;
            z-index: 10000;
        }

        .jqsfield {
            color: white;
            font: 10px arial, san serif;
            text-align: left;
        }
    </style>
</head>

<body>
    <div id="background-gray"></div>
    <!-- Chưa làm -->
   {{--  <p>Đăng nhập google</p>
    <p>Đăng nhập facebook</p> --}}
    <div class="horizontalMenucontainer">
        <div class="switcher-wrapper ">
            <div class="demo_changer">
                <div class="demo-icon bg_dark"><i class="fa fa-cog fa-spin  text_primary"></i></div>
                <div class="form_holder right-sidebar">
                    <div class="row">
                        <div class="predefined_styles">
                            <div class="skin-theme-switcher">
                                <div class="skin-theme-switcher">
                                    <div class="swichermainleft">
                                        <h4>Color-Skins</h4> <a class="colorcode blackborder color1"
                                            data-theme="assets/colorskins/color-skins/color1.css"></a> <a
                                            class="colorcode blackborder color2"
                                            data-theme="assets/colorskins/color-skins/color2.css"></a> <a
                                            class="colorcode blackborder color3"
                                            data-theme="assets/colorskins/color-skins/color3.css"></a> <a
                                            class="colorcode blackborder color4"
                                            data-theme="assets/colorskins/color-skins/color4.css"></a> <a
                                            class="colorcode blackborder color5"
                                            data-theme="assets/colorskins/color-skins/color5.css"></a> <a
                                            class="colorcode blackborder color6"
                                            data-theme="assets/colorskins/color-skins/color6.css"></a> <a
                                            class="colorcode blackborder color7"
                                            data-theme="assets/colorskins/color-skins/color7.css"></a> <a
                                            class="colorcode blackborder color8"
                                            data-theme="assets/colorskins/color-skins/color8.css"></a> <a
                                            class="colorcode blackborder color9"
                                            data-theme="assets/colorskins/color-skins/color9.css"></a> <a
                                            class="colorcode blackborder color10"
                                            data-theme="assets/colorskins/color-skins/color10.css"></a>
                                        <h4>Gradient Color-Skins</h4> <a class="colorcode blackborder color11"
                                            data-theme="assets/colorskins/color-skins/color11.css"></a> <a
                                            class="colorcode blackborder color12"
                                            data-theme="assets/colorskins/color-skins/color12.css"></a> <a
                                            class="colorcode blackborder color13"
                                            data-theme="assets/colorskins/color-skins/color13.css"></a> <a
                                            class="colorcode blackborder color14"
                                            data-theme="assets/colorskins/color-skins/color14.css"></a> <a
                                            class="colorcode blackborder color15"
                                            data-theme="assets/colorskins/color-skins/color15.css"></a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="swichermainleft border-top  mt-2 text-center">
                                    <div class="p-3"> <a href="../../index.html"
                                            class="btn btn-warning btn-block mt-0">LTR VERSION</a> <a
                                            href="../../index1.html" class="btn btn-success btn-block">RTL VERSION</a>
                                    </div>
                                </div>
                                <div class="swichermainleft border-top  mt-2 text-center">
                                    <div class="p-3"> <a href="../../index.html"
                                            class="btn btn-primary btn-block mt-0">View Demo</a> <a
                                            href="https://themeforest.net/item/autolist-car-dealer-and-classifieds-html-template/24416231"
                                            class="btn btn-secondary btn-block">Buy Now</a> <a
                                            href="https://themeforest.net/user/sprukosoft/portfolio"
                                            class="btn btn-info btn-block">Our Portfolio</a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End Switcher -->
            @if(Session::has('messages'))
            <div class="div_messages" id="status_0">
                <strong class="messages_sussesses"> {{Session::get('messages')}}</strong>
            </div>
            @endif

        <!--Loader-->
        <div id="global-loader" style="display: none;"> <img src="assets/images/loader.svg" class="loader-img " alt="">
        </div>
        <!--Topbar-->
        <div class="header-main">
            <div class="top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-8 col-sm-4 col-7">
                            <div class="top-bar-left d-flex">
                                <div class="clearfix">
                                    <ul class="socials">
                                        <li> <a class="social-icon text-dark" href="#"><i
                                                    class="fab fa-facebook-f"></i></a> </li>
                                        <li> <a class="social-icon text-dark" href="#"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li> <a class="social-icon text-dark" href="#"><i
                                                    class="fa fa-linkedin"></i></a> </li>
                                        <li> <a class="social-icon text-dark" href="#"><i
                                                    class="fa fa-google-plus"></i></a> </li>
                                    </ul>
                                </div>
                                <div class="clearfix">
                                    <ul class="contact border-left">
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-sm-8 col-5">
                            <div class="top-bar-right">
                                <ul class="custom">
                                    <li>
                                        @if(Auth::check())
                                            <i class="fa fa-user mr-1"></i>
                                            <span>{{Auth::User()->lastname}}</span>
                                        @else
                                        <a href="{{route('getRegister')}}" class="text-dark"><i
                                                class="fa fa-user mr-1"></i>
                                            <span>Register</span></a> </li>
                                        @endif
                                    <li>
                                        @if(Auth::check())@else
                                            <a href="{{route('getLogin')}}" class="text-dark"><i
                                                class="fa fa-sign-in mr-1"></i>
                                            <span>Login</span></a>
                                        @endif
                                    </li>
                                    @if(Auth::check())
                                    <li class="dropdown"> <a class="text-dark" data-toggle="dropdown"><i
                                                class="fa fa-home mr-1"></i><span>Bảng Điều Khiển</span></a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a href="{{route('getProfile',['id'=>Auth::User()->id])}}"
                                                class="dropdown-item"> <i class="dropdown-icon icon icon-user"></i>Thông tin của tôi </a>
                                            <a class="dropdown-item" href="#"> <i
                                                    class="dropdown-icon icon icon-speech"></i>Hộp Thư Đến</a> <a
                                                class="dropdown-item" href="#"> <i
                                                    class="dropdown-icon icon icon-bell"></i>Thông Báo</a> <a
                                                href="mydash.html" class="dropdown-item"> <i
                                                    class="dropdown-icon  icon icon-settings"></i>Cài Đặt Tài Khoản</a>
                                            <a class="dropdown-item" href="{{route('logout')}}"> <i
                                                    class="dropdown-icon icon icon-power"></i>Đăng Xuất</a> </div>
                                    </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- Horizontal Header -->
            <div id="sticky-wrapper" class="sticky-wrapper" style="height: 34.2778px;">
                <div class="horizontal-header clearfix " style="width: 1639.11px;">
                    <div class="container"> <a id="horizontal-navtoggle" class="animated-arrow"><span></span></a> 
                        <span
                            class="smllogo"><img src="assets/images/brand/logo.png" width="120" alt=""></span> <a
                            href="tel:245-6325-3256" class="callusbtn"><i class="fa fa-phone"
                                aria-hidden="true"></i></a> </div>
                </div>
            </div> <!-- /Horizontal Header -->
            <!----------------------------------------------- phần nav -->
            <!-- Horizontal Main -->
            <div id="sticky-wrapper" class="sticky-wrapper" style="height: 88px;">
                <div class="horizontal-main bg-dark-transparent clearfix">
                    <div class="horizontal-mainwrapper container clearfix">
                        <div class="desktoplogo"> <a href="{{URL::to('/')}}"><img src="assets/images/brand/logo1.png"
                                    alt=""></a> </div>
                        <div class="desktoplogo-1"> <a href="{{URL::to('/')}}"><img src="assets/images/brand/logo.png"
                                    alt=""></a> </div>
                        <!--Nav-->
                        <nav class="horizontalMenu clearfix d-md-flex">
                            <div class="outsidebg"></div>
                            <ul class="horizontalMenu-list">

                                <li aria-haspopup="true">
                                    <span class="horizontalMenu-click">
                                        <i class="horizontalMenu-arrow fa fa-angle-down"></i>
                                    </span>
                                    <a href="{{route('getHome')}}">Trang Chủ
                                        <span class="fa fa-caret-down m-0"></span>
                                    </a>
                                </li>
                                <li aria-haspopup="true"><a href="{{route('getAbout')}}">Về Chúng Tôi </a></li>
                                <li aria-haspopup="true"><a href="widgets.html">Vật Dụng</a></li>
                                <li aria-haspopup="true"><span class="horizontalMenu-click"><i
                                            class="horizontalMenu-arrow fa fa-angle-down"></i></span><a href="#">Các Trang
                                        <span class="fa fa-caret-down m-0"></span></a>
                                    <div class="horizontal-megamenu clearfix">
                                        <div class="container">
                                            <div class="megamenu-content">
                                                <div class="row">
                                                    <ul class="col link-list">
                                                        <li class="title">Trang Danh Sách</li>
                                                        <li><a href="{{route('Car_List')}}">Danh Sách Ô Tô</a></li>
                                                        <li><a href="cars-list-right.html">Cars List Right</a></li>
                                                        <li><a href="cars-list-map.html">Cars Map list</a></li>
                                                        <li><a href="cars-list-map2.html">Cars Map list 02</a></li>
                                                        <li><a href="cars-list-map3.html">Cars Map style 03</a></li>
                                                        <li><a href="cars-auction-list.html">Cars Auction List</a></li>
                                                        <li><a href="cars-rental-list.html">Cars Rental List</a></li>
                                                        <li><a href="cars-parts-list.html">Cars Parts List</a></li>
                                                        <li><a href="cars-service-list.html">Cars Service List</a></li>
                                                        <li><a href="ad-list.html">Ad Listing</a></li>
                                                        <li><a href="ad-list-right.html">Ad Listing Right</a></li>
                                                    </ul>
                                                    <ul class="col link-list">
                                                        <li class="title">Trang Chi Tiết &amp; Bài Đăng</li>
                                                        <li><a href="cars.html">Cars Details Left</a></li>
                                                        <li><a href="cars-right.html">Cars Details Right </a></li>
                                                        <li><a href="{{route('Car-Detail-01',1)}}">Ô Tô Chi Tiết 01</a></li>
                                                        <li><a href="car-details2.html">Ô Tô Chi Tiết 02 </a></li>
                                                        <li><a href="car-details3.html">Ô Tô Chi Tiết 03 </a></li>
                                                        <li><a href="ad-details.html">Chi Tiết Quảng Cáo</a></li>
                                                        <li><a href="ad-details-right.html">Ad Details Right</a></li>
                                                        <li><a href="car-compare.html">Car Compare Detials</a></li>
                                                        <li><a href="cars-auction-detail.html">Car Auction Details</a>
                                                        </li>
                                                        <li><a href="cars-rental-detial.html">Car Rental Details</a>
                                                        </li>
                                                        <li><a href="cars-parts-detail.html">Car Parts Detail</a></li>
                                                        <li><a href="cars-services-detail.html">Car Services Details</a>
                                                        </li>
                                                    </ul>
                                                    <ul class="col link-list">
                                                        <li class="title">Trang Tổng Quan</li>
                                                        <li><a href="mydash.html">My Dashboard</a></li>
                                                        <li><a href="myads.html">Ads</a></li>
                                                        <li><a href="myfavorite.html">Favorite Ads</a></li>
                                                        <li><a href="managed.html">Managed Ads</a></li>
                                                        <li><a href="payments.html">Payments</a></li>
                                                        <li><a href="orders.html"> Orders</a></li>
                                                        <li><a href="settings.html"> Settings</a></li>
                                                        <li><a href="tips.html">Tips</a></li>
                                                        <li class="title mt-4">Các Trang Phần Tử Khác</li>
                                                        <li><a href="categories.html">Categories</a></li>
                                                        <li><a href="inovice.html">Invoice</a></li>
                                                        <li><a href="usersall.html">User Lists</a></li>
                                                        <li><a href="pricing.html">Pricing</a></li>
                                                    </ul>
                                                    <ul class="col link-list">
                                                        <li class="title">Trang Người Dùng</li>
                                                        <li><a href="underconstruction.html">Under Construction</a></li>
                                                        <li><a href="404.html">404</a></li>
                                                        <li><a href="register.html">Register</a></li>
                                                        <li><a href="login.html">Login</a></li>
                                                        <li><a href="login-2.html">Login 02</a></li>
                                                        <li><a href="forgot.html">Forgot Password</a></li>
                                                        <li><a href="lockscreen.html">Lock Screen</a></li>
                                                        <li class="title mt-4">Trang Hồ Sơ</li>
                                                        <li><a href="userprofile.html"> User Profile</a></li>
                                                        <li><a href="personal-blog.html"> Personal Blog</a></li>
                                                        <li class="title mt-4">Đăng Trang</li>
                                                        <li><a href="ad-posts.html">Ad Posts</a></li>
                                                        <li><a href="ad-posts2.html">Ad Posts2</a></li>
                                                    </ul>
                                                    <ul class="col link-list">
                                                       <li class="title">Trang Đầu Trang &amp; Chân Trong</li>
                                                       <li><a href="header-style1.html">Tiêu đề kiểu 01</a></li>
                                                       <li><a href="header-style2.html">Tiêu đề kiểu 02</a></li>
                                                       <li><a href="header-style3.html">Tiêu đề kiểu 03</a></li>
                                                       <li><a href="header-style4.html">Tiêu đề kiểu 04</a></li>
                                                       <li><a href="header-style5.html">Tiêu đề kiểu 05</a></li>
                                                       <li><a href="footer-style.html">Kiểu chân trang 01</a></li>
                                                       <li><a href="footer-style2.html">Kiểu chân trang 02</a></li>
                                                       <li><a href="footer-style3.html">Kiểu chân trang 03</a></li>
                                                       <li><a href="footer-style4.html">Kiểu chân trang 04</a></li>
                                                       <li class="title mt-4">Các trang phần tử khác</li>
                                                       <li><a href="typography.html">Kiểu chữ</a></li>
                                                       <li><a href="faq.html">FAQ</a></li>
                                                       <li><a href="testimonial.html">Chứng thực</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li aria-haspopup="true"><span class="horizontalMenu-click"><i
                                            class="horizontalMenu-arrow fa fa-angle-down"></i></span><a href="#">Ô Tô
                                        <span class="fa fa-caret-down m-0"></span></a>
                                    <div class="horizontal-megamenu clearfix car-brands">
                                        <div class="container">
                                            <div class="megamenu-content overflow-hidden ">
                                                <div class="row">
                                                    <ul class="col-lg-6 link-list">
                                                        <li class="title">Hình Dạng Thân Xe</li>
                                                        <li>
                                                            <div class="row">
                                                                <div class="col-xl-4 col-md-6 text-center"> <a
                                                                        href="#"><img
                                                                            src="assets/images/car-body/body-shapes/1.png"
                                                                            alt=""></a>
                                                                    <h4 class="mb-0 font-weight-bold text-uppercase">
                                                                        Có Thể chuyển đổi</h4>
                                                                </div>
                                                                <div class="col-xl-4 col-md-6 text-center"> <a
                                                                        href="#"><img
                                                                            src="assets/images/car-body/body-shapes/2.png"
                                                                            alt=""></a>
                                                                    <h4 class="mb-0 font-weight-bold text-uppercase">
                                                                        Cắt Nhở Ra</h4>
                                                                </div>
                                                                <div class="col-xl-4 col-md-6 text-center"> <a
                                                                        href="#"><img
                                                                            src="assets/images/car-body/body-shapes/3.png"
                                                                            alt=""></a>
                                                                    <h4 class="mb-0 font-weight-bold text-uppercase">
                                                                        Ringer Ace</h4>
                                                                </div>
                                                                <div class="col-xl-4 col-md-6 text-center"> <a
                                                                        href="#"><img
                                                                            src="assets/images/car-body/body-shapes/4.png"
                                                                            alt=""></a>
                                                                    <h4 class="mb-0 font-weight-bold text-uppercase">
                                                                        Van/minivan</h4>
                                                                </div>
                                                                <div class="col-xl-4 col-md-6 text-center"> <a
                                                                        href="#"><img
                                                                            src="assets/images/car-body/body-shapes/5.png"
                                                                            alt=""></a>
                                                                    <h4 class="mb-0 font-weight-bold text-uppercase">
                                                                        Xe Tải</h4>
                                                                </div>
                                                                <div class="col-xl-4 col-md-6 text-center"> <a
                                                                        href="#"><img
                                                                            src="assets/images/car-body/body-shapes/6.png"
                                                                            alt=""></a>
                                                                    <div class="clearfix"></div>
                                                                    <h4 class="mb-0 font-weight-bold text-uppercase">
                                                                        Hỗn Hợp</h4>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <ul class="col-lg-6 link-list mt-3 mt-md-0 top-brands">
                                                        <li class="title">Thương Hiệu Xe Hơi Hàng Đầu</li>
                                                        <li class="mt-4">
                                                            <div class="row">
                                                                <div class="col-lg col-sm-6">
                                                                    <div class="card border-2 box-shadow2"> <a href="#"
                                                                            class="cat-img mx-auto text-center bg-transparent"><img
                                                                                src="assets/images/products/categories/28.png"
                                                                                class="w-80" alt=""></a> </div>
                                                                </div>
                                                                <div class="col-lg col-sm-6">
                                                                    <div class="card border-2 mx-auto box-shadow2"> <a
                                                                            href="#"
                                                                            class="cat-img mx-auto text-center bg-transparent"><img
                                                                                src="assets/images/products/categories/53.png"
                                                                                class="w-80" alt=""></a> </div>
                                                                </div>
                                                                <div class="col-lg col-sm-6">
                                                                    <div class="card border-2 mx-auto box-shadow2"> <a
                                                                            href="#"
                                                                            class="cat-img mx-auto text-center bg-transparent"><img
                                                                                src="assets/images/products/categories/42.png"
                                                                                class="w-80" alt=""></a> </div>
                                                                </div>
                                                                <div class="col-lg col-sm-6">
                                                                    <div class="card border-2 mx-auto box-shadow2"> <a
                                                                            href="#"
                                                                            class="cat-img mx-auto text-center bg-transparent"><img
                                                                                src="assets/images/products/categories/34.png"
                                                                                class="w-80" alt=""></a> </div>
                                                                </div>
                                                                <div class="col-lg col-sm-12">
                                                                    <div class="card border-2 mx-auto box-shadow2"> <a
                                                                            href="#"
                                                                            class="cat-img mx-auto text-center bg-transparent"><img
                                                                                src="assets/images/products/categories/30.png"
                                                                                class="w-80" alt=""></a> </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg col-sm-6">
                                                                    <div class="card border-2 d-block box-shadow2"> <a
                                                                            href="#"
                                                                            class="cat-img mx-auto text-center bg-transparent"><img
                                                                                src="assets/images/products/categories/51.png"
                                                                                class="w-80" alt=""></a> </div>
                                                                </div>
                                                                <div class="col-lg col-sm-6">
                                                                    <div class="card border-2 mx-auto box-shadow2"> <a
                                                                            href="#"
                                                                            class="cat-img mx-auto text-center bg-transparent"><img
                                                                                src="assets/images/products/categories/26.png"
                                                                                class="w-80" alt=""></a> </div>
                                                                </div>
                                                                <div class="col-lg col-sm-6">
                                                                    <div class="card border-2 mx-auto box-shadow2"> <a
                                                                            href="#"
                                                                            class="cat-img mx-auto text-center bg-transparent"><img
                                                                                src="assets/images/products/categories/31.png"
                                                                                class="w-80" alt=""></a> </div>
                                                                </div>
                                                                <div class="col-lg col-sm-6">
                                                                    <div class="card border-2 mx-auto box-shadow2"> <a
                                                                            href="#"
                                                                            class="cat-img mx-auto text-center bg-transparent"><img
                                                                                src="assets/images/products/categories/01.png"
                                                                                class="w-80" alt=""></a> </div>
                                                                </div>
                                                                <div class="col-lg col-sm-12">
                                                                    <div class="card border-2 mx-auto box-shadow2"> <a
                                                                            href="#"
                                                                            class="cat-img mx-auto text-center bg-transparent"><img
                                                                                src="assets/images/products/categories/41.png"
                                                                                class="w-80" alt=""></a> </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li aria-haspopup="true"><span class="horizontalMenu-click"><i
                                            class="horizontalMenu-arrow fa fa-angle-down"></i></span><a href="#">Blog
                                        <span class="fa fa-caret-down m-0"></span></a>
                                    <ul class="sub-menu">
                                        <li aria-haspopup="true"><span class="horizontalMenu-click02"><i
                                                    class="horizontalMenu-arrow fa fa-angle-down"></i></span><a
                                                href="#">Lưới Blog
                                                <i class="fa fa-angle-right float-right mt-1 d-none d-lg-block"></i></a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="blog-grid.html">Blog Bên Trái</a>
                                                </li>
                                                <li aria-haspopup="true"><a href="blog-grid-right.html">Blog Bên Phải</a></li>
                                                <li aria-haspopup="true"><a href="blog-grid-center.html">Blog Ở Giữa</a></li>
                                            </ul>
                                        </li>
                                        <li aria-haspopup="true"><span class="horizontalMenu-click02"><i
                                                    class="horizontalMenu-arrow fa fa-angle-down"></i></span><a
                                                href="#">Danh Sách Blog
                                                <i class="fa fa-angle-right float-right mt-1 d-none d-lg-block"></i></a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="#">Blog Bên Trái</a>
                                                </li>
                                                <li aria-haspopup="true"><a href="{{route('listBlog')}}">Blog Bên Phải</a></li>
                                                <li aria-haspopup="true"><a href="blog-list-center.html">Blog Ở Giữa</a></li>
                                            </ul>
                                        </li>
                                        <li aria-haspopup="true"><span class="horizontalMenu-click02"><i
                                                    class="horizontalMenu-arrow fa fa-angle-down"></i></span><a
                                                href="#">Chi Tiết Blog <i
                                                    class="fa fa-angle-right float-right mt-1 d-none d-lg-block"></i></a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="blog-details.html">Chi Tiết Blog Còn Lại</a></li>
                                                <li aria-haspopup="true"><a href="blog-details-right.html">Chi Tiết Quyền Blog</a></li>
                                                <li aria-haspopup="true"><a href="blog-details-center.html">Chi Tiết Trung Tâm Blog</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li aria-haspopup="true"><a href="{{route('contact')}}">Liên Hệ Chúng Tôi <span
                                            class="horizontalarrow"></span></a></li>
                                <li aria-haspopup="true" class="d-lg-none mt-5 pb-5 mt-lg-0">
                                    <span>
                                        <a class="postAds btn btn-orange" href="{{route('addAds')}}">
                                            <i class="fa fa-car text-white mr-1"></i> 
                                        </a>
                                    </span>
                                </li>
                            </ul>
                            <?php
                                $id_user = Auth::User();
                                if ($id_user != null) 
                                {   
                            ?>  
                            <ul class="mb-0">
                                <li aria-haspopup="true" class="mt-5 d-none d-lg-block ">
                                    <span>
                                        <a class="postAds btn btn-green ad-post" href="{{route('addAds')}}">
                                            <i class="fa fa-car text-white mr-1"></i> Bán ô tô của bạn
                                        </a>
                                    </span>
                                </li>
                            </ul>
                            <?php
                                }else
                                {
                            ?>
                            <ul class="mb-0">
                                <li aria-haspopup="true" class="mt-5 d-none d-lg-block "> 
                                    <span>
                                        <a class="postAds btn btn-green ad-post" href="{{route('getLogin')}}">
                                            <i class="fa fa-car text-white mr-1"></i> Bán ô tô của bạn
                                        </a>
                                    </span>
                                </li>
                            </ul>
                            <?php
                                }
                            ?>
                            <!-- 'check'=>$Request->check_, -->
                        </nav>
                        <!--Nav-->
                    </div>
                </div>
            </div> <!-- /Horizontal Main -->
            <!----------------------------------------------------->
            @yield('content')
            @if(Auth::check())
                <input type="hidden" id="id_user" name="id_user" value="{{Auth::User()->id}}">
            @endif

            <!-- Newsletter-->
            <section class="sptb2 bg-white border-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-xl-6 col-md-12">
                            <div class="sub-newsletter">
                                <h3 class="mb-2"><i class="fa fa-paper-plane-o mr-2"></i> Subscribe To Our Newsletter
                                </h3>
                                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    tempor</p>
                            </div>
                        </div>
                        <div class="col-lg-5 col-xl-6 col-md-12">
                            <div class="input-group sub-input mt-1"> <input type="text" class="form-control input-lg "
                                    placeholder="Enter your Email">
                                <div class="input-group-append "> <button type="button"
                                        class="btn btn-primary btn-lg br-tr-3  br-br-3"> Subscribe </button> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/Newsletter-->
            <!--latest Posts-->
            <section class="sptb2 border-top">
                <div class="container">
                    <h6 class="fs-18 mb-4">Latest Posts</h6>
                    <hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
                    <div class="row">
                        <div class="col-md-12 col-lg-4">
                            <div class="d-flex mt-0 mb-5 mb-lg-0 border bg-white p-4 box-shadow2"> <img
                                    class="w-8 h-8 mr-4" src="assets/images/products/6.png" alt="img">
                                <div class="media-body">
                                    <h4 class="mt-0 mb-1 fs-16"><a class="text-body" href="#">Buy a
                                            CrusaderRecusandae</a>
                                    </h4> <span class="fs-12 text-muted"><i class="fa fa-calendar"></i> 13th May
                                        2019</span>
                                    <div class="h6 mb-0 mt-1 font-weight-normal"><span
                                            class="font-weight-semibold">Price:</span>
                                        $128 <del>$218</del></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="d-flex mt-0 mb-5 mb-lg-0 border bg-white p-4 box-shadow2"> <img
                                    class="w-8 h-8 mr-4" src="assets/images/products/4.png" alt="img">
                                <div class="media-body">
                                    <h4 class="mt-0 mb-1 fs-16"><a class="text-body" href="#">Best New Car</a></h4>
                                    <span class="fs-12 text-muted"><i class="fa fa-calendar"></i> 20th Jun 2019</span>
                                    <div class="h6 mb-0 mt-1 font-weight-normal"><span
                                            class="font-weight-semibold">Price:</span>
                                        $245 <del>$354</del></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="d-flex mt-0 mb-0 border bg-white p-4 box-shadow2"> <img class="w-8 h-8 mr-4"
                                    src="assets/images/products/2.png" alt="img">
                                <div class="media-body">
                                    <h4 class="mt-0 mb-1 fs-16"><a class="text-body" href="#">Fuel Effeciency Car</a>
                                    </h4>
                                    <span class="fs-12 text-muted"><i class="fa fa-calendar"></i> 14th Aug 2019</span>
                                    <div class="h6 mb-0 mt-1 font-weight-normal"><span
                                            class="font-weight-semibold">Price:</span>
                                        $214 <del>$562</del></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--latest Posts-->
            <!--Footer Section-->
            <section>
                <footer class="bg-dark-purple text-white">
                    <div class="footer-main">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-2 col-md-12">
                                    <h6>Resources</h6>
                                    <hr class="deep-purple text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
                                    <ul class="list-unstyled mb-0">
                                        <li><a href="javascript:;">Our Team</a></li>
                                        <li><a href="javascript:;">Contact US</a></li>
                                        <li><a href="javascript:;">About</a></li>
                                        <li><a href="javascript:;">CrusaderRecusandae</a></li>
                                        <li><a href="javascript:;">Blog</a></li>
                                        <li><a href="javascript:;">Terms and Conditions</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-12">
                                    <h6>Contact</h6>
                                    <hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
                                    <ul class="list-unstyled mb-0">
                                        <li> <a href="#"><i class="fa fa-car mr-3 text-primary"></i> New York, NY 10012,
                                                US</a> </li>
                                        <li> <a href="#"><i class="fa fa-envelope mr-3 text-primary"></i>
                                                info12323@example.com</a></li>
                                        <li> <a href="#"><i class="fa fa-phone mr-3 text-primary"></i> + 01 234 567
                                                88</a>
                                        </li>
                                        <li> <a href="#"><i class="fa fa-print mr-3 text-primary"></i> + 01 234 567
                                                89</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <h6>Download App</h6>
                                    <hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
                                    <div class=""> <a class="btn bg-store fs-16" href=""><i
                                                class="fa fa-apple mr-2"></i>
                                            App Store</a> <a class="btn bg-store fs-16" href=""><i
                                                class="fa fa-android mr-2"></i> Google Pay</a> </div>
                                    <h6 class="mb-0 mt-5">Payments</h6>
                                    <hr class="deep-purple  text-primary accent-2 mb-2 mt-3 d-inline-block mx-auto">
                                    <div class="clearfix"></div>
                                    <ul class="footer-payments">
                                        <li class="pl-0"><a href="javascript:;"><i class="fa fa-cc-amex"
                                                    aria-hidden="true"></i></a></li>
                                        <li><a href="javascript:;"><i class="fa fa-cc-visa" aria-hidden="true"></i></a>
                                        </li>
                                        <li><a href="javascript:;"><i class="fa fa-credit-card-alt"
                                                    aria-hidden="true"></i></a></li>
                                        <li><a href="javascript:;"><i class="fa fa-cc-mastercard"
                                                    aria-hidden="true"></i></a></li>
                                        <li><a href="javascript:;"><i class="fa fa-cc-paypal"
                                                    aria-hidden="true"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-md-12">
                                    <h6>Subscribe</h6>
                                    <hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
                                    <div class="clearfix"></div>
                                    <div class="input-group w-100"> <input type="text"
                                            class="form-control br-tl-3  br-bl-3 " placeholder="Email">
                                        <div class="input-group-append "> <button type="button"
                                                class="btn btn-primary br-tr-3  br-br-3"> Subscribe </button> </div>
                                    </div>
                                    <h6 class="mt-5 mb-3">Follow Us</h6>
                                    <hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
                                    <ul class="list-unstyled list-inline">
                                        <li class="list-inline-item"> <a
                                                class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
                                                <i class="fab fa-facebook-f bg-facebook"></i> </a> </li>
                                        <li class="list-inline-item"> <a
                                                class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
                                                <i class="fa fa-twitter bg-info"></i> </a> </li>
                                        <li class="list-inline-item"> <a
                                                class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
                                                <i class="fa fa-google-plus bg-danger"></i> </a> </li>
                                        <li class="list-inline-item"> <a
                                                class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
                                                <i class="fa fa-linkedin bg-linkedin"></i> </a> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-dark-purple text-white p-0">
                        <div class="container">
                            <div class="row d-flex">
                                <div class="col-lg-12 col-sm-12 mt-3 mb-3 text-center "> Copyright © 2019 <a href="#"
                                        class="fs-14 text-primary">Autolist</a>. Designed by <a href="#"
                                        class="fs-14 text-primary">Spruko</a> All rights reserved. </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </section>
            <!--Footer Section-->
            <!-- Back to top -->
            <a href="#top" id="back-to-top"><i class="fa fa-rocket"></i></a> <!-- JQuery js-->
            <script src="assets/js/vendors/jquery-3.2.1.min.js"></script>
            <script src="assets/plugins/bootstrap-4.3.1-dist/js/popper.min.js"></script>
            <script src="assets/plugins/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
            <script src="assets/js/vendors/jquery.sparkline.min.js"></script>
            <script src="assets/js/vendors/circle-progress.min.js"></script>
            <script src="assets/plugins/rating/jquery.rating-stars.js"></script>
            <script src="assets/plugins/counters/counterup.min.js"></script>
            <script src="assets/plugins/counters/waypoints.min.js"></script>
            <script src="assets/plugins/counters/numeric-counter.js"></script>
            <script src="assets/plugins/owl-carousel/owl.carousel.js"></script>
            <script src="assets/plugins/horizontal-menu/horizontal.js"></script>
            <script src="assets/js/jquery.touchSwipe.min.js"></script>
            <script src="assets/plugins/select2/select2.full.min.js"></script>
            <script src="assets/js/select2.js"></script>
            <script src="assets/js/sticky.js"></script>
            <script src="assets/plugins/cookie/jquery.ihavecookies.js"></script>
            <script src="assets/plugins/cookie/cookie.js"></script>
            <script src="assets/plugins/jquery-uislider/jquery-ui.js"></script>
            <script src="assets/plugins/jquery-uislider/jquery.ui.touch-punch.min.js"></script>
            <script src="assets/js/jquery.showmore.js"></script>
            <script src="assets/js/showmore.js"></script>
            <script src="assets/plugins/scroll-bar/jquery.mCustomScrollbar.js"></script>
            <script src="assets/js/swipe.js"></script>
            <script src="assets/switcher/js/switcher.js"></script>
            <script src="assets/js/owl-carousel.js"></script>
            <script src="assets/js/custom.js"></script>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
            <div role="log" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
        </div>
        <!-- js page edit_ads -->
        <script type="text/javascript" src="assets/js/edit_ads.js"></script>
        <!-- js page ad_detail -->
        <script type="text/javascript" src="assets/js/ad_detail.js"></script>
        <!-- js page myads -->
        <script type="text/javascript" src="assets/js/myads.js"></script>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f24ca995af4c9d1"></script>
        <!-- js page add_ads -->
        <script type="text/javascript" src="assets/js/_add_ads.js"></script>
        <!-- js page master -->
        <script type="text/javascript" src="assets/js/master.js"></script>
        <script type="text/javascript" src="assets/js/_master.js"></script>
        <!-- js page index -->
        <script type="text/javascript" src="assets/js/index.js"></script>
        <script type="text/javascript" src="assets/js/filter.js"></script>
        <!-- js page list_ads_search -->
        <script type="text/javascript" src="assets/js/list_ads_search.js"></script>
        <!-- js page orders -->
        <script type="text/javascript" src="assets/js/orders.js"></script>

        @yield('js')

        <!-- js page select_search -->
        <script type="text/javascript" src="assets/js/select_search.js"></script>

        <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
</body>
</html>
