
@extends('client.master')
@section('content')
<section class="sptb">
   <?php use App\Model\Comment; ?>
   <?php use App\Model\Blog; ?>
   <div class="container">
      <div class="row">
         <!--Left Side Content--> 
         <div class="col-xl-4 col-lg-4 col-md-12">
            <div class="card">
               <div class="card-body">
                  <div class="input-group">
                     <input type="text" class="form-control br-tl-3  br-bl-3" placeholder="Search"> 
                     <div class="input-group-append "> <button type="button" class="btn btn-primary br-tr-3  br-br-3"> Search </button> </div>
                  </div>
               </div>
            </div>
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Categories</h3>
               </div>
               <div class="card-body p-0">
                  <div class="list-catergory">
                     <div class="item-list">
                        @foreach($category_blog as $blog)
                        <ul class="list-group mb-0">
                           <li class="list-group-item"> <a href="{{route('listBlogID',$blog->id)}}" class="text-dark"> <i class="fa fa-car bg-primary text-primary"></i>{{$blog->name}}<span class="badgetext badge badge-pill badge-light mb-0 mt-1 mt-1"><?php $blogCount = Blog::blogCount($blog->id); echo $blogCount; ?> </span> </a> </li>
                        </ul>
                        @endforeach
                     </div>
                  </div>
                        
               </div>
            </div>
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Popular Tags</h3>
               </div>
               <div class="card-body">
                  <div class="product-tags clearfix">
                      @foreach($tag_blog_id as $tag_blog_id)
                     <ul class="list-unstyled mb-0">
                        <li><a href="{{route('detailBlog',$tag_blog_id->id)}}">{{$tag_blog_id->name}}</a></li>
                     </ul>
                     @endforeach
                  </div>
               </div>
            </div>
            <div class="card mb-0">
               <div class="card-header">
                  <h3 class="card-title">Blog Authors</h3>
               </div>
               <div class="card-body p-0">
                  <ul class="vertical-scroll" style="overflow-y: hidden; height: 326px;">
                     @foreach($author_blog as $author_blog)
                     <li style="" class="item2">
                        <div class="footerimg d-flex mt-0 mb-0">
                           <div class="d-flex footerimg-l mb-0"> <img src="img_user/{{$author_blog->img}}" alt="image" class="avatar brround  mr-2"> <a href="#" class="time-title p-0 leading-Automatic mt-2">{{ $author_blog->firstname}} {{-- {{$author_blog->lastname}} --}} <i class="icon icon-check text-success fs-12 ml-1" data-toggle="tooltip" data-placement="top" title="" data-original-title="verified"></i></a> </div>
                           <div class="mt-2 footerimg-r ml-auto"> <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Articles"><span class="text-muted mr-2"><i class="fa fa-comment-o"></i><?php $commentCount = Comment::commentCount($author_blog->id); echo $commentCount; ?></span></a> <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Likes"><span class="text-muted"><i class="fa fa-thumbs-o-up"></i> 32</span></a> </div>
                        </div>
                     </li>
                     @endforeach
                  </ul>
               </div>
            </div>
         </div>
         <!--/Left Side Content--> 
         <div class="col-xl-8 col-lg-8 col-md-12">
            <!--Lists--> 
            <div class="row">
            	@foreach($list_blog as $lb )
               <div class="col-xl-12 col-lg-12 col-md-12">
                  <div class="card overflow-hidden">
                     {{-- @if() --}}
                     <div class="ribbon ribbon-top-left text-warning"><span class="bg-warning">Offer</span></div>
                     <div class="row no-gutters blog-list">
                        <div class="col-xl-4 col-lg-12 col-md-12">
                           <div class="item7-card-img ">
                              <a href="ad-details.html"></a> <img src="img_blog/{{$lb->img}}" alt="img" class="cover-image"> 
                           </div>
                        </div>
                        <div class="col-xl-8 col-lg-12 col-md-12">
                           <div class="card-body">
                              <div class="item7-card-desc d-flex mb-1">
                                 <a href="#"><i class="fa fa-calendar-o text-muted mr-2"></i>{{$lb->created_at}}</a> <a href="#"><i class="fa fa-user text-muted mr-2"></i>{{$lb->User->name}}</a> 
                                 <div class="ml-auto"> <a href="#"><i class="fa fa-comment-o text-muted mr-2"></i><?php $commentCount = Comment::commentCount($lb->id); echo $commentCount; ?> Comments</a> </div>
                              </div>
                              <a href="#" class="text-dark">
                                 <h4 class="font-weight-semibold mt-3 mb-1">{{$lb->name}}</h4>
                              </a>
                              <p class="mb-1">{!!$lb->description!!} </p>
                              <a href="{{route('detailBlog',$lb->id)}}" class="btn btn-primary btn-sm mt-4">Read More</a> 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               @endforeach
            </div>
            
           {{--  <div class="center-block text-center">
               <ul class="pagination mb-0">
                  <li class="page-item page-prev disabled"> <a class="page-link" href="#" tabindex="-1">Prev</a> </li>
                  <li class="page-item active"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item page-next"> <a class="page-link" href="#">Next</a> </li>
               </ul>
            </div> --}}
            {{ $list_blog->links() }}
         </div>
         <!--/Lists--> 
      </div>
   </div>
</section>
@endsection