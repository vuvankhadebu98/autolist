@extends('client.master')
@section('content')
<section class="sptb">
   <?php use App\Model\Comment; ?>
   <?php use App\Model\Blog; ?>
   <div class="container">
      <div class="row">
         <!--Left Side Content--> 
         <div class="col-xl-4 col-lg-4 col-md-12">
            <div class="card">
               <div class="card-body">
                  <div class="input-group">
                     <input type="text" class="form-control br-tl-3  br-bl-3" placeholder="Search"> 
                     <div class="input-group-append "> <button type="button" class="btn btn-primary br-tr-3  br-br-3"> Search </button> </div>
                  </div>
               </div>
            </div>
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Categories</h3>
               </div>
               <div class="card-body p-0">
                  <div class="list-catergory">
                     <div class="item-list">
                        @foreach($category_blog as $blog)
                        <ul class="list-group mb-0">
                           <li class="list-group-item"> <a href="{{route('listBlogID',$blog->id)}}" class="text-dark"> <i class="fa fa-car bg-primary text-primary"></i>{{$blog->name}}<span class="badgetext badge badge-pill badge-light mb-0 mt-1 mt-1"><?php $blogCount = Blog::blogCount($blog->id); echo $blogCount; ?> </span> </a> </li>
                        </ul>
                        @endforeach
                     </div>
                  </div>
                        
               </div>
            </div>
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Popular Tags</h3>
               </div>
               <div class="card-body">
                  <div class="product-tags clearfix">
                      @foreach($tag_blog_id as $tag_blog_id)
                     <ul class="list-unstyled mb-0">
                        <li><a href="{{route('detailBlog',$tag_blog_id->id)}}">{{$tag_blog_id->name}}</a></li>
                     </ul>
                     @endforeach
                  </div>
               </div>
            </div>
            <div class="card mb-0">
               <div class="card-header">
                  <h3 class="card-title">Blog Authors</h3>
               </div>
               <div class="card-body p-0">
                  <ul class="vertical-scroll" style="overflow-y: hidden; height: 326px;">
                     @foreach($author_blog as $author_blog)
                     <li style="" class="item2">
                        <div class="footerimg d-flex mt-0 mb-0">
                           <div class="d-flex footerimg-l mb-0"> <img src="img_user/{{$author_blog->img}}" alt="image" class="avatar brround  mr-2"> <a href="#" class="time-title p-0 leading-Automatic mt-2">{{ $author_blog->name}} <i class="icon icon-check text-success fs-12 ml-1" data-toggle="tooltip" data-placement="top" title="" data-original-title="verified"></i></a> </div>
                           <div class="mt-2 footerimg-r ml-auto"> <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Articles"><span class="text-muted mr-2"><i class="fa fa-comment-o"></i>{{-- {{$author_blog->Comment->count()}} --}}12</span></a> <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Likes"><span class="text-muted"><i class="fa fa-thumbs-o-up"></i> 32</span></a> </div>
                        </div>
                     </li>
                     @endforeach
                  </ul>
               </div>
            </div>
         </div>
         <!--/Left Side Content--> <!--Lists--> 
         <div class="col-xl-8 col-lg-8 col-md-12">
            <div class="card">
               <div class="card-body">
                  <div class="item7-card-img">
                     <img src="img_blog/{{$detail_blog->img}}" alt="img" class="w-100"> 
                     <div class="item7-card-text"> <span class="badge badge-info fs-16"> $876</span> </div>
                  </div>
                  <div class="item7-card-desc d-flex mb-2 mt-3">
                     <a href="#"><i class="fa fa-calendar-o text-muted mr-2"></i>{{$detail_blog->created_at}}</a> <a href="#"><i class="fa fa-user text-muted mr-2"></i>{{$detail_blog->user->firstname}} {{$detail_blog->user->lastname}}</a> 
                     <div class="ml-auto"> <a href="#"><i class="fa fa-comment-o text-muted mr-2"></i> <?php $commentCount = Comment::commentCount($detail_blog->id); echo $commentCount; ?> Comments</a> </div>
                  </div>
                  <a href="#" class="text-dark">
                     <h2 class="font-weight-semibold">{{$detail_blog->name}}</h2>
                  </a>
                  <p>{!!$detail_blog->description!!} </p>
               </div>
            </div>
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Comments</h3>
               </div>
             @foreach($comment_blog as $comment_blog)
               <div class="card-body p-0">
                  <div class="media mt-0 p-5">
                     <div class="d-flex mr-3"> <a href="#"><img class="media-object brround" alt="64x64" src="img_user/{{-- {{$comment_blog->User->img}} --}}"> </a></div>
                    
                     <div class="media-body">
                        <h5 class="mt-0 mb-1 font-weight-semibold">{{$comment_blog->user->firstname}} {{$comment_blog->user->lastname}}<span class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" title="" data-original-title="verified"><i class="fa fa-check-circle-o text-success"></i></span> <span class="fs-14 ml-2"> 4.5 <i class="fa fa-star text-yellow"></i></span> </h5>
                        <small class="text-muted"><i class="fa fa-calendar"></i><i class=" ml-3 fa fa-clock-o"></i> {{$comment_blog->user->created_at }}<i class=" ml-3 fa fa-map-marker"></i> Việt Nam</small> 
                        <p class="font-13  mb-2 mt-2">{{$comment_blog->content}} </p>
                        {{-- <a href="#" class="mr-2"><span class="badge badge-primary">Helpful</span></a>
                        @if(Auth::check())
                         <a href="" class="mr-2" data-toggle="modal" data-target="#Comment"><span class="">Comment</span></a>
                          <a href="" class="mr-2" data-toggle="modal" data-target="#report"><span class="">Report</span></a> 
                        @endif
                        <div class="media mt-5">
                           <div class="d-flex mr-3"> <a href="#"> <img class="media-object brround" alt="64x64" src="../assets/images/faces/female/2.jpg"> </a> </div>
                           <div class="media-body">
                              <h5 class="mt-0 mb-1 font-weight-semibold">Rose Slater <span class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" title="" data-original-title="verified"><i class="fa fa-check-circle-o text-success"></i></span></h5>
                              <small class="text-muted"><i class="fa fa-calendar"></i> Dec 22st <i class=" ml-3 fa fa-clock-o"></i> 6.00 <i class=" ml-3 fa fa-map-marker"></i> Brezil</small> 
                              <p class="font-13  mb-2 mt-2"> Lorem Ipsum available, quis nostrud exercitation ullamco laboris commodo Lorem Ipsum available, but the majority have suffered alteration in some form laboriosam, nisi ut aliquid ex ea commodi consequatur consequat. </p>
                              <a href="" data-toggle="modal" data-target="#Comment"><span class="badge badge-default">Comment</span></a> 
                           </div>
                        </div> --}}
                     </div>
                     <br>

                   {{--   <div class="card-body">
                       <h4>Display Comments</h4>
                       @foreach($post->comments as $comment)
                           <div class="display-comment">
                               <strong>{{ $comment->user->name }}</strong>
                               <p>{{ $comment->body }}</p>
                           </div>
                       @endforeach
                       <hr />
                       <h4>Add comment</h4>
                       <form method="post" action="{{ route('comment.add') }}">
                           @csrf
                           <div class="form-group">
                               <input type="text" name="comment_body" class="form-control" />
                               <input type="hidden" name="post_id" value="{{ $post->id }}" />
                           </div>
                           <div class="form-group">
                               <input type="submit" class="btn btn-warning" value="Add Comment" />
                           </div>
                       </form>
                   </div> --}}
                  </div>
                {{--   <div class="media p-5 border-top mt-0">
                     <div class="d-flex mr-3"> <a href="#"> <img class="media-object brround" alt="64x64" src="../assets/images/faces/male/3.jpg"> </a> </div>
                     <div class="media-body">
                        <h5 class="mt-0 mb-1 font-weight-semibold">Edward <span class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" title="" data-original-title="verified"><i class="fa fa-check-circle-o text-success"></i></span> <span class="fs-14 ml-2"> 4 <i class="fa fa-star text-yellow"></i></span> </h5>
                        <small class="text-muted"><i class="fa fa-calendar"></i> Dec 21st <i class=" ml-3 fa fa-clock-o"></i> 16.35 <i class=" ml-3 fa fa-map-marker"></i> UK</small> 
                        <p class="font-13  mb-2 mt-2"> Lorem Ipsum available, quis Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et nostrud exercitation ullamco laboris commodo consequat. </p>
                        <a href="#" class="mr-2"><span class="badge badge-primary">Helpful</span></a> <a href="" class="mr-2" data-toggle="modal" data-target="#Comment"><span class="">Comment</span></a> <a href="" class="mr-2" data-toggle="modal" data-target="#report"><span class="">Report</span></a> 
                     </div>
                  </div> --}}
               </div>
               @endforeach
               
            </div>
            @if(Auth::check())
            <div class="card mb-0">
               <div class="card-header">
                  @if(session('thongbao'))
                     {{session('thongbao')}}
                  @endif
                  <h3 class="card-title">Write Your Comments</h3>
               </div>
               <form action="{{route('postComment',['id'=>$id])}}" method="post">
                  @csrf
                  <div class="card-body">
                     <div class="form-group"> <textarea class="form-control" name="content" rows="6" placeholder="Write Your Comment"></textarea> </div>
                     <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            @else
            <a href="{{route('admin.user.login')}}" class="btn btn-green">Login</a>
            <a href="{{route('getRegister')}}" class="btn btn-primary">Register</a>
            @endif
         </div>
         <!--/Lists--> 
      </div>
   </div>
</section>
<script>
   $(document).ready(function(){
       function submitComment(){
         // var name = $('.name_comment').val();
         // var email = $('.email_comment').val();
         var content = $('.body_comment').val();
    }
   });
  
</script>
@endsection