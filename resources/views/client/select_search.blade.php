<div class="header-text mb-0">
    <div class="container">
        <div class="text-center text-white ">
            <h1 class="mb-1">Tìm những chiếc xe tốt nhất được mua và bán ở gần bạn</h1>
            <p>Một thực tế đã được thiết lập từ lâu rằng người đọc sẽ bị phân tâm khi nhìn vào bố cục của nó.</p>
        </div>
        <div class="row">
            <div class="col-xl-10 col-lg-12 col-md-12 d-block mx-auto">
                <div class="item-search-tabs">
                    <div class="item-search-menu">
                        <ul class="nav">
                            <li class=""><a href="#tab1" class="active" data-toggle="tab" id="search_poster_and_product">Xe</a></li>
                            <li><a href="#tab2" data-toggle="tab" id="search_phu_tung">Phụ tùng</a></li>
                            <li><a href="#tab3" data-toggle="tab" id="search_dich_vu">Dịch vụ</a></li>
                        </ul>
                    </div>
                    <form method="post" action="{{route('searchHome')}} " id="searchHome" >
                        @csrf
                        <input type="hidden" name="check_new" id="check_new" value="">
                        <input type="hidden" id="search_type" name="search_type" value="@if(isset($search_type)) {{$search_type}} @endif">
                        <input type="hidden" id="val_search_name" value="@if(isset($name)) {{$name}} @endif">
                        <input type="hidden" id="val_search_brand" value="@if(isset($id_brand)) {{$id_brand}} @endif">
                        <input type="hidden" id="val_search_seats" value="@if(isset($seats)) {{$seats}} @endif">
                        <input type="hidden" id="val_search_price" value="@if(isset($price)) {{$price}} @endif">
                        <input type="hidden" id="val_search_city" value="@if(isset($city)) {{$city}} @endif">
                        <div class="tab-content index-search-select" >
                           
                                <!-- search xe -->
                                <div class="tab-pane active search_poster_and_product" id="tab1" >
                                    <div class="search-background">
                                        <div class="form row no-gutters">
                                            <div class="form-group col-xl-2 col-lg-2 col-md-12 mb-0" style="height:43px">
                                                <input placeholder="Nhâp tên" style="height: 100%;border-radius: 0;border-left: 0px;border: 1px solid #f1f5fd;" type="text" class="form-control" name="search_name_car" id="search_name_car">
                                            </div>
                                            <div class="form-group col-xl- col-lg-2 col-md-12 mb-0" style="height:43px">
                                                <select name="search_brand_car" id="search_brand_car"
                                                class="form-control select2-show-search border-bottom-0 border-left-0 w-100 select2-hidden-accessible"
                                                data-placeholder="Select" aria-hidden="true">
                                                <optgroup label="Thương Hiệu">
                                                    <option @if(!isset($id_brand)) selected="" @endif value="">chọn thương hiệu</option>
                                                    @foreach($brand_car as $value)
                                                    <option @if(isset($id_brand) && ($value->id==$id_brand)) selected="" @endif
                                                        value="{{$value->id}}">{{$value->name}}</option>
                                                        @endforeach
                                                    </optgroup>
                                                </select>
                                                </div>
                                                <div class="form-group col-xl-2 col-lg-2 col-md-12 mb-0" style="height:43px">
                                                    <select name="search_seats_car" id="search_seats_car"
                                                    class="form-control select2-show-search border-bottom-0 border-left-0 w-100 select2-hidden-accessible"
                                                    data-placeholder="Select" aria-hidden="true">
                                                    <optgroup label="Số Ghế">
                                                        <option value="" @if(!isset($seats)) selected="" @endif>Chỗ ngồi</option>
                                                        <option value="2" @if(isset($seats) && $seats==2) selected="" @endif>2</option>
                                                        <option value="3" @if(isset($seats) && $seats==3) selected="" @endif>3</option>
                                                        <option value="4" @if(isset($seats) && $seats==4) selected="" @endif>4</option>
                                                        <option value="5" @if(isset($seats) && $seats==5) selected="" @endif)>5</option>
                                                        <option value="6" @if(isset($seats) && $seats==6) selected="" @endif)>6</option>
                                                        <option value="7" @if(isset($seats) && $seats==7) selected="" @endif)>7</option>
                                                        <option value="8" @if(isset($seats) && $seats==8) selected="" @endif)>8</option>
                                                        <option value="9" @if(isset($seats) && $seats==9) selected="" @endif)>9</option>
                                                    </optgroup>
                                                </select>
                                                </div>
                                                <div class="form-group col-xl-2 col-lg-2 col-md-12 mb-0" style="height:43px">
                                                    <select name="search_price_car" id="search_price_car"
                                                    class="form-control select2-show-search border-bottom-0 w-100 select2-hidden-accessible"
                                                    data-placeholder="Select" aria-hidden="true">
                                                    <optgroup label="Giá">
                                                        <option value="" @if(!isset($price)) selected="" @endif>Giá</option>
                                                        <option value="1" @if(isset($price) && $price==1) selected="" @endif>$10k</option>
                                                        <option value="2" @if(isset($price) && $price==2) selected="" @endif>$10k-$20K
                                                        </option>
                                                        <option value="3" @if(isset($price) && $price==3) selected="" @endif>$20K-$30K
                                                        </option>
                                                        <option value="4" @if(isset($price) && $price==4) selected="" @endif>$30K-$40K
                                                        </option>
                                                        <option value="5" @if(isset($price) && $price==5) selected="" @endif>$40K-$50K
                                                        </option>
                                                        <option value="6" @if(isset($price) && $price==6) selected="" @endif>$50K-$60K
                                                        </option>
                                                        <option value="7" @if(isset($price) && $price==7) selected="" @endif>$60K-$70K
                                                        </option>
                                                        <option value="8" @if(isset($price) && $price==8) selected="" @endif>$70k-$80K
                                                        </option>
                                                        <option value="9" @if(isset($price) && $price==9) selected="" @endif>$80K &lt; Above
                                                        </option>
                                                    </optgroup>
                                                </select>
                                                </div>
                                                <div class="form-group col-xl-3 col-lg-6  col-md-12 mb-0 location">
                                                    <div class="row no-gutters bg-white br-2">
                                                        <div class="form-group  col-xl-8 col-lg-7 col-md-12 mb-0">
                                                            <select name="search_city_car" id="search_city_car"
                                                            class="form-control select2-show-search border-bottom-0 w-100 select2-hidden-accessible"
                                                            data-placeholder="Select" aria-hidden="true">
                                                            <optgroup label="Địa Chỉ">
                                                                <option value="" @if(!isset($city)) selected="" @endif value="">Choose Make
                                                                </option>
                                                                @foreach($provinces as $value)
                                                                <option @if(isset($city) && ($value->id==$city)) selected="" @endif
                                                                    value="{{$value->id}}">{{$value->_name}}</option>
                                                                    @endforeach
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                        <div class="col-xl-4 col-lg-5 col-md-12 mb-0">
                                                            <button type='submit' style="width: 160px;height: 43px;border-radius: 0"
                                                            class="btn btn-danger"><i class="fas fa-search"></i> Search</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <!-- search phụ tùng -->
                                <div class="tab-pane search_phu_tung" id="tab2" style="display: none;">
                                    <div class="search-background">
                                        <div class="form row no-gutters">
                                            <div class="form-group col-xl-4 col-lg-2 col-md-12 mb-0" style="height:43px">
                                                <input placeholder="Nhâp tên" style="height: 100%;border-radius: 0;border-left: 0px;border: 1px solid #f1f5fd;" type="text" class="form-control" name="search_phu_tung" id="search_phu_tung">
                                            </div>
                                            <div class="form-group col-xl- col-lg-2 col-md-12 mb-0" style="height:43px">
                                                <select name="search_brand_phu_tung" id="search_brand_phu_tung"
                                                class="form-control select2-show-search border-bottom-0 border-left-0 w-100 select2-hidden-accessible"
                                                data-placeholder="Select" aria-hidden="true">
                                                <optgroup label="Thương Hiệu">
                                                    <option @if(!isset($id_brand)) selected="" @endif value="">Choose Make</option>
                                                    @foreach($brand_car as $value)
                                                    <option @if(isset($id_brand) && ($value->id==$id_brand)) selected="" @endif
                                                        value="{{$value->id}}">{{$value->name}}</option>
                                                    @endforeach
                                                    </optgroup>
                                                </select>
                                                </div>
                                                <div class="form-group col-xl-2 col-lg-2 col-md-12 mb-0" style="height:43px">
                                                    <select name="search_price_phu_tung" id="search_price_phu_tung"
                                                    class="form-control select2-show-search border-bottom-0 w-100 select2-hidden-accessible"
                                                    data-placeholder="Select" aria-hidden="true">
                                                    <optgroup label="Giá">
                                                        <option value="" @if(!isset($price)) selected="" @endif>Price</option>
                                                        <option value="1" @if(isset($price) && $price==1) selected="" @endif>$10k</option>
                                                        <option value="2" @if(isset($price) && $price==2) selected="" @endif>$10k-$20K
                                                        </option>
                                                        <option value="3" @if(isset($price) && $price==3) selected="" @endif>$20K-$30K
                                                        </option>
                                                        <option value="4" @if(isset($price) && $price==4) selected="" @endif>$30K-$40K
                                                        </option>
                                                        <option value="5" @if(isset($price) && $price==5) selected="" @endif>$40K-$50K
                                                        </option>
                                                        <option value="6" @if(isset($price) && $price==6) selected="" @endif>$50K-$60K
                                                        </option>
                                                        <option value="7" @if(isset($price) && $price==7) selected="" @endif>$60K-$70K
                                                        </option>
                                                        <option value="8" @if(isset($price) && $price==8) selected="" @endif>$70k-$80K
                                                        </option>
                                                        <option value="9" @if(isset($price) && $price==9) selected="" @endif>$80K &lt; Above
                                                        </option>
                                                    </optgroup>
                                                </select>
                                                </div>
                                                <div class="form-group col-xl-3 col-lg-6  col-md-12 mb-0 location">
                                                    <div class="row no-gutters bg-white br-2">
                                                        <div class="form-group  col-xl-8 col-lg-7 col-md-12 mb-0">
                                                            <select name="search_city_phu_tung" id="search_city_phu_tung"
                                                            class="form-control select2-show-search border-bottom-0 w-100 select2-hidden-accessible"
                                                            data-placeholder="Select" aria-hidden="true">
                                                            <optgroup label="Địa Chỉ">
                                                                <option value="" @if(!isset($city)) selected="" @endif value="">Choose Make
                                                                </option>
                                                                @foreach($provinces as $value)
                                                                <option @if(isset($city) && ($value->id==$city)) selected="" @endif
                                                                    value="{{$value->id}}">{{$value->_name}}</option>
                                                                    @endforeach
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                        <div class="col-xl-4 col-lg-5 col-md-12 mb-0">
                                                            <button type='submit' style="width: 160px;height: 43px;border-radius: 0"
                                                            class="btn btn-danger"><i class="fas fa-search"></i> Search</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <!-- search dịch vụ -->
                                <div class="tab-pane search_dich_vu" id="tab3" style="display: none;">
                                    <div class="search-background">
                                        <div class="form row no-gutters">
                                            <div class="form-group col-xl-5 col-lg-2 col-md-12 mb-0" style="height:43px">
                                                <input placeholder="Nhâp tên" style="height: 100%;border-radius: 0;border-left: 0px;border: 1px solid #f1f5fd;" type="text" class="form-control" name="search_dich_vi"id="search_dich_vu">
                                            </div>
                                                <div class="form-group col-xl-7 col-lg-6  col-md-12 mb-0 location">
                                                    <div class="row no-gutters bg-white br-2">
                                                        <div class="form-group  col-xl-8 col-lg-7 col-md-12 mb-0">
                                                            <select name="search_city" id="search_city"
                                                            class="form-control select2-show-search border-bottom-0 w-100 select2-hidden-accessible"
                                                            data-placeholder="Select" aria-hidden="true">
                                                                <optgroup label="Địa Chỉ">
                                                                <option value="" @if(!isset($city)) selected="" @endif value="">Choose Make
                                                                </option>
                                                                @foreach($provinces as $value)
                                                                <option @if(isset($city) && ($value->id==$city)) selected="" @endif
                                                                    value="{{$value->id}}">{{$value->_name}}</option>
                                                                    @endforeach
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                        <div class="col-xl-4 col-lg-5 col-md-12 mb-0">
                                                            <button type='submit' style="width: 185px;height: 43px;border-radius: 0"
                                                            class="btn btn-danger"><i class="fas fa-search"></i> Search</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                    </form>
                </div>
            </div>
        </div>
    </div><!-- /header-text -->

