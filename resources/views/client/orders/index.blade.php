@extends('client.master')
@section('content')
</div> <!-- Topbar -->
<!--Breadcrumb-->
<!-- <div id="display_gray">
</div>
<div id="details_orders">
    <div id="title">
        Đơn hàng <span class="title" id="id_orders">#123456</span>
        <p id="cancel"><i style="line-height: 50px" class="fas fa-times"></i></p>
    </div>
    <div id="div_details_orders">
        <div id="content_order">
            <th>Order ID</th>
                                        <th>Category</th>
                                        <th>Date</th>
                                        <th>Price</th>
                                        <th>Status</th>
            <ul>
                <li>ID đơn hàng : #123456</li>
                <li>Tên mặt hàng : Tua bin khí</li>
                <li>Người đặt : Đặng Quang Nam</li>
                <li></li>
                <li>ID đơn hàng : #123456</li>
                <li>ID đơn hàng : #123456</li>
                <li>ID đơn hàng : #123456</li>
            </ul>
        </div>
    </div>
</div> -->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg"
    style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
    <div class="header-text mb-0">
        <div class="container">
            <div class="text-center text-white">
                <h1 class="">Orders</h1>
                <ol class="breadcrumb text-center">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">My Dashboard</a></li>
                    <li class="breadcrumb-item active text-white" aria-current="page">Orders</li>
                </ol>
            </div>
        </div>
    </div>
</div>
</section>
<!--Breadcrumb-->
<!--Section-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <!-- sidebar -->
            @include('client.sidebar',['data'=>$data])
            <!-- end sidebar -->
            <div class="col-xl-9 col-lg-12 col-md-12">
                <div class="card mb-0">
                    <div class="card-header">
                        <h3 class="card-title">Orders List</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive border-top">
                            <table class="table table-bordered table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th>Order ID</th>
                                        <th>Image</th>
                                        <th>Category</th>
                                        <th>Date</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-primary"><span class="id_phu_tung">#89345</span></td>
                                        <td>
                                            <img src="" style="height: 50px; width: 50px">
                                        </td>
                                        <td>Exercitationem</td>
                                        <td>07-12-2019</td>
                                        <td class="font-weight-semibold fs-16">$893</td>
                                        <td> <a href="#" class="badge badge-danger">Pending</a> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <ul class="pagination">
                            <li class="page-item page-prev disabled"> <a class="page-link" href="#"
                                tabindex="-1">Prev</a> </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item page-next"> <a class="page-link" href="#">Next</a> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Orders Payment Method</h3>
                        </div>
                        <div class="card-body">
                            <div class="card-pay">
                                <ul class="tabs-menu nav">
                                    <li class=""><a href="#tab1" class="active" data-toggle="tab"><i
                                        class="fa fa-credit-card"></i> Credit Card</a></li>
                                        <li><a href="#tab2" data-toggle="tab" class=""><i class="fa fa-paypal"></i>
                                        Paypal</a></li>
                                        <li><a href="#tab3" data-toggle="tab" class=""><i class="fa fa-university"></i>
                                        Bank Transfer</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active show" id="tab1">
                                            <div class="form-group"> <label class="form-label">CardHolder Name</label>
                                                <input type="text" class="form-control" id="name1" placeholder="First Name">
                                            </div>
                                            <div class="form-group"> <label class="form-label">Card number</label>
                                                <div class="input-group"> <input type="text" class="form-control"
                                                    placeholder="Search for..."> <span class="input-group-append">
                                                        <button class="btn btn-info" type="button"><i class="fa fa-cc-visa"></i>
                                                            &nbsp; <i class="fa fa-cc-amex"></i> &nbsp; <i
                                                            class="fa fa-cc-mastercard"></i></button> </span> </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-8">
                                                                <div class="form-group"> <label class="form-label">Expiration</label>
                                                                    <div class="input-group"> <input type="number" class="form-control"
                                                                        placeholder="MM" name="expire-month"> <input type="number"
                                                                        class="form-control" placeholder="YY" name="expire-year"> </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group"> <label class="form-label">CVV <i
                                                                        class="fa fa-question-circle"></i></label> <input type="number"
                                                                        class="form-control" required=""> </div>
                                                                    </div>
                                                                </div> <a href="#" class="btn btn-primary">Submit</a>
                                                            </div>
                                                            <div class="tab-pane" id="tab2">
                                                                <p>Paypal is easiest way to pay online</p>
                                                                <p><a href="#" class="btn btn-primary"><i class="fa fa-paypal"></i> Log in
                                                                my Paypal</a></p>
                                                                <p class="mb-0"><strong>Note:</strong> Nemo enim ipsam voluptatem quia
                                                                    voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                                                                dolores eos qui ratione voluptatem sequi nesciunt. </p>
                                                            </div>
                                                            <div class="tab-pane" id="tab3">
                                                                <p>Bank account details</p>
                                                                <dl class="card-text">
                                                                    <dt>BANK: </dt>
                                                                    <dd> THE UNION BANK 0456</dd>
                                                                </dl>
                                                                <dl class="card-text">
                                                                    <dt>Accaunt number: </dt>
                                                                    <dd> 67542897653214</dd>
                                                                </dl>
                                                                <dl class="card-text">
                                                                    <dt>IBAN: </dt>
                                                                    <dd>543218769</dd>
                                                                </dl>
                                                                <p class="mb-0"><strong>Note:</strong> Nemo enim ipsam voluptatem quia
                                                                    voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                                                                dolores eos qui ratione voluptatem sequi nesciunt. </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!--/Section-->
                            @endsection
