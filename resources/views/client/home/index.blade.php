@extends('client.master')
@section('content')
</div> <!-- Topbar -->
<!--Section-->
<section>
    <?php use App\Model\Poster; ?>
    <?php use App\Model\Comment; ?>
    <div class="banner-2 cover-image sptb-2 sptb-tab bg-background2" data-image-src="assets/images/banners/banner1.jpg"
        style="background: url(&quot;assets/images/banners/banner1.jpg&quot;) center center;">
        <!-------------------------------------phần search -->

        @include('client.select_search')
        <!----------------------------------------------------->
    </div>
</section>
<!--Section-->
<!-------------------------- phần categories ---------------------->
<!--Section-->
<section class="categories">
    <div class="container">
        <div id="small-categories" class="owl-carousel owl-carousel-icons2 owl-loaded owl-drag">
            <div class="owl-stage-outer">
                <div class="owl-stage" style="transform: translate3d(-4804px, 0px, 0px); transition: all 0.25s ease 0s; width: 7206px;">
                    @foreach($brand_car as $value_brand)
                        <div class="owl-item active" style="width: 275.25px; margin-right: 25px;">
                            <div class="item active">
                                <div class="card mb-0 active">
                                    <div class="card-body active">
                                        <div class="cat-item text-center"> <a href="cars-list.html"></a>
                                            <div class="cat-img1"> <img src="../img_brand/{{$value_brand->img}}" alt="img"
                                                    class="mx-auto"> </div>
                                            <div class="cat-desc">
                                                <h5 class="mb-1">{{$value_brand->name}}</h5> <small
                                                    class="badge badge-pill badge-primary mr-2"><?php $posterCount = Poster::posterCount($value_brand->id); echo $posterCount; ?></small><span
                                                    class="text-muted">Quảng cáo được đăng</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="owl-nav"><button type="button" role="presentation" class="owl-prev"><span
                        aria-label="Previous">‹</span></button><button type="button" role="presentation"
                    class="owl-next"><span aria-label="Next">›</span></button></div>
            <div class="owl-dots disabled"></div>
        </div>
    </div>
</section>
<!--Section-->
<!-- --------------------------------------------- -->
<!--Section-->
{{-- xe đăng bán --}}
<section class="sptb bg-white" style="padding-top: 2rem;padding-bottom: 3rem;">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Xe đăng bán</h2>
            <p>Moors và chạy ngay bây giờ. Nhật Bản duyệt bóng đá tại các phương tiện chính</p>
        </div>
        <div id="myCarousel1" class="owl-carousel owl-carousel-icons2 owl-loaded owl-drag">
            <div class="owl-stage-outer">
                <div class="owl-stage"
                    style="transform: translate3d(-1201px, 0px, 0px); transition: all 0.25s ease 0s; width: 3904px;">
                    @foreach($product as $products)
                    <div class="owl-item active" style="width: 275.25px; margin-right: 25px;">
                        <div class="item">
                            <div class="card mb-0">
                            <?php
                                if($products->check_sp == 0)
                                {
                            ?>
                                <div class="power-ribbon power-ribbon-top-left text-warning">
                                    <span class="bg-warning">
                                        <i class="fa fa-bolt"></i>
                                    </span>
                                </div>
                            <?php
                                }elseif($products->check_sp == 1){
                            ?>
                                <div class="ribbon ribbon-top-left text-danger">
                                    <span class="bg-danger">
                                        <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">Hết Hàng</font>
                                        </font>
                                    </span>
                                </div>
                            <?php
                               }
                            ?> 
                                <div class="item-card2-img"> 
                                    <a class="link" href="{{route('Car-Detail-01',$products->id)}}"></a> 
                                    <img src="../img_product/{{$products->image_product}}" alt="img" class="cover-image" style="height: 180px">
                                    <div class="item-tag-overlaytext">
                                    <?php
                                        if($products->check_new == 1)
                                        {
                                    ?>
                                        <span class="text-white bg-success">mới</span>
                                    <?php
                                        }elseif($products->check_new == 2){
                                    ?>
                                        <span class="text-white bg-gray">đã sử dụng</span>
                                    <?php
                                        }elseif($products->check_new == 3){
                                    ?>  
                                        <span class="text-white bg-success">mới</span>
                                        <span class="text-white bg-danger">cho thê</span>
                                    <?php
                                      }
                                    ?> 
                                    </div>
                                    <div class="item-card2-icons" style="display: flex;align-items: center;justify-content: space-around;"> 
                                        <a href="#" class="item-card2-icons-l bg-primary">
                                            @if($products->category->id == 1)
                                             <i class="car car-toyota" style="margin-top: 7px"></i>
                                            @elseif($products->category->id == 2)
                                             <i class="car car-honda" style="margin-top: 7px"></i>
                                            @elseif($products->category->id == 3)
                                             <i class="car car-volkswagen" style="margin-top: 7px"></i>
                                            @elseif($products->category->id == 4)
                                             <i class="car car-ferrari" style="margin-top: 7px"></i>
                                            @elseif($products->category->id == 5)
                                             <i class="car car-vinfast" style="margin-top: 7px"></i>
                                            @endif
                                        </a> 
                                        <a href="#"  onclick="toggle()"
                                        class="item-card2-icons-r wishlist active" style="margin-left: 3px">
                                        <i id="btn" class="fa fa-heart" style="margin-top: 10px"></i></a> 
                                    </div>
                                </div>
                                
                                <div class="card-body pb-0" style="height: 200px">
                                    <div class="item-card2">
                                        <div class="item-card2-desc">
                                            <div class="item-card2-text"> <a href="{{route('Car-Detail-01',$products->id)}}" class="text-dark">
                                                    <h4 class="mb-0">{{$products->name}}</h4>
                                                </a> </div>
                                            <div class="d-flex pb-0 pt-0"> <a href="">
                                                    <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                            class="fa fa-map-marker text-danger mr-2"></i>Viet Nam</p>
                                                </a> <span
                                                    class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">
                                                    {{number_format($products->price,0,',','.')}} vnđ</span>
                                            </div>
                                            <p class="">{!!$products->description!!}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item-card2-footer mt-4 mb-4">
                                        <div class="item-card2-footer-u">
                                            <div class="d-md-flex"> 
                                                <span class="review_score mr-2 badge badge-primary">{{$products->rate}}/5</span>
                                                <?php $rate = 1 ?>
                                                @for($i=1;$i<=$products->rate;$i++)
                                                    <i class="fa fa-star text-warning"></i>
                                                    <?php $rate ++ ?>
                                                @endfor
                                                @if($rate <5)
                                                    @for($i=$rate;$i<=5;$i++)
                                                        <i class="fa fa-star" style="color: #cac5c5"></i>
                                                    @endfor
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer"> <a href="#" class="mr-2" data-toggle="tooltip"
                                        data-placement="bottom" data-original-title="Manual"><i
                                            class="fa fa-car text-muted"></i> 
                                            @if($products->transmission_type != 1)
                                                <span class="text-default">Auto</span>
                                            @else
                                                <span class="text-default">Manual</span>
                                            @endif
                                        </a> <a href="#" class="mr-2"
                                        data-toggle="tooltip" data-placement="bottom"
                                        data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i> <span
                                            class="text-default">{{$products->arai_milage}}</span></a> <a href="#" class=""
                                        data-toggle="tooltip" data-placement="bottom" data-original-title="FuelType"><i
                                            class="fa fa-tachometer text-muted"></i> 
                                            @if($products->fuel_type == 1)
                                                <span class="text-default">Dầu</span>
                                            @else
                                                <span class="text-default">Xăng</span>
                                            @endif
                                        </a>
                                        </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="owl-nav"><button type="button" role="presentation" class="owl-prev"><span
                        aria-label="Previous">‹</span></button><button type="button" role="presentation"
                    class="owl-next"><span aria-label="Next">›</span></button></div>
            <div class="owl-dots disabled"></div>
        </div>
    </div>
</section>
<!--Section-->

<!--Section-->
{{-- quảng cáo xe mới --}}
<section class="sptb bg-white" style="padding-top: 2rem;padding-bottom: 3rem;">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Quảng cáo xe mới</h2>
            <p>Moors và chạy ngay bây giờ. Nhật Bản duyệt bóng đá tại các phương tiện chính</p>
        </div>
        <div id="myCarousel1" class="owl-carousel owl-carousel-icons2 owl-loaded owl-drag">
            <div class="owl-stage-outer">
                <div class="owl-stage"
                    style="transform: translate3d(-1201px, 0px, 0px); transition: all 0.25s ease 0s; width: 3904px;">
                    @foreach($ads_car as $value_ads_car)
                    <div class="owl-item active" style="width: 275.25px; margin-right: 25px;">
                        <div class="item">
                            <div class="card mb-0">
                            <?php
                                if($value_ads_car->check == 0)
                                {
                            ?>
                                <div class="power-ribbon power-ribbon-top-left text-warning">
                                    <span class="bg-warning">
                                        <i class="fa fa-bolt"></i>
                                    </span>
                                </div>
                            <?php
                                }elseif($value_ads_car->check == 1){
                            ?>
                                <div class="ribbon ribbon-top-left text-danger">
                                    <span class="bg-danger">
                                        <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">Hết Hàng</font>
                                        </font>
                                    </span>
                                </div>
                            <?php
                               }
                            ?> 
                                <div class="item-card2-img"> 
                                    <a class="link" href="{{URL::to('show-detail-poster/'.$value_ads_car->id)}}"></a> 
                                    <img src="../img_poster/{{$value_ads_car->img}}" alt="img" class="cover-image" style="height: 180px">
                                    <div class="item-tag-overlaytext"> 
                                        <?php
                                            if($value_ads_car->check_new == 0)
                                            {
                                        ?>
                                            <span class="text-white bg-success">mới</span>
                                        <?php
                                            }elseif($value_ads_car->check_new == 1){
                                        ?>
                                            <span class="text-white bg-gray">đã sử dụng</span>
                                        <?php
                                            }elseif($value_ads_car->check_new == 2){
                                        ?>  
                                            <span class="text-white bg-success">mới</span>
                                            <span class="text-white bg-danger">cho thê</span>
                                        <?php
                                          }
                                        ?> 
                                    </div>
                                    <div class="item-card2-icons" style="display: flex;align-items: center;justify-content: space-around;"> 
                                        <a href="#" class="item-card2-icons-l bg-primary">
                                            @if($value_ads_car->category->id == 1)
                                             <i class="car car-toyota" style="margin-top: 7px"></i>
                                            @elseif($value_ads_car->category->id == 2)
                                             <i class="car car-honda" style="margin-top: 7px"></i>
                                            @elseif($value_ads_car->category->id == 3)
                                             <i class="car car-volkswagen" style="margin-top: 7px"></i>
                                            @elseif($value_ads_car->category->id == 4)
                                             <i class="car car-ferrari" style="margin-top: 7px"></i>
                                            @elseif($value_ads_car->category->id == 5)
                                             <i class="car car-vinfast" style="margin-top: 7px"></i>
                                            @endif
                                        </a> 
                                        <a href="#"  onclick="toggle()"
                                        class="item-card2-icons-r wishlist active" style="margin-left: 3px">
                                        <i id="btn" class="fa fa-heart" style="margin-top: 10px"></i></a> 
                                    </div>
                                </div>
                                
                                <div class="card-body pb-0" style="height: 200px">
                                    <div class="item-card2">
                                        <div class="item-card2-desc">
                                            <div class="item-card2-text"> <a href="{{URL::to('show-detail-poster/'.$value_ads_car->id)}}" class="text-dark">
                                                    <h4 class="mb-0">{{$value_ads_car->title}}</h4>
                                                </a> </div>
                                            <div class="d-flex pb-0 pt-0"> <a href="">
                                                    <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                            class="fa fa-map-marker text-danger mr-2"></i>Viet Nam</p>
                                                </a> <span
                                                    class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">
                                                    {{number_format($value_ads_car->price,0,',','.')}}</span>
                                            </div>
                                            <p class="">{!!$value_ads_car->description!!}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item-card2-footer mt-4 mb-4">
                                        <div class="item-card2-footer-u">
                                            <div class="d-md-flex">
                                                <span class="review_score mr-2 badge badge-primary">{{$value_ads_car->rate}}/5</span>
                                                <?php $rate = 1 ?>
                                                @for($i=1;$i<=$value_ads_car->rate;$i++)
                                                    <i class="fa fa-star text-warning"></i>
                                                    <?php $rate ++ ?>
                                                @endfor
                                                @if($rate <5)
                                                    @for($i=$rate;$i<=5;$i++)
                                                        <i class="fa fa-star" style="color: #cac5c5"></i>
                                                    @endfor
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer"> <a href="#" class="mr-2" data-toggle="tooltip"
                                        data-placement="bottom" data-original-title="Manual"><i
                                            class="fa fa-car text-muted"></i>
                                            @if($value_ads_car->transmission_type != 1)
                                                <span class="text-default">Auto</span>
                                            @else
                                                <span class="text-default">Manual</span>
                                            @endif
                                        </a> <a href="#" class="mr-2"
                                        data-toggle="tooltip" data-placement="bottom"
                                        data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i> <span
                                            class="text-default">{{$value_ads_car->arai_milage}}</span></a> <a href="#" class=""
                                        data-toggle="tooltip" data-placement="bottom" data-original-title="FuelType"><i
                                            class="fa fa-tachometer text-muted"></i>
                                            @if($value_ads_car->fuel_type == 1)
                                                <span class="text-default">Dầu</span>
                                            @else
                                                <span class="text-default">Xăng</span>
                                            @endif
                                        </a>
                                        </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
            <div class="owl-nav"><button type="button" role="presentation" class="owl-prev"><span
                        aria-label="Previous">‹</span></button><button type="button" role="presentation"
                    class="owl-next"><span aria-label="Next">›</span></button></div>
            <div class="owl-dots disabled"></div>
        </div>
    </div>
</section>
<!--Section-->
<!--Section-->

{{-- các quảng cáo --}}
<section class="sptb bg-white" style="padding-top: 2rem;padding-bottom: 3rem;">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Các Quảng Cáo</h2>
            <p>Moors và chạy ngay bây giờ. Nhật Bản duyệt bóng đá tại các phương tiện chính</p>
        </div>
        <div id="myCarousel1" class="owl-carousel owl-carousel-icons2 owl-loaded owl-drag">
            <div class="owl-stage-outer">
                <div class="owl-stage"
                    style="transform: translate3d(-1201px, 0px, 0px); transition: all 0.25s ease 0s; width: 3904px;">
                    @foreach($all_poster as $show_poster)
                    <div class="owl-item active" style="width: 275.25px; margin-right: 25px;">
                        <div class="item">
                            <div class="card mb-0">
                            <?php
                                if($show_poster->check_new == 0)
                                {
                            ?>
                                <div class="arrow-ribbon bg-success">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">phục vụ</font>
                                    </font>
                                </div>
                            <?php
                                }elseif($show_poster->check_new == 1){
                            ?>  
                                <div class="arrow-ribbon bg-secondary">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">giảm giá</font>
                                    </font>
                                </div>
                            <?php
                                }elseif($show_poster->check_new == 2){
                            ?>      
                                <div class="arrow-ribbon bg-primary">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">giảm giá</font>
                                    </font>
                                </div>
                            <?php
                                }
                            ?>     
                                <div class="item-card7-imgs">
                                    <a class="link" href="{{URL::to('show-detail-poster/'.$show_poster->id)}}"></a> 
                                        <img src="../img_poster/{{$show_poster->img}}" alt="img" class="cover-image"
                                        style="height: 180px"> 
                                    <div class="item-card7-overlaytext"> 
                                    <?php
                                        if($show_poster->check_new == 0)
                                        {
                                    ?>    
                                        <span class="text-white bg-success">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;"> Mới</font>
                                            </font>
                                        </span>
                                    <?php
                                        }elseif($show_poster->check_new == 1){
                                    ?>    
                                        <span class="text-white bg-gray">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">đã sử dụng</font>
                                            </font>
                                        </span>
                                    <?php
                                        }elseif($show_poster->check_new == 2){
                                    ?>  
                                        <span class="text-white bg-success">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;"> Mới</font>
                                            </font>
                                        </span>
                                        <span class="text-white bg-danger">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">cho thê</font>
                                            </font>
                                        </span>
                                    <?php
                                        }
                                    ?>  
                                    </div>
                                    <div class="item-tag">
                                       <h4 class="mb-0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 552,99</font></font></h4>
                                    </div>
                                </div>
                                
                                <div class="card-body pb-0" style="height: 200px">
                                    <div class="item-card2">
                                        <div class="item-card2-desc">
                                            <div class="item-card2-text"> <a href="{{URL::to('show-detail-poster/'.$show_poster->id)}}" class="text-dark">
                                                    <h4 class="mb-0">{{$show_poster->title}}</h4>
                                                </a> </div>
                                            <div class="d-flex pb-0 pt-0"> <a href="">
                                                    <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                            class="fa fa-map-marker text-danger mr-2"></i>Viet Nam</p>
                                                </a> <span
                                                    class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">
                                                    {{number_format($show_poster->price,0,',','.')}} vnđ</span>
                                            </div>
                                            <p class="">{!!$show_poster->description!!}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item-card2-footer mt-4 mb-4">
                                        <div class="item-card2-footer-u">
                                            <div class="d-md-flex"> 
                                                <span class="review_score mr-2 badge badge-primary">{{$show_poster->rate}}/5</span>
                                                <?php $rate = 1 ?>
                                                @for($i=1;$i<=$show_poster->rate;$i++)
                                                    <i class="fa fa-star text-warning"></i>
                                                    <?php $rate ++ ?>
                                                @endfor
                                                @if($rate <5)
                                                    @for($i=$rate;$i<=5;$i++)
                                                        <i class="fa fa-star" style="color: #cac5c5"></i>
                                                    @endfor
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer"> <a href="#" class="mr-2" data-toggle="tooltip"
                                        data-placement="bottom" data-original-title="Manual"><i
                                            class="fa fa-car text-muted"></i> 
                                            @if($show_poster->transmission_type != 1)
                                                <span class="text-default">Auto</span>
                                            @else
                                                <span class="text-default">Manual</span>
                                            @endif
                                        </a> <a href="#" class="mr-2"
                                        data-toggle="tooltip" data-placement="bottom"
                                        data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i> <span
                                            class="text-default">{{$products->arai_milage}}</span></a> <a href="#" class=""
                                        data-toggle="tooltip" data-placement="bottom" data-original-title="FuelType"><i
                                            class="fa fa-tachometer text-muted"></i> 
                                            @if($show_poster->fuel_type == 1)
                                                <span class="text-default">Dầu</span>
                                            @else
                                                <span class="text-default">Xăng</span>
                                            @endif
                                        </a>
                                        </a> 
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
            </div>
            <div class="owl-nav"><button type="button" role="presentation" class="owl-prev"><span
                        aria-label="Previous">‹</span></button><button type="button" role="presentation"
                    class="owl-next"><span aria-label="Next">›</span></button></div>
            <div class="owl-dots disabled"></div>
        </div>
    </div>
</section>
{{-- kết thúc quảng cáo --}}
{{-- phụ tùng  ô tô--}}
<section class="sptb bg-white" style="padding-top: 2rem;padding-bottom: 3rem;">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Phụ Tùng Ô Tô</h2>
            <p>Moors và chạy ngay bây giờ. Nhật Bản duyệt bóng đá tại các phương tiện chính</p>
        </div>
        <div id="myCarousel1" class="owl-carousel owl-carousel-icons2 owl-loaded owl-drag">
            <div class="owl-stage-outer">
                <div class="owl-stage"
                    style="transform: translate3d(-1201px, 0px, 0px); transition: all 0.25s ease 0s; width: 3904px;">
                    @foreach($phu_tung_b as $value_pt)
                    <div class="owl-item active" style="width: 275.25px; margin-right: 25px;">
                        <div class="item">
                            <div class="card mb-0">
                                <div class="power-ribbon power-ribbon-top-left text-warning">
                                    <span class="bg-warning">
                                        <i class="fa fa-bolt"></i>
                                    </span>
                                </div>
                                <div class="item-card2-img"> <a class="link" href="cars.html"></a> <img
                                        src="../img_accessary/{{$value_pt->img}}" alt="img" class="cover-image" style="height: 205px">
                                    <div class="item-tag-overlaytext"> 
                                        <span class="text-white bg-success">Mới</span>
                                    </div>
                                    <div class="item-card2-icons" style="display: flex;align-items: center;justify-content: space-around;"> 
                                        <a href="#" class="item-card2-icons-l bg-primary">
                                             <i class="car car-toyota" style="margin-top: 7px"></i>
                                        </a> 
                                        <a href="#"  onclick="toggle()"
                                        class="item-card2-icons-r wishlist active" style="margin-left: 3px">
                                        <i id="btn" class="fa fa-heart" style="margin-top: 10px"></i></a> 
                                    </div>
                                </div>
                                <div class="card-body pb-0" style="height: 200px;">
                                    <div class="item-card2">
                                        <div class="item-card2-desc" tyle="display: flex;align-items: center;justify-content: space-around;">
                                            <div class="item-card2-text"> 
                                                <a href="{{route('ChiTietPhuTung',['id'=>$value_pt->id])}}" class="text-dark">
                                                    <h4 class="mb-0">{{$value_pt->name}}</h4>
                                                </a> </div>
                                            <div class="d-flex pb-0 pt-0"> 
                                                <a href="">
                                                    <p class="pb-0 pt-0 mb-2 mt-2">
                                                        <i class="fa fa-map-marker text-danger mr-2"></i>Viet Nam</p>
                                                </a> 
                                                <span class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">
                                                    {{number_format($value_pt->price,0,',','.')}} vnđ</span>
                                            </div>
                                            <p class="" style="margin-top: -5px">{!!$value_pt->description!!}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item-card2-footer mt-4 mb-4">
                                        <div class="item-card2-footer-u">
                                            <div class="d-md-flex"> 
                                                <span class="review_score mr-2 badge badge-primary">{{$value_pt->rate}}/5</span>
                                                <?php $rate = 1 ?>
                                                @for($i=1;$i<=$value_ads_car->rate;$i++)
                                                    <i class="fa fa-star text-warning"></i>
                                                    <?php $rate ++ ?>
                                                @endfor
                                                @if($rate <5)
                                                    @for($i=$rate;$i<=5;$i++)
                                                        <i class="fa fa-star" style="color: #cac5c5"></i>
                                                    @endfor
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="owl-nav"><button type="button" role="presentation" class="owl-prev"><span
                        aria-label="Previous">‹</span></button><button type="button" role="presentation"
                    class="owl-next"><span aria-label="Next">›</span></button></div>
            <div class="owl-dots disabled"></div>
        </div>
    </div>
</section>
<!--Section-->
<!--Section-->
{{-- ô tô đã qua sử dụng có thương hiệu hàng đầu--}}
<section class="sptb" style="padding-top: 2rem;padding-bottom: 3rem;">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Ô tô đã qua sử dụng có thương hiệu hàng đầu</h2>
            <p>Moors và chạy ngay bây giờ. Nhật Bản duyệt bóng đá tại các phương tiện chính</p>
        </div>
        <div class="row">
            @foreach($brand as $brand)
            <div class="col-xl-2 col-lg-4 col-md-6 col-sm-6"> 
                <a href="#"
                    class="top-cities card text-center active mb-xl-0 box-shadow2"><img
                        src="../img_brand/{{$brand->img}}" alt="img"
                        class="bg-pink-transparent">
                    <div class="servic-data mt-3">
                        <h4 class="font-weight-semibold mb-0">{{$brand->name}}</h4>
                    </div>
                </a> </div>
            @endforeach

        </div>
    </div>
</section>
<!--Section-->
<!--Section-->
{{-- quảng cáo phụ tùng --}}
<section class="sptb bg-white" style="padding-top: 2rem;padding-bottom: 3rem;">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Quảng cáo phụ tùng</h2>
            <p>Moors và chạy ngay bây giờ. Nhật Bản duyệt bóng đá tại các phương tiện chính</p>
        </div>
        <div id="myCarousel1" class="owl-carousel owl-carousel-icons2 owl-loaded owl-drag">
            <div class="owl-stage-outer">
                <div class="owl-stage"
                    style="transform: translate3d(-1201px, 0px, 0px); transition: all 0.25s ease 0s; width: 3904px;">
                    @foreach($phu_tung_qc as $ac_qc)
                    <div class="owl-item active" style="width: 275.25px; margin-right: 25px;">
                        <div class="item">
                            <div class="card mb-0">
                                <div class="power-ribbon power-ribbon-top-left text-warning">
                                    <span class="bg-warning">
                                        <i class="fa fa-bolt"></i>
                                    </span>
                                </div>
                                <div class="item-card2-img"> 
                                    <a class="link" href="cars.html">  
                                    </a> 
                                    <img src="../img_accessary/{{$ac_qc->img}}" alt="img" class="cover-image"
                                    style="height: 180px">
                                    <div class="item-tag-overlaytext"> 
                                        <span class="text-white bg-success">Mới</span>
                                    </div>
                                    <div class="item-card2-icons" style="display: flex;align-items: center;justify-content: space-around;"> 
                                        <a href="#" class="item-card2-icons-l bg-primary"> 
                                            <i class="car car-toyota" style="margin-top: 10px"></i>
                                        </a> 
                                        <a href="#"class="item-card2-icons-r wishlist active">
                                            <i onclick="toggle()" id="btn" class="fa fa-heart" style="margin-top: 10px"></i></a> </div>
                                </div>
                                <div class="card-body" style="height: 200px">
                                    <div class="item-card7-desc">
                                        <div class="item-card7-text  d-flex"> <a href="cars.html" class="text-dark">
                                                <h4 class="">{{$ac_qc->title}}</h4>
                                            </a> </div>
                                        <div class="d-flex pb-0 pt-0"> <a href="">
                                                    <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                            class="fa fa-map-marker text-danger mr-2"></i>Viet Nam</p>
                                                </a> <span
                                                    class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">
                                                    {{number_format($ac_qc->price,0,',','.')}} vnđ</span>
                                            </div>
                                            <p class="">{!!$ac_qc->description!!}
                                            </p>
                                    </div>
                                    <div class="item-card2-footer mt-4 mb-4">
                                        <div class="item-card2-footer-u">
                                            <div class="d-md-flex"> 
                                                <span class="review_score mr-2 badge badge-primary">{{$ac_qc->rate}}/5</span>
                                                <?php $rate = 1 ?>
                                                @for($i=1;$i<=$ac_qc->rate;$i++)
                                                    <i class="fa fa-star text-warning"></i>
                                                    <?php $rate ++ ?>
                                                @endfor
                                                @if($rate <5)
                                                    @for($i=$rate;$i<=5;$i++)
                                                        <i class="fa fa-star" style="color: #cac5c5"></i>
                                                    @endfor
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            <div class="owl-nav"><button type="button" role="presentation" class="owl-prev"><span
                        aria-label="Previous">‹</span></button><button type="button" role="presentation"
                    class="owl-next"><span aria-label="Next">›</span></button></div>
            <div class="owl-dots disabled"></div>
        </div>
    </div>
</section>
<!--Section-->
<!--Section-->
{{-- ô tô thịnh hành --}}
<section class="sptb" style="padding-top: 2rem;padding-bottom: 3rem;">
   <div class="container">
      <div class="section-title center-block text-center">
         <h2>Ô tô thịnh hành</h2>
         <p>Moors và chạy ngay bây giờ. Nhật Bản duyệt bóng đá tại các phương tiện chính</p>
      </div>
      <div class="col-md-12">
         <div class="items-gallery">
            <div class="items-blog-tab text-center">
               <div class="items-blog-tab-heading row">
                  <div class="col-12">
                     <ul class="nav items-blog-tab-menu">
                        <li><a href="#tab-1" data-toggle="tab" class="active">Tất cả</a></li>
                        <li><a href="#tab-2" data-toggle="tab" class="">Audi</a></li>
                        <li><a href="#tab-3" data-toggle="tab" class="">Mercedes</a></li>
                        <li><a href="#tab-4" data-toggle="tab" class="">Bmw</a></li>
                        <li><a href="#tab-5" data-toggle="tab" class="">lamborghini</a></li>
                        <li><a href="#tab-6" data-toggle="tab" class="">toyota</a></li>
                     </ul>
                  </div>
               </div>
               <div class="tab-content">
                {{-- tất cả --}}
                   <div class="tab-pane active" id="tab-1"> 
                      <div class="row closed" id="cars-containers" style="height: 490px; overflow: hidden;">
                        @foreach($productTrendAll as $productTrendAll )
                         <div class="col-xl-4 col-lg-4 col-md-12">
                            <div class="card">
                               
                                <?php
                                if ($productTrendAll->brand_id == 1 || $productTrendAll->brand_id == 4 
                                    || $productTrendAll->brand_id == 7) {
                                ?> 
                                <span class="ribbon-3">
                                    <span>
                                        <i class="car car-suzuki"></i>
                                    </span> 
                                </span>
                                <?php
                                    }elseif($productTrendAll->brand_id || 2 && $productTrendAll->brand_id == 5
                                        || $productTrendAll->brand_id == 8){
                                ?>
                                <span class="ribbon-2">
                                    <span>
                                        <i class="car car-volvo"></i>
                                    </span> 
                                </span>
                                <?php
                                    }elseif($productTrendAll->brand_id || 3 && $productTrendAll->brand_id == 6){
                                ?>
                                <span class="ribbon-3">
                                    <span>
                                        <i class="car car-suzuki"></i>
                                    </span> 
                                </span>
                                <?php
                                    }
                                ?>  
                                 
                               <div class="item-card8-img  br-tr-7 br-tl-7"> 
                                    <img src="../img_product/{{$productTrendAll->image_product}}" alt="img" class="cover-image" 
                                    style="height: 240px"> 
                                </div>
                               <div class="item-card8-overlaytext">
                                <?php
                                if ($productTrendAll->brand_id == 1 || $productTrendAll->brand_id == 4 
                                    || $productTrendAll->brand_id == 7) {
                                ?> 
                                   <h6 class="bg-info fs-20 mb-0">{{number_format($productTrendAll->price,0,',','.')}} vnđ</h6>
                                <?php
                                    }elseif($productTrendAll->brand_id == 2 || $productTrendAll->brand_id == 5
                                        || $productTrendAll->brand_id == 8){
                                ?> 
                                    <h6 class=" bg-secondary fs-20 mb-0">{{number_format($productTrendAll->price,0,',','.')}} vnđ</h6>
                                <?php
                                    }elseif($productTrendAll->brand_id == 3 || $productTrendAll->brand_id == 6){
                                ?>
                                    <h6 class=" bg-primary fs-20 mb-0">{{number_format($productTrendAll->price,0,',','.')}} vnđ</h6>
                                <?php
                                    }
                                ?>
                               </div>
                               <div class="card-body">
                                  <div class="item-card8-desc" style="height: 155px">
                                     <p class="text-muted mb-1 fs-13">{{Carbon\Carbon::parse($productTrendAll->created_at)->format('d/m/Y') }}.</p>
                                     <a class="text-dark" href="cars.html">
                                        <h4 class="font-weight-semibold">{{$productTrendAll->name}}</h4>
                                     </a>
                                     <p class="mb-2">{!!$productTrendAll->description!!}</p>
                                     <div class="d-flex align-items-center pt-2 mt-auto">
                                        <img src="../img_user/{{$productTrendAll->User->img}}" 
                                            class="avatar brround avatar-md mr-3" alt="avatar-img"> 
                                        <div> 
                                            <a href="profile.html" class="text-default">{{$productTrendAll->User->firstname}}</a> 
                                            <small class="d-block text-muted">
                                                {{Carbon\Carbon::parse($productTrendAll->User->created_at)->format('d/m/Y') }}</small> 
                                        </div>
                                        <div class="ml-auto text-muted"> 
                                            <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                                <i class="fe fe-heart mr-1"></i>
                                            </a> 
                                            <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                                <i class="fa fa-thumbs-o-up"></i>
                                            </a> 
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                         </div>
                        @endforeach
                      </div>
                      {{-- <div class="showmore-button">
                         <div class="showmore-button-inner more">Show more</div>
                      </div> --}}
                   </div>
                   {{-- audi --}}
                   <div class="tab-pane" id="tab-2">
                      <div class="row">
                        @foreach($productTrendID1 as $productTrendID1 )
                        <div class="col-xl-4 col-lg-4 col-md-12">
                            <div class="card mb-xl-0">
                               <span class="ribbon-3"> <span>
                                    <i class="car car-suzuki"></i>
                                </span> 
                            </span> 
                               <div class="item-card8-img  br-tr-7 br-tl-7"> 
                                    <img src="../img_product/{{$productTrendID1->image_product}}" alt="img" class="cover-image"
                                    style="height: 240px"> 
                                </div>
                               <div class="item-card8-overlaytext">
                                  <h6 class="bg-info fs-20 mb-0">{{number_format($productTrendID1->price,0,',','.')}} đ</h6>
                               </div>
                               <div class="card-body">
                                  <div class="item-card8-desc" style="height: 155px">
                                     <p class="text-muted mb-1 fs-13">{{Carbon\Carbon::parse($productTrendID1->created_at)->format('d/m/Y') }}.</p>
                                     <a class="text-dark" href="cars.html">
                                        <h4 class="font-weight-semibold">{{$productTrendID1->name}}</h4>
                                     </a>
                                     <p class="mb-2">{!!$productTrendID1->description!!}</p>
                                     <div class="d-flex align-items-center pt-2 mt-auto">
                                        <img src="../img_user/{{$productTrendID1->User->img}}" class="avatar brround avatar-md mr-3" alt="avatar-img"> 
                                        <div> <a href="profile.html" class="text-default">{{$productTrendID1->User->firstname}}</a> <small class="d-block text-muted">{{Carbon\Carbon::parse($productTrendID1->User->created_at)->format('d/m/Y') }}</small> </div>
                                        <div class="ml-auto text-muted"> 
                                            <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                                <i class="fe fe-heart mr-1"></i>
                                            </a> 
                                            <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                                <i class="fa fa-thumbs-o-up"></i>
                                            </a> 
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                        </div>
                        @endforeach
                      </div>
                   </div>
                   {{-- mercedes --}}
                   <div class="tab-pane" id="tab-3">
                      <div class="row">
                        @foreach($productTrendID2 as $productTrendID2 )
                        <div class="col-xl-4 col-lg-4 col-md-12">
                            <div class="card mb-xl-0">
                               <span class="ribbon-3"> <span>
                                    <i class="car car-suzuki"></i>
                                </span> 
                            </span> 
                               <div class="item-card8-img  br-tr-7 br-tl-7"> 
                                    <img src="../img_product/{{$productTrendID2->image_product}}" alt="img" class="cover-image"
                                    style="height: 240px"> 
                                </div>
                               <div class="item-card8-overlaytext">
                                  <h6 class="bg-info fs-20 mb-0">{{number_format($productTrendID2->price,0,',','.')}} đ</h6>
                               </div>
                               <div class="card-body">
                                  <div class="item-card8-desc" style="height: 155px">
                                     <p class="text-muted mb-1 fs-13">{{Carbon\Carbon::parse($productTrendID2->created_at)->format('d/m/Y') }}.</p>
                                     <a class="text-dark" href="cars.html">
                                        <h4 class="font-weight-semibold">{{$productTrendID2->name}}</h4>
                                     </a>
                                     <p class="mb-2">{!!$productTrendID2->description!!}</p>
                                     <div class="d-flex align-items-center pt-2 mt-auto">
                                        <img src="../img_user/{{$productTrendID2->User->img}}" class="avatar brround avatar-md mr-3" alt="avatar-img"> 
                                        <div> <a href="profile.html" class="text-default">{{$productTrendID2->User->firstname}}</a> <small class="d-block text-muted">{{Carbon\Carbon::parse($productTrendID2->User->created_at)->format('d/m/Y') }}</small> </div>
                                        <div class="ml-auto text-muted"> 
                                            <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                                <i class="fe fe-heart mr-1"></i>
                                            </a> 
                                            <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                                <i class="fa fa-thumbs-o-up"></i>
                                            </a> 
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                        </div>
                        @endforeach
                      </div>
                   </div>
                   {{-- bmw --}}
                   <div class="tab-pane" id="tab-4">
                      <div class="row">
                        @foreach($productTrendID3 as $productTrendID3 )
                        <div class="col-xl-4 col-lg-4 col-md-12">
                            <div class="card mb-xl-0">
                               <span class="ribbon-3"> <span>
                                    <i class="car car-suzuki"></i>
                                </span> 
                            </span> 
                               <div class="item-card8-img  br-tr-7 br-tl-7"> 
                                    <img src="../img_product/{{$productTrendID3->image_product}}" alt="img" class="cover-image"
                                    style="height: 240px"> 
                                </div>
                               <div class="item-card8-overlaytext">
                                  <h6 class="bg-info fs-20 mb-0">{{number_format($productTrendID3->price,0,',','.')}} đ</h6>
                               </div>
                               <div class="card-body">
                                  <div class="item-card8-desc" style="height: 155px">
                                     <p class="text-muted mb-1 fs-13">{{Carbon\Carbon::parse($productTrendID3->created_at)->format('d/m/Y') }}.</p>
                                     <a class="text-dark" href="cars.html">
                                        <h4 class="font-weight-semibold">{{$productTrendID3->name}}</h4>
                                     </a>
                                     <p class="mb-2">{!!$productTrendID3->description!!}</p>
                                     <div class="d-flex align-items-center pt-2 mt-auto">
                                        <img src="../img_user/{{$productTrendID3->User->img}}" class="avatar brround avatar-md mr-3" alt="avatar-img"> 
                                        <div> <a href="profile.html" class="text-default">{{$productTrendID3->User->firstname}}</a> <small class="d-block text-muted">{{Carbon\Carbon::parse($productTrendID3->User->created_at)->format('d/m/Y') }}</small> </div>
                                        <div class="ml-auto text-muted"> 
                                            <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                                <i class="fe fe-heart mr-1"></i>
                                            </a> 
                                            <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                                <i class="fa fa-thumbs-o-up"></i>
                                            </a> 
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                        </div>
                        @endforeach
                      </div>
                   </div>
                   {{-- lamborghini --}}
                   <div class="tab-pane" id="tab-5">
                      <div class="row">
                        @foreach($productTrendID4 as $productTrendID4 )
                        <div class="col-xl-4 col-lg-4 col-md-12">
                            <div class="card mb-xl-0">
                               <span class="ribbon-3"> <span>
                                    <i class="car car-suzuki"></i>
                                </span> 
                            </span> 
                               <div class="item-card8-img  br-tr-7 br-tl-7"> 
                                    <img src="../img_product/{{$productTrendID4->image_product}}" alt="img" class="cover-image"
                                    style="height: 240px"> 
                                </div>
                               <div class="item-card8-overlaytext">
                                  <h6 class="bg-info fs-20 mb-0">{{number_format($productTrendID4->price,0,',','.')}} đ</h6>
                               </div>
                               <div class="card-body">
                                  <div class="item-card8-desc" style="height: 155px">
                                     <p class="text-muted mb-1 fs-13">{{Carbon\Carbon::parse($productTrendID4->created_at)->format('d/m/Y') }}.</p>
                                     <a class="text-dark" href="cars.html">
                                        <h4 class="font-weight-semibold">{{$productTrendID4->name}}</h4>
                                     </a>
                                     <p class="mb-2">{!!$productTrendID4->description!!}</p>
                                     <div class="d-flex align-items-center pt-2 mt-auto">
                                        <img src="../img_user/{{$productTrendID4->User->img}}" class="avatar brround avatar-md mr-3" alt="avatar-img"> 
                                        <div> <a href="profile.html" class="text-default">{{$productTrendID4->User->firstname}}</a> <small class="d-block text-muted">{{Carbon\Carbon::parse($productTrendID4->User->created_at)->format('d/m/Y') }}</small> </div>
                                        <div class="ml-auto text-muted"> 
                                            <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                                <i class="fe fe-heart mr-1"></i>
                                            </a> 
                                            <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                                <i class="fa fa-thumbs-o-up"></i>
                                            </a> 
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                        </div>
                        @endforeach
                      </div>
                   </div>
                    {{-- toyota --}}
                   <div class="tab-pane" id="tab-6">
                      <div class="row">
                        @foreach($productTrendID5 as $productTrendID5 )
                        <div class="col-xl-4 col-lg-4 col-md-12">
                            <div class="card mb-xl-0">
                               <span class="ribbon-3"> <span>
                                    <i class="car car-suzuki"></i>
                                </span> 
                            </span> 
                               <div class="item-card8-img  br-tr-7 br-tl-7"> 
                                    <img src="../img_product/{{$productTrendID5->image_product}}" alt="img" class="cover-image"
                                    style="height: 240px"> 
                                </div>
                               <div class="item-card8-overlaytext">
                                  <h6 class="bg-info fs-20 mb-0">{{number_format($productTrendID5->price,0,',','.')}} đ</h6>
                               </div>
                               <div class="card-body">
                                  <div class="item-card8-desc" style="height: 155px">
                                     <p class="text-muted mb-1 fs-13">{{Carbon\Carbon::parse($productTrendID5->created_at)->format('d/m/Y') }}.</p>
                                     <a class="text-dark" href="cars.html">
                                        <h4 class="font-weight-semibold">{{$productTrendID5->name}}</h4>
                                     </a>
                                     <p class="mb-2">{!!$productTrendID5->description!!}</p>
                                     <div class="d-flex align-items-center pt-2 mt-auto">
                                        <img src="../img_user/{{$productTrendID5->User->img}}" class="avatar brround avatar-md mr-3" alt="avatar-img"> 
                                        <div> <a href="profile.html" class="text-default">{{$productTrendID5->User->firstname}}</a> <small class="d-block text-muted">{{Carbon\Carbon::parse($productTrendID5->User->created_at)->format('d/m/Y') }}</small> </div>
                                        <div class="ml-auto text-muted"> 
                                            <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                                <i class="fe fe-heart mr-1"></i>
                                            </a> 
                                            <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                                <i class="fa fa-thumbs-o-up"></i>
                                            </a> 
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                        </div>
                        @endforeach
                      </div>
                   </div>
                </div>
            </div>
         </div>
      </div>
   </div>
</section>

<!--Section-->
{{-- tin tức gần đây --}}
<section class="sptb" style="padding-top: 2rem;padding-bottom: 3rem;">
   <div class="container">
      <div class="section-title center-block text-center">
         <h2>Tin tức gần đây</h2>
         <p>Moors và chạy ngay bây giờ. Nhật Bản duyệt bóng đá tại các phương tiện chính</p>
      </div>
      <div id="defaultCarousel1" class="owl-carousel Card-owlcarousel owl-carousel-icons owl-loaded owl-drag">
         <div class="owl-stage-outer">
            <div class="owl-stage" style="transform: translate3d(-3202px, 0px, 0px); transition: all 0.25s ease 0s; width: 4804px;">
                @foreach($recent as $blog_h)
                <div class="owl-item cloned active" style="width: 375.333px; margin-right: 25px;">
                  <div class="item">
                     <div class="card mb-0">
                        <div class="item7-card-img"> 
                            <a href="#"></a> 
                            <img src="../img_blog/{{$blog_h->img}}" alt="img" class="cover-image" style="height: 240px"> 
                        </div>
                        <div class="card-body p-4">
                           <div class="item7-card-desc d-flex mb-2">
                                <a href="#">
                                    <i class="fa fa-calendar-o text-muted mr-2"></i>
                                    {{Carbon\Carbon::parse($blog_h->created_at)->format('d/m/Y') }}</a> 
                                <div class="ml-auto"> 
                                <a href="#">
                                    <i class="fa fa-comment-o text-muted mr-2"></i>8 bình luận
                                </a> 
                            </div>
                           </div>
                           <a href="blog-details.html" class="text-dark">
                              <h4 class="font-weight-semibold">{{$blog_h->name}}</h4>
                           </a>
                           <p>{!!$blog_h->description!!}</p>
                           <div class="d-flex align-items-center pt-2 mt-auto">
                                <img src="../assets/images/faces/male/6.jpg" class="avatar brround avatar-md mr-3" alt="avatar-img"> 
                                <div> 
                                    <a href="profile.html" class="text-default">{{$blog_h->User->firstname}}</a> 
                                    <small class="d-block text-muted">{{Carbon\Carbon::parse($blog_h->User->created_at)->format('d/m/Y') }}</small> 
                                </div>
                                <div class="ml-auto text-muted"> 
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                        <i class="fe fe-heart mr-1"></i>
                                    </a> 
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3">
                                        <i class="fa fa-thumbs-o-up"></i>
                                    </a> 
                                </div>
                           </div>
                        </div>
                     </div>
                  </div>
                </div>
                @endforeach
            </div>
         </div>
         <div class="owl-nav">
            <button type="button" role="presentation" class="owl-prev">
                <span aria-label="Previous">‹</span>
            </button>
            <button type="button" role="presentation" class="owl-next">
                <span aria-label="Next">›</span>
            </button>
        </div>
         <div class="owl-dots disabled"></div>
      </div>
   </div>
</section>
<!--Section-->
<!--Section-->
{{-- ô tô  --}}
<section>
    <div class="about-1 cover-image sptb bg-background-color" data-image-src="assets/images/banners/banner2.jpg"
        style="background: url(&quot;assets/images/banners/banner2.jpg&quot;) center center;">
        <div class="content-text mb-0 text-white info">
            <div class="container">
                <div class="row text-center">
                    <div class="col-lg-3 col-md-6">
                        <div class="counter-status md-mb-0">
                            <div class="counter-icon"> <i class="ti-user"></i> </div>
                            <h5>Customers</h5>
                            <h2 class="counter mb-0">{{$countCus}}</h2>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="counter-status status-1 md-mb-0">
                            <div class="counter-icon text-warning"> <i class="ti-car"></i> </div>
                            <h5>Car Sales</h5>
                            <h2 class="counter mb-0">{{$carsSale}}</h2>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="counter-status status md-mb-0">
                            <div class="counter-icon text-primary"> <i class="ti-package"></i> </div>
                            <h5>Rented Cars</h5>
                            <h2 class="counter mb-0">{{$rentedCars}}</h2>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="counter-status status">
                            <div class="counter-icon text-success"> <i class="ti-face-smile"></i> </div>
                            <h5>Happy Customers</h5>
                            <h2 class="counter mb-0">8459</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Section-->
<!--Section-->

<!--Section-->
<!--Section-->
{{-- ô tô phổ biến theo loại thân xe --}}
<section class="sptb  bg-white" style="padding-top: 2rem;padding-bottom: 3rem;">
    <div class="container">
        <div class="section-title center-block text-center">
            <h2>Ô tô phổ biến theo loại thân xe</h2>
            <p>Moors và chạy ngay bây giờ. Nhật Bản duyệt bóng đá tại các phương tiện chính</p>
        </div>
        <div class="row">
            {{-- danh mục category = 1(xe con) --}}
            @foreach($category_home as $cate_h)
            <div class="col-xl-2 col-lg-4 col-md-6 col-sm-6"> 
            <?php
                if($cate_h->id == 0)
                {
            ?>   
                <a href="{{URL::to('show-category/'.$cate_h->id)}}"
                    class="car-body-shapes card text-center p-4 bg-primary box-shadow2"
                    >
                    <div class="car-body-img"style="width: 100px;height: 100px">
                        <img src="../img_category/{{$cate_h->img}}" alt="img"></div>
                    <div class="servic-data">
                        <h4 class="font-weight-semibold mb-0 text-white">{{$cate_h->name}}</h4>
                    </div>
                </a>
            <?php
                }elseif($cate_h->id == 1){
            ?>   
                <a href="{{URL::to('show-category/'.$cate_h->id)}}"
                    class="car-body-shapes card text-center p-4 bg-secondary box-shadow2">
                    <div class="car-body-img">
                        <img src="../img_category/{{$cate_h->img}}" alt="img"></div>
                    <div class="servic-data">
                        <h4 class="font-weight-semibold mb-0 text-white">{{$cate_h->name}}</h4>
                    </div>
                </a>
            <?php
                }elseif($cate_h->id == 2){
            ?>   
                <a href="{{URL::to('show-category/'.$cate_h->id)}}"
                    class="car-body-shapes card text-center p-4 bg-info box-shadow2">
                    <div class="car-body-img">
                        <img src="../img_category/{{$cate_h->img}}" alt="img"></div>
                    <div class="servic-data">
                        <h4 class="font-weight-semibold mb-0 text-white">{{$cate_h->name}}</h4>
                    </div>
                </a> 
            <?php
                }elseif($cate_h->id == 3){
            ?>  
                <a href="{{URL::to('/show-category/'.$cate_h->id)}}"
                    class="car-body-shapes card text-center p-4 bg-success box-shadow2">
                    <div class="car-body-img">
                        <img src="../img_category/{{$cate_h->img}}" alt="img"></div>
                    <div class="servic-data">
                        <h4 class="font-weight-semibold mb-0 text-white">{{$cate_h->name}}</h4>
                    </div>
                </a>
            <?php
                }elseif($cate_h->id == 4){
            ?>  
                <a href="{{URL::to('show-category/'.$cate_h->id)}}"
                    class="car-body-shapes card text-center p-4 bg-danger box-shadow2">
                    <div class="car-body-img">
                        <img src="../img_category/{{$cate_h->img}}" alt="img"></div>
                    <div class="servic-data">
                        <h4 class="font-weight-semibold mb-0 text-white">{{$cate_h->name}}</h4>
                    </div>
                </a> 
            <?php
                }elseif($cate_h->id == 5){
            ?>      
                <a href="{{URL::to('show-category/'.$cate_h->id)}}"
                    class="car-body-shapes card text-center p-4 bg-purple box-shadow2">
                    <div class="car-body-img">
                        <img src="../img_category/{{$cate_h->img}}" alt="img"></div>
                    <div class="servic-data">
                        <h4 class="font-weight-semibold mb-0 text-white">{{$cate_h->name}}</h4>
                    </div>
                </a>
            <?php
                }elseif($cate_h->id == 6){
            ?>     
                <a href="{{URL::to('show-category/'.$cate_h->id)}}"
                    class="car-body-shapes card text-center p-4 bg-blue box-shadow2">
                    <div class="car-body-img">
                        <img src="../img_category/{{$cate_h->img}}" alt="img"></div>
                    <div class="servic-data">
                        <h4 class="font-weight-semibold mb-0 text-white">{{$cate_h->name}}</h4>
                    </div>
                </a>
            <?php
                }
            ?>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!--Section-->
<!--Section-->
<section class="sptb border-top" style="padding-top: 2rem;padding-bottom: 3rem;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="bg-white p-0 border box-shadow2">
                    <div class="card-body">
                        <h6 class="fs-18 mb-4">Bạn có muốn bán một chiếc xe hơi?</h6>
                        <hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
                        <p>nó trông giống như tiếng Anh có thể đọc được.  Nhiều gói xuất bản trên máy tính để bàn và trình chỉnh sửa trang web hiện sử dụng Lorem Ipsum làm văn bản mô hình mặc định của họ</p><a href="#"
                            class="btn btn-primary text-white">Bán xe</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="bg-white p-0 mt-5 mt-md-0 border box-shadow2">
                    <div class="card-body">
                        <h6 class="fs-18 mb-4">Bạn đang tìm kiếm một chiếc xe hơi?</h6>
                        <hr class="deep-purple  accent-2 border-success mb-4 mt-0 d-inline-block mx-auto">
                        <p>nó trông giống như tiếng Anh có thể đọc được. Nhiều gói xuất bản trên máy tính để bàn và trình chỉnh sửa trang web hiện sử dụng Lorem Ipsum làm văn bản mô hình mặc định của họ</p><a href="#"
                            class="btn btn-success text-white">Mua xe</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Section-->
<!--Section-->
<section class="sptb bg-white" style="padding-top: 2rem;padding-bottom: 3rem;">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="">
                    <div class="mb-lg-0 mb-4">
                        <div class="service-card text-center">
                            <div class="bg-light icon-bg icon-service text-purple about box-shadow2"> <img
                                    src="assets/images/products/about/badge.png" alt="img"> </div>
                            <div class="servic-data mt-3">
                                <h4 class="font-weight-semibold mb-2">Số một thế giới</h4>
                                <p class="text-muted mb-0">Nam libero tempore, cum soluta nobis est eligendi
                                    cumque facere possimus</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="">
                    <div class="mb-lg-0 mb-4">
                        <div class="service-card text-center">
                            <div class="bg-light icon-bg icon-service text-purple about box-shadow2"> <img
                                    src="assets/images/products/about/car.png" alt="img"> </div>
                            <div class="servic-data mt-3">
                                <h4 class="font-weight-semibold mb-2">Ô tô đã bán</h4>
                                <p class="text-muted mb-0">Nam libero tempore, cum soluta nobis est eligendi
                                    cumque facere possimus</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="">
                    <div class="mb-sm-0 mb-4">
                        <div class="service-card text-center">
                            <div class="bg-light icon-bg icon-service text-purple about box-shadow2"> <img
                                    src="assets/images/products/about/discount.png" alt="img"> </div>
                            <div class="servic-data mt-3">
                                <h4 class="font-weight-semibold mb-2">Ưu đãi</h4>
                                <p class="text-muted mb-0">Nam libero tempore, cum soluta nobis est eligendi
                                    cumque facere possimus</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="">
                    <div class="">
                        <div class="service-card text-center">
                            <div class="bg-light icon-bg icon-service text-purple about box-shadow2"> <img
                                    src="assets/images/products/about/double-arrow.png" alt="img">
                            </div>
                            <div class="servic-data mt-3">
                                <h4 class="font-weight-semibold mb-2">Đối chiếu</h4>
                                <p class="text-muted mb-0">Nam libero tempore, cum soluta nobis est eligendi
                                    cumque facere possimus</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    // $(document).ready(function(){
        function likeCar(){
            var like = $()
        }
        var btn = document.getElementById('btn');
        function toggle(){
            if(btn.classList.contains("fa")){
                btn.classList.remove("fa");
                btn.classList.add("fas");
            }else{
                btn.classList.remove("fas");
                btn.classList.add("fa");
            }
        }
    // });
</script>
<!--Section-->
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script type="text/javascript" src="{{asset('client/assets/js/accessary.js')}}"></script>
@endsection
