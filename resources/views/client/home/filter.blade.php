<div class="col-xl-3 col-lg-3 col-md-12">
    <div class="card overflow-hidden">
        <form method="get" action="{{route('filter')}}" id="formFilter">
            @csrf
            <div class="px-4 py-3 border-bottom">
                <h4 class="mb-0">Categories</h4>
            </div>
            <div class="card-body">
                <div class="closed" id="container" style="height: 250px; overflow: hidden;">
                    <div class="filter-product-checkboxs">
                        @foreach($brand_car as $value_brand)
                        <?php $count = 0 ?>
                        <label class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" class="custom-control-input categories-checked" name="checkbox1[]" value="{{$value_brand->id}}">
                            <span class="custom-control-label">
                                @foreach($poster as $value_poster)
                                @if($value_poster->brand_id == $value_brand->id)
                                <?php $count++ ?>
                                @endif
                                @endforeach
                                <a href="#" class="text-dark">{{$value_brand->name}}
                                    <span class="label label-secondary float-right">{{$count}}</span>
                                </a>
                            </span>
                        </label>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="px-4 py-3 border-bottom border-top">
                <h4 class="mb-0">Price</h4>
            </div>
            <div class="card-body">
                <input type="text" id="vol" name="price_min" min="0" max="50" class="form-control" placeholder="0"
                    style="padding-left: 10px;width: 100px;margin: 0px;display: inline-block;">
                <i class="fas fa-chevron-right"></i>
                <input type="text" id="vol" name="price_max" min="0" max="50" class="form-control" placeholder="0"
                    style="padding-left: 10px;width: 100px;margin: 0px;display: inline-block;">
            </div>
            <hr style="margin: 0">
            <div class="px-4 py-3 border-bottom">
                <h4 class="mb-0">Condition</h4>
            </div>
            <div class="card-body">
                <div>
                    <input type="radio" value="3" name="radio22" style="margin: 7px;">All<br>
                    <input type="radio" value="1" name="radio22" style="margin: 7px;">New<br>
                    <input type="radio" value="2" name="radio22" style="margin: 7px;">Used
                </div>
            </div>
            <div class="px-4 py-3 border-bottom border-top">
                <h4 class="mb-0">Fuel Type</h4>
            </div>
            <div class="card-body">
                <div class="filter-product-checkboxs">
                    <label class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" name="checkbox11[]" value="1">
                        <span class="custom-control-label"> 1 </span>
                    </label>
                    <label class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" name="checkbox11[]" value="2">
                        <span class="custom-control-label"> 2 </span>
                    </label>
                    <label class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" name="checkbox11[]" value="3">
                        <span class="custom-control-label"> 3 </span>
                    </label>
                    <label class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" name="checkbox11[]" value="4">
                        <span class="custom-control-label"> 4 </span>
                    </label>
                    <label class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" name="checkbox11[]" value="5">
                        <span class="custom-control-label"> 5 </span>
                    </label>
                </div>
            </div>
            <div class="px-4 py-3 border-bottom border-top">
                <h4 class="mb-0">Body Type</h4>
            </div>
            <div class="card-body">
                <div class="filter-product-checkboxs">
                    @foreach($categories as $value_cate)
                    <label class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" name="checkbox12[]"
                            value="{{$value_cate->id}}">
                        <span class="custom-control-label"> {{$value_cate->name}} </span>
                    </label>
                    @endforeach
                </div>
            </div>
            <div class="px-4 py-3 border-bottom border-top">
                <h4 class="mb-0">Transmission</h4>
            </div>
            <div class="card-body">
                <div class="filter-product-checkboxs">
                    <label class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" name="checkbox13[]" value="1">
                        <span class="custom-control-label"> 1 </span>
                    </label>
                    <label class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" name="checkbox13[]" value="2">
                        <span class="custom-control-label"> 2 </span>
                    </label>
                    <label class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" name="checkbox13[]" value="3">
                        <span class="custom-control-label"> 3 </span>
                    </label>
                    <label class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" name="checkbox13[]" value="4">
                        <span class="custom-control-label"> 4 </span>
                    </label>
                    <label class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" name="checkbox13[]" value="5">
                        <span class="custom-control-label"> 5 </span>
                    </label>
                    <label class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" name="checkbox13[]" value="6">
                        <span class="custom-control-label"> 6 </span>
                    </label>
                </div>
            </div>
            <div class="card-footer">
                <input style="padding: 10px 10px;width: 100%;border: 1px solid #8e0fa0;border-radius: 5px;color: white;font-weight: 600;background-color: #8e0fa0"type="submit" name="filter" value="Apply Filter">
            </div>
        </form>
    </div>
    <div class="card mb-0">
        <div class="card-header">
            <h3 class="card-title">Shares</h3>
        </div>
        <div class="card-body product-filter-desc">
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <div class="addthis_inline_share_toolbox"></div>
        </div>
    </div>
</div>
<input type="" name="">
<input type="" name="">
<input type="" name="">
<input type="" name="">
<input type="" name="">
