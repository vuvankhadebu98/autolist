<div class=" mb-0">
    <div class="">
        <div class="bg-white p-5 item2-gl-nav d-flex">
            <h6 class="mb-0 mt-3 text-left">Showing 1 to 10 of 500 entries ---
                {{count($poster)}}</h6>
            <ul class="nav item2-gl-menu ml-auto mt-1">
                <li class="">
                    <a href="#tab-11" class="active show" id="danh_sach_ngang" data-toggle="tab" title="List style">
                        <i class="fa fa-list"></i>
                    </a>
                </li>
                <li>
                    <a href="#tab-12" class="" data-toggle="tab" id="danh_sach_doc" title="Grid">
                        <i class="fa fa-th"></i>
                    </a>
                </li>
            </ul>
            <div class="d-sm-flex"> <label class="mr-2 mt-2 mb-sm-1">Sort
                    By:</label>
                <div class="selectgroup">
                    <label class="selectgroup-item mb-md-0" id="order_by_1">
                        <input type="radio" name="value" value="Popularity" class="selectgroup-input">
                        <span class="selectgroup-button">Price</span>
                    </label>
                    <label class="selectgroup-item mb-md-0" id="order_by_4">
                        <input type="radio" name="value" value="Popularity" class="selectgroup-input">
                        <span class="selectgroup-button">Date</span>
                    </label>
                    <label class="selectgroup-item mb-md-0" id="order_by_2">
                        <input type="radio" name="value" value="Popularity" class="selectgroup-input">
                        <span class="selectgroup-button">Popularity</span>
                    </label>
                    <label class="selectgroup-item mb-0" id="order_by_3">
                        <input type="radio" name="value" value="Rating" class="selectgroup-input">
                        <span class="selectgroup-button">Rating</span>
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
@if($type_show==0)
<div class="tab-pane active" id="tab-11" style="margin-top: 30px;">
    @foreach($poster as $value_p)
    <div class="card overflow-hidden">
        <div class="d-md-flex">
            <div class="item-card9-img">
                <div class="arrow-ribbon bg-primary">Sale</div>
                <div class="item-card9-imgs"> <a class="link" href="{{route('getDetail',['id'=>$value_p->id])}}"></a>
                    <img src="../../public/img_thuong_hieu/audi.jfif" alt="img" class="cover-image"
                        style="width: 450px"> </div>
                <div class="item-card9-icons"> <a href="{{route('getDetail',['id'=>$value_p->id])}}"
                        class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a>
                </div>
                <div class="item-overly-trans">
                    <div class="rating-stars"> <input type="number" readonly="readonly" class="rating-value star"
                            name="rating-stars-value" value="3">
                        <div class="rating-stars-container">
                            <?php $count = 1 ?>
                            @for ($j = 1; $j <= $value_p->rate; $j++)
                                <i class="fa fa-star" style="color: #f1c40f;font-size: 15px;margin: 0 2px"></i>
                                <?php $count ++ ?>
                                @endfor
                                @if($count <= 5) @for ($j=$count; $j <=5; $j++) <i class="fa fa-star"
                                    style="color: #8492a6;font-size: 15px;margin: 0 2px"></i>
                                    @endfor
                                    @endif
                        </div>
                    </div> <span><a href="cars.html" class="bg-gray">Used</a></span>
                </div>
            </div>
            <div class="card border-0 mb-0">
                <div class="card-body ">
                    <div class="item-card9"> <a href="cars.html" class="text-dark">
                            <h4 class="font-weight-semibold mt-1"> {{$value_p->title}}
                            </h4>
                        </a>
                        <div class="item-card9-desc mb-2">
                            <a href="{{route('getDetail',['id'=>$value_p->id])}}" class="mr-4">
                                <span class="">
                                    <i class="fa fa-map-marker text-muted mr-1"></i>{{$value_p->city}}
                                </span>
                            </a>
                            <a href="#" class="mr-4">
                                <span class="">
                                    <i class="fa fa-calendar-o text-muted mr-1"></i>{{$value_p->created_at}}
                                </span>
                            </a>
                            <a href="#" class="mr-4">
                                <span class="">
                                    <i class="fas fa-eye text-muted mr-1"></i>{{$value_p->view}}
                                </span>
                            </a>

                        </div>
                        <p class="mb-0 leading-tight">{{$value_p->description}}
                        </p>
                    </div>
                </div>
                <div class="card-footer pt-4 pb-4 pr-4 pl-4">
                    <div class="item-card9-footer d-sm-flex">
                        <div class="item-card9-cost">
                            <h4 class="text-dark font-weight-bold mb-0 mt-0">
                                {{$value_p->price}}</h4>
                        </div>
                        <div class="ml-auto"> <a href="#" class="mr-2" title="Car type"><i
                                    class="fa fa-car  mr-1 text-muted"></i>
                                {{$value_p->title}}</a> <a href="#" class="mr-2" title="Kilometrs"><i
                                    class="fa fa-road text-muted mr-1 "></i>{{$value_p->km_went}}</a>
                            <a href="#" class="" title="FuealType"><i
                                    class="fas fa-tachometer-alt"></i>{{$value_p->fuel_type}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif
@if($type_show==1)
<div class="tab-pane" id="tab-12" style="margin-top: 30px">
    <div class="row" id="tab-12-append">
        @foreach($poster as $value_p)
        <div class="col-lg-6 col-md-12 col-xl-4">
            <div class="card overflow-hidden">
                <div class="item-card9-img">
                    <div class="arrow-ribbon bg-primary">{{$value_p->title}}</div>
                    <div class="item-card9-imgs"> <a class="link"
                            href="{{route('getDetail',['id'=>$value_p->id])}}"></a>
                        <img src="../../public/img_thuong_hieu/audi.jfif" alt="img" class="cover-image"> </div>
                    <div class="item-card9-icons"> <a href="{{route('getDetail',['id'=>$value_p->id])}}"
                            class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a> </div>
                    <div class="item-overly-trans">
                        <div class="rating-stars"> <input type="number" readonly="readonly" class="rating-value star"
                                name="rating-stars-value" value="3">
                            <div class="rating-stars-container">
                                {{$value_p->title}}
                            </div>
                        </div> <span><a href="cars.html" class="bg-gray">Used</a></span>
                    </div>
                </div>
                <div class="card border-0 mb-0">
                    <div class="card-body ">
                        <div class="item-card9"> <a href="cars.html" class="text-dark">
                                <h4 class="font-weight-semibold mt-1">
                                    {{$value_p->title}}</h4>
                            </a>
                            <div class="item-card9-desc mb-2"> <a href="{{route('getDetail',['id'=>$value_p->id])}}"
                                    class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i>
                                        {{$value_p->title}}}</span></a> <a href="#" class="mr-4"><span class=""><i
                                            class="fa fa-calendar-o text-muted mr-1"></i>
                                        {{$value_p->title}}</span></a>
                                <a href="#" class="mr-4">
                                    <span class="">
                                        <i class="fas fa-eye text-muted mr-1"></i>{{$value_p->title}}
                                    </span>
                                </a></div>
                            <p class="mb-0 leading-tight">{{$value_p->title}}</p>
                        </div>
                    </div>
                    <div class="card-footer pt-4 pb-4 pr-4 pl-4">
                        <div class="item-card9-footer d-sm-flex">
                            <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Car type"><i
                                        class="fa fa-car  mr-1 text-muted"></i>
                                    {{$value_p->title}}</a> <a href="#" class="w-50 mt-1 mb-1 float-left"
                                    title="Kilometrs"><i class="fa fa-road text-muted mr-1 "></i>{{$value_p->title}}</a>
                                <a href="#" class="w-50 mt-1 mb-1 float-left" title="FuealType"><i
                                        class="fas fa-tachometer-alt"></i>{{$value_p->title}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endif
<nav aria-label="Page navigation example">
    {{$poster->links()}}
</nav>
<input type="hidden" id="type_show" value="{{$type_show}}">
<input type="hidden" id="order_by" value="{{$order_by}}">
<script type="text/javascript">
    $("#danh_sach_ngang").click(function () {
        $("input#type_show").val(0);
    })
    $("#danh_sach_doc").click(function () {
        $("input#type_show").val(1);
    })

    $(document).ready(function () {
        // Xử lí xắp xếp quảng cáo theo tiền, lượt quan tâm và sao
        $("label.selectgroup-item").click(function (e) {
            e.preventDefault();
            var type_order_by = $(this).attr('id').substring(9, 10);
            $("input#order_by").val(type_order_by);

            var url_order_by = $("input#url_order_by").val();
            var id_brand = $("input#val_search_brand").val();
            var seats = $("input#val_search_seats").val();
            var price = $("input#val_search_price").val();
            var city = $("input#val_search_city").val();
            var order_by = $("input#order_by").val();
            var type_show = $("input#type_show").val();
            $.ajax({
                url: url_order_by,
                data: {
                    _token: CSRF_TOKEN,
                    id_brand: id_brand,
                    seats: seats,
                    price: price,
                    city: city,
                    order_by: order_by,
                    type_show: type_show
                },
                type: 'get',
                success: function (data) {
                    $("div#search_list_poster").html(data);
                }
            })
        });
    })

</script>
