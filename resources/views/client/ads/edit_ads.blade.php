@extends('client.master')
@section('content')
</div>
<!--/Topbar-->
<!--Section-->
@foreach($poster as $value)

<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg"
        style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1>Edit Post "<span style="color: #ffbdbd">{{$value->title}}</span>"</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">Ad Post</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Section-->
<!--Section-->
<section class="sptb">
    <div class="container">
        <div class="row ">
            <div class="col-lg-8 col-md-12 col-md-12">
                <form method="post" enctype="multipart/form-data" action="{{route('postEditAdvertisement')}}" >
                    <input type="hidden" name="id_poster" value="{{$value->id}}">
                    @csrf
                    <div class="card ">
                        <div class="card-header ">
                            <h3 class="card-title">Edit Post</h3>
                        </div>
                        <h3 style="margin: auto;padding-top: 30px;color: red;font-size: 40px;">{{$value->title}}</h3>
                        <div class="card-body">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="height: 500px; margin-bottom:20px ">
                                <div class="product-slider">
                                    <div id="carousel" class="carousel slide" data-ride="carousel">
                                        <div class="arrow-ribbon2 bg-primary">{{$value->price}}</div>
                                        <div class="carousel-inner">  
                                            @if($value->img == null)
                                                <img src="../../public/client/xe.png" alt="img" style="width: 60px;height: 60px;margin-top: 12px;"> 
                                            @else
                                                @foreach((array)$value->img as $key=>$img)
                                                    @if($key == 1)
                                                        <div class="carousel-item active"> 
                                                            <img style="height: 500px" src="../../public/img_poster/{{$img}}" alt="img"> 
                                                        </div>
                                                    @else
                                                        <div class="carousel-item"> 
                                                            <img style="height: 500px" src="../../public/img_poster/{{$img}}" alt="img"> 
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div> <a class="carousel-control-prev" href="#carousel" role="button"
                                            data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a>
                                        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next"> <i
                                                class="fa fa-angle-right" aria-hidden="true"></i> </a>
                                    </div>
                                    <div class="clearfix">
                                        <div id="thumbcarousel" class="carousel thumbcarousel slide" data-interval="false">
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    @if($value->img == null)
                                                        <img src="../../public/client/xe.png" alt="img" style="width: 60px;height: 60px;margin-top: 12px;"> 
                                                    @else
                                                        <?php $to = 0 ?>
                                                        @foreach((array)$value->img as $key=>$img)
                                                            <div data-target="#carousel" data-slide-to="{{$to}}" class="thumb">
                                                                <img style="height: 100px"  src="../../public/img_poster/{{$img}}" alt="img">
                                                            </div>
                                                            <?php $to++ ?>
                                                        @endforeach
                                                    @endif
                                                </div>
                                                <div class="carousel-item">
                                                     @if($value->img == null)
                                                        <img src="../../public/client/xe.png" alt="img" style="width: 60px;height: 60px;margin-top: 12px;"> 
                                                    @else
                                                        <?php $to = 0 ?>
                                                        @foreach((array)$value->img as $key=>$img)
                                                            <div data-target="#carousel" data-slide-to="{{$to}}" class="thumb">
                                                                <img style="height: 100px" src="../../public/img_poster/{{$img}}" alt="img">
                                                            </div>
                                                            <?php $to++ ?>
                                                        @endforeach
                                                    @endif                                          
                                                </div>
                                            </div> <a class="carousel-control-prev" href="#thumbcarousel" role="button"
                                                data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i>
                                            </a> <a class="carousel-control-next" href="#thumbcarousel" role="button"
                                                data-slide="next"> <i class="fa fa-angle-right" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"> 
                                <div style="height: 120px"></div>
                                <label class="form-label text-dark">Type of Condition</label>
                                <div class="d-md-flex ad-post-details"> 
                                    <label class="custom-control custom-radio mb-2 mr-4"> 
                                        <input type="radio" class="custom-control-input" name="radios2" value="0" @if($value->check ==0)checked=""@endif>
                                        <span class="custom-control-label"><a href="#" class="text-muted">New</a></span> 
                                    </label> 
                                    <label class="custom-control custom-radio  mb-2">
                                        <input type="radio" class="custom-control-input" name="radios2" value="1" @if($value->check == 1)checked=""@endif>
                                        <span class="custom-control-label"><a href="#" class="text-muted">Used</a></span>
                                    </label> 
                                </div>
                            </div>
                            <div class="form-group"> <label class="form-label text-dark">Ad Title</label> <input type="text"
                                    class="form-control" placeholder="" value="{{$value->title}}" name="title"> </div>
                            <label class="form-label text-dark">Image</label>
                            <div class="p-2 border mb-4 form-group">

                                <input type="file" id="image_list" name="files[]" value="" multiple />
                                    <?php $qty_img = count($value->img);?>
                                    <input type="hidden" name="qty_img_detail" id="qty_img_detail" value="{{$qty_img}}">
                                    @foreach((array)$value->img as $key=>$img)
                                        <div class="list_detail_photo" id="{{$qty_img}}" style="margin: 0px 6px;display: inline-block; width: 100px; height: 140px">
                                            <input type="text" class="custom-file-input" id="{{$qty_img}}" name="files_compare[]" readonly style="display: none;"/>
                                            <span class="pip">
                                                <img class="imageThumb" style="width:100px ; height:100px" src="../../public/img_poster/{{$img}}" title="file.name"/>
                                                <br/>
                                                <span class="remove" id="{{$qty_img}}" onclick="document.getElementById('{{$qty_img}}').value = '' " >Xóa</span>
                                            </span>
                                        </div>   
                                        <input type="hidden" class="_img_detail" id="img_{{$qty_img}}" name="img_{{$qty_img}}" value="{{$img}}">
                                        <?php $qty_img ++; ?>
                                    @endforeach
                                <br>
                                <br>
                                
                            </div>

                            <div class="form-group"> <label class="form-label">Price</label> <input type="number"
                                                class="form-control" placeholder="price" name="price" value="{{$value->price}}"></div>
                                
                            <div class="form-group"> <label class="form-label text-dark">Description</label>
                                <textarea class="form-control" rows="6" id="editor1" placeholder="text here.." name="message" type="text" value="">
                                    {{$value->description}}
                                </textarea>
                            </div>
                            @if($value->check == 0)
                            <span id="info_xe">
                                <div class="row">
                                    <div class="form-group col-md-6"> <label class="form-label text-dark">Brand</label>
                                        <select class="form-control custom-select" name="brand">
                                            @foreach($brand_car as $value_brand_car)
                                            @if(($value->brand_id == $value_brand_car->id))
                                            <option value="{{$value_brand_car->id}}" selected="selected">{{$value_brand_car->name}}</option>
                                            @else
                                            <option value="{{$value_brand_car->id}}">{{$value_brand_car->name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6"> <label class="form-label text-dark">Category</label>
                                        <select class="form-control custom-select" name="category">
                                            @foreach($categories as $value_cate)
                                            @if(($value->category_id == $value_cate->id))
                                                <option  value="{{$value_cate->id}}">{{$value_cate->name}}</option>
                                            @else
                                                <option value="{{$value_cate->id}}">{{$value_cate->name}}</option>  
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">    
                                    <div class="col-sm-6 col-md-4">
                                        <div class="form-group"> <label class="form-label">Price</label> <input type="number"
                                                class="form-control" placeholder="price" name="price" value="{{$value->price}}"></div>
                                    </div>                               
                                    <div class="col-sm-6 col-md-4">
                                        <div class="form-group"> <label class="form-label">Seats</label> <input type="number"
                                            name="seats" class="form-control" placeholder="seats" value="{{$value->seats}}"></div>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="form-group"> <label class="form-label">Km</label> <input type="number"
                                                class="form-control" placeholder="Km" name="km" value="{{$value->km_went}}"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4"> <label class="form-label text-dark">Transmission_type</label>
                                        <select class="form-control custom-select" name="transmission_type">
                                            <option @if($value->transmission_type == 1) selected="selected" @endif value="1">1</option>
                                            <option @if($value->transmission_type == 2) selected="selected" @endif value="2">2</option>
                                            <option @if($value->transmission_type == 3) selected="selected" @endif value="3">3</option>
                                            <option @if($value->transmission_type == 4) selected="selected" @endif value="4">4</option>
                                            <option @if($value->transmission_type == 5) selected="selected" @endif value="5">5</option>
                                            <option @if($value->transmission_type == 6) selected="selected" @endif value="6">6</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4"> <label class="form-label text-dark">Fuel</label>
                                        <select class="form-control custom-select" name="fuel">
                                            <option @if($value->fuel == 1) selected="selected" @endif value="1">1</option>
                                            <option @if($value->fuel == 2) selected="selected" @endif value="2">2</option>
                                            <option @if($value->fuel == 3) selected="selected" @endif value="3">3</option>
                                            <option @if($value->fuel == 4) selected="selected" @endif value="4">4</option>
                                            <option @if($value->fuel == 5) selected="selected" @endif value="5">5</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4"> <div class="form-group"> <label class="form-label">Fuel-tank-capacity</label> <input type="number"
                                                class="form-control" placeholder="0" name="fuel_tank_capacity" value="{{$value->fuel_tank_capacity}}"> </div>
                                    </div>
                                </div>
                                <div class="row">
                                        <div class="form-group col-md-6"> <label class="form-label">Engine</label> <input type="text"
                                                class="form-control" placeholder="Engine" name="engine" value="{{$value->engine}}"> </div>
                                        <div class="form-group col-md-6"> <label class="form-label">Enegine_displacement</label> <input type="text"
                                                class="form-control" placeholder="Enegine_displacement" name="enegine_displacement" value="{{$value->enegine_displacement}}"> </div>
                                        <div class="form-group col-md-6"> <label class="form-label">Break</label> <input type="text"
                                                class="form-control" placeholder="Break" name="break" value="{{$value->break}}"> </div>
                                        <div class="form-group col-md-6"> <label class="form-label">Bootspace</label> <input type="text"
                                                class="form-control" placeholder="Bootspace" name="bootspace" value="{{$value->bootspace}}"> </div>
                                        <div class="form-group col-md-6"> <label class="form-label">Arai_milage</label> <input type="text"
                                                class="form-control" placeholder="Arai_milage" name="arai_milage" value="{{$value->arai_milage}}"> </div>
                                        <div class="form-group col-md-6"> <label class="form-label">Max-torque</label> <input type="text"
                                                class="form-control" placeholder="Max-torque" name="max_torque" value="{{$value->max_torque}}"> </div>
                                        <div class="form-group col-md-6"> <label class="form-label">Max-power</label> <input type="text"
                                                class="form-control" placeholder="Max-power" name="max_power" value="{{$value->max_power}}"> </div>                                
                                </div>                            
                                <label class="form-label text-dark">Video</label>
                                <div class="p-2 border mb-4 form-group"> 
                                    <input type="file" name="video_poster" id="video_poster">  
                                    @if($value->video != null)
                                    <div>
                                        <video width="320" height="240" controls style="width: 600px;margin: 10px;margin-left: 60px;border: 1px solid #d8dde6;padding: 3px;">
                                            <source src="../../public/video_poster/{{$value->video}}" type="video/mp4" >
                                        </video>
                                    </div>
                                    @endif
                                </div>
                            </span>
                            @endif
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">Name</label> <input type="text"
                                            class="form-control" placeholder="Name" @if($data['name'] !=null)
                                            value="{{$data['name']}}" @endif name="name"> </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">Email</label> <input type="email"
                                            class="form-control" placeholder="Email Address" @if($data['email'] !=null)
                                            name="emai" value="{{$data['email']}}" @endif> </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group mb-0"> <label class="form-label">Phone Number</label>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone"
                                            @if($data['phone'] !=null) value="{{$data['phone']}}" @endif> </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group mb-0"> <label class="form-label">Address</label>
                                        <input name="address" type="text" class="form-control" placeholder="Address"
                                            @if($data['address'] !=null) value="{{$data['address']}}" @endif> </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <label class="form-label">City</label>
                                    <select class="form-control" id="select-countries" name="city">
                                            @foreach($Province as $value_Province)
                                                <option data-select2-id="5" value="{{$value_Province->id}}" @if($data['city'] == $value_Province->id) selected="selected" @endif>{{$value_Province->_name}}</option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                            <div class="control-group form-group mt-4"> <label class="form-label text-dark">Ad Post
                                    Package</label>
                                <div class="d-md-flex ad-post-details"> <label
                                        class="custom-control custom-radio mb-2 mr-4"> <input type="radio"
                                            class="custom-control-input" name="radios1" value="0" @if($value->package ==
                                        0)checked=""@endif >
                                        <span class="custom-control-label"><a class="text-muted">30 Days
                                                Free</a></span> </label> <label
                                        class="custom-control custom-radio  mb-2 mr-4"> <input type="radio"
                                            class="custom-control-input" name="radios1" value="1" @if($value->package ==
                                        1)checked=""@endif > <span class="custom-control-label"><a class="text-muted">60
                                                days /
                                                20$</a></span> </label> <label
                                        class="custom-control custom-radio  mb-2 mr-4"> <input type="radio"
                                            class="custom-control-input" name="radios1" value="2" @if($value->package ==
                                        2)checked=""@endif > <span class="custom-control-label"><a
                                                class="text-muted">6months /
                                                50$</a></span> </label> <label class="custom-control custom-radio  mb-2">
                                        <input type="radio" class="custom-control-input" name="radios1" value="3"
                                            @if($value->package == 3)checked=""@endif >
                                        <span class="custom-control-label"><a class="text-muted">1 year /
                                                80$</a></span> </label> </div>
                            </div>
                        </div>
                        <div class="card-footer "> <input type="Submit" name="edit" id="edit" value="Submit Now"></div>
                    </div>
                </form>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Terms And Conditions</h3>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled widget-spec  mb-0">
                            <li> <i class="fa fa-check text-success" aria-hidden="true"></i>Money Not Refundable
                            </li>
                            <li> <i class="fa fa-check text-success" aria-hidden="true"></i>You can renew your
                                Premium ad after experted. </li>
                            <li> <i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads are
                                active for depend on package. </li>
                            <li class="ml-5 mb-0"> <a href="tips.html"> View more..</a> </li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Benefits Of Premium Ad</h3>
                    </div>
                    <div class="card-body pb-2">
                        <ul class="list-unstyled widget-spec vertical-scroll mb-0"
                            style="overflow-y: hidden; height: 166px;">
                            <li style="" class="undefined"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium Ads Active </li>
                            <li style="" class="undefined"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium ads are displayed on top </li>
                            <li style="" class="undefined"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium ads will be Show in Google results </li>
                            <li> <i class="fa fa-check text-success" aria-hidden="true"></i>Premium Ads Active
                            </li>
                            <li style="display: none;"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium ads are displayed on top </li>
                            <li style="display: none;"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium ads will be Show in Google results </li>
                            <li style="display: none;"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium Ads Active </li>
                            <li style="display: none;"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium ads are displayed on top </li>
                            <li style="display: none;"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium ads will be Show in Google results </li>
                            <li style="display: none;"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium Ads Active </li>
                            <li style="display: none;"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium ads are displayed on top </li>
                            <li style="display: none;"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium ads will be Show in Google results </li>
                            <li style="display: none;"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium Ads Active </li>
                            <li style="display: none;"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium ads are displayed on top </li>
                            <li style="display: none;"> <i class="fa fa-check text-success"
                                    aria-hidden="true"></i>Premium ads will be Show in Google results </li>
                        </ul>
                    </div>
                </div>
                <div class="card mb-5 mb-xl-0">
                    <div class="card-header">
                        <h3 class="card-title">Safety Tips For Buyers</h3>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled widget-spec  mb-0">
                            <li> <i class="fa fa-check text-success" aria-hidden="true"></i> Meet Seller at
                                public Place </li>
                            <li> <i class="fa fa-check text-success" aria-hidden="true"></i> Check item before
                                you buy </li>
                            <li> <i class="fa fa-check text-success" aria-hidden="true"></i> Pay only after
                                collecting item </li>
                            <li class="ml-5 mb-0"> <a href="tips.html"> View more..</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-8">
                <div class="card mb-xl-0">
                    <div class="card-header">
                        <h3 class="card-title">Payments</h3>
                    </div>
                    <div class="card-body">
                        <div class="tab-content card-body border mb-0 b-0">
                            <div class="panel panel-primary">
                                <div class=" tab-menu-heading border-0 pl-0 pr-0 pt-0">
                                    <div class="tabs-menu1 ">
                                        <!-- Tabs -->
                                        <ul class="nav panel-tabs">
                                            <li><a href="#tab5" class="active" data-toggle="tab">Credit/ Debit
                                                    Card</a></li>
                                            <li><a href="#tab6" data-toggle="tab">Pay-pal</a></li>
                                            <li><a href="#tab7" data-toggle="tab">Net Banking</a></li>
                                            <li><a href="#tab8" data-toggle="tab">Gift Voucher</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel-body tabs-menu-body pl-0 pr-0 border-0">
                                    <div class="tab-content">
                                        <div class="tab-pane active " id="tab5">
                                            <div class="form-group"> <label class="form-label">CardHolder
                                                    Name</label> <input type="text" class="form-control" id="name1"
                                                    placeholder="First Name"> </div>
                                            <div class="form-group"> <label class="form-label">Card
                                                    number</label>
                                                <div class="input-group"> <input type="text" class="form-control"
                                                        placeholder="Search for..."> <span class="input-group-append">
                                                        <button class="btn btn-info" type="button"><i
                                                                class="fa fa-cc-visa"></i> &nbsp;
                                                            <i class="fa fa-cc-amex"></i> &nbsp; <i
                                                                class="fa fa-cc-mastercard"></i></button>
                                                    </span> </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="form-group mb-sm-0"> <label
                                                            class="form-label">Expiration</label>
                                                        <div class="input-group"> <input type="number"
                                                                class="form-control" placeholder="MM"
                                                                name="expiremonth"> <input type="number"
                                                                class="form-control" placeholder="YY" name="expireyear">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 ">
                                                    <div class="form-group mb-0"> <label class="form-label">CVV
                                                            <i class="fa fa-question-circle"></i></label> <input
                                                            type="number" class="form-control" required="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane " id="tab6">
                                            <h6 class="font-weight-semibold">Paypal is easiest way to pay online
                                            </h6>
                                            <p><a href="#" class="btn btn-primary"><i class="fa fa-paypal"></i>
                                                    Log in my Paypal</a></p>
                                            <p class="mb-0"><strong>Note:</strong> Nemo enim ipsam voluptatem
                                                quia voluptas sit aspernatur aut odit aut fugit, sed quia
                                                consequuntur magni dolores eos qui ratione voluptatem sequi
                                                nesciunt. </p>
                                        </div>
                                        <div class="tab-pane " id="tab7">
                                            <div class="control-group form-group">
                                                <div class="form-group"> <label class="form-label text-dark">All
                                                        Banks</label> <select
                                                        class="form-control custom-select required category">
                                                        <option value="0">Select Bank</option>
                                                        <option value="1">Credit Agricole Group</option>
                                                        <option value="2">Bank of America</option>
                                                        <option value="3">Mitsubishi UFJ Financial Group
                                                        </option>
                                                        <option value="4">BNP Paribas</option>
                                                        <option value="5">JPMorgan Chase &amp; Co.</option>
                                                        <option value="6">HSBC Holdings</option>
                                                        <option value="7">Bank of China</option>
                                                        <option value="8">Agricultural Bank of China</option>
                                                        <option value="9">ChinaQuaerat Bank Corp.</option>
                                                        <option value="10">Industrial &amp; Commercial Bank of
                                                            China, or ICBC</option>
                                                    </select> </div>
                                            </div>
                                            <p><a href="#" class="btn btn-primary">Log in Bank</a></p>
                                        </div>
                                        <div class="tab-pane " id="tab8">
                                            <div class="form-group"> <label class="form-label">Gift
                                                    Voucher</label>
                                                <div class="input-group"> <input type="text" class="form-control"
                                                        placeholder="Enter Your Gv Number">
                                                    <span class="input-group-append"> <button class="btn btn-info"
                                                            type="button"> Apply</button>
                                                    </span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row clearfix">
                                <div class="col-lg-12">
                                    <div class="checkbox checkbox-info"> <label
                                            class="custom-control mt-4 custom-checkbox"> <input type="checkbox"
                                                class="custom-control-input"> <span
                                                class="custom-control-label text-dark pl-2">I agree with the
                                                Terms and Conditions.</span> </label> </div>
                                </div>
                                <ul class=" mb-b-4 ">
                                    <li class="float-right"><a href="#" class="btn btn-primary  mb-0 mr-2">Continue</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url_up" value="{{route('upImg')}}">
    <input type="hidden" id="url_del" value="{{route('delImg')}}">
    <style type="text/css">
        .error{
            color: red
        }
    </style>
    <script type="">
         $('form').validate({ // initialize the plugin
            rules: {
                "title":"required",
                message: {
                    required: true,
                },
                "price": "required",
                // "files[]" : "required",
                brand : {
                    required:true,
                    nullable:true
                },
                category : {
                    required:true,
                    nullable:true
                },
                "seats": "required",
                "km": "required",
                transmission_type : {
                    required:true,
                    nullable:true
                },
                fuel : {
                    required:true,
                    nullable:true
                },
                "fuel_tank_capacity": "required",
                "engine": "required",
                "enegine_displacement": "required",
                "break": "required",
                "bootspace": "required",
                "arai_milage": "required",
                "bootspace": "required",
                "max_torque": "required",
                "max_power": "required",
                "video_poster": "required",
            },
        });
    </script>
</section>
<!--/Section-->
@endforeach
@endsection
<!-- <script type="text/javascript">
    window.onbeforeunload = function() { 
        // var url_del = $("input#url_del").val();
        
        // for (var i = Ơ$qty_img; i >= 0; i--) {
        //     Things[i]
        // }
        // // var name_img_del = $("input#img_"+qty_img+"").val(); 
                       
        // $.ajaxSetup({
        //     headers : {
        //         'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
        // }
        // });
        // $.ajax({
        //     url : url_del,
        //     type : "POST",
        //     cache: false,
        //     data : {_token:CSRF_TOKEN,name_img_del:name_img_del},
        //     success:function(data){ 
        //     },error:function(error){
        //         alert("Thêm thất bại");
        //     }
        // });
        // $("div#" + id).remove();
        return "oke";
    };
</script> -->
