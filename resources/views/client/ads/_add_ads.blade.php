@extends('client.master')
@section('content')
</div>
<!--/Topbar-->
<!--Section-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg"
        style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1>Thêm Bài Đăng</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Trang Chủ</a></li>
                        <li class="breadcrumb-item"><a href="#">Các Trang</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">Sau</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Section-->
<!--Section-->
<section class="sptb">
    <div class="container">
        <div class="row ">
            <!-- sidebar infomation user -->
            @include('client.sidebar',['data'=>$data])
            <!-- sidebar infomation user -->
            <div class="col-lg-8 col-md-12 col-md-12">
                <form method="post" enctype="multipart/form-data" id="form_add_ads" action="{{route('postaddAds')}}">
                    @csrf
                    <div class="card ">
                        <div class="card-header ">
                            <h3 class="card-title">Thêm Bài Đăng</h3>
                        </div>
                        <div class="card-body">
                            <h3 style="margin-bottom: 20px;display: none;" class="errors" id="errors_all">Vui lòng điền
                                đầy đủ thông tin</h3>
                            <div class="form-group"> <label class="form-label text-dark">Loại quảng cáo</label>
                                <div class="d-md-flex ad-post-details">
                                    <label class="custom-control custom-radio mb-2 mr-4">
                                        <input type="radio" id="sell" class="custom-control-input" name="radios_type_1"
                                            value="0" checked="">
                                        <span class="custom-control-label"><a class="text-muted">Tôi muốn bán</a></span>
                                    </label>
                                    <label class="custom-control custom-radio  mb-2">
                                        <input type="radio" id="buy" class="custom-control-input" name="radios_type_1"
                                            value="1">
                                        <span class="custom-control-label"><a class="text-muted">Tôi muốn mua</a></span>
                                    </label>
                                </div>
                            </div>
                            <!-- Phần quảng cáo mua --> 
                            <div id="div_buy">
                                <div class="form-group">
                                    <label class="form-label text-dark">Nội dung</label>
                                    <textarea class="form-control" rows="6" id="editor1" placeholder="text here.." name="message" type="text" value=""></textarea>
                                </div>
                            </div>
                            <!-- end quảng cáo mua -->
                            <div id="div_sell">
                                <div class="form-group">
                                    <label class="form-label text-dark">Chọn đồ cần bán</label>
                                    <div class="d-md-flex ad-post-details">
                                        <label class="custom-control custom-radio mb-2 mr-4">
                                            <input type="radio" checked="" id="sell_xe" class="custom-control-input" value="0" name="check_">
                                            <span class="custom-control-label"><a href="#" class="text-muted">Xe</a></span>
                                        </label>
                                        <label class="custom-control custom-radio  mb-2">
                                            <input type="radio" id="sell_phu_tung" value="1" class="custom-control-input" name="check_" >
                                            <span class="custom-control-label">
                                                <a href="#" class="text-muted">Phụ tùng</a>
                                            </span>
                                        </label>
                                    </div>
                                </div>

                                    <div class="form-group">
                                        <label class="form-label text-dark">Loại Điều Kiện</label>
                                        <div class="d-md-flex ad-post-details">
                                            <label class="custom-control custom-radio mb-2 mr-4">
                                                <input checked="" type="radio" class="custom-control-input" value="0" name="radios2">
                                                <span class="custom-control-label"><a href="#" class="text-muted">Mới</a></span>
                                            </label>
                                            <label class="custom-control custom-radio  mb-2">
                                                <input type="radio" class="custom-control-input" name="radios2" value="1">
                                                <span class="custom-control-label"><a href="#" class="text-muted">Đã Sử Dụng</a></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group"> <label class="form-label text-dark">Tiêu đề quảng cáo</label>
                                        <input type="text" class="form-control" placeholder="" value="" name="title">
                                    </div>
                                    
                                    <label class="form-label text-dark">Hình ảnh</label>
                                    <div class="p-2 border mb-4 form-group">
                                        <input type="file" id="image_list" name="files[]" value="" multiple />
                                        <input type="hidden" name="qty_img_detail" id="qty_img_detail" value="0">
                                        <br>
                                        <br>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Giá Bán</label>
                                        <input type="number" class="form-control" placeholder="0" name="price" value="">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label text-dark">Sự mô tả</label>
                                        <textarea class="form-control" rows="6" id="editor1" placeholder="text here.." name="message" type="text" value=""></textarea>
                                        <script>
                                            CKEDITOR.replace('editor1');
                                        </script>
                                    </div>
                                    {{-- danh mục phụ tùng --}}
                                    @foreach($category as $key => $cate_pt)
                                    <div class="form-group"> 
                                        <label class="form-label text-dark">Danh Mục Phụ Tùng</label> 
                                        <select class="form-control custom-select" name="category_pt" id="category_pt">
                                            <option value="">-chọn danh mục phụ tùng-</option>
                                            <option value="{{$cate_pt->id}}">{{$cate_pt->name}}</option> 
                                        </select> 
                                    </div>
                                    @endforeach
                                <span id="buy_xe"> 
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label text-dark">Thương Hiệu</label>
                                            <select class="form-control custom-select" name="brand" id="brand">
                                                <option value="">-- Chọn thương hiệu --</option>
                                                @foreach($brand_car as $value_brand_car)
                                                    <option value="{{$value_brand_car->id}}">{{$value_brand_car->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label text-dark">kiểu dáng</label>
                                            <select class="form-control custom-select" name="category">
                                                <option value="">-- Chọn thương kiểu dáng --</option>
                                                @foreach($categories as $value_cate)
                                                    <option value="{{$value_cate->id}}">{{$value_cate->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-md-4">
                                            <div class="form-group">
                                                <label class="form-label">Số Ghế</label>
                                                <input type="number" name="seats" class="form-control" placeholder="0" value="">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <div class="form-group">
                                                <label class="form-label">Km</label>
                                                <input type="number" class="form-control" placeholder="Km" name="km" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-4"> <label
                                                class="form-label text-dark">mẫu di truyền</label>
                                            <select class="form-control custom-select" name="transmission_type">
                                                <option value="">-- Chọn kiểu mẫu di truyền --</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4"> <label class="form-label text-dark">Nhiên liệu</label>
                                            <select class="form-control custom-select" name="fuel">
                                                <option value="">-- Chọn kiểu nhiên liệu --</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <div class="form-group">
                                                <label class="form-label">Dung tích bình nhiên liệu</label>
                                                <input type="number" class="form-control" placeholder="0" name="fuel_tank_capacity" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label">Động Cơ</label>
                                            <input type="text" class="form-control" placeholder="Engine" name="engine" value="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label">Enegine_displacement</label>
                                            <input type="text" class="form-control" placeholder="Enegine_displacement" name="enegine_displacement" value="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label">Tính năng đặc biệt</label>
                                            <input type="text" class="form-control" placeholder="Break" name="break" value="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label">Không Gian Khởi Động</label>
                                            <input type="text" class="form-control" placeholder="Bootspace" name="bootspace" value="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label">Arai_milage</label>
                                            <input type="text" class="form-control" placeholder="Arai_milage" name="arai_milage" value="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label">Momen xoắn cực đại</label>
                                            <input type="text" class="form-control" placeholder="Max-torque" name="max_torque" value="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label">Cộng xuất tối đa</label>
                                            <input type="text" class="form-control" placeholder="Max-power" name="max_power" value="">
                                        </div>
                                    </div>
                                    <label class="form-label text-dark">Video</label>
                                    <div class="p-2 border mb-4 form-group">
                                        <input type="file" name="video_poster" id="video_poster">
                                    </div>
                                </span>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">Tên</label> <input type="text"
                                            class="form-control" placeholder="Name" name="name"
                                            value="{{$list_comment['name']}}"></div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">Email</label> <input type="email"
                                            class="form-control" placeholder="Email Address"
                                            value="{{$list_comment['email']}}"></div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group mb-0"> <label class="form-label">Số điện thoại</label>
                                        <input type="text" name="phone" value="{{$list_comment['phone']}}"
                                            class="form-control" placeholder="Phone">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group mb-0"> <label class="form-label">Địa chỉ</label>
                                        <input name="address" type="text" class="form-control" placeholder="Address"
                                            value="{{$list_comment['address']}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <label class="form-label">Thành phố</label>
                                    <select class="form-control" id="select-countries" name="city">
                                        @foreach($Province as $value_Province)
                                        <option data-select2-id="5" @if($list_comment['id_city']==($value_Province->id))
                                            selected @endif value="{{$value_Province->id}}">{{$value_Province->_name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="control-group form-group mt-4"> <label class="form-label text-dark">Gói đăng quảng cáo</label>
                                <div class="d-md-flex ad-post-details">
                                    <label class="custom-control custom-radio mb-2 mr-4">
                                        <input type="radio" class="custom-control-input" id="package_0" name="radios1" value="0" checked="">
                                        <span class="custom-control-label">
                                            <a class="text-muted">30 ngày miễn phí</a>
                                        </span>
                                    </label>
                                    <label class="custom-control custom-radio  mb-2 mr-4">
                                        <input type="radio" class="custom-control-input" id="package_1" name="radios1" value="1">
                                        <span class="custom-control-label">
                                            <a class="text-muted">60 ngày /20$</a>
                                        </span>
                                    </label>
                                    <label class="custom-control custom-radio  mb-2 mr-4">
                                        <input type="radio" class="custom-control-input" id="package_2" name="radios1" value="2">
                                        <span class="custom-control-label">
                                            <a class="text-muted">6 tháng /50$</a>
                                        </span>
                                    </label>
                                    <label class="custom-control custom-radio  mb-2">
                                        <input type="radio" class="custom-control-input" id="package_3" name="radios1" value="3">
                                        <span class="custom-control-label">
                                            <a class="text-muted">1 năm /80$</a>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id_poster" value="">
                        <div class="card-footer "> <input class="confirm" type="Submit" name="edit" id="edit"
                                value="Submit Now"></div>
                    </div>
                </form>
                <div class="card mb-xl-0">
                    <div class="card-header">
                        <h3 class="card-title">Thanh toán</h3>
                    </div>
                    <div class="card-body">
                        <div class="tab-content card-body border mb-0 b-0">
                            <div class="panel panel-primary">
                                <div class=" tab-menu-heading border-0 pl-0 pr-0 pt-0">
                                    <div class="tabs-menu1 ">
                                        <!-- Tabs -->
                                        <ul class="nav panel-tabs">
                                            <li><a href="#tab5" class="active" data-toggle="tab">Thể tín dụng / thẻ ghi nợ
                                                    </a></li>
                                            <li><a href="#tab6" data-toggle="tab">Pay-pal</a></li>
                                            <li><a href="#tab7" data-toggle="tab">Ngân hàng trực tuyến</a></li>
                                            <li><a href="#tab8" data-toggle="tab">Phiếu quà tặng</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel-body tabs-menu-body pl-0 pr-0 border-0">
                                    <div class="tab-content">
                                        <div class="tab-pane active " id="tab5">
                                            <div class="form-group"> <label class="form-label">Tên thẻ</label> <input type="text" class="form-control" id="name1"
                                                    placeholder="Tên đầu tiên"> </div>
                                            <div class="form-group"> <label class="form-label">Số thẻ</label>
                                                <div class="input-group"> <input type="text" class="form-control"
                                                        placeholder="Tìm kiếm..."> <span class="input-group-append">
                                                        <button class="btn btn-info" type="button"><i
                                                                class="fa fa-cc-visa"></i> &nbsp;
                                                            <i class="fa fa-cc-amex"></i> &nbsp; <i
                                                                class="fa fa-cc-mastercard"></i></button>
                                                    </span> </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="form-group mb-sm-0"> <label
                                                            class="form-label">Hết hạn</label>
                                                        <div class="input-group"> <input type="number"
                                                                class="form-control" placeholder="MM"
                                                                name="expiremonth"> <input type="number"
                                                                class="form-control" placeholder="YY" name="expireyear">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 ">
                                                    <div class="form-group mb-0"> <label class="form-label">CVV
                                                            <i class="fa fa-question-circle"></i></label> <input
                                                            type="number" class="form-control" required="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane " id="tab6">
                                            <h6 class="font-weight-semibold">Paypal là cách dễ nhất để thanh toán trực tuyến
                                            </h6>
                                            <p><a href="#" class="btn btn-primary"><i class="fa fa-paypal"></i>
                                                    Đăng nhập Paypal của tôi</a></p>
                                            <p class="mb-0"><strong>Note:</strong> Nemo enim ipsam voluptatem
                                                quia voluptas sit aspernatur aut odit aut fugit, sed quia
                                                consequuntur magni dolores eos qui ratione voluptatem sequi
                                                nesciunt. </p>
                                        </div>
                                        <div class="tab-pane " id="tab7">
                                            <div class="control-group form-group">
                                                <div class="form-group"> <label class="form-label text-dark">Tất cả ngân hàng</label> <select
                                                        class="form-control custom-select required category">
                                                        <option value="0">Chọn ngân hàng</option>
                                                        <option value="1">Credit Agricole Group</option>
                                                        <option value="2">Ngân hàng Mỹ</option>
                                                        <option value="3">Tập đoàn tài chính Mitsubishi UFJ
                                                        </option>
                                                        <option value="4">BNP Paribas</option>
                                                        <option value="5">JPMorgan Chase &amp; Co.</option>
                                                        <option value="6">HSBC Holdings</option>
                                                        <option value="7">ngân hàng Trung Quốc</option>
                                                        <option value="8">Ngân hàng Nông nghiệp Trung Quốc</option>
                                                        <option value="9">ChinaQuaerat Bank Corp.</option>
                                                        <option value="10">Công nghiệp và Ngân hàng thương mại của Trung Quốc hoặc ICBC</option>
                                                    </select> </div>
                                            </div>
                                            <p><a href="#" class="btn btn-primary">Đăng nhập ngân hàng</a></p>
                                        </div>
                                        <div class="tab-pane " id="tab8">
                                            <div class="form-group"> <label class="form-label">Quà tặng phiếu thưởng</label>
                                                <div class="input-group"> <input type="text" class="form-control"
                                                        placeholder="Enter Your Gv Number">
                                                    <span class="input-group-append"> <button class="btn btn-info"
                                                            type="button">Ứng dụng</button>
                                                    </span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row clearfix">
                                <div class="col-lg-12">
                                    <div class="checkbox checkbox-info"> <label
                                            class="custom-control mt-4 custom-checkbox"> <input type="checkbox"
                                                class="custom-control-input"> <span
                                                class="custom-control-label text-dark pl-2">
                                                Tôi đồng ý với các Điều khoản và Điều kiện.</span> </label> </div>
                                </div>
                                <ul class=" mb-b-4 ">
                                    <li class="float-right"><a href="#" class="btn btn-primary  mb-0 mr-2">Tiếp tục</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>              
            </div>
            </div>           
        </div>
    </div>
    <input type="hidden" id="url_up" value="{{route('upImg')}}">
    <input type="hidden" id="url_del" value="{{route('delImg')}}">
    <style type="text/css">
        .error{
            color: red
        }
    </style>
    <script type="">

         $('#form_add_ads').validate({  //initialize the plugin
             rules: {
                 "title":"required",
                 message: {
                     required: true,
                 },
                 "price": "required",
                 "files[]" : "required",
                 brand : {
                     required:true,
                     nullable:true
                 },
                 category : {
                     required:true,
                     nullable:true
                 },
                 "seats": "required",
                 "km": "required",
                 transmission_type : {
                     required:true,
                     nullable:true
                 },
                 fuel : {
                     required:true,
                     nullable:true
                 },
                 "fuel_tank_capacity": "required",
                 "engine": "required",
                 "enegine_displacement": "required",
                 "break": "required",
                 "bootspace": "required",
                 "arai_milage": "required",
                 "bootspace": "required",
                 "max_torque": "required",
                 "max_power": "required",
                 "video_poster": "required",
             },
         });

    </script>
</section>
<!--/Section-->
@endsection

