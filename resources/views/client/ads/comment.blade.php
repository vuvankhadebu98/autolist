@for ($i = 0; $i < count($list_comment) ; $i++) 
	@if( $list_comment[$i]['comment_id']==$id ) 
	<div class="media p-5">
	    <div class="d-flex mr-3">
	        <a href="#">
	            @if($list_comment[$i]['img'] != null)
	            <img class="media-object brround" alt="64x64" src="../assets/images/faces/male/3.jpg">
	            @else
	            <img class="media-object brround" alt="64x64" src="../../public/client/user.png">
	            @endif
	        </a>
	    </div>
	    <div class="media-body">
	        <span id="div_2">
	            <h5 class="mt-0 mb-1 font-weight-semibold"> {{$list_comment[$i]['name']}}<span class="fs-14 ml-0"
	                    data-toggle="tooltip" data-placement="top" title="" data-original-title="verified"><i
	                        class="fa fa-check-circle-o text-success"></i></i>
	                    {{$list_comment[$i]['created_at']}}</span>
	            </h5>
	        </span>
	        <span class="content_rep">
	            <p class="font-13  mb-2 mt-2"> {{$list_comment[$i]['content']}}</p>
	            <a class="reply mr-2" id="{{$list_comment[$i]['id']}}">
	                <span class="badge badge-primary">Reply</span>
	            </a>
	        </span>
	        <br>
	        <form method="post" class="reply_comment" id="reply_comment_{{$list_comment[$i]['id']}}" style="display: none;"
	            action="{{route('rep_comment')}}">
	            @csrf
	            <div class="media p-5 border-top mt-0">
	                <div class="d-flex mr-3 img_user"></div>
	                <div class="media-body">
	                    <span class="div_2"></span>
	                    <br>
	                    <p class="content_rep">
	                        <textarea style="width: 520px;margin-top: -10px;" rows="2" placeholder="text here.."
	                            id="rep_comment_{{$list_comment[$i]['id']}}" name="rep_comment"></textarea>
	                        <input type="hidden" id="comment_id" name="comment_id" value="{{$list_comment[$i]['id']}}">
	                        <p style="width: 520px;margin-top: -13px;" class="errors errors_bottom" id="error_rep_comment_{{$list_comment[$i]['id']}}"></p>
	                        <input type="submit" value="Reply" name="reply_comment">
	                    </p>
	                </div>
	            </div>
	        </form>
	    </div>
    </div>
    @endif
@endfor
