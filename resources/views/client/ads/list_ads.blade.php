@extends('client.master')
@section('content')
</div>
<!--/Topbar-->
<!--Section-->
<div>
    <div class=" cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg"
        style="background: url(&quot;../assets/images/banners/banner1.jpg&quot;) center center;">
        <div class="header-text1 mb-0">
            <div class="container">
<!-------------------------------------phần search -->
        @if(!isset($id_brand) && !isset($price) && !isset($seats) && !isset($city))
            @include('client.select_search')
        @else
            @include('client.select_search',['id_brand'=>$id_brand,'price'=>$price,'seats'=>$seats,'city'=>$city])
        @endif    
        <!----------------------------------------------------->
            </div>
        </div><!-- /header-text -->
    </div>
</div>
<!--/Section-->
<!--Breadcrumb-->
<div class="bg-white border-bottom">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title">Ad List</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Pages</a></li>
                <li class="breadcrumb-item active" aria-current="page">Ad List</li>
            </ol>
        </div>
    </div>
</div>
<!--/Breadcrumb-->
<!--listing-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-12">
                <!--Lists-->
                <div class=" mb-0">
                    <div class="">
                        <div class="item2-gl ">
                            <div class=" mb-0">
                                <div class="">
                                    <div class="bg-white p-5 item2-gl-nav d-flex">
                                        <h6 class="mb-0 mt-3 text-left">Showing 1 to 10 of 30 entries</h6>
                                        <ul class="nav item2-gl-menu ml-auto mt-1">
                                            <li class=""><a href="#tab-11" class="active show" data-toggle="tab"
                                                    title="List style"><i class="fa fa-list"></i></a></li>
                                            <li><a href="#tab-12" data-toggle="tab" class="" title="Grid"><i
                                                        class="fa fa-th"></i></a></li>
                                        </ul>
                                        <div class="d-sm-flex"> <label class="mr-2 mt-2 mb-sm-1">Sort
                                                By:</label>
                                            <div class="selectgroup"> <label class="selectgroup-item mb-md-0">
                                                    <input type="radio" name="value" value="Price"
                                                        class="selectgroup-input" checked=""> <span
                                                        class="selectgroup-button">Price <i
                                                            class="fa fa-sort ml-1"></i></span> </label> <label
                                                    class="selectgroup-item mb-md-0"> <input type="radio" name="value"
                                                        value="Popularity" class="selectgroup-input"> <span
                                                        class="selectgroup-button">Popularity</span> </label>
                                                <label class="selectgroup-item mb-0"> <input type="radio" name="value"
                                                        value="Latest" class="selectgroup-input">
                                                    <span class="selectgroup-button">Latest</span> </label>
                                                <label class="selectgroup-item mb-0"> <input type="radio" name="value"
                                                        value="Rating" class="selectgroup-input">
                                                    <span class="selectgroup-button">Rating</span> </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-11">
                                    @foreach($Poster as $value_poster)
                                        <div class="card overflow-hidden">
                                            <div class="d-md-flex">
                                                <div class="item-card9-img">
                                                    <div class="arrow-ribbon bg-primary">Sale</div>
                                                    <div class="item-card9-imgs"> <a class="link" href="{{route('getDetail',['id'=>$value_poster->id])}}"></a> <img
                                                            src="../../public/img_poster/{{$value_poster->img}}" alt="img"
                                                            class="cover-image"> </div>
                                                    <div class="item-card9-icons"> <a href="{{route('getDetail',['id'=>$value_poster->id])}}"
                                                            class="item-card9-icons1 wishlist"> <i
                                                                class="fa fa fa-heart-o"></i></a> </div>
                                                    <div class="item-overly-trans">
                                                        <div class="rating-stars"> <input type="number" readonly="readonly"
                                                                class="rating-value star" name="rating-stars-value"
                                                                value="3">
                                                            <div class="rating-stars-container">
                                                                <div class="rating-star sm is--active"> <i
                                                                        class="fa fa-star"></i> </div>
                                                                <div class="rating-star sm is--active"> <i
                                                                        class="fa fa-star"></i> </div>
                                                                <div class="rating-star sm is--active"> <i
                                                                        class="fa fa-star"></i> </div>
                                                                <div class="rating-star sm"> <i class="fa fa-star"></i>
                                                                </div>
                                                                <div class="rating-star sm"> <i class="fa fa-star"></i>
                                                                </div>
                                                            </div>
                                                        </div> <span><a href="cars.html" class="bg-gray">Used</a></span>
                                                    </div>
                                                </div>
                                                <div class="card border-0 mb-0">
                                                    <div class="card-body ">
                                                        <div class="item-card9"> <a href="cars.html" class="text-dark">
                                                                <h4 class="font-weight-semibold mt-1"> {{$value_poster->name}}
                                                                </h4>
                                                            </a>
                                                            <div class="item-card9-desc mb-2"> <a href="{{route('getDetail',['id'=>$value_poster->id])}}"
                                                                    class="mr-4"><span class=""><i
                                                                            class="fa fa-map-marker text-muted mr-1"></i>
                                                                        USA</span></a> <a href="#" class="mr-4"><span
                                                                        class=""><i
                                                                            class="fa fa-calendar-o text-muted mr-1"></i>
                                                                        {{$value_poster->created_at}}</span></a> </div>
                                                            <p class="mb-0 leading-tight">{{$value_poster->content}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer pt-4 pb-4 pr-4 pl-4">
                                                        <div class="item-card9-footer d-sm-flex">
                                                            <div class="item-card9-cost">
                                                                <h4 class="text-dark font-weight-bold mb-0 mt-0">
                                                                    $748.00</h4>
                                                            </div>
                                                            <div class="ml-auto"> <a href="#" class="mr-2"
                                                                    title="Car type"><i
                                                                        class="fa fa-car  mr-1 text-muted"></i>
                                                                    Automatic</a> <a href="#" class="mr-2"
                                                                    title="Kilometrs"><i
                                                                        class="fa fa-road text-muted mr-1 "></i>4000Kms</a>
                                                                <a href="#" class="" title="FuealType"><i
                                                                        class="fa fa-tachometer text-muted mr-1"></i>Electric</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="tab-pane" id="tab-12">
                                    <div class="row">
                                        @foreach($Poster as $value_poster)
                                            <div class="col-lg-6 col-md-12 col-xl-4">
                                                <div class="card overflow-hidden">
                                                    <div class="item-card9-img">
                                                        <div class="arrow-ribbon bg-primary">$748.00</div>
                                                        <div class="item-card9-imgs"> <a class="link" href="{{route('getDetail',['id'=>$value_poster->id])}}"></a>
                                                            <img src="../../public/img_poster/{{$value_poster->img}}" alt="img"
                                                                class="cover-image"> </div>
                                                        <div class="item-card9-icons"> <a href="{{route('getDetail',['id'=>$value_poster->id])}}"
                                                                class="item-card9-icons1 wishlist"> <i
                                                                    class="fa fa fa-heart-o"></i></a> </div>
                                                        <div class="item-overly-trans">
                                                            <div class="rating-stars"> <input type="number"
                                                                    readonly="readonly" class="rating-value star"
                                                                    name="rating-stars-value" value="3">
                                                                <div class="rating-stars-container">
                                                                    <div class="rating-star sm is--active"> <i
                                                                            class="fa fa-star"></i> </div>
                                                                    <div class="rating-star sm is--active"> <i
                                                                            class="fa fa-star"></i> </div>
                                                                    <div class="rating-star sm is--active"> <i
                                                                            class="fa fa-star"></i> </div>
                                                                    <div class="rating-star sm"> <i class="fa fa-star"></i>
                                                                    </div>
                                                                    <div class="rating-star sm"> <i class="fa fa-star"></i>
                                                                    </div>
                                                                </div>
                                                            </div> <span><a href="cars.html" class="bg-gray">Used</a></span>
                                                        </div>
                                                    </div>
                                                    <div class="card border-0 mb-0">
                                                        <div class="card-body ">
                                                            <div class="item-card9"> <a href="cars.html" class="text-dark">
                                                                    <h4 class="font-weight-semibold mt-1">
                                                                        {{$value_poster->title}}</h4>
                                                                </a>
                                                                <div class="item-card9-desc mb-2"> <a href="{{route('getDetail',['id'=>$value_poster->id])}}"
                                                                        class="mr-4"><span class=""><i
                                                                                class="fa fa-map-marker text-muted mr-1"></i>
                                                                            USA</span></a> <a href="#" class="mr-4"><span
                                                                            class=""><i
                                                                                class="fa fa-calendar-o text-muted mr-1"></i>
                                                                            {{$value_poster->created_at}}</span></a> </div>
                                                                <p class="mb-0 leading-tight">{{$value_poster->content}}</p>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer pt-4 pb-4 pr-4 pl-4">
                                                            <div class="item-card9-footer d-sm-flex">
                                                                <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left"
                                                                        title="Car type"><i
                                                                            class="fa fa-car  mr-1 text-muted"></i>
                                                                        Automatic</a> <a href="#"
                                                                        class="w-50 mt-1 mb-1 float-left"
                                                                        title="Kilometrs"><i
                                                                            class="fa fa-road text-muted mr-1 "></i>4000Kms</a>
                                                                    <a href="#" class="w-50 mt-1 mb-1 float-left"
                                                                        title="FuealType"><i
                                                                            class="fa fa-tachometer text-muted mr-1"></i>Electric</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="center-block text-center">
                            <ul class="pagination mb-3">
                                <li class="page-item page-prev disabled"> <a class="page-link" href="#"
                                        tabindex="-1">Prev</a> </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item page-next"> <a class="page-link" href="#">Next</a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--/Lists-->
            </div>
            <!--Right Side Content-->
            <div class="col-xl-3 col-lg-3 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="input-group"> <input type="text" class="form-control br-tl-3  br-bl-3"
                                placeholder="Search">
                            <div class="input-group-append "> <button type="button"
                                    class="btn btn-primary br-tr-3  br-br-3"> Search </button> </div>
                        </div>
                    </div>
                </div>
                <div class="card overflow-hidden">
                    <div class="px-4 py-3 border-bottom">
                        <h4 class="mb-0">Categories</h4>
                    </div>
                    <div class="card-body">
                        <div class="closed" id="container" style="height: 250px; overflow: hidden;">
                            <div class="filter-product-checkboxs"> <label class="custom-control custom-checkbox mb-3">
                                    <input type="checkbox" class="custom-control-input" name="checkbox1"
                                        value="option1"> <span class="custom-control-label"> <a href="#"
                                            class="text-dark">Lamborghini<span
                                                class="label label-secondary float-right">14</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox2" value="option2"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Tarragon<span
                                                class="label label-secondary float-right">22</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox3" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Specter<span
                                                class="label label-secondary float-right">78</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox4" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Chinnka Digli<span
                                                class="label label-secondary float-right">35</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox5" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Exercitationem<span
                                                class="label label-secondary float-right">23</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox6" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Subaru<span
                                                class="label label-secondary float-right">14</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox7" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Eiusmod<span
                                                class="label label-secondary float-right">45</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox7" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Quaerat<span
                                                class="label label-secondary float-right">34</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox7" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Interstate<span
                                                class="label label-secondary float-right">12</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox7" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Alfa Romeo<span
                                                class="label label-secondary float-right">18</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox7" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Harani<span
                                                class="label label-secondary float-right">02</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox7" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Perspiciatis<span
                                                class="label label-secondary float-right">15</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox7" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Slea<span
                                                class="label label-secondary float-right">32</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox7" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Blanditiis<span
                                                class="label label-secondary float-right">23</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox7" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Shrinking <span
                                                class="label label-secondary float-right">19</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox7" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Volkswagen<span
                                                class="label label-secondary float-right">12</span></a> </span>
                                </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox"
                                        class="custom-control-input" name="checkbox7" value="option3"> <span
                                        class="custom-control-label"> <a href="#" class="text-dark">Quaerat<span
                                                class="label label-secondary float-right">05</span></a> </span>
                                </label> </div>
                        </div>
                        <div class="showmore-button">
                            <div class="showmore-button-inner more">Show more</div>
                        </div>
                    </div>
                    <div class="px-4 py-3 border-bottom border-top">
                        <h4 class="mb-0">Price Range</h4>
                    </div>
                    <div class="card-body">
                        <div class="h6"> <input type="text" id="price"> </div>
                        <div id="mySlider"
                            class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                            <div class="ui-slider-range ui-widget-header ui-corner-all"
                                style="left: 19.2113%; width: 30.3337%;"></div><span
                                class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"
                                style="left: 19.2113%;"></span><span
                                class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"
                                style="left: 49.545%;"></span>
                        </div>
                    </div>
                    <div class="px-4 py-3 border-bottom">
                        <h4 class="mb-0">Condition</h4>
                    </div>
                    <div class="card-body">
                        <div class="filter-product-checkboxs"> <label class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2">
                                <span class="custom-control-label"> All </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox1" value="option1"> <span
                                    class="custom-control-label"> New </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox2" value="option2"> <span
                                    class="custom-control-label"> Used </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox3" value="option2"> <span
                                    class="custom-control-label"> Certified Pre-Owned </span> </label> </div>
                    </div>
                    <div class="px-4 py-3 border-bottom border-top">
                        <h4 class="mb-0">Year</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-6 mb-0"> <label for="inputState1"
                                    class="col-form-label">Min</label> <select id="inputState1"
                                    class="form-control select2 select2-hidden-accessible" data-select2-id="inputState1"
                                    tabindex="-1" aria-hidden="true">
                                    <option data-select2-id="2">1995</option>
                                    <option>1996</option>
                                    <option>1997</option>
                                    <option>1998</option>
                                    <option>1999</option>
                                    <option>2000</option>
                                    <option>2001</option>
                                    <option>2002</option>
                                    <option>2003</option>
                                    <option>2004</option>
                                    <option>2005</option>
                                    <option>2006</option>
                                    <option>2007</option>
                                    <option>2008</option>
                                    <option>2009</option>
                                    <option>2010</option>
                                    <option>2011</option>
                                    <option>2012</option>
                                    <option>2013</option>
                                    <option>2014</option>
                                    <option>2015</option>
                                    <option>2016</option>
                                    <option>2017</option>
                                    <option>2019</option>
                                    <option>2019</option>
                                    <option>2020</option>
                                </select><span class="select2 select2-container select2-container--default" dir="ltr"
                                    data-select2-id="1" style="width: 101px;"><span class="selection"><span
                                            class="select2-selection select2-selection--single" role="combobox"
                                            aria-haspopup="true" aria-expanded="false" tabindex="0"
                                            aria-labelledby="select2-inputState1-container"><span
                                                class="select2-selection__rendered" id="select2-inputState1-container"
                                                role="textbox" aria-readonly="true" title="1995">1995</span><span
                                                class="select2-selection__arrow" role="presentation"><b
                                                    role="presentation"></b></span></span></span><span
                                        class="dropdown-wrapper" aria-hidden="true"></span></span> </div>
                            <div class="form-group col-md-6 mb-0"> <label for="inputState2"
                                    class="col-form-label">Max</label> <select id="inputState2"
                                    class="form-control select2 select2-hidden-accessible" data-select2-id="inputState2"
                                    tabindex="-1" aria-hidden="true">
                                    <option data-select2-id="4">2020</option>
                                    <option>2019</option>
                                    <option>2019</option>
                                    <option>2017</option>
                                    <option>2016</option>
                                    <option>2015</option>
                                    <option>2014</option>
                                    <option>2013</option>
                                    <option>2012</option>
                                    <option>2011</option>
                                    <option>2010</option>
                                    <option>2009</option>
                                    <option>2008</option>
                                    <option>2007</option>
                                    <option>2006</option>
                                    <option>2005</option>
                                    <option>2004</option>
                                    <option>2003</option>
                                    <option>2002</option>
                                    <option>2001</option>
                                    <option>2000</option>
                                    <option>1999</option>
                                    <option>1998</option>
                                    <option>1997</option>
                                    <option>1996</option>
                                    <option>1995</option>
                                </select><span class="select2 select2-container select2-container--default" dir="ltr"
                                    data-select2-id="3" style="width: 101px;"><span class="selection"><span
                                            class="select2-selection select2-selection--single" role="combobox"
                                            aria-haspopup="true" aria-expanded="false" tabindex="0"
                                            aria-labelledby="select2-inputState2-container"><span
                                                class="select2-selection__rendered" id="select2-inputState2-container"
                                                role="textbox" aria-readonly="true" title="2020">2020</span><span
                                                class="select2-selection__arrow" role="presentation"><b
                                                    role="presentation"></b></span></span></span><span
                                        class="dropdown-wrapper" aria-hidden="true"></span></span> </div>
                        </div>
                    </div>
                    <div class="px-4 py-3 border-bottom border-top">
                        <h4 class="mb-0">Posted By</h4>
                    </div>
                    <div class="card-body">
                        <div class="filter-product-checkboxs"> <label class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="checkbox1" value="option1">
                                <span class="custom-control-label"> Dealer </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox2" value="option2"> <span
                                    class="custom-control-label"> Individual </span> </label> <label
                                class="custom-control custom-checkbox mb-0"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox2" value="option2"> <span
                                    class="custom-control-label"> Reseller </span> </label> </div>
                    </div>
                    <div class="px-4 py-3 border-bottom border-top">
                        <h4 class="mb-0">Fuel Type</h4>
                    </div>
                    <div class="card-body">
                        <div class="filter-product-checkboxs"> <label class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="checkbox11" value="option1">
                                <span class="custom-control-label"> Electric </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox11" value="option2"> <span
                                    class="custom-control-label"> Diesel </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox11" value="option2"> <span
                                    class="custom-control-label"> Petrol </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox11" value="option2"> <span
                                    class="custom-control-label"> Hybrid </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox11" value="option2"> <span
                                    class="custom-control-label"> Petrol+CNG </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox11" value="option2"> <span
                                    class="custom-control-label"> Petrol+LPG </span> </label> </div>
                    </div>
                    <div class="px-4 py-3 border-bottom border-top">
                        <h4 class="mb-0">Body Type</h4>
                    </div>
                    <div class="card-body">
                        <div class="filter-product-checkboxs"> <label class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="checkbox12" value="option1">
                                <span class="custom-control-label"> Convertable </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox12" value="option2"> <span
                                    class="custom-control-label"> Coupe </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox12" value="option2"> <span
                                    class="custom-control-label"> Crossover </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox12" value="option2"> <span
                                    class="custom-control-label"> Hatchback </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox12" value="option2"> <span
                                    class="custom-control-label"> Muv </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox12" value="option2"> <span
                                    class="custom-control-label"> Quadricycle </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox12" value="option2"> <span
                                    class="custom-control-label"> Ringer Ace </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox12" value="option2"> <span
                                    class="custom-control-label"> SUV </span> </label> </div>
                    </div>
                    <div class="px-4 py-3 border-bottom border-top">
                        <h4 class="mb-0">Transmission</h4>
                    </div>
                    <div class="card-body">
                        <div class="filter-product-checkboxs"> <label class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="checkbox13" value="option1">
                                <span class="custom-control-label"> AMT </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox13" value="option2"> <span
                                    class="custom-control-label"> Automatic </span> </label> <label
                                class="custom-control custom-checkbox mb-2"> <input type="checkbox"
                                    class="custom-control-input" name="checkbox13" value="option2"> <span
                                    class="custom-control-label"> Manual </span> </label> </div>
                    </div>
                    <div class="card-footer"> <a href="#" class="btn btn-secondary btn-block">Apply Filter</a>
                    </div>
                </div>
                <div class="card mb-0">
                    <div class="card-header">
                        <h3 class="card-title">Shares</h3>
                    </div>
                    <div class="card-body product-filter-desc">
                        <div class="product-filter-icons text-center"> <a href="#" class="facebook-bg"><i
                                    class="fa fa-facebook"></i></a> <a href="#" class="twitter-bg"><i
                                    class="fa fa-twitter"></i></a> <a href="#" class="google-bg"><i
                                    class="fa fa-google"></i></a> <a href="#" class="dribbble-bg"><i
                                    class="fa fa-dribbble"></i></a> <a href="#" class="pinterest-bg"><i
                                    class="fa fa-pinterest"></i></a> </div>
                    </div>
                </div>
            </div>
            <!--/Right Side Content-->
        </div>
    </div>
</section>
<!--/Listing-->
@endsection
