@extends('client.master')
@section('content')
</div>
<!--/Topbar-->
<!--Section-->
<script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
<form id="form-send" method="post" action="{{route('sendContact')}}">
    @csrf
    <label id="label-form">
        <p class="label-form">Send message</p>
        <p class="label-form" id="label-cancel">X</p>
    </label>
    <hr>
    <div>
        <input type="text" class="form-control form-form-control" name="name_contact" placeholder="Your Name">
        <p></p>
        <input type="text" placeholder="Email Address" class="form-control form-form-control" name="email_contact">
        <p></p>
        <textarea id="form-form-control" name="content_contact" class="form-control form-form-control"
            placeholder="Messages"></textarea>
    </div>
    <hr>
    <div id="button-send">
        <input type="hidden" name="id_user_ads" value="{{$data_info_user['id']}}">
        <input type="submit" id="btn-cancel" value="Cancel">
        <input type="submit" id="btn-send" value="Send" name="Send">
    </div>
</form>

<form id="form-report" method="post">
    <label id="label-form">
        <p class="label-form">Report Abuse</p>
        <p class="label-form" id="label-cancel">X</p>
    </label>
    <hr>
    <div>
        <input type="text" class="form-control form-form-control" name="" placeholder="Enter url">
        <p></p>

        <input type="text" class="form-control form-form-control" name="" placeholder="Title">
        <p></p> <input type="text" placeholder="Email Address" class="form-control form-form-control" name="">
        <p></p>
        <textarea id="form-form-control" class="form-control form-form-control" placeholder="Messages"></textarea>
    </div>
    <hr>
    <div id="button-send">
        <input type="submit" id="btn-cancel" value="Cancel">
        <input type="submit" id="btn-send" value="Send" name="Send">
    </div>

</form>


<div>
    <div class="cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg"
        style="background: url(&quot;../assets/images/banners/banner1.jpg&quot;) center center;">
        <div class="header-text1 mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 col-lg-12 col-md-12 d-block mx-auto">
                        <div class="search-background bg-transparent">
                            <div class="form row no-gutters ">
                                <div class="form-group  col-xl-4 col-lg-3 col-md-12 mb-0 bg-white">
                                    <input type="text" class="form-control input-lg br-tr-md-0 br-br-md-0" id="text4"
                                        placeholder="Find Product">
                                </div>
                                <div class="form-group  col-xl-3 col-lg-3 col-md-12 mb-0 bg-white"> <input type="text"
                                        class="form-control input-lg br-md-0" id="text5" placeholder="Enter Location">
                                    <span><i class="fa fa-map-marker location-gps mr-1"></i> </span> </div>
                                <div class="form-group col-xl-3 col-lg-3 col-md-12 select2-lg  mb-0 bg-white">
                                    <select
                                        class="form-control select2-show-search border-bottom-0 w-100 select2-hidden-accessible"
                                        data-placeholder="Select" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                        <optgroup label="Categories" data-select2-id="9">
                                            <option data-select2-id="3">Choose Make</option>
                                            <option value="1" data-select2-id="10">Lamborghini</option>
                                            <option value="2" data-select2-id="11">Tarragon</option>
                                            <option value="3" data-select2-id="12">Specter</option>
                                            <option value="4" data-select2-id="13">Chinnka Digli</option>
                                            <option value="5" data-select2-id="14">Chilkara Megha</option>
                                            <option value="6" data-select2-id="15">Subaru</option>
                                            <option value="7" data-select2-id="16">Eiusmod</option>
                                            <option value="8" data-select2-id="17">Quaerat</option>
                                            <option value="9" data-select2-id="18">Interstate</option>
                                            <option value="10" data-select2-id="19">Alfa Romeo</option>
                                        </optgroup>
                                    </select><span class="select2 select2-container select2-container--default"
                                        dir="ltr" data-select2-id="20" style="width: 100%;"><span
                                            class="selection"><span class="select2-selection select2-selection--single"
                                                role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0"
                                                aria-labelledby="select2-bvus-container"><span
                                                    class="select2-selection__rendered" id="select2-bvus-container"
                                                    role="textbox" aria-readonly="true" title="Choose Make">Choose
                                                    Make</span><span class="select2-selection__arrow"
                                                    role="presentation"><b
                                                        role="presentation"></b></span></span></span><span
                                            class="dropdown-wrapper" aria-hidden="true"></span></span> </div>
                                <div class="col-xl-2 col-lg-3 col-md-12 mb-0"> <a href="#"
                                        class="btn btn-lg btn-block btn-primary br-tl-md-0 br-bl-md-0">Search
                                        Here</a> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /header-text -->
    </div>
</div>
<!--/Section-->
<!--Breadcrumb-->
<div class="bg-white border-bottom">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title">Ad Details</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Pages</a></li>
                <li class="breadcrumb-item active" aria-current="page">Ad Details</li>
            </ol>
        </div>
    </div>
</div>
<!--/Breadcrumb-->
<!--listing-->
@foreach($poster as $value)
<section class="sptb">
    <div class="container">
        <div class="row">

            <!--Right Side Content-->
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Posted By</h3>
                    </div>
                    <div class="card-body  item-user">
                        <div class="profile-pic mb-0">
                            @if($data_info_user['img'] != null)
                            <img src="../../public/img_user/{{$data_info_user['img']}}" class="brround avatar-xxl"
                                alt="user">
                            @else
                            <img src="../../public/client/user.png" class="brround avatar-xxl" alt="user">
                            @endif
                            <div> <a href="userprofile.html" class="text-dark">
                                    <h4 class="mt-3 mb-1 font-weight-semibold">{{$data_info_user['name']}}</h4>
                                </a>
                                <h6 class="text-muted font-weight-normal">Web designer</h6> <span
                                    class="text-muted">Member Since November 2008</span>
                                <h6 class="mt-2 mb-0"><a href="{{route('getPersonal',['id'=>$value->id_user])}}"
                                        class="btn btn-primary btn-sm">See
                                        All Ads</a></h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-body item-user">
                        <h4 class="mb-4">Contact Info</h4>
                        <div>
                            <h6><span class="font-weight-semibold"><i class="fa fa-envelope mr-3 mb-2"></i></span><a
                                    href="#" class="text-body">{{$data_info_user['email']}}</a></h6>
                            <h6><span class="font-weight-semibold"><i class="fa fa-phone mr-3  mb-2"></i></span><a
                                    href="#" class="text-body">
                                    {{$data_info_user['phone']}}</a></h6>
                            <h6><span class="font-weight-semibold"><i class="fa fa-link mr-3 mb-2"></i></span><a
                                    href="{{$data_info_user['face']}}" class="text-body">{{$data_info_user['face']}}</a>
                            </h6>
                            <h6><span class="font-weight-semibold"><i class="fa fa-map-marker mr-3 "></i></span><a
                                    class="text-body"> {{$data_info_user['address']}} {{$data_info_user['city']}}</a>
                            </h6>
                        </div>
                        <div class=" item-user-icons mt-4"> <a href="{{$data_info_user['face']}}"
                                class="facebook-bg mt-0"><i class="fa fa-facebook"></i></a> <a
                                href="{{$data_info_user['google']}}" class="twitter-bg"><i
                                    class="fa fa-twitter"></i></a> <a href="{{$data_info_user['twitter']}}"
                                class="google-bg"><i class="fa fa-google"></i></a> <a
                                href="{{$data_info_user['print']}}" class="dribbble-bg"><i
                                    class="fa fa-dribbble"></i></a> </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-left"> <a href="#" class="btn  btn-info"><i class="fa fa-envelope"></i>
                                Chat</a> <a id="contact-me" class="btn btn-primary" data-toggle="modal"
                                data-target="#contact"><i class="fa fa-user"></i> Contact Me</a> </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Keywords</h3>
                    </div>
                    <div class="card-body product-filter-desc">
                        <div class="product-tags clearfix">
                            <ul class="list-unstyled mb-0">
                                <li><a href="#">Herkime</a></li>
                                <li><a href="#">Bennington</a></li>
                                <li><a href="#">Eternity</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Shares</h3>
                    </div>
                    <div class="card-body product-filter-desc">

                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <div class="addthis_inline_share_toolbox"></div>

                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Map location</h3>
                    </div>
                    <div class="card-body">
                        <div class="map-header">
                            <div class="map-header-layer" id="map2">
                                <div id="_map2">
                                    <div class="gm-err-container">
                                        <div class="gm-err-content">
                                            <div class="gm-err-icon"><img
                                                    src="https://maps.gstatic.com/mapfiles/api-3/images/icon_error.png"
                                                    draggable="false" style="user-select: none;"></div>
                                            <div class="gm-err-title">Rất tiếc! Đã xảy ra lỗi.</div>
                                            <div class="gm-err-message">Trang này đã không tải Google Maps đúng
                                                cách. Hãy xem bảng điều khiển JavaScript để biết chi tiết kỹ
                                                thuật.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Search Ads</h3>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('searchPost')}}">
                            @csrf
                            <input type="text" name="car_search" class="form-control" placeholder="Nhập tên xe">
                            <select name="cate_search" class="form-control">
                                @foreach($brand_car as $value_brand)
                                <option value="{{$value_brand->id}}">{{$value_brand->name}}</option>
                                @endforeach
                            </select>
                            <input type="submit" name="search" value="Search">
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Popular Tags</h3>
                    </div>
                    <div class="card-body">
                        <div class="product-tags clearfix">
                            <ul class="list-unstyled mb-0">
                                <li><a href="#">Herkime</a></li>
                                <li><a href="#">Eiusmod</a></li>
                                <li><a href="#">Shrinking Moco</a></li>
                                <li><a href="#">Chilkara Megha</a></li>
                                <li><a href="#">CrusaderRecusandae</a></li>
                                <li><a href="#">Eternity</a></li>
                                <li><a href="#">Shrinking Moco</a></li>
                                <li><a href="#">Lamborghini</a></li>
                                <li><a href="#">Millenium</a></li>
                                <li><a href="#">Provident</a></li>
                                <li><a href="#" class="mb-0">Bennington</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card mb-0">
                    <div class="card-header">
                        <h3 class="card-title">Latest Seller Ads</h3>
                    </div>
                    <div class="card-body pb-3">
                        <div class="rated-products">
                            <ul class="vertical-scroll" id="vertical-scroll">
                                @foreach($all_poster_by_brand as $value_all_poster_by_brand)
                                    <li style="" class="item">
                                        <div class="media p-5 mt-0"> 
                                            @if((array)$value_all_poster_by_brand->img == null)
                                                <img class=" mr-4" src="../../public/client/xe.png" alt="img">
                                            @else
                                                <img class=" mr-4" src="../../public/img_poster/{{$value_all_poster_by_brand->img[0]}}" alt="img">
                                            @endif
                                            <div class="media-body">
                                                <h4 class="mt-2 mb-1">{{$value_all_poster_by_brand->title}}</h4> 
                                                <span class="rated-products-ratings">
                                                    <?php $rate = 0 ?>
                                                    @if($value_all_poster_by_brand->rate == 0)
                                                        @for($i=$rate;$i<5;$i++)
                                                            <i class="fa fa-star" style="color: #cac5c5"></i>
                                                        @endfor
                                                    @else
                                                        @for($i=1;$i<=$value_all_poster_by_brand->rate;$i++)
                                                            <i class="fa fa-star text-warning"></i>
                                                            <?php $rate++ ?>
                                                        @endfor
                                                        @if($rate <5)
                                                            @for($i=$rate;$i<5;$i++)
                                                                <i class="fa fa-star" style="color: #cac5c5"></i>
                                                            @endfor
                                                        @endif
                                                    @endif
                                                </span>
                                                <div class="h5 mb-0 font-weight-semibold mt-1">{{$value_all_poster_by_brand->price}}</div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--Right Side Content-->
            <div class="col-xl-8 col-lg-8 col-md-12">
                <!--Description-->
                <div class="card overflow-hidden">
                    <div class="ribbon ribbon-top-right text-danger"><span class="bg-danger">Offer</span></div>
                    <div class="card-body">
                        <div class="item-det mb-4"> <a href="#" class="text-dark">
                                <h3 class="">{{$value->title}}</h3>
                            </a>
                            <ul class="d-flex">
                                <li class="mr-5"><a href="#" class="icons"><i
                                            class="icon icon-location-pin text-muted mr-1"></i> Hà Nội</a></li>
                                <li class="mr-5"><a href="#" class="icons"><i
                                            class="icon icon-calendar text-muted mr-1"></i>{{$value->created_at}}</a>
                                </li>
                                <li class="mr-5"><a href="#" class="icons"><i class="icon icon-eye text-muted mr-1"></i>
                                        765</a></li>
                                <li class="">
                                    <a href="#" class="icons">
                                    <?php $rate = 1 ?>
                                    @for($i=1;$i<=$avg;$i++)
                                        <i class="fa fa-star text-warning"></i>
                                        <?php $rate++ ?>
                                    @endfor
                                    @if($rate <5)
                                        @for($i=$rate;$i<=5;$i++)
                                            <i class="fa fa-star" style="color: #cac5c5"></i>
                                        @endfor
                                    @endif
                                    </a>
                                    {{$avg}}
                                </li>
                            </ul>
                        </div>
                        <div class="product-slider">
                            <div id="carousel" class="carousel slide" data-ride="carousel">
                                <div class="arrow-ribbon2 bg-primary">{{$value->price}}</div>
                                <div class="carousel-inner">  
                                    @if($value->img == null)
                                        <img src="../../public/client/xe.png" alt="img" style="width: 60px;height: 60px;margin-top: 12px;"> 
                                    @else
                                        @foreach((array)$value->img as $key=>$img)
                                            @if($key == 1)
                                                <div class="carousel-item active"> 
                                                    <img style="height: 500px" src="../../public/img_poster/{{$img}}" alt="img"> 
                                                </div>
                                            @else
                                                <div class="carousel-item"> 
                                                    <img style="height: 500px" src="../../public/img_poster/{{$img}}" alt="img"> 
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div> <a class="carousel-control-prev" href="#carousel" role="button"
                                    data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a>
                                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next"> <i
                                        class="fa fa-angle-right" aria-hidden="true"></i> </a>
                            </div>
                            <div class="clearfix">
                                <div id="thumbcarousel" class="carousel thumbcarousel slide" data-interval="false">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            @if($value->img == null)
                                                <img src="../../public/client/xe.png" alt="img" style="width: 60px;height: 60px;margin-top: 12px;"> 
                                            @else
                                                <?php $to = 0 ?>
                                                @foreach((array)$value->img as $key=>$img)
                                                    <div data-target="#carousel" data-slide-to="{{$to}}" class="thumb">
                                                        <img style="height: 100px"  src="../../public/img_poster/{{$img}}" alt="img">
                                                    </div>
                                                    <?php $to++ ?>
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="carousel-item">
                                             @if($value->img == null)
                                                <img src="../../public/client/xe.png" alt="img" style="width: 60px;height: 60px;margin-top: 12px;"> 
                                            @else
                                                <?php $to = 0 ?>
                                                @foreach((array)$value->img as $key=>$img)
                                                    <div data-target="#carousel" data-slide-to="{{$to}}" class="thumb">
                                                        <img style="height: 100px" src="../../public/img_poster/{{$img}}" alt="img">
                                                    </div>
                                                    <?php $to++ ?>
                                                @endforeach
                                            @endif                                          
                                        </div>
                                    </div> <a class="carousel-control-prev" href="#thumbcarousel" role="button"
                                        data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i>
                                    </a> <a class="carousel-control-next" href="#thumbcarousel" role="button"
                                        data-slide="next"> <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="">
                        <div class="border-0 mb-5">
                            <div class="wideget-user-tab wideget-user-tab3">
                                <div class="tab-menu-heading">
                                    <div class="tabs-menu1">
                                        <ul class="nav">
                                            <li class=""><a href="#tab-1" class="active" data-toggle="tab">Overview</a>
                                            </li>
                                            @if($value->check == 0)
                                                <li><a href="#tab-4" data-toggle="tab" class="">Vehicle
                                                    Information</a></li>
                                                <li><a href="#tab-5" data-toggle="tab" class="">About Video</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content border-left border-right border-top br-tr-3 p-5 bg-white">
                                        <div class="tab-pane active" id="tab-1">
                                            <h3 class="card-title mb-3 font-weight-semibold">Overview</h3>
                                            <div class="mb-4">
                                                {{$value->description}}
                                            </div>
                                        </div>
                                        @if($value->check == 0)
                                            <div class="tab-pane" id="tab-4">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered border-top mb-0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Km</td>
                                                                        <td>{{$value->km_went}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Engine Displacement (cc)</td>
                                                                        <td>{{$value->enegine_displacement}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Max Torque (nm@rpm)</td>
                                                                        <td>{{$value->max_torque}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Seating Capacity</td>
                                                                        <td>{{$value->seats}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Boot Space (Litres)</td>
                                                                        <td>{{$value->bootspace}}</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered border-top mb-0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Fuel Type</td>
                                                                        <td>{{$value->fuel_type}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Max Power (bhp@rpm)</td>
                                                                        <td>{{$value->max_power}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>TransmissionType</td>
                                                                        <td>{{$value->transmission_type}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Fuel Tank Capacity</td>
                                                                        <td>{{$value->fuel_tank_capacity}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Body Type</td>
                                                                        <td>{{$value->category_id}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Break</td>
                                                                        <td>{{$value->break}}</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab-5">
                                                <ul class="list-unstyled video-list-thumbs row ">
                                                    <li class="mb-0"> 
                                                        <a data-toggle="modal" data-target="#homeVideo"> 
                                                            <video width="1000" height="350" controls>
                                                                <source src="../../public/video_poster/videoplayback.mp4" type="video/mp4">
                                                                Trình duyệt của bạn không hỗ trợ HTML5.
                                                            </video> 
                                                            <span class="mdi mdi-arrow-right-drop-circle-outline text-white"></span>
                                                        </a> </li>
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                            <div class="card-footer bg-white border-left border-right border-bottom br-br-3 br-bl-3">
                                <div class="icons"> <a href="#" class="btn btn-info icons"><i
                                            class="icon icon-share mr-1"></i> Share Ad</a>
                                    <a id="report-abuse" class="btn btn-danger icons" data-toggle="modal"
                                        data-target="#report"><i class="icon icon-exclamation mr-1"></i> Report
                                        Abuse</a>

                                    <a id="a_like" class="btn btn-primary icons"><i
                                            class="icon icon-heart  mr-1"></i><span id="qty_like">{{$like}}</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/Description-->
                <!--Comments-->
                <div class="card">
                    <div class="card-body p-0" id="all_comment">
                        @for ($i = 0; $i < count($list_comment) ; $i++)
                            @if( $list_comment[$i]['comment_id'] == null ) 
                                <div class="media p-5 border-top mt-0">
                                    <div class="d-flex mr-3"> 
                                        <a href="#">
                                            @if($list_comment[$i]['img'] != null)
                                            <img class="media-object brround" alt="64x64"
                                                src="../assets/images/faces/male/3.jpg">
                                            @else
                                            <img class="media-object brround" alt="64x64" src="../../public/client/user.png">
                                            @endif
                                        </a> 
                                    </div>
                                    <div class="media-body">
                                        <span id="div_2">
                                            <h5 class="mt-0 mb-1 font-weight-semibold"> {{$list_comment[$i]['name']}}<span
                                                    class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" title=""
                                                    data-original-title="verified"><i
                                                        class="fa fa-check-circle-o text-success"></i><i class="fa fa-calendar"></i>
                                                {{$list_comment[$i]['created_at']}}</span>
                                            </h5> 
                                        </span>
                                        <span class="content_rep">
                                            <p class="font-13  mb-2 mt-2"> {{$list_comment[$i]['content']}}</p>
                                            <a class="reply mr-2" id="{{$list_comment[$i]['id']}}">
                                                <span class="badge badge-primary">Reply</span>
                                            </a>
                                        </span>
                                        @include('client.ads.comment',['list_comment'=>$list_comment,'id'=>$list_comment[$i]['id']])
                                        <form method="post" class="reply_comment" id="reply_comment_{{$list_comment[$i]['id']}}" action="{{route('rep_comment')}}" style="display: none;">
                                            @csrf
                                            <div class="media p-5">
                                                <div class="d-flex mr-3 img_user"></div>
                                                <div class="media-body">
                                                    <span class="div_2"></span>
                                                    <br>
                                                    <p class="content_rep">
                                                        <textarea rows="2" placeholder="text here.." id="rep_comment_{{$list_comment[$i]['id']}}" name="rep_comment_{{$list_comment[$i]['id']}}" class="textarea_rep"></textarea>
                                                        <input type="hidden" id="comment_id_{{$list_comment[$i]['id']}}" name="comment_id__{{$list_comment[$i]['id']}}" value="{{$list_comment[$i]['id']}}">
                                                        <p class="errors errors_bottom" id="error_rep_comment_{{$list_comment[$i]['id']}}"></p>
                                                        <input type="submit" value="Reply" name="reply_comment">
                                                    </p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div> 
                            @endif
                        @endfor
                </div>
            </div>
            <!--/Comments-->
            @if($check==0)
            <div class="card mb-lg-0">
                <div class="card-body" id="alert_rate">                   
                        <label>Hãy đánh giá chất lượng sản phẩm</label>
                        <form id="rate">
                            @csrf
                                    <div id="rate" style="display: inline-block;">
                                        <i class="fas fa-star" id="fa-star-rate_0" style="color: #ffa22b; font-size: 45px"></i>
                                        <i class="fas fa-star fa-star-rate" id="fa-star-rate_1"></i>
                                        <i class="fas fa-star fa-star-rate" id="fa-star-rate_2"></i>
                                        <i class="fas fa-star fa-star-rate" id="fa-star-rate_3"></i>
                                        <i class="fas fa-star fa-star-rate" id="fa-star-rate_4"></i>
                                        <input type="hidden" id="__sum_rate" name="sum_rate" value="0">
                                    </div>
                                    <input style="display: block;" type="submit" value="Đánh giá">
                        </form>                   
                </div>             
            </div>
            @endif  
            <div class="card mb-lg-0" style="margin-top: 30px">
                <div class="card-header">
                    <h3 class="card-title">Leave a reply</h3>
                </div>
                <div class="card-body">
                    <form method="post" action="{{route('sendReply')}}">
                        @csrf
                        <div>
                            <div class="form-group">
                                @if($errors->has('your_name_reply'))<p style="color: red" style="color: red">
                                    <strong>{{$errors->first('your_name_reply')}}</strong></p>@endif
                                <input type="text" class="form-control" value="{{old('your_name_reply')}}"
                                    name="your_name_reply" id="name1" placeholder="Your Name">
                            </div>
                            <div class="form-group">
                                @if($errors->has('email_reply'))<p style="color: red" style="color: red">
                                    <strong>{{$errors->first('email_reply')}}</strong></p>@endif
                                <input type="text" class="form-control" value="{{old('email_reply')}}"
                                    name="email_reply" id="email" placeholder="Email Address">
                            </div>
                            <div class="form-group">
                                @if($errors->has('textarea_reply'))<p style="color: red" style="color: red">
                                    <strong>{{$errors->first('textarea_reply')}}</strong></p>@endif
                                <textarea class="form-control" value="{{old('textarea_reply')}}" name="textarea_reply"
                                    name="example-textarea-input" rows="6" placeholder="Comment"></textarea>
                            </div>
                            <input type="submit" id="send-reply" name="Send" value="Send">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    <input type="hidden" id="route" value="{{route('like')}}">
    <input type="hidden" id="id_poster" value=" {{$value->id}}">
    @if(Auth::check())
    <input type="hidden" id="id_user" value=" {{Auth::User()->id}}">
    @endif
    <input type="hidden" id="url_get_info" value=" {{route('AjaxGetInfo')}}">
    <input type="hidden" id="url_rep_comment" value=" {{route('rep_comment')}}">
    <input type="hidden" id="url_rate" value=" {{route('rate')}}">
</section>
@endforeach
<!--/Add listing-->
@endsection
