

@extends('client.master')
@section('content')
</div> <!-- Topbar -->
<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg"
        style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
        <div class="header-text mb-0">
            <div class="text-center text-white">
                <h1 class="">My Ads</h1>
                <ol class="breadcrumb text-center">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">My Dashboard </a></li>
                    <li class="breadcrumb-item text-white" aria-current="page">My Ads</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!--/Breadcrumb-->
<!--Section-->

<section class="sptb">
    <div class="container">
        <div class="row">
            <!-- sidebar -->
            @include('client.sidebar',['data'=>$data])
            <!-- end sidebar -->
            <div class="col-xl-9 col-lg-12 col-md-12">
                <div class="card mb-0">
                        <div class="card-header">
                            <h3 class="card-title">My Ads</h3>
                        </div>
                        <div class="card-body">
                            <div class="ads-tabs">
                                <div class="tabs-menus">
                                    <!-- Tabs -->
                                    <ul class="nav">
                                        <li class=""><a href="#tab1" id="all" class="brand active" data-toggle="tab">12</a></li>
                                        @foreach($brand_car as $value_brand)
                                            <li class=""><a id="{{$value_brand->id}}" class="brand active" data-toggle="tab">{{$value_brand->name}}</a></li>
                                        @endforeach

                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active table-responsive border-top userprof-tab" id="tab1">
                                        <table class="table table-bordered table-hover mb-0 text-nowrap">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Item</th>
                                                    <th>Category</th>
                                                    <th>Price</th>
                                                    <th>Ad Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="reload">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>
<input type="hidden" id="get_poster_info_by_click" value="{{route('get_poster_info_by_click')}}">
<input type="hidden" id="get_poster_info_pagination" value="{{route('get_poster_info_pagination')}}">
<input type="hidden" id="id_brand" value="">
<script type="text/javascript">  
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function(){
        $("a.brand").click(function(){
            var id_brand = $(this).attr("id");
            $("input#id_brand").val(id_brand);
            var url = $("input#get_poster_info_by_click").val();
            $.ajax({
                url: url,
                data: {id_brand:id_brand},
                type: 'get',
                success: function (data) {
                    $("tbody#reload").html(data);
                }
            });
        }) 
    });
$(document).on('click','.pagination a',function(e){
    e.preventDefault();
    var page=$(this).attr('href').split('page=')[1];  
    getData(page, $("input#id_brand").val());
}); 

function getData(page,id_brand){
    var url = $("input#get_poster_info_pagination").val();
    $.ajax({
        url: url+'?page=' + page,
        data: {id_brand:id_brand},
        type: 'get',
        success: function (data) {
            $("tbody#reload").html(data);
        }
    });
}
</script>
<!--Section-->
@endsection
    
