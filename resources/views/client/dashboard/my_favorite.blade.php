@extends('client.master')
@section('content')
</div>
<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg"
        style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">My Favorite Ads</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Dashboard</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">My Favorite Ads</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Breadcrumb-->
<!--Section-->
<section class="sptb">
    <div class="container">
        <div class="row">

            @include('client.sidebar',['data'=>$data])

            <div class="col-xl-9 col-lg-12 col-md-12">
                <div class="card mb-0">
                    <div class="card-header">
                        <h3 class="card-title">My Favorite Ads</h3>
                    </div>
                    <div class="card-body">
                        <div class="my-favadd table-responsive border-top userprof-tab">
                            @if(Session::has('messages'))
                            <strong style="color: green">{{Session::get('messages')}}</strong>
                            @endif
                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Item</th>
                                        <th>Category</th>
                                        <th>Price</th>
                                        <th>Ad Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($poster as $value_poster)
                                        @foreach($favorite as $value_favorite)
                                            @if( ($value_favorite->poster_id == $value_poster->id) && ($value_poster->check==0) )
                                            <tr>
                                                <td> <label class="custom-control custom-checkbox"> <input type="checkbox"
                                                            class="custom-control-input" name="checkbox" value="checkbox"> <span
                                                            class="custom-control-label"></span>
                                                    </label> </td>
                                                <td>
                                                    <div class="media mt-0 mb-0">
                                                        <div class="card-aside-img"> <a href="#"></a> 
                                                            @if($value_poster->img == null)
                                                                    <img src="../../public/client/xe.png" alt="img" style="width: 60px;height: 60px;margin-top: 12px;"> 
                                                                @else
                                                                    @foreach((array)$value_poster->img as $key=>$img)
                                                                        @if($key == 0)
                                                                            <img src="../../public/img_poster/{{$img}}" alt="img" style="width: 60px;height: 60px;margin-top: 12px;"> 
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                        </div>
                                                        <div class="media-body">
                                                            <div class="card-item-desc ml-4 p-0 mt-2"> <a href="#"
                                                                    class="text-dark">
                                                                    <h4 class="font-weight-semibold">{{$value_poster->title}}
                                                                    </h4>
                                                                </a> <a href="#"><i class="fa fa-clock-o mr-1"></i>
                                                                    {{$value_poster->created_at}}</a><br><a href="#"><i
                                                                        class="fa fa-tag mr-1"></i> Offer</a> </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    @foreach($category as $value_cate)
                                                    @if($value_poster->category_id == $value_cate->id)
                                                    {{$value_cate->name}}
                                                    @endif
                                                    @endforeach
                                                </td>
                                                <td class="font-weight-semibold fs-16">$89</td>
                                                <td> <a href="#" class="badge badge-primary">Active</a> </td>
                                                <td> <a href="{{route('deleteFavorite',['id'=>$value_favorite->id])}}"
                                                        class="btn btn-info btn-sm text-white" data-toggle="tooltip"
                                                        data-original-title="Delete from Wishlist" onclick="return confirm('Bạn có muốn xóa? ')"><i
                                                            class="fa fa-trash"></i></a>
                                                    <a href="{{route('getDetail',['id'=>$value_poster->id])}}" class="btn btn-primary btn-sm text-white" data-toggle="tooltip"
                                                            data-original-title="View"><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <nav aria-label="Page navigation example" style="float: right;margin-top: 20px">
                            {!! $favorite->links() !!}
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Section-->

@endsection
