                                                @if(count($poster) > 0)
                                                @foreach($poster as $value_poster)
                                                <tr>
                                                    <td> <label class="custom-control custom-checkbox"> <input
                                                                type="checkbox" class="custom-control-input" name="checkbox"
                                                                value="checkbox"> <span class="custom-control-label"></span>
                                                        </label> </td>
                                                    <td>
                                                        <div class="media mt-0 mb-0">
                                                            <div class="card-aside-img"> <a href="#"></a> 
                                                                @if($value_poster->img == null)
                                                                    <img src="../../public/client/xe.png" alt="img" style="width: 60px;height: 60px;margin-top: 12px;"> 
                                                                @else
                                                                    @foreach((array)$value_poster->img as $key=>$img)
                                                                        @if($key == 0)
                                                                            <img src="../../public/img_poster/{{$img}}" alt="img" style="width: 60px;height: 60px;margin-top: 12px;"> 
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </div>
                                                            <div class="media-body">
                                                                <div class="card-item-desc ml-4 p-0 mt-2"> <a href="#"
                                                                        class="text-dark">
                                                                        <h4 class="font-weight-semibold">{{$value_poster->title}}
                                                                        </h4>
                                                                    </a> <a href="#"><i class="fa fa-clock-o mr-1"></i>
                                                                        {{$value_poster->created_at}}</a><br><a href="#"><i
                                                                            class="fa fa-tag mr-1"></i>sale</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        @foreach($category as $value_cate)
                                                            @if($value_poster->category_id == $value_cate->id)
                                                                {{$value_cate->name}}
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td class="font-weight-semibold fs-16">{{$value_poster->price}} VNĐ</td>
                                                    <td> <a href="#" class="badge badge-warning">Published</a> </td>
                                                    <td> 
                                                        <a href="{{route('getEditAdvertisement',['id'=>$value_poster->id])}}" class="btn btn-success btn-sm text-white" data-toggle="tooltip"
                                                            data-original-title="Edit"><i class="fa fa-pencil"></i></a>

                                                        <a href="{{route('deleteAds',['id'=>$value_poster->id])}}" onclick="return confirm('Bạn có muốn xóa? ')" class="btn btn-danger btn-sm text-white" data-toggle="tooltip"
                                                            data-original-title="Delete"><i class="fa fa-trash-o"></i></a>

                                                        <a href="{{route('getDetail',['id'=>$value_poster->id])}}" class="btn btn-primary btn-sm text-white" data-toggle="tooltip"
                                                            data-original-title="View"><i class="fa fa-eye"></i></a> </td>
                                                </tr>
                                                @endforeach
                                                <tr>
                                                    <td colspan="6" style="border-color: white">
                                                        <nav id="pagination" aria-label="Page navigation example" style="float: right;margin-top: 20px">
                                                            {!! $poster->render() !!}
                                                        </nav>
                                                    </td>
                                                </tr>      
                                                @endif