@extends('client.master')
@section('content')
</div>
<!--Topbar-->
<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg"
        style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">My Dashboard</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">My Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->
<!--Section-->
@foreach($user as $value)
<section class="sptb">
    <div class="container">
        <div class="row">

            <!-- sidebar -->
            @include('client.sidebar',['data'=>$data])
            <!-- end sidebar -->

            <div class="col-xl-9 col-lg-12 col-md-12">
                <div class="card mb-0">
                    <div class="card-header">
                        <h3 class="card-title">Edit Profile</h3>
                    </div> 
                    @if(Session::has('messages'))
                    <strong style="color: green">{{Session::get('messages')}}</strong>
                    @endif
                    <form method="post" enctype="multipart/form-data" action="{{route('updateProfile')}}">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">First Name</label>
                                        @if($value->firstname == null)<strong class="null"> *</strong>@endif<input name="firstname"
                                            type="text" class="form-control color_input" placeholder="First Name"
                                            value="{{$value->firstname}}" class="color_input"></div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">Last Name</label>
                                        @if($value->lastname == null)<strong class="null"> *</strong>@endif<input name="lastname"
                                            type="text" class="form-control color_input" placeholder="Last Name"
                                            value="{{$value->lastname}}"> </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">Email
                                            address</label>@if($value->email == null)<strong class="null">
                                            *</strong>@endif<input name="email" type="email" class="form-control color_input"
                                            placeholder="Email" value="{{$value->email}}"></div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">Phone Number</label>
                                        @if($value->phone == null)<strong class="null"> *</strong>@endif
                                        <input value="{{$value->phone}}" name="phone" type="tel" class="form-control color_input" placeholder="Number"
                                            pattern="^\+?(?:[0-9]??).{5,14}[0-9]$"> </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"> <label class="form-label">Address</label> @if($value->phone ==
                                        null)<strong class="null"> *</strong>@endif <input value="{{$value->street}}" name="address" type="text"
                                            class="form-control color_input" placeholder="Vehicle Address"> </div>
                                </div>
                                <div class="form-group col-md-6"> <label class="form-label">City</label> @if($value->city
                                        == null)<strong class="null"> *</strong>@endif
                                        <select class="form-control" id="select-countries" name="city">
                                            @foreach($Province as $value_Province)
                                                <option data-select2-id="5" value="{{$value_Province->id}}">{{$value_Province->_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                <!-- <div class="col-sm-6 col-md-3">
                                    <div class="form-group"> <label class="form-label">Postal Code</label> <input name="zcode"
                                            type="number" class="form-control color_input" placeholder="ZIP Code"> </div>
                                </div> -->
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">Facebook</label> @if($value->url_face == null)<strong class="null"> *</strong>@endif<input name="url_face" type="url" value="{{$value->url_face}}"
                                            class="form-control color_input" placeholder="https://www.facebook.com/"> </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">Google</label> @if($value->url_google == null)<strong class="null"> *</strong>@endif<input name="url_google" type="url" value="{{$value->url_google}}"
                                            class="form-control color_input" placeholder="https://www.google.com/">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">Twitter</label> @if($value->url_twitter == null)<strong class="null"> *</strong>@endif<input name="url_twitter" type="url" value="{{$value->url_twitter}}"
                                            class="form-control color_input" placeholder="https://twitter.com/">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group"> <label class="form-label">Pinterest</label> @if($value->url_print == null)<strong class="null"> *</strong>@endif<input name="url_print" type="url" value="{{$value->url_print}}"
                                            class="form-control color_input" placeholder="https://in.pinterest.com/"> </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group"> <label class="form-label">About Me</label> @if($value->aboutme == null)<strong class="null"> *</strong>@endif<textarea rows="5"  name="aboutme"
                                            class="form-control color_input" placeholder="Enter About your description">
                                                {{$value->detail_aboutme}}
                                            </textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group mb-0"> <label class="form-label">Upload Image</label>
                                        @if($value->img == null)<strong class="null"> *</strong>@endif
                                        <div class="custom-file">
                                            <input type="file" name="img" id="bar-chose-file" class="form-control">
                                            <!-- <label class="custom-file-label" id="chose-file">Choose file</label> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$value->id}}">
                        <div class="card-footer"> <input type="submit" name="update" class="btn btn-success" value="Updated Profile"></div>
                    </form>
                </div>
            </div>  
        </div>
    </div>
</section>
@endforeach
<!--/Section-->
@endsection
