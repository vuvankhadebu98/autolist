@extends('client.master')
@section('content')
<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" type="text/css" media="all" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
  </script>
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <style type="text/css">
    .ui-slider-range {
      background: #3FE331;

    }
    .item-card9 p{
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
    }
    /*.tab-content>.tab-pane {
      display: block !important;
    }*/
  </style>
</head>

<div>
    <div class="cover-image sptb-1 bg-background" data-image-src="assets/images/banners/banner1.jpg"
        style="background: url(&quot;assets/images/banners/banner1.jpg&quot;) center center;">
        <div class="header-text1 mb-0">
            <div class="container">
                <!-------------------------------------phần search -->
                @include('client.select_search')
                <!----------------------------------------------------->
            </div>
        </div><!-- /header-text -->
    </div>
</div>
<div class="bg-white border-bottom">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title">Cars</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Categories</a></li>
                <li class="breadcrumb-item active" aria-current="page">Cars</li>
            </ol>
        </div>
    </div>
</div>
<section class="sptb">
   <div class="container">
      <div class="row">
         <div class="col-xl-9 col-lg-9 col-md-12">
            <!--Lists--> 
            <div class=" mb-0">
               <div class="">
                  <div class="item2-gl ">
                     <div class=" mb-0">
                        <div class="">
                           <div class="bg-white p-5 item2-gl-nav d-flex">
                              <h6 class="mb-0 mt-3 text-left">Showing 1 to 10 of 30 entries</h6>
                              <ul class="nav item2-gl-menu ml-auto mt-1">
                                 <li class=""><a href="#tab-11" class="active show" data-toggle="tab" title="List style"><i class="fa fa-list"></i></a></li>
                                 <li><a href="#tab-12" data-toggle="tab" class="" title="Grid"><i class="fa fa-th"></i></a></li>
                              </ul>
                              <div class="d-sm-flex">
                                 <label class="mr-2 mt-2 mb-sm-1">Sort By:</label> 
                                 <div class="selectgroup">
                                  <label class="selectgroup-item mb-md-0"> 
                                    <input type="radio" name="value" value="Price" class="selectgroup-input" checked="" > 
                                    <span  class="selectgroup-button " data-toggle="dropdown">
                                      Giá <i class="fa fa-sort ml-1"></i>
                                    </span>
                                    <div class="dropdown-menu">
                                      <span class="dropdown-item btnDecrease" 
                                      data-url="{{route('ajax')}}">Giảm dần</span>
                                      <span class="dropdown-item btnAscending" 
                                      data-url="{{route('Ajax')}}">Tăng dần</span>
                                    </div> 
                                  </label>
                                  <label class="selectgroup-item mb-md-0"> 
                                    <input type="radio" name="value" value="Popularity" class="selectgroup-input"> 
                                    <span class="selectgroup-button">Phổ biến</span> 
                                  </label> 
                                  <label class="selectgroup-item mb-0"> 
                                    <input type="radio" name="value" value="Latest" class="selectgroup-input"> 
                                    <span class="selectgroup-button">Cũ nhất</span> 
                                  </label> 
                                  <label class="selectgroup-item mb-0"> 
                                    <input type="radio" name="value" value="Rating" class="selectgroup-input"> 
                                    <span class="selectgroup-button">Xếp hạng</span> 
                                  </label> 
                                </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-content">
                        <div class="tab-pane active" id="tab-11">
                          <ul id="id01">
                        	@foreach($product as $pr)
                          <li value="">
                           <div class="card overflow-hidden">
                              <div class="d-md-flex">
                                 <div class="item-card9-img">
                                 	@if($pr->promotion >0)
                                    <div class="arrow-ribbon bg-primary">Sale</div>
                                    @endif
                                    @php
                                    	$imageImplode = $pr->image_product;
                              			  $imageExplode = explode(',',$imageImplode);

                                      $tien = ($pr->price)- ($pr->price * $pr->promotion/100);
                                    @endphp
                                    @foreach(array_slice($imageExplode,0,1) as $img)
                                    <div class="item-card9-imgs"> 
                                      <a class="link" href="{{route('Car-Detail-01',$pr->id)}}"></a> <img src="./../img_product/{{$img}}" alt="img" class="cover-image"> </div>
                                    @endforeach
                                    <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a> </div>
                                    <div class="item-overly-trans">
                                       <div class="rating-stars">
                                          <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> 
                                          <div class="rating-stars-container">
                                             <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                             <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                             <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                             <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                             <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                          </div>
                                       </div>
                                       <span><a class="bg-gray">Used</a></span> 
                                    </div>
                                 </div>
                                 <div class="card border-0 mb-0">
                                    <div class="card-body " >
                                       <div class="item-card9" id="carbody">
                                          <a href="{{route('Car-Detail-01',$pr->id)}}" class="text-dark" >
                                             <h4 class="font-weight-semibold mt-1">
                                             	{{$pr->name}}</h4>
                                          </a>
                                          <div class="item-card9-desc mb-2"> <a class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a> <a class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1">
                                          </i>{{Carbon\Carbon::parse($pr->created_at)->format('d/m/Y')}}</span></a> </div>
                                          <div class="description">
                                           <p class="mb-0 leading-tight">
                                            {!!$pr->description!!}
                                           </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                       <div class="item-card9-footer d-sm-flex">
                                          <div class="item-card9-cost" id="ida">
                                             <h4 class="text-dark font-weight-bold mb-0 mt-0">{{$tien}} Triệu </h4>
                                          </div>
                                          <div class="ml-auto"> 
                                          	<a class="mr-2" title="Car type"><i class="fa fa-car  mr-1 text-muted"></i> 
	                                          	@if($pr->transmission_type == 1)
    			                                      Hộp số sàn
    			                                    @elseif($pr->transmission_type == 2)
    			                                      Hộp số tự động
    			                                    @elseif($pr->transmission_type == 3)
    			                                      Hộp số tự động vô cấp CVT
    			                                    @elseif($pr->transmission_type == 4)
    			                                      Hộp số ly hợp kép DCT
    			                                    @endif
                                          	</a> 
                                          	<a class="mr-2" title="Kilometrs"><i class="fa fa-road text-muted mr-1 "></i>
                                          		{{$pr->max_power}}</a> 
                                          	<a class="" title="FuealType"><i class="fa fa-tachometer text-muted mr-1">
                                            </i>
                                          	  @if($pr->fuel_type == 1)
    		                                        Diesel
    		                                      @elseif($pr->fuel_type == 2)
    		                                        Xăng
    		                                      @elseif($pr->fuel_type == 3)
    		                                        CNG
    		                                      @elseif($pr->fuel_type == 4)
    		                                        Điện
    		                                      @endif
                                          	</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                          </li>
                           @endforeach
                          </ul>
                        </div>
                        <div class="tab-pane" id="tab-12">
                            <div class="row">
                              @foreach($product as $pr)
                              <div class="col-lg-6 col-md-12 col-xl-4">
                                 <div class="card overflow-hidden">
                                    <div class="item-card9-img">
                                       <div class="arrow-ribbon bg-primary">
                                        {{($pr->price)- ($pr->price * $pr->promotion/100)}} Triệu</div>
                                        @php
                                          $imageImplode = $pr->image_product;
                                          $imageExplode = explode(',',$imageImplode);
                                        @endphp
                                        @foreach(array_slice($imageExplode,0,1) as $img)
                                        <div class="item-card9-imgs"> 
                                          <a class="link" href="{{route('Car-Detail-01',$pr->id)}}"></a> <img 
                                          src="./../img_product/{{$img}}" alt="img" class="cover-image" style="width: 274px; height: 209px;"> </div>
                                       @endforeach
                                       <div class="item-card9-icons"> <a class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a> </div>
                                       <div class="item-overly-trans">
                                          <div class="rating-stars">
                                             <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> 
                                             <div class="rating-stars-container">
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                             </div>
                                          </div>
                                          <span><a class="bg-gray">Used</a></span> 
                                       </div>
                                    </div>
                                    <div class="card border-0 mb-0">
                                       <div class="card-body ">
                                          <div class="item-card9" >
                                             <a href="{{route('Car-Detail-01',$pr->id)}}" class="text-dark">
                                                <h4 class="font-weight-semibold mt-1"> {{$pr->name}}</h4>
                                             </a>
                                             <div class="item-card9-desc mb-2"> <a class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a> <a class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i>
                                              {{Carbon\Carbon::parse($pr->created_at)->format('d/m/Y')}}</span></a></div>
                                             <p class="mb-0 leading-tight">
                                              {!!$pr->description!!}
                                             </p>
                                          </div>
                                       </div>
                                       <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                          <div class="item-card9-footer d-sm-flex">
                                             <div class=""> 
                                              <a class="w-50 mt-1 mb-1 float-left" title="Car type"><i class="fa fa-car  mr-1 text-muted"></i> 
                                                @if($pr->transmission_type == 1)
                                                  Hộp số sàn
                                                @elseif($pr->transmission_type == 2)
                                                  Hộp số tự động
                                                @elseif($pr->transmission_type == 3)
                                                  Hộp số tự động vô cấp CVT
                                                @elseif($pr->transmission_type == 4)
                                                  Hộp số ly hợp kép DCT
                                                @endif
                                              </a> 
                                              <a class="w-50 mt-1 mb-1 float-left" title="Kilometrs"><i class="fa fa-road text-muted mr-1 "></i>
                                                {{$pr->max_power}}
                                              </a> 
                                              <a class="w-50 mt-1 mb-1 float-left" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i>
                                                @if($pr->fuel_type == 1)
                                                  Diesel
                                                @elseif($pr->fuel_type == 2)
                                                  Xăng
                                                @elseif($pr->fuel_type == 3)
                                                  CNG
                                                @elseif($pr->fuel_type == 4)
                                                  Điện
                                                @endif
                                              </a> 
                                            </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                            </div>
                        </div>
                     </div>
                  </div>
                  <div class="center-block text-center">
                     {{-- <ul class="pagination mb-3">
                        <li class="page-item page-prev disabled"> <a class="page-link" href="#" tabindex="-1">Prev</a> </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item page-next"> <a class="page-link" href="#">Next</a> </li>
                     </ul> --}}
                     <div class="row">{{$product->links()}}</div>
                  </div>
               </div>
            </div>
            <!--/Lists--> 
         </div>
         <!--Right Side Content--> 
         <div class="col-xl-3 col-lg-3 col-md-12">
            <div class="card">
               <div class="card-body">
                  <div class="input-group">
                     <input type="text" id="search" class="form-control br-tl-3  br-bl-3" placeholder="Search"> 
                     <div class="input-group-append "> <button type="button" class="btn btn-primary br-tr-3  br-br-3 btnSearch"> Tìm kiếm </button> </div>
                  </div>
               </div>
            </div>
            <div class="card overflow-hidden" data-select2-id="33">
              {{-- <form action="{{route('filter')}}" method="POST"> --}}
              <div class="px-4 py-3 border-bottom">
                <h4 class="mb-0">Thể loại</h4>
              </div>
              <div class="card-body">
                <div class="closed" id="hideShow" style=" overflow: hidden;">
                   <div class="filter-product-checkboxs"> 
                    @foreach($cons as $row)
                   	<label class="custom-control custom-checkbox mb-3"> 
                      <input type="checkbox" onclick="return getProduct(this);"
                      class="custom-control-input chkCate" name="checkboxCate" value="{{$row->id}}"> 
                      <span class="custom-control-label"> 
                        <a href="#" class="text-dark">{{$row->name}}<span class="label label-secondary float-right">{{$row->product_id_count}}</span></a> 
                      </span> 
                    </label> 
                   	@endforeach
                   </div>
                </div>
                {{-- <div class="showmore-button">
                   <div class="showmore-button-inner more" id="show">Show more</div>
                </div> --}}
              </div>
              <div class="px-4 py-3 border-bottom border-top">
                <h4 class="mb-0">Khoảng giá</h4>
              </div>
              <div class="form-price-range-filter" style="padding: 1.5rem;">
                <div style="">
                  <input type="number" min=0 max="9900" oninput="validity.valid||
                  (value='0');" id="min_price" class="price-range-field" 
                  style="border: 0;font-size: 18px;width: 64px;
                  font-weight: 500;"/>-
                  <input type="number" min=0 max="10000" oninput="validity.valid||
                  (value='10000');" id="max_price" class="price-range-field"
                  style="border: 0;font-size: 18px;width: 72px;
                  font-weight: 500;"/>Triệu
                </div>
  			        <div id="slider-range" onchange="return PriceFilter(this);" class="price-filter-range pricerange" 
                name="rangeInput">
                </div>
      					<button class="price-range-search" id="price-range-submit">
                Search</button>
    			    </div>
               <div class="px-4 py-3 border-bottom">
                  <h4 class="mb-0">Tình trạng</h4>
               </div>
               <div class="card-body">
                  <div class="filter-product-checkboxs"> 
                    <label class="custom-control custom-checkbox mb-2"> 
                      <input type="radio" class="custom-control-input" name="checkbox2" value="option2"> 
                      <span class="custom-control-label"> Tất cả </span>
                    </label> 
                    <label class="custom-control custom-checkbox mb-2"> 
                      <input type="radio" class="custom-control-input" name="checkbox2" value="option1"> 
                      <span class="custom-control-label"> Mới </span>
                    </label>
                    <label class="custom-control custom-checkbox mb-2"> 
                      <input type="radio" class="custom-control-input" name="checkbox2" value="option2"> 
                      <span class="custom-control-label"> Đã sử dụng</span>
                    </label> 
                  </div>
               </div>
               <div class="px-4 py-3 border-bottom border-top">
                  <h4 class="mb-0">Posted By</h4>
               </div>
               <div class="card-body">
                  <div class="filter-product-checkboxs"> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox1" value="option1"> <span class="custom-control-label"> Dealer </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2"> <span class="custom-control-label"> Individual </span> </label> <label class="custom-control custom-checkbox mb-0"> <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2"> <span class="custom-control-label"> Reseller </span> </label> </div>
               </div>
               <div class="px-4 py-3 border-bottom border-top">
                  <h4 class="mb-0">Kiểu nhiên liệu</h4>
               </div>
               <div class="card-body">
                  <div class="filter-product-checkboxs"> 
                    <label class="custom-control custom-checkbox mb-2"> 
                      <input type="checkbox" class="custom-control-input chkFuel" name="checkbox11" 
                      onclick="return getFuelType(this);" value="1"> 
                      <span class="custom-control-label"> Diesel </span> 
                    </label> 
                    <label class="custom-control custom-checkbox mb-2"> 
                      <input type="checkbox" class="custom-control-input chkFuel" name="checkbox11" 
                      onclick="return getFuelType(this);" value="2"> 
                      <span class="custom-control-label"> Xăng </span> 
                    </label> 
                    <label class="custom-control custom-checkbox mb-2"> 
                      <input type="checkbox" class="custom-control-input chkFuel" name="checkbox11" 
                      onclick="return getFuelType(this);" value="3"> 
                      <span class="custom-control-label"> CNG </span> 
                    </label> 
                    <label class="custom-control custom-checkbox mb-2"> 
                      <input type="checkbox" class="custom-control-input chkFuel" name="checkbox11" 
                      onclick="return getFuelType(this);" value="4"> 
                      <span class="custom-control-label"> Điện </span> 
                    </label> 
                  </div>
               </div>
               <div class="px-4 py-3 border-bottom border-top">
                  <h4 class="mb-0">Kiểu xe</h4>
               </div>
               <div class="card-body">
                  <div class="filter-product-checkboxs"> 
                    @foreach($brand as $br)
                    <label class="custom-control custom-checkbox mb-2"> 
                      <input type="checkbox" onclick="return getBrand(this);" class="custom-control-input" name="checkboxBrand" value="{{$br->id}}"> 
                      <span class="custom-control-label"> {{$br->name}} </span> 
                    </label> 
                    @endforeach
                  </div>
               </div>
               <div class="px-4 py-3 border-bottom border-top">
                  <h4 class="mb-0">Loại hộp số</h4>
               </div>
               <div class="card-body">
                  <div class="filter-product-checkboxs"> 
                    <label class="custom-control custom-checkbox mb-2"> 
                      <input type="checkbox" class="custom-control-input" name="checkbox13"
                      onclick="return getTransmissionType(this);" value="1"> 
                      <span class="custom-control-label"> Hộp số sàn </span> 
                    </label> 
                    <label class="custom-control custom-checkbox mb-2"> 
                      <input type="checkbox" class="custom-control-input" name="checkbox13"
                      onclick="return getTransmissionType(this)" value="2"> 
                      <span class="custom-control-label"> Hộp số tự động </span> 
                    </label> 
                    <label class="custom-control custom-checkbox mb-2"> 
                      <input type="checkbox" class="custom-control-input" name="checkbox13"
                      onclick="return getTransmissionType(this)" value="3"> 
                      <span class="custom-control-label"> Hộp số tự động vô cấp CVT </span> 
                    </label> 
                    <label class="custom-control custom-checkbox mb-2"> 
                      <input type="checkbox" class="custom-control-input" name="checkbox13"
                      onclick="return getTransmissionType(this)" value="4"> 
                      <span class="custom-control-label">Hộp số ly hợp kép DCT</span>
                    </label>
                  </div>
               </div>
               <div class="card-footer"> 
                <button id="btnFilter" class="btn btn-secondary btn-block">
                  Áp dụng bộ lọc
                </button>
              </div>
              <div class="card mb-0">
                 <div class="card-header">
                    <h3 class="card-title">Shares</h3>
                 </div>
                 <div class="card-body product-filter-desc">
                    <div class="product-filter-icons text-center"> <a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a> <a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a> <a href="#" class="google-bg"><i class="fa fa-google"></i></a> <a href="#" class="dribbble-bg"><i class="fa fa-dribbble"></i></a> <a href="#" class="pinterest-bg"><i class="fa fa-pinterest"></i></a> </div>
                 </div>
              </div>
            {{-- </form> --}}
            </div>
         <!--/Right Side Content--> 
      </div>
   </div>
</section>

<script type="text/javascript">

  //<---------------Áp dụng bộ lọc------------->

  var array_category = [];
  var array_fuel_type = [];
  var array_transmission_type = [];
  var array_brand = [];

 //<---Lọc theo kiểu danh mục--->
    function getProduct(e) {
      var categores = $(e).val();
      if ($(e).is(":checked")) {
        array_category.push({
          'categores':(categores),
        });
      }else {
        $.each(array_category, function(i){
            if(array_category[i].categores === categores) {
                array_category.splice(i,1);
                return false;
            }
        });
      }
      $.ajax({
      url: "{{route('filter')}}",
      method: "GET",
      data: {
        brand: array_brand,
        categores: array_category,
        fuel_type: array_fuel_type,
        transmission_type: array_transmission_type,
        min_price: min_price,
        max_price: max_price,
      },
      success:function(data) {
        $("#tab-11").empty();
        $("#tab-11").html(data);
      }
      })
    }

 //<---Lọc theo kiểu thương hiệu--->
    function getBrand(e) {
      var brand = $(e).val();
      if ($(e).is(":checked")) {
        array_brand.push({
          'brand':(brand),
        });
      }else {
        $.each(array_brand, function(i){
            if(array_brand[i].brand === brand) {
                array_brand.splice(i,1);
                return false;
            }
        });
      }
      $.ajax({
      url: "{{route('filter')}}",
      method: "GET",
      data: {
        brand: array_brand,
        categores: array_category,
        fuel_type: array_fuel_type,
        transmission_type: array_transmission_type,
        min_price: min_price,
        max_price: max_price,
      },
      success:function(data) {
        $("#tab-11").empty();
        $("#tab-11").html(data);
      }
      })
    }

 //<-----------Lọc theo kiểu nhiên liệu------->
    function getFuelType(argument) {
      var fuel_type = $(argument).val();
      if ($(argument).is(":checked")) {
        array_fuel_type.push({
          'value':(fuel_type),
        });
      }else {
        $.each(array_fuel_type, function(i){
            if(array_fuel_type[i].value === fuel_type) {
                array_fuel_type.splice(i,1);
                return false;
            }
        });
      }
       $.ajax({
      url: "{{route('filter')}}",
      method: "GET",
      data: {
        brand: array_brand,
        categores: array_category,
        fuel_type: array_fuel_type,
        transmission_type: array_transmission_type,
        min_price: min_price,
        max_price: max_price,
      },
      success:function(data) {
        $("#tab-11").empty();
        $("#tab-11").html(data);
      }
    })
      // console.log(array_fuel_type);
    }

 //<-----------Lọc theo kiểu hộp số------->
    function getTransmissionType(e) {
      var transmission_type = $(e).val();
      if ($(e).is(":checked")) {
        array_transmission_type.push({
          'value': (transmission_type),
        });
      }else{
        $.each(array_transmission_type,function(i){
          if (array_transmission_type[i].value === transmission_type) {
            array_transmission_type.splice(i,1);
            return false;
          }
        });
      }
      $.ajax({
        url : "{{route('filter')}}",
        method: "GET",
        data: {
          brand: array_brand,
          categores: array_category,
          fuel_type: array_fuel_type,
          transmission_type: array_transmission_type,
          min_price: min_price,
          max_price: max_price,
        },
        success:function(data){
          $("#tab-11").empty();
          $("#tab-11").html(data);
        }
      })
    }

	$(document).ready(function(){

 //<-----------Áp dụng bộ lọc khoảng giá------->
	
	$('#price-range-submit').hide();

  $("#min_price,#max_price").on('change', function () {

    var min_price_range = parseInt($("#min_price").val());

    var max_price_range = parseInt($("#max_price").val());

    $.ajax({
      url: "{{route('filter')}}",
      method: "GET",
      data: {
        brand: array_brand,
        categores: array_category,
        fuel_type: array_fuel_type,
        transmission_type: array_transmission_type,
        min_price: min_price_range,
        max_price: max_price_range,
      },
      success:function(data){
        $('#tab-11').empty();
        $('#tab-11').html(data);
      }
    })

    if (min_price_range > max_price_range) {
    $('#max_price').val(min_price_range);
    }

    $("#slider-range").slider({
    values: [min_price_range, max_price_range]
    });
    
  });


  $("#min_price,#max_price").on("paste keyup", function () {                                        

    var min_price_range = parseInt($("#min_price").val());

    var max_price_range = parseInt($("#max_price").val());
    
    if(min_price_range == max_price_range){

      max_price_range = min_price_range + 100;
      
      $("#min_price").val(min_price_range);   
      $("#max_price").val(max_price_range);
    }

    $("#slider-range").slider({
    values: [min_price_range, max_price_range]
    });

  });

	$(function () {
	  $("#slider-range").slider({
		range: true,
		orientation: "horizontal",
		min: 0,
		max: 10000,
		values: [0, 10000],
		step: 100,

		slide: function (event, ui) {
		  if (ui.values[0] == ui.values[1]) {
			  return false;
		  }
		  
		  $("#min_price").val(ui.values[0]);
		  $("#max_price").val(ui.values[1]);
		}
	  });

	  $("#min_price").val($("#slider-range").slider("values", 0));
	  $("#max_price").val($("#slider-range").slider("values", 1));

	});

	$("#slider-range,#price-range-submit").click(function () {

	  var min_price = $('#min_price').val();
	  var max_price = $('#max_price').val();

    $.ajax({
      url: "{{route('filter')}}",
      method: "GET",
      data: {
        brand: array_brand,
        categores: array_category,
        fuel_type: array_fuel_type,
        transmission_type: array_transmission_type,
        min_price: min_price,
        max_price: max_price,
      },
      success:function(data){
        $('#tab-11').empty();
        $('#tab-11').html(data);
      }
    })
    // console.log(array_price);

	  // $("#searchResults").text("Here List of products will be shown which are cost between " + min_price  +" "+ "and" + " "+ max_price + ".");
	});

//<--------Xử lí sắp xếp sản phẩm theo giá--------->
//<--------------Giá giảm dần----------->
  $('.btnDecrease').click(function(){
    var url= $(this).attr('data-url');
    $.ajax({
      url: url,
      method: 'GET',
      success:function(response) {
        $("#tab-11").empty();
        $("#tab-11").html(response);
      }
    })
  });
//<--------------Giá tăng dần----------->
  $('.btnAscending').click(function(){
    var url= $(this).attr('data-url');
    $.ajax({
      url: url,
      method: 'GET',
      success:function(response) {
        $("#tab-11").empty();
        $("#tab-11").html(response);
      }
    })
  });
//<--------------Tìm kiếm----------->
  $('#search').keyup(function(){
    var query = $('#search').val();
    if (query !='') {
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      // var _token = $('input[name="_token"]').val();
      $.ajax({
      url: "{{route('search')}}",
      method: "POST",
      data: {query:query},
      success:function(data) {
        $("#tab-11").empty();
        $("#tab-11").html(data);
      }
    })
    }
  });

  $(document).on('click', 'li', function(){  
    $('#search').val($(this).text());  
    $('#tab-11').fadeOut();  
  });


//<-----------nếu muốn dùng button để lọc thì cmt code '.chkCate'------->

  //  $('#btnFilter').click(function(e){
  //     $.ajaxSetup({
  //       headers: {
  //           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  //       }
  //     });

  //     $.ajax({
  //     url: "",
  //     method: "GET",
  //     data: {
  //       categores:array_category
  //     },
  //     success:function(data) {
  //       $("#tab-11").empty();
  //       $("#tab-11").html(data);
  //     }
  //   })
  // });

//<-----------và bỏ cmt nội dung code phần này------->

});
</script>

@endsection