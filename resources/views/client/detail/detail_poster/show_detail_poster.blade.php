@extends('client.master')
@section('content')
</div> <!-- Topbar -->
<!--Section-->
<section>
    <?php use App\Model\Poster; ?>
    <?php use App\Model\Comment; ?>
    <div class="banner-2 cover-image sptb-2 sptb-tab bg-background2" data-image-src="assets/images/banners/banner1.jpg"
        style="background: url(&quot;assets/images/banners/banner1.jpg&quot;) center center;">
        <!-------------------------------------phần search -->

        @include('client.select_search')
        <!----------------------------------------------------->
    </div>
</section>
<!--Section-->
<!--BreadCrumb-->
<div class="bg-white border-bottom"> 
   <div class="container"> 
      <div class="page-header"> 
         <h4 class="page-title">Chi Tiết Quảng Cáo Ô Tô</h4> 
         <ol class="breadcrumb"> 
            <li class="breadcrumb-item">
               <a href="{{URL::to('/')}}">Trang Chủ</a></li> 
               <li class="breadcrumb-item">
               <a href="{{route('Car_List')}}">Quảng Cáo Ô Tô</a>
            </li> 
            <li class="breadcrumb-item active" aria-current="page">Chi Tiết Quảng Cáo Ô Tô</li> 
         </ol> 
      </div> 
   </div> 
</div>
{{-- chi tiết quảng cáo --}}
<section class="sptb">
   <div class="container">
      <div class="row">
         @foreach($detail_poster as $key=> $detail_ps)
         <div class="col-xl-8 col-lg-8 col-md-12">
            <!--Classified Description--> 
            <div class="card overflow-hidden">
               <div class="ribbon ribbon-top-right text-danger">
                  <?php
                     if($detail_ps->check_new == 0){
                  ?>
                     <span class="bg-success">Phục vụ</span>
                  <?php
                      }elseif($detail_ps->check_new == 1){
                  ?>  
                     <span class="bg-danger">Giảm giá</span>
                  <?php
                      }elseif($detail_ps->check_new == 2){
                  ?>  
                     <span class="bg-danger">Giảm giá</span> 
                  <?php
                     }
                  ?> 
               </div>
               <div class="card-body">
                  <div class="item-det mb-4">
                     <a href="#" class="text-dark">
                        <h3>{{$detail_ps->title}}</h3>
                     </a>
                     <div class=" d-flex">
                        <ul class="d-flex mb-0">
                           <li class="mr-5"><a href="#" class="icons"><i class="ti-car text-muted mr-1 fs-18"></i> Cars</a></li>
                           <li class="mr-5"><a href="#" class="icons"><i class="ti-location-pin text-muted mr-1"></i> USA</a></li>
                           <li class="mr-5"><a href="#" class="icons"><i class="ti-calendar text-muted mr-1"></i> 5 hours ago</a></li>
                           <li class="mr-5"><a href="#" class="icons"><i class="ti-eye text-muted mr-1 fs-15"></i> 765</a></li>
                        </ul>
                        <div class="rating-stars d-flex mr-5">
                           <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" id="rating-stars-value" value="4"> 
                           <div class="rating-stars-container mr-2">
                              <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                              <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                              <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                              <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                              <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                           </div>
                           4.0 
                        </div>
                        <div class="rating-stars d-flex">
                           <div class="rating-stars-container mr-2">
                              <div class="rating-star sm"> <i class="fa fa-heart"></i> </div>
                           </div>
                           135 
                        </div>
                     </div>
                  </div>
                  <div class="product-slider">
                     <div id="carousel" class="carousel slide" data-ride="carousel">
                        <div class="arrow-ribbon2 bg-primary">{{number_format($detail_ps->price,0,',','.')}}đ</div>
                        <div class="carousel-inner">
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item active"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                           <div class="carousel-item"> <img src="../img_poster/{{$detail_ps->img}}" alt="img"> </div>
                        </div>
                        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev"> 
                           <i class="fa fa-angle-left" aria-hidden="true"></i> 
                        </a> 
                        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next"> 
                           <i class="fa fa-angle-right" aria-hidden="true"></i> 
                        </a> 
                     </div>
                     <div class="clearfix">
                        <div id="thumbcarousel" class="carousel slide thumbcarousel" data-interval="false">
                           <div class="carousel-inner">
                              <div class="carousel-item active">
                                 <div data-target="#carousel" data-slide-to="0" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="1" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="2" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="3" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="4" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                              </div>
                              <div class="carousel-item ">
                                 <div data-target="#carousel" data-slide-to="5" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="6" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="7" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="8" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="9" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                              </div>
                              <div class="carousel-item ">
                                 <div data-target="#carousel" data-slide-to="10" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="11" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="12" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="13" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="14" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                              </div>
                              <div class="carousel-item ">
                                 <div data-target="#carousel" data-slide-to="15" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="16" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="17" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="18" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                                 <div data-target="#carousel" data-slide-to="19" class="thumb"><img src="../img_poster/{{$detail_ps->img}}" alt="img"></div>
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#thumbcarousel" role="button" data-slide="prev"> 
                              <i class="fa fa-angle-left" aria-hidden="true"></i> 
                           </a> 
                           <a class="carousel-control-next" href="#thumbcarousel" role="button" data-slide="next"> 
                              <i class="fa fa-angle-right" aria-hidden="true"></i> 
                           </a> 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="">
               <div class="">
                  <div class="border-0">
                     <div class="wideget-user-tab wideget-user-tab3">
                        <div class="tab-menu-heading">
                           <div class="tabs-menu1">
                              <ul class="nav">
                                 <li class=""><a href="#tab-1" class="active" data-toggle="tab">Tổng Quan</a></li>
                                 <li><a href="#tab-3" data-toggle="tab" class="">Tính năng &amp; tùy chọn</a></li>
                                 <li><a href="#tab-4" data-toggle="tab" class="">Thông tin xe</a></li>
                                 <li><a href="#tab-5" data-toggle="tab" class="">giới thiệu Video</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="tab-content border-left border-right border-top br-tr-3 border-bottom br-br-3 br-bl-3 p-5 bg-white mb-4">
                        <div class="tab-pane active" id="tab-1">
                           <h3 class="card-title mb-3 font-weight-semibold">Tổng quan</h3>
                           <div class="mb-4">
                              <p>Con người phải tìm kiếm sự nhạt nhẽo của những người mà chúng ta cho là đáng căm ghét, những phiền toái của thú vui hiện tại, nỗi đau, và cũng chào đón những kẻ mất dạy mà họ mù quáng là mong muốn của những người đã không tự đánh cược bản thân, những người thực hiện một chức vụ bỏ rơi tinh thần của tướng quân, và trong cùng một chương có lỗi, tức là lao động và đau đớn của ông ta.</p>
                              <p>Mặt khác, chúng ta tố cáo với sự phẫn nộ chính đáng và chán ghét những người đàn ông bị dụ dỗ và làm mất tinh thần của thú vui nhất thời, bị dục vọng làm cho mù quáng, đến nỗi họ không thể lường trước được nỗi đau và rắc rối sắp xảy ra; và trách nhiệm bình đẳng thuộc về những người thất bại trong nhiệm vụ của họ do yếu kém về ý chí, điều này cũng giống như nói khi thu mình lại vì vất vả và đau đớn.</p>
                           </div>
                           <h4 class="mb-4">Thông số kỹ thật</h4>
                           <div class="row">
                              <div class="col-xl-12 col-md-12">
                                 <div class="table-responsive">
                                    <table class="table table-bordered w-100 m-0 text-nowrap ">
                                       <tbody>
                                          <tr>
                                             <td><span class="font-weight-bold"> Loại nhiên liệu :</span>{{$detail_ps->fuel_type}}</td>
                                             <td><span class="font-weight-bold">Nghỉ ngơi :</span> Phía trước, Phía sau </td>
                                          </tr>
                                          <tr>
                                             <td><span class="font-weight-bold">Chỗ Ngồi :</span>{{$detail_ps->seats}} ghế </td>
                                             <td><span class="font-weight-bold">Màu :</span> Đỏ , Hồng, Xám </td>
                                          </tr>
                                          <tr>
                                             <td><span class="font-weight-bold">Túi Khí : </span> Có Sẵn</td>
                                             <td><span class="font-weight-bold">Colors :</span> Đỏ, Hồng, Xám </td>
                                          </tr>
                                          <tr>
                                             <td><span class="font-weight-bold">Động cơ :</span> {{$detail_ps->engine}} </td>
                                             <td><span class="font-weight-bold">Windows Power :</span> có sẵn </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane" id="tab-3">
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="table-responsive">
                                    <table class="table table-bordered border-top mb-0">
                                       <tbody>
                                          <tr>
                                             <td>Tay lái trợ năng</td>
                                             <td><i class="icon icon-check text-success"></i></td>
                                          </tr>
                                          <tr>
                                             <td>Power Windows Front</td>
                                             <td><i class="icon icon-check text-success"></i></td>
                                          </tr>
                                          <tr>
                                             <td>Máy điều hòa</td>
                                             <td><i class="icon icon-check text-success"></i></td>
                                          </tr>
                                          <tr>
                                             <td>Túi Khí hành khách</td>
                                             <td><i class="icon icon-close text-danger"></i></td>
                                          </tr>
                                          <tr>
                                             <td>Đèn sương mù</td>
                                             <td><i class="icon icon-close text-danger"></i></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="table-responsive">
                                    <table class="table table-bordered border-top mb-0">
                                       <tbody>
                                          <tr>
                                             <td>Hệ thống chống khóa phanh</td>
                                             <td><i class="icon icon-check text-success"></i></td>
                                          </tr>
                                          <tr>
                                             <td>Túi khí tài xế</td>
                                             <td><i class="icon icon-check text-success"></i></td>
                                          </tr>
                                          <tr>
                                             <td>Vỏ bánh xe</td>
                                             <td><i class="icon icon-check text-success"></i></td>
                                          </tr>
                                          <tr>
                                             <td>Kiểm soát khí hậu tự động</td>
                                             <td><i class="icon icon-close text-danger"></i></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane" id="tab-4">
                           <div class="row">
                              <div class="col-lg-6">
                                 <div class="table-responsive">
                                    <table class="table table-bordered border-top mb-0">
                                       <tbody>
                                          <tr>
                                             <td>Vận Tốc(Bốc Phá m/s)</td>
                                             <td>{{$detail_ps->arai_milage}}</td>
                                          </tr>
                                          <tr>
                                             <td>Hệ thống động cơ (cc)</td>
                                             <td>{{$detail_ps->enegine_displacement}}</td>
                                          </tr>
                                          <tr>
                                             <td>Momen xoắn cực đại (nm@rpm)</td>
                                             <td>{{$detail_ps->max_torque}}</td>
                                          </tr>
                                          <tr>
                                             <td>Số ghế ngồi</td>
                                             <td>{{$detail_ps->seats}}</td>
                                          </tr>
                                          <tr>
                                             <td>Không gian khởi động(lít)</td>
                                             <td>{{$detail_ps->bootspace}}</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="table-responsive">
                                    <table class="table table-bordered border-top mb-0">
                                       <tbody>
                                          <tr>
                                             <td>Loại nhiên liệu</td>
                                             <td>{{$detail_ps->engine}}</td>
                                          </tr>
                                          <tr>
                                             <td>Cộng Xuất Tối đa(bhp@rpm)</td>
                                             <td>{{$detail_ps->max_power}}</td>
                                          </tr>
                                          <tr>
                                             <td>kiểu hộp số</td>
                                             <td>{{$detail_ps->transmission_type}}</td>
                                          </tr>
                                          <tr>
                                             <td>Dung Tích Bình Xăng(lit)</td>
                                             <td>{{$detail_ps->fuel_tank_capacity}} lít</td>
                                          </tr>
                                          <tr>
                                             <td>Loại cơ thể</td>
                                             <td>hatchback</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane" id="tab-5">
                           <ul class="list-unstyled video-list-thumbs row ">
                              <li class="mb-0"> 
                                 <a data-toggle="modal" data-target="#homeVideo"> 
                                    <img src="../img_poster/{{$detail_ps->img}}" alt="Barca" class="img-responsive" 
                                    style="width: 730px; height: 480px;"> 
                                    <link rel="stylesheet" href="{{$detail_ps->video}}">
                                    <span class="mdi mdi-arrow-right-drop-circle-outline text-white"></span> 
                                 </a> 
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--/Classified Description--> 
            <h3 class="mb-5 mt-6">Bài viết liên quan</h3>
            <!--Related Posts--> 
            <div id="myCarousel5" class="owl-carousel owl-carousel-icons3 owl-loaded owl-drag">
               <!-- Wrapper for carousel items --> <!-- Wrapper for carousel items -->      
               <div class="owl-stage-outer">
                  <div class="owl-stage" style="transform: translate3d(-1602px, 0px, 0px); transition: all 0.25s ease 0s; width: 4406px;">
                     @foreach($poster_home as $key => $ps_home)
                     <div class="owl-item active" style="width: 375.5px; margin-right: 25px;">
                        <div class="item">
                           <div class="card mb-0">
                              <?php
                                 if ($ps_home->check == 0) {
                              ?>
                              <div class="power-ribbon power-ribbon-top-left text-warning">
                                 <span class="bg-warning">
                                    <i class="fa fa-bolt"></i>
                                 </span>
                              </div>
                               <?php
                                 }else{
                              ?>
                              <?php
                                 }
                              ?> 
                              <div class="item-card2-img"> 
                                 <a class="link" href="cars.html"></a> 
                                 <img src="../img_poster/{{$ps_home->img}}" alt="img" class="cover-image" style="height: 240px"> 
                              </div>
                              <div class="item-card2-icons" style="display: flex;align-items: center;justify-content: space-around;"> 
                                 <a href="#" class="item-card2-icons-l bg-primary"> 
                                    <i class="fa fa-share-alt" style="margin-top: 10px"></i>
                                 </a>  
                                 <?php
                                    if ($ps_home->check == 0) {
                                 ?> 
                                    <a href="#" class="item-card2-icons-r wishlist active">
                                       <i class="fa fa fa-heart" style="margin-top: 10px"></i>
                                    </a>
                                 <?php
                                    }elseif($ps_home->check == 1){
                                 ?> 
                                    <a href="#" class="item-card2-icons-r wishlist">
                                       <i class="fa fa fa-heart" style="margin-top: 10px"></i>
                                    </a>
                                 <?php
                                    }
                                 ?> 
                              </div>
                              <div class="card-body pb-0">
                                 <div class="item-card2">
                                    <div class="item-card2-desc">
                                       <div class="item-card2-text">
                                          <a href="cars.html" class="text-dark">
                                             <h4 class="mb-0">{{$ps_home->title}}</h4>
                                          </a>
                                       </div>
                                       <div class="d-flex pb-0 pt-0">
                                          <a href="">
                                             <p class="pb-0 pt-0 mb-2 mt-2"><i class="fa fa-map-marker text-danger mr-2"></i>Viện Nam, VN</p>
                                          </a>
                                          <span class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">{{number_format($ps_home->price,0,',','.')}} vnđ</span> 
                                       </div>
                                       <p class="">{!! $ps_home->description !!}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="card-footer"> 
                                 <a href="#" class="mr-4" data-toggle="tooltip" 
                                    data-placement="bottom" data-original-title="Automatic">
                                    <i class="fa fa-car text-muted"></i> 
                                    <span class="text-default">
                                       <?php
                                          if ($ps_home->transmission_type == 0) {
                                       ?>
                                          Sàn
                                       <?php
                                          }elseif($ps_home->transmission_type == 1) {
                                       ?>
                                          Tự động    
                                       <?php
                                          }elseif($ps_home->transmission_type == 2) {
                                       ?>
                                          CVT    
                                       <?php
                                          }elseif($ps_home->transmission_type == 3) {
                                       ?>
                                          DCT   
                                       <?php
                                          }elseif($ps_home->transmission_type == 4) {
                                       ?>
                                          DSG   
                                       <?php
                                          }
                                       ?>
                                    </span>
                                 </a> 
                                 <a href="#" class="mr-4" data-toggle="tooltip" 
                                    data-placement="bottom" data-original-title="2300 Kilometrs">
                                    <i class="fa fa-road text-muted"></i> 
                                    <span class="text-default">4000</span>
                                 </a> 
                                 <a href="#" class="" data-toggle="tooltip" 
                                 data-placement="bottom" data-original-title="FuelType">
                                    <i class="fa fa-tachometer text-muted"></i> 
                                    <span class="text-default">
                                       <?php
                                          if ($ps_home->fuel_type == 0) {
                                       ?>
                                          Diesel
                                       <?php
                                          }elseif($ps_home->fuel_type == 1) {
                                       ?>
                                          Xăng    
                                       <?php
                                          }elseif($ps_home->fuel_type == 2) {
                                       ?>
                                          NNMT    
                                       <?php
                                          }elseif($ps_home->fuel_type == 3) {
                                       ?>
                                          Nitơ   
                                       <?php
                                          }elseif($ps_home->fuel_type == 4) {
                                       ?>
                                          Amoniac   
                                       <?php
                                          }elseif($ps_home->fuel_type == 5) {
                                       ?>
                                          Cồn   
                                       <?php
                                          }elseif($ps_home->fuel_type == 6) {
                                       ?>
                                          LPG   
                                       <?php
                                          }elseif($ps_home->fuel_type == 7) {
                                       ?>
                                          CNG   
                                       <?php
                                          }
                                       ?>
                                    </span>
                                 </a> 
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
               </div>
               <div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button></div>
               <div class="owl-dots disabled"></div>
            </div>
            <!--/Related Posts--> <!--Comments--> 
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Đánh Giá</h3>
               </div>
               <div class="card-body">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="mb-4">
                           <p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i>5</span> </p>
                           <div class="progress progress-md mb-4 h-4">
                              <div class="progress-bar bg-success w-100">9,232</div>
                           </div>
                        </div>
                        <div class="mb-4">
                           <p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i>4</span> </p>
                           <div class="progress progress-md mb-4 h-4">
                              <div class="progress-bar bg-info w-80">8,125</div>
                           </div>
                        </div>
                        <div class="mb-4">
                           <p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i> 3</span> </p>
                           <div class="progress progress-md mb-4 h-4">
                              <div class="progress-bar bg-primary w-60">6,263</div>
                           </div>
                        </div>
                        <div class="mb-4">
                           <p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i> 2</span> </p>
                           <div class="progress progress-md mb-4 h-4">
                              <div class="progress-bar bg-secondary w-30">3,463</div>
                           </div>
                        </div>
                        <div class="mb-5">
                           <p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i> 1</span> </p>
                           <div class="progress progress-md mb-4 h-4">
                              <div class="progress-bar bg-orange w-20">1,456</div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-body p-0">
                  <div class="media mt-0 p-5">
                     <div class="d-flex mr-3"> 
                        <a href="#">
                           <img class="media-object brround" alt="64x64" src="../img_user/default.jpg"> 
                        </a> 
                     </div>
                     <div class="media-body">
                        <h5 class="mt-0 mb-1 font-weight-semibold">van kha 
                           <span class="fs-14 ml-0" data-toggle="tooltip" 
                              data-placement="top" title="" data-original-title="verified">
                              <i class="fa fa-check-circle-o text-success"></i>
                           </span> 
                           <span class="fs-14 ml-2"> 4.5 
                              <i class="fa fa-star text-yellow"></i>
                           </span> 
                        </h5>
                        <small class="text-muted">
                           <i class="fa fa-calendar"></i> Ngày 21st 
                           <i class=" ml-3 fa fa-clock-o"></i> 13.00 
                           <i class=" ml-3 fa fa-map-marker"></i> viện nam
                        </small> 
                        <p class="font-13  mb-2 mt-2"> Lorem Ipsum available, quis Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et nostrud exercitation ullamco laboris commodo consequat. </p>
                        <a href="#" class="mr-2">
                           <span class="badge badge-primary">Hữu ích</span>
                        </a> 
                        <a href="" class="mr-2" data-toggle="modal" data-target="#Comment">
                           <span>Báo cáo</span>
                        </a> 
                        <a href="" class="mr-2" data-toggle="modal" data-target="#report">
                           <span>bình luận</span>
                        </a> 
                        <div class="media mt-5">
                           <div class="d-flex mr-3"> 
                              <a href="#"> 
                                 <img class="media-object brround" alt="64x64" src="../img_user/default.jpg"> 
                              </a> 
                           </div>
                           <div class="media-body">
                              <h5 class="mt-0 mb-1 font-weight-semibold">TTS cc 
                                 <span class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" 
                                    title="" data-original-title="verified">
                                    <i class="fa fa-check-circle-o text-success"></i>
                                 </span>
                              </h5>
                              <small class="text-muted">
                                 <i class="fa fa-calendar"></i> Ngày 22st 
                                 <i class=" ml-3 fa fa-clock-o"></i> 6.00 
                                 <i class=" ml-3 fa fa-map-marker"></i> Việt Nam
                              </small> 
                              <p class="font-13  mb-2 mt-2"> Lorem Ipsum available, quis nostrud exercitation ullamco laboris commodo Lorem Ipsum available, but the majority have suffered alteration in some form laboriosam, nisi ut aliquid ex ea commodi consequatur consequat. </p>
                              <a href="" data-toggle="modal" data-target="#Comment">
                                 <span class="badge badge-default">Bình luận</span>
                              </a> 
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="media p-5 border-top mt-0">
                     <div class="d-flex mr-3"> 
                        <a href="#"> 
                           <img class="media-object brround" alt="64x64" src="../img_user/default.jpg"> 
                        </a> 
                     </div>
                     <div class="media-body">
                        <h5 class="mt-0 mb-1 font-weight-semibold">Van kha 
                           <span class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" 
                              title="" data-original-title="verified">
                              <i class="fa fa-check-circle-o text-success"></i>
                           </span> 
                           <span class="fs-14 ml-2"> 4 
                              <i class="fa fa-star text-yellow"></i>
                           </span> 
                        </h5>
                        <small class="text-muted">
                           <i class="fa fa-calendar"></i> Ngày 21st 
                           <i class=" ml-3 fa fa-clock-o"></i> 16.35 
                           <i class=" ml-3 fa fa-map-marker"></i> Việt Nam
                        </small> 
                        <p class="font-13  mb-2 mt-2"> Lorem Ipsum available, quis Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et nostrud exercitation ullamco laboris commodo consequat. </p>
                        <a href="#" class="mr-2">
                           <span class="badge badge-primary">Hữu ích</span>
                        </a> 
                        <a href="" class="mr-2" data-toggle="modal" data-target="#Comment">
                           <span>Báo cáo</span>
                        </a>
                        <a href="" class="mr-2" data-toggle="modal" data-target="#report">
                           <span>Bình luận</span>
                        </a> 
                     </div>
                  </div>
               </div>
            </div>
            <!--/Comments--> 
            <div class="card mb-lg-0">
               <div class="card-header">
                  <h3 class="card-title">Để lại trả lời</h3>
               </div>
               <div class="card-body">
                  <div>
                     <div class="form-group"> <input type="text" class="form-control" id="name1" placeholder="Tên của bạn"> </div>
                     <div class="form-group"> <input type="email" class="form-control" id="email" placeholder="Địa chỉ email"> </div>
                     <div class="form-group"> <textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Bình luận"></textarea> </div>
                     <a href="#" class="btn btn-primary">Gửi trả lời</a> 
                  </div>
               </div>
            </div>
         </div>
         @endforeach
         <!--Right Side Content--> 
         <div class="col-xl-4 col-lg-4 col-md-12">

            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Gửi bởi</h3>
               </div>
               <div class="card-body  item-user">
                  <div class="profile-pic mb-0">
                     <img src="../img_user/{{$detail_ps->user->img}}" class="brround avatar-xxl" alt="user"> 
                     <div>
                        <a href="userprofile.html" class="text-dark">
                           <h4 class="mt-3 mb-1 font-weight-semibold">{{$detail_ps->user->firstname}} {{$detail_ps->user->lastname}}</h4>
                        </a>
                        <h6 class="text-muted font-weight-normal">nhà thiết kế web</h6>
                        <span class="text-muted">Thành viên từ năm 2015</span> 
                        <h6 class="mt-2 mb-0"><a href="personal-blog.html" class="btn btn-primary btn-sm">Xem tất cả quảng cáo</a></h6>
                     </div>
                  </div>
               </div>
               <div class="card-body item-user">
                  <h4 class="mb-4">Thông tin liên hệ</h4>
                  
                  <div>
                     <h6>
                        <span class="font-weight-semibold">
                           <i class="fa fa-envelope mr-3 mb-2"></i>
                        </span>
                        <a href="#" class="text-body">
                           {{$detail_ps->user->email}}
                        </a>
                     </h6>
                     <h6>
                        <span class="font-weight-semibold">
                           <i class="fa fa-phone mr-3  mb-2"></i>
                        </span>
                        <a href="#" class="text-body">{{$detail_ps->user->phone}}</a>
                     </h6>
                     <h6>
                        <span class="font-weight-semibold">
                           <i class="fa fa-link mr-3 mb-2"></i>
                        </span>
                        <a href="#" class="text-body">http://spruko.com/</a>
                     </h6>
                     <h6>
                        <span class="font-weight-semibold">
                           <i class="fa fa-map-marker mr-3 "></i>
                        </span><a class="text-body">{{$detail_ps->user->city}},{{$detail_ps->user->address}}</a>
                     </h6>
                  </div>
                  <div class=" item-user-icons mt-4"> 
                     <a href="#" class="facebook-bg mt-0">
                        <i class="fa fa-facebook"></i>
                     </a> 
                     <a href="#" class="twitter-bg">
                        <i class="fa fa-twitter"></i>
                     </a> 
                     <a href="#" class="google-bg">
                        <i class="fa fa-google">
                        </i>
                     </a> 
                     <a href="#" class="dribbble-bg">
                        <i class="fa fa-dribbble"></i>
                     </a> 
                  </div>
               </div>
               <div class="card-footer">
                  <div class="text-left"> 
                     <a href="#" class="btn  btn-info">
                        <i class="fa fa-envelope"></i>Trò Chuyện
                     </a> 
                     <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#contact">
                        <i class="fa fa-user"></i> Liên hệ chúng tôi
                     </a> 
                     <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#contact">
                        <i class="fa fa-share-alt"></i> Chia sẻ
                     </a> 
                  </div>
               </div>
            </div>
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Từ Khóa</h3>
               </div>
               <div class="card-body product-filter-desc">
                  <div class="product-tags clearfix">
                     <ul class="list-unstyled mb-0">
                        <li><a href="#">Linh kiện</a></li>
                        <li><a href="#">Phục vụ</a></li>
                        <li><a href="#">Xe cộ</a></li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Chia Sẻ</h3>
               </div>
               <div class="card-body product-filter-desc">
                  <div class="product-filter-icons text-center"> 
                     <a href="#" class="facebook-bg">
                        <i class="fa fa-facebook"></i>
                     </a> 
                     <a href="#" class="twitter-bg">
                        <i class="fa fa-twitter"></i>
                     </a> 
                     <a href="#" class="google-bg">
                        <i class="fa fa-google"></i>
                     </a> 
                     <a href="#" class="dribbble-bg">
                        <i class="fa fa-dribbble"></i>
                     </a> 
                     <a href="#" class="pinterest-bg">
                        <i class="fa fa-pinterest"></i>
                     </a> 
                  </div>
               </div>
            </div>
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Vị trí bản đồ</h3>
               </div>
               <div class="card-body">
                  <div class="map-header">
                     <div class="map-header-layer" id="map2" style="position: relative; overflow: hidden;">
                        <div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);">
                           <div class="gm-err-container">
                              <div class="gm-err-content">
                                 <div class="gm-err-icon"><img src="https://maps.gstatic.com/mapfiles/api-3/images/icon_error.png" draggable="false" style="user-select: none;"></div>
                                 <div class="gm-err-title">Rất tiếc! Đã xảy ra lỗi.</div>
                                 <div class="gm-err-message">Trang này đã không tải Google Maps đúng cách. Hãy xem bảng điều khiển JavaScript để biết chi tiết kỹ thuật.</div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Quảng cáo tìm kiếm</h3>
               </div>
               <div class="card-body">
                  <div class="form-group"> <input type="text" class="form-control" id="search-text" placeholder="Bạn đang tìm kiếm cái gì?"> </div>
                  <div class="form-group search-cars1">
                     <select name="category" class="form-control m-bot15">
                     @foreach($categories as $key=>$cate_home_ps)
                        <option value="">Tất cả danh mục</option>
                        <option value="{{$cate_home_ps->id}}">{{$cate_home_ps->name}}</option>
                     @endforeach
                     </select>
                  </div>
                  <div> <a href="#" class="btn btn-block btn-primary"><i class="fa fa-search"></i>Tìm kiếm</a> </div>
               </div>
            </div>
            <div class="card">
               <div class="card-header">
                  <div class="card-title">Các mẫu xe mới</div>
               </div>
               <div class="card-body ">
                  <ul class="vertical-scroll" style="overflow-y: hidden; height: 264px;">
                     @foreach($product_home as $key => $pro_h_ps)
                     <li style="" class="news-item">
                        <table class="categpry-poster" style="width: 300px;">
                           <tbody>
                              <tr>
                                 <td><img src="../img_product/{{$pro_h_ps->image_product}}" alt="image" class="w-8 border"></td>
                                 <td class="pl-3">
                                    <h5 class="mb-1 ">{{$pro_h_ps->name}}</h5>
                                    <a href="#" class="btn-link">Xem chi tiết</a>
                                    <span class="float-right font-weight-bold" style="font-size: 12px;">
                                       {{number_format($pro_h_ps->price,0,',','.')}}đ</span>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </li>
                     @endforeach
                  </ul>
               </div>
            </div>
            {{-- các quảng cáo người bán mới --}}
            <div class="card mb-0">
               <div class="card-header">
                  <h3 class="card-title">Các quảng cáo người bán mới</h3>
               </div>
               <div class="card-body">
                  <div class="rated-products">
                     <ul class="vertical-scroll" style="overflow-y: hidden; height: 488px;">
                        @foreach($poster_home as $det_h_ps)
                           <li style="" class="item">
                              <div class="media p-5 mt-0">
                                 <a href="{{URL::to('show-detail-poster/'.$det_h_ps->id)}}">
                                    <img class="mr-4" src="../img_poster/{{$det_h_ps->img}}" alt="img"> 
                                 </a>
                                 <div class="media-body">
                                    <h4 class="mt-2 mb-1">{{$det_h_ps->title}}</h4>
                                    <span class="rated-products-ratings"> <i class="fa fa-star text-warning"> </i> <i class="fa fa-star text-warning"> </i> <i class="fa fa-star text-warning"> </i> <i class="fa fa-star text-warning"> </i> <i class="fa fa-star-o text-warning"> </i> </span> 
                                    <div class="h5 mb-0 font-weight-semibold mt-1">
                                       <?php
                                          if ($det_h_ps->package == 0) {
                                       ?>
                                          $30 - $60
                                       <?php
                                          }elseif($det_h_ps->package == 1) {
                                       ?>
                                          $60 - $90    
                                       <?php
                                          }elseif($det_h_ps->package == 2) {
                                       ?>
                                          $90 - $120   
                                       <?php
                                          }elseif($det_h_ps->package == 3) {
                                       ?>
                                          $120 - $150 
                                       <?php
                                          }elseif($det_h_ps->package == 4) {
                                       ?>
                                          $150 - $180
                                       <?php
                                          }
                                       ?>
                                    </div>
                                 </div>
                              </div>
                           </li>
                       @endforeach
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <!--/Right Side Content--> 
      </div>
   </div>
</section>
@endsection