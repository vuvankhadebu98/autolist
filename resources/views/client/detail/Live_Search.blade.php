<div class="tab-pane active" id="tab-11">
  <ul id="id01">
    @foreach($data as $pr)
      <li value="{{$pr->price}}">
       <div class="card overflow-hidden">
          <div class="d-md-flex">
             <div class="item-card9-img">
               @if($pr->promotion >0)
               <div class="arrow-ribbon bg-primary">Sale</div>
               @endif
               @php
               $imageImplode = $pr->image_product;
               $imageExplode = explode(',',$imageImplode);
               @endphp
               @foreach(array_slice($imageExplode,0,1) as $img)
               <div class="item-card9-imgs"> <a class="link" href="cars.html"></a> <img src="./../img_product/{{$img}}" alt="img" class="cover-image"> </div>
               @endforeach
               <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a> </div>
               <div class="item-overly-trans">
                <div class="rating-stars">
                   <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> 
                   <div class="rating-stars-container">
                      <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                      <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                      <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                      <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                      <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                   </div>
                </div>
                <span><a href="cars.html" class="bg-gray">Used</a></span> 
             </div>
          </div>
          <div class="card border-0 mb-0">
             <div class="card-body " >
                <div class="item-card9" id="carbody">
                   <a href="cars.html" class="text-dark" >
                      <h4 class="font-weight-semibold mt-1">
                        {{$pr->name}}</h4>
                     </a>
                     <div class="item-card9-desc mb-2"> <a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a> <a href="#" class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i>
                     {{Carbon\Carbon::parse($pr->created_at)->format('d/m/Y')}}</span></a> </div>
                     <div class="description">
                       <p class="mb-0 leading-tight">{!!$pr->description!!}
                       </p>
                    </div>
                 </div>
              </div>
              <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                <div class="item-card9-footer d-sm-flex">
                   <div class="item-card9-cost" id="ida">
                      <h4 class="text-dark font-weight-bold mb-0 mt-0">{{($pr->price)- ($pr->price * $pr->promotion/100)}} Triệu </h4>
                   </div>
                   <div class="ml-auto"> 
                     <a href="#" class="mr-2" title="Car type"><i class="fa fa-car  mr-1 text-muted"></i> 
                       @if($pr->transmission_type == 1)
                       Hộp số sàn
                       @elseif($pr->transmission_type == 2)
                       Hộp số tự động
                       @elseif($pr->transmission_type == 3)
                       Hộp số tự động vô cấp CVT
                       @elseif($pr->transmission_type == 4)
                       Hộp số ly hợp kép DCT
                       @endif
                    </a> 
                    <a href="#" class="mr-2" title="Kilometrs"><i class="fa fa-road text-muted mr-1 "></i>
                       {{$pr->max_power}}</a> 
                       <a href="#" class="" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i>
                         @if($pr->fuel_type == 1)
                         Diesel
                         @elseif($pr->fuel_type == 2)
                         Xăng
                         @elseif($pr->fuel_type == 3)
                         CNG
                         @elseif($pr->fuel_type == 4)
                         Điện
                         @endif
                      </a>
                   </div>
                </div>
             </div>
          </div>
       </div>
      </div>
      </li>
    @endforeach
  </ul>
</div>