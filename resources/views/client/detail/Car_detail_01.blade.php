@extends('client.master')
@section('content')

<!--/Topbar-->
<!--Section-->
<style type="text/css">
  .error{
    color: red;
  }
  .media-body ul li.active span{
    color: #fff;
    background-color: #e72a1a;
  }
</style>
 <meta name="description" content="{{$description}}">
<meta name="keywords" content="{{$keyword}}"/>
<meta name="robots" content="INDEX,FOLLOW"/>

<link rel='stylesheet' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'>
<script src='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js'></script>

<div id="car_detail">
    <div class="cover-image sptb-1 bg-background" data-image-src="assets/images/banners/banner1.jpg"
        style="background: url(&quot;assets/images/banners/banner1.jpg&quot;) center center;">
        <div class="header-text1 mb-0">
            <div class="container">
                <!-------------------------------------phần search -->
                @include('client.select_search')
                <!----------------------------------------------------->
            </div>
        </div><!-- /header-text -->
    </div>
</div>
<!--Section-->
<!--BreadCrumb-->
<div class="bg-white border-bottom">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title">Cars</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Categories</a></li>
                <li class="breadcrumb-item active" aria-current="page">Cars</li>
            </ol>
        </div>
    </div>
</div>
<!--/BreadCrumb-->
<!--listing-->
<section class="sptb">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="card">
               <div class="single-productslide">
                  <div class="row no-gutter">
                     <div class="col-lg-6 pr-lg-0">
                        <div class="product-slider px-4 py-5 border-right">
                           <div  id="carousel" class="carousel slide" data-ride="carousel">
                              <div class="arrow-ribbon2 bg-primary">{{$product->price}} Triệu</div>
                              <div class="carousel-inner">
                                @php 
                                  $imageImplode = $product->image_product;
                                  $imageExplode = explode(',',$imageImplode);
                                @endphp
                                @foreach($imageExplode as $key => $img)
                                 <div class="carousel-item {{$key == 0 ? 'active' : '' }}"> 
                                    <img src="./../img_product/{{$img}}" alt="img"> 
                                 </div>
                                 @endforeach
                              </div>
                              
                              <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a>
                                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next"> <i class="fa fa-angle-right" aria-hidden="true"></i> </a> 
                           </div>
                           <div class="clearfix">
                              <div id="thumbcarousel" class="carousel thumbcarousel slide" data-interval="false">
                                 <div class="carousel-inner">
                                    <div class="carousel-item active">
                                      
                                        @foreach($imageExplode as $key=>$img)
                                            <div data-target="#carousel" data-slide-to="{{$key++}}" class="thumb">
                                              <img src="{{asset('img_product/'.$img)}}" alt="img"></div>
                                        @endforeach
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-6 pl-lg-0">
                        <div class="product-gallery-data mb-0 px-4 py-5">
                           <div class="item-det mb-4">
                              <a href="{{route('Car-Detail-01',$product->id)}}" class="text-dark">
                                 <h3 >{{$product->name}}</h3>
                              </a>
                              <div class=" d-xl-flex">
                                 <ul class="d-flex mb-0">
                                    <li class="mr-5"><a href="#" class="icons"><i class="ti-location-pin text-muted mr-1"></i> USA</a></li>
                                    <li class="mr-5"><a href="#" class="icons"><i class="ti-calendar text-muted mr-1"></i>{{Carbon\Carbon::parse($product->created_at)->format('d/m/Y')}}</a></li>
                                    <li class="mr-5"><a href="#" class="icons"><i class="ti-eye text-muted mr-1 fs-15"></i> {{$product->view}}</a></li>
                                 </ul>
                                 <div class="rating-stars d-flex mr-5">
                                    <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" id="rating-stars-value" value="4"> 
                                    <div class="rating-stars-container mr-2">
                                       <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                       <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                       <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                       <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                       <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                    </div>
                                    4.0 
                                 </div>
                                 <div class="rating-stars d-flex" id="idLove">
                                    <div class="rating-stars-container mr-2">
                                       <div class="rating-star sm" id="iddiv" onclick="divClick()"> <i class="fa fa-heart"></i> </div>
                                    </div>
                                    {{$product->like}} 
                                 </div>
                              </div>
                           </div>
                           <div class="mb-4">
                            @php
                              $imDess = $product->description;
                              $exDess = explode('-',$imDess);
                            @endphp
                            @foreach($exDess as $ex)
                              <p>{!!$ex!!}</p>
                            @endforeach
                           </div>
                           <div class="d-sm-flex mb-4">
                              <div class="h4">Colors:</div>
                              <div class="pl-4 d-flex">
                                 <div class="w-5 h-5 bg-primary brround mr-1"></div>
                                 <div class="w-5 h-5 bg-secondary brround mr-1"></div>
                                 <div class="w-5 h-5 bg-success brround mr-1"></div>
                                 <div class="w-5 h-5 bg-danger brround mr-1"></div>
                                 <div class="w-5 h-5 bg-orange brround mr-1"></div>
                                 <div class="w-5 h-5 bg-light brround mr-1"></div>
                              </div>
                           </div>
                           <a class="btn btn-primary btn-lg text-white">Đặt lịch</a>
                           <div class="icons"> 
                              {{-- <div class="dropdown" style="float: left;">
                                <button class="btn btn-light border icons mt-1 mb-1 mr-1 " data-toggle="dropdown"><i class="fa fa-share-alt mr-1"></i>Chia sẻ
                                </button>
                                <ul class="dropdown-menu">
                                    <li class="fb-share-button" data-href="http://localhost/autolist_2/public/" data-layout="button_count" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Flocalhost%2Fautolist_2%2Fpublic%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></li>
                                  <li><a href="#">CSS</a></li>
                                  <li><a href="#">JavaScript</a></li>
                                </ul>
                              </div> --}}
                              </style>
                                  <div class="fb-share-button btn-light  icons mt-1 mb-1 mr-1" data-href="http://localhost/autolist_2/public/" data-layout="button_count" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$url_canonical}}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
                               <?php 
                              if($product->like == 0){
                                ?>
                                <a style="height: 28px;padding: 0.25rem;" href="{{route('like-product',$product->id)}}" class="btn btn-light border icons mt-1 mb-1 mr-1"><i class="fa fa-heart  mr-1" style="color:#83829c;"></i> {{$product->like}} </a>
                              <?php 
                              }
                              else{
                              ?>
                                <a style="height: 28px;padding: 0.25rem;" href="{{route('unlike-product',$product->id)}}" class="btn btn-light border icons mt-1 mb-1 mr-1"><i class="fa fa-heart  mr-1"></i> {{$product->like}} </a>
                              <?php
                              }
                              ?> 
                            </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="pt-4 pb-4 pl-5 pr-5 border-top border-top">
                  <div class="list-id">
                     <div class="row">
                        <div class="col"> <a class="mb-0">Classified ID : #8256358</a> </div>
                        <div class="col col-auto"> Đăng bởi <a class="mb-0 font-weight-bold">Individual</a> / 21st Dec 2019 </div>
                     </div>
                  </div>
               </div>
               <div class="card-footer">

               </div>
            </div>
            <div class="">
               <div class="">
                  <div class="border-0">
                     <div class="wideget-user-tab wideget-user-tab3">
                        <div class="tab-menu-heading">
                           <div class="tabs-menu1">
                              <ul class="nav">
                                 <li class=""><a href="#tab-1" class="active" data-toggle="tab">Tổng quát</a></li>
                                 <li><a href="#tab-3" data-toggle="tab" class="">Tính năng &amp; Tùy chọn</a></li>
                                 <li><a href="#tab-4" data-toggle="tab" class="">Thông tin xe</a></li>
                                 <li><a href="#tab-5" data-toggle="tab" class="">Video giới thiệu</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="tab-content border-left border-right border-top br-tr-3 border-bottom br-br-3 br-bl-3 p-5 bg-white mb-4">
                        <div class="tab-pane active" id="tab-1">
                           <h3 class="card-title mb-3 font-weight-semibold">Tổng quát</h3>
                           <div class="mb-4">
                              @foreach($exDess as $ex)
                                <p>{!!$ex!!}</p>
                              @endforeach
                           </div>
                           <h4 class="mb-4">Thông số kĩ thuật</h4>
                           <div class="row">
                              <div class="col-xl-12 col-md-12">
                                 <div class="table-responsive">
                                    <table class="table table-bordered w-100 m-0 text-nowrap ">
                                       <tbody>
                                          <tr>
                                             <td><span class="font-weight-bold">Loại nhiên liệu :</span>
                                              @if($product->fuel_type == 1)
                                                Diesel
                                              @elseif($product->fuel_type == 2)
                                                Xăng
                                              @elseif($product->fuel_type == 3)
                                                CNG
                                              @elseif($product->fuel_type == 4)
                                                Điện
                                              @endif
                                            </td>
                                             <td><span class="font-weight-bold">Loại phanh :</span> {{$product->break}}</td>
                                          </tr>
                                          <tr>
                                             <td><span class="font-weight-bold">Chỗ ngồi :</span> {{$product->seats}} chỗ</td>
                                             <td><span class="font-weight-bold">Màu :</span> Red , pink, Gray</td>
                                          </tr>
                                          <tr>
                                             <td><span class="font-weight-bold">Túi khí :</span> Available</td>
                                          </tr>
                                          <tr>
                                             <td><span class="font-weight-bold">Động cơ :</span> {{$product->engine}}</td>
                                             <td><span class="font-weight-bold">Năng lượng nguồn :</span> 
                                              {{$product->max_power}}
                                            </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane" id="tab-3">
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="table-responsive">
                                    <table class="table table-bordered border-top mb-0">
                                       <tbody>
                                        @foreach($product_val as $pr)
                                          @foreach($attri as $att)
                                            @if($product->id == $pr->product_id && 
                                              $pr->attribute_id == $att->id)
                                              <tr>
                                                 <td>{{$att->attribute_name}}</td>
                                                 <td><i class="icon icon-check text-success"></i></td>
                                              </tr>
                                              {{-- @elseif($product->id == $pr->product_id && $pr->attribute_id != $att->id)
                                              <tr>
                                                 <td>{{$att->attribute_name}}</td>
                                                 <td><i class="icon icon-close text-danger"></i></td>
                                              </tr> --}}
                                            @endif
                                          @endforeach
                                        @endforeach
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              {{-- <div class="col-md-6">
                                 <div class="table-responsive">
                                    <table class="table table-bordered border-top mb-0">
                                       <tbody>
                                          <tr>
                                             <td>Anti Lock Braking System</td>
                                             <td><i class="icon icon-check text-success"></i></td>
                                          </tr>
                                          <tr>
                                             <td>Driver Airbag</td>
                                             <td><i class="icon icon-check text-success"></i></td>
                                          </tr>
                                          <tr>
                                             <td>Wheel Covers</td>
                                             <td><i class="icon icon-check text-success"></i></td>
                                          </tr>
                                          <tr>
                                             <td>Automatic Climate Control</td>
                                             <td><i class="icon icon-close text-danger"></i></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div> --}}
                           </div>
                        </div>
                        <div class="tab-pane" id="tab-4">
                           <div class="row">
                              <div class="col-lg-6">
                                 <div class="table-responsive">
                                    <table class="table table-bordered border-top mb-0">
                                       <tbody>
                                          <tr>
                                             <td>Số dặm</td>
                                             <td>{{$product->arai_milage}}</td>
                                          </tr>
                                          <tr>
                                             <td>Mã lực (cc)</td>
                                             <td>{{$product->enegine_displacement}}</td>
                                          </tr>
                                          <tr>
                                             <td>Momen xoắn cực đại (nm@rpm)</td>
                                             <td>{{$product->max_torque}}</td>
                                          </tr>
                                          <tr>
                                             <td>Sức chứa chỗ ngồi</td>
                                             <td>{{$product->seats}}</td>
                                          </tr>
                                          <tr>
                                             <td>Không gian khởi động (Litres)</td>
                                             <td>{{$product->bootspace}}</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="table-responsive">
                                    <table class="table table-bordered border-top mb-0">
                                       <tbody>
                                          <tr>
                                             <td>nhiên liệu</td>
                                             <td>
                                              @if($product->fuel_type == 1)
                                                Diesel
                                              @elseif($product->fuel_type == 2)
                                                Xăng
                                              @elseif($product->fuel_type == 3)
                                                CNG
                                              @elseif($product->fuel_type == 4)
                                                Điện
                                              @endif
                                              </td>
                                          </tr>
                                          <tr>
                                             <td>Công suất tối đa (bhp@rpm)</td>
                                             <td>{{$product->max_power}}</td>
                                          </tr>
                                          <tr>
                                             <td>Hộp số</td>
                                             <td>
                                               @if($product->fuel_type == 1)
                                                Hộp số sàn
                                              @elseif($product->fuel_type == 2)
                                                Hộp số tự động
                                              @elseif($product->fuel_type == 3)
                                                Hộp số tự động vô cấp CVT
                                              @elseif($product->fuel_type == 4)
                                                Hộp số ly hợp kép DCT
                                              @endif
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>Dung tích bình xăng</td>
                                             <td>{{$product->fuel_tank_capacity}}</td>
                                          </tr>
                                          <tr>
                                             <td>Kiểu xe</td>
                                             @foreach($category as $key => $cat)
                                             @if($product->category_id == $cat->id)
                                             <td>{{$cat->name}}</td>
                                             @endif
                                             @endforeach
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane" id="tab-5">
                          <iframe width="560" height="315" src="{{$product->video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xl-12 col-lg-12 col-md-12">
            <h3 class="mb-5 mt-4">Bài viết liên quan</h3>
            <!--Related Posts--> 
            <div id="myCarousel5" class="owl-carousel owl-carousel-icons owl-loaded owl-drag">
               <!-- Wrapper for carousel items --> <!-- Wrapper for carousel items -->      
               <div class="owl-stage-outer">
                  <div class="owl-stage" style="transform: translate3d(-1601px, 0px, 0px); transition: all 0.25s ease 0s; width: 4404px;">
                    @foreach($productt as $key => $pro)
                     <div class="owl-item {{$key == 0 ? 'active' : '' }}" style="width: 375.333px; margin-right: 25px;">
                        <div class="item">
                           <div class="card">
                            @php 
                              $imageImplode = $pro->image_product;
                              $imageExplode = explode(',',$imageImplode);
                            @endphp
                              @foreach(array_slice($imageExplode,0,1) as $img)
                              <div class="item-card2-img">
                               <a class="link" href="{{route('Car-Detail-01',$pro->id)}}"></a>
                               <img style="width: 373.33px; height: 221.97px" src="./../img_product/{{$img}}" alt="img" class="cover-image"> 
                              </div>
                              @endforeach
                              <div class="item-card2-icons"> <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-share-alt"></i></a> <a href="#" class="item-card2-icons-r wishlist"><i class="fa fa fa-heart-o"></i></a> </div>
                              <div class="card-body pb-0">
                                 <div class="item-card2">
                                    <div class="item-card2-desc">
                                       <div class="item-card2-text">
                                          <a href="{{route('Car-Detail-01',$pro->id)}}" class="text-dark">
                                             <h4 class="mb-0">{{$pro->name}}</h4>
                                          </a>
                                       </div>
                                       <div class="d-flex pb-0 pt-0">
                                          <a>
                                             <p class="pb-0 pt-0 mb-2 mt-2"><i class="fa fa-map-marker text-danger mr-2"></i>Florida, Uk</p>
                                          </a>
                                          <span class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">{{$pro->price}} Triệu
                                          </span> 
                                       </div>
                                       <div class="description" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                                         <p class="">{!!$pro->description!!}
                                         </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="card-footer"> 
                                <a class="mr-4" data-toggle="tooltip" data-placement="bottom" data-original-title="Automatic"><i class="fa fa-car text-muted"></i> 
                                  <span class="text-default">
                                    @if($pro->transmission_type  == 1)
                                      Hộp số sàn
                                    @elseif($pro->transmission_type  == 2)
                                      Hộp số tự động
                                    @elseif($pro->transmission_type  == 3)
                                      Hộp số tự động vô cấp CVT
                                    @elseif($pro->transmission_type  == 4)
                                      Hộp số ly hợp kép DCT
                                    @endif
                                </span></a> 
                                <a class="mr-4" data-toggle="tooltip" data-placement="bottom" data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i> 
                                  <span class="text-default">
                                  {{$pro->max_power}}
                                  </span></a> 
                                  <a class="" data-toggle="tooltip" data-placement="bottom" data-original-title="FuelType"><i class="fa fa-tachometer text-muted"></i> 
                                    <span class="text-default">
                                      @if($pro->fuel_type == 1)
                                        Diesel
                                      @elseif($pro->fuel_type == 2)
                                        Xăng
                                      @elseif($pro->fuel_type == 3)
                                        CNG
                                      @elseif($pro->fuel_type == 4)
                                        Điện
                                      @endif
                                    </span></a> </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
               </div>
               <div class="owl-nav"><button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button></div>
               <div class="owl-dots disabled"></div>
            </div>
            <!--/Related Posts--> <!--Comments--> 
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Đánh giá</h3>
               </div>
               <div class="card-body">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="mb-4">
                           <p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i>5</span> </p>
                           <div class="progress progress-md mb-4 h-4">
                              <div class="progress-bar bg-success w-100">9,232</div>
                           </div>
                        </div>
                        <div class="mb-4">
                           <p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i>4</span> </p>
                           <div class="progress progress-md mb-4 h-4">
                              <div class="progress-bar bg-info w-80">8,125</div>
                           </div>
                        </div>
                        <div class="mb-4">
                           <p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i> 3</span> </p>
                           <div class="progress progress-md mb-4 h-4">
                              <div class="progress-bar bg-primary w-60">6,263</div>
                           </div>
                        </div>
                        <div class="mb-4">
                           <p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i> 2</span> </p>
                           <div class="progress progress-md mb-4 h-4">
                              <div class="progress-bar bg-secondary w-30">3,463</div>
                           </div>
                        </div>
                        <div class="mb-5">
                           <p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i> 1</span> </p>
                           <div class="progress progress-md mb-4 h-4">
                              <div class="progress-bar bg-orange w-20">1,456</div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-body p-0">
                @php
                  $proId = $product->id;
                @endphp
                @foreach($comment as $com)
                  @if($com->comment_id == null)
                    @foreach($users as $u)
                      @if($u->id == $com->user_id && $com->product_id == $proId)
                        <div class="media mt-0 border-top p-5">
                           <div class="d-flex mr-3"> <a href="#"><img class="media-object brround" alt="64x64"
                            src="./../img_user/{{$u->img}}"> </a> </div>
                          <div class="media-body">
                            <h5 class="mt-0 mb-1 font-weight-semibold">{{$u->username}}<span class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" title="" data-original-title="verified"><i class="fa fa-check-circle-o text-success"></i></span> <span class="fs-14 ml-2"> 4.5 <i class="fa fa-star text-yellow"></i></span> </h5>
                            <small class="text-muted"><i class="fa fa-calendar"></i>{{Carbon\Carbon::parse($com->created_at)->format('d/m/Y')}}<i class=" ml-3 fa fa-clock-o"></i> 
                              {{Carbon\Carbon::parse($com->created_at)->format('H:i')}}<i class=" ml-3 fa fa-map-marker"></i> Brezil</small> 
                            <p class="font-13  mb-2 mt-2">{{$com->content}}</p>
                            <ul class="Rep">
                              <li style="float: left">
                                <a href="" class="mr-2"><span class="badge ">Hữu ích</span></a>
                              </li>
                              <li style="float: left">
                                <a href="#RepComment_{{$com->id}}" class="mr-2" data-toggle="modal" data-target="#rep"><span class="badge">Trả lời</span></a>
                              </li>
                              <li >
                                <a href="" class="mr-2" data-toggle="modal" data-target="#report"><span class="badge">Báo cáo</span></a>
                              </li>
                            </ul> 
                            <div class="mediaRep" id="RepComment_{{$com->id}}">
                              @foreach($comment2 as $com2)
                                @if($com->id == $com2->comment_id)
                                  @foreach($users as $u)
                                    @if($u->id == $com2->user_id && $com2->product_id == $proId)
                                      <div class="media mt-5 "> 
                                        <div class="d-flex mr-3"> 
                                          <a href="#"> <img class="media-object brround" alt="64x64" 
                                            src="./../img_user/{{$u->img}}"> </a> 
                                        </div>
                                        <div class="media-body" > 
                                          <h5 class="mt-0 mb-1 font-weight-semibold">{{$u->username}}<span class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" title="" data-original-title="verified"><i class="fa fa-check-circle-o text-success"></i></span></h5> 
                                          <small class="text-muted"><i class="fa fa-calendar"></i>{{Carbon\Carbon::parse($com2->created_at)->format('d/m/Y')}}<i class=" ml-3 fa fa-clock-o"></i>{{Carbon\Carbon::parse($com2->created_at)->format('H:i')}}<i class=" ml-3 fa fa-map-marker"></i> Brezil</small> 
                                          <p class="font-13  mb-2 mt-2">{{$com2->content}}</p>
                                          <a href="" data-toggle="modal" onclick="return warningComment(this)" data-target="#rep" ><span class="badge badge-default">Trả lời</span></a> 
                                        </div> 
                                      </div>
                                    @endif
                                  @endforeach
                                @endif
                              @endforeach
                              @if($com->comment_id == null)
                                @foreach($users as $u)
                                  @if($u->id == $idUser )
                                    <div class="media mt-5 "> 
                                      <div class="d-flex mr-3"> 
                                        <a href="#"> <img class="media-object brround" alt="64x64" 
                                          src="./../img_user/{{$u->img}}"> </a> 
                                      </div>
                                      <div class="media-body" > 
                                        <h5 class="mt-0 mb-1 font-weight-semibold">
                                          {{$u->username}}
                                        </h5> 
                                        <small class="text-muted">
                                          <i class=" ml-3 fa fa-map-marker"></i> Brezil
                                        </small> 
                                        <p class="font-13 mb-2 mt-2"></p>
                                        <a href="" data-id="{{$com->id}}" onclick="return CommentRep(this)" data-toggle="modal" data-target="#rep">
                                          <span class="badge badge-default" style="color: #fff;background-color: #e72a1a">Nhập trả lời</span>
                                        </a>
                                      </div> 
                                    </div>
                                  @endif
                                @endforeach
                              @endif
                            </div>
                          </div>
                        </div>
                      @endif
                    @endforeach
                  @endif
                @endforeach
               </div>
            </div>
            <!--/Comments--> 
            <div class="card mb-lg-0">
               <div class="card-header">
                  <h3 class="card-title">Để lại nhận xét</h3>
               </div>
               <div class="card-body">
                <form action="{{route('postComment')}}" name="myForm" method="post" id="frmAddCmt" 
                enctype="multipart/form-data">
                  @csrf
                  <div>
                    <div class="form-group"> 
                      <input type="text" class="form-control" name="txtName" id="txtName" placeholder="Tên của bạn">
                      <input type="hidden" id="productID" value="{{$product->id}}" >
                      <div class="toast" data-autohide="false" id="Toast" style="position: absolute; top: -61px;left: 209px;">
                          <div class="toast fade show" style="background: red;">
                          <div class="toast-header">
                              <strong class="mr-auto" style="color: black;"><i class="fa fa-globe"></i> Cảnh báo!</strong>
                              <small class="text-muted">ngay bây giờ</small>
                              <button style="color: black;" type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
                          </div>
                          <div class="toast-body" style="color: #fff;">Bạn phải điền đầy đủ các thông tin.</div>
                          </div>
                      </div>
                    </div>
                    <div class="form-group"> 
                      <input type="email" class="form-control" name="txtEmail" id="txtEmail" placeholder="Địa chỉ Email"> 
                    </div>
                    <div class="form-group"> 
                      <textarea id="contentComment" class="form-control" 
                      name="txtComment" rows="6" placeholder="Bình luận"></textarea> 
                    </div>
                    <button type="button" id="btnSend" class="btnSend btn btn-primary" >
                    Gửi nhận xét</button>
                  </div>
                </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<div class="modal fade" id="rep" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true"> 
  <div class="modal-dialog" role="document"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <h5 class="modal-title" id="exampleCommentLongTitle">Trả lời Comment 
        </h5> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
          <span aria-hidden="true">×</span> 
        </button> 
      </div>
      <form action="{{route('postComment')}}" name="RepComment" method="post" enctype="multipart/form-data">
        <div class="modal-body"> 
        <div class="form-group"> 
          <input type="text" class="form-control" id="NameComment" placeholder="Nhập tên"> 
        </div> 
        <div class="form-group"> 
          <input type="email" class="form-control" id="EmailComment" placeholder="Nhập Email"> 
        </div> 
        <div class="form-group mb-0"> 
          <textarea class="form-control" id="Comment" name="example-textarea-input" rows="6" placeholder="Nội dung"></textarea> 
        </div> 
      </div> 
      <div class="modal-footer"> 
        <button type="button" class="btn btn-danger" data-dismiss="modal">Thoát</button> 
        <button type="button" id="btnRep" class="btn btn-success">Gửi</button> 
      </div>
      </form> 
       
    </div> 
  </div> 
</div>

<script type="text/javascript">
  function CommentRep(e){
    var idComment = $(e).data('id');

    $('#btnRep').click(function(){
      var nameComment = document.forms["RepComment"]["NameComment"].value;
      var emailComment = document.forms["RepComment"]["EmailComment"].value;
      var content = document.forms["RepComment"]["Comment"].value;

      if (nameComment == "") {
        alert("Tên không được để trống!");
        document.getElementById("NameComment").focus();
      } else if(emailComment == ""){
        alert("Email không được để trống!");
        document.getElementById("EmailComment").focus();
      }else if(content == ""){
        alert("Nội dung không được để trống!");
        document.getElementById("Comment").focus();
      }

      if (nameComment !="" && emailComment !="" && content !="") {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        var productID = $('#productID').val();

        $.ajax({
            url: "{{route('RepComment')}}",
            method: "POST",
            data: {
              commentRep: content,
              productID: productID,
              idComment: idComment,
            },
            success:function(data){
              if(data == 0){
                swal({
                  title: "Cảnh báo!",
                  text: "Bạn cần phải đăng nhập để Comment!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: '#DD6B55',
                  confirmButtonText: 'Đăng nhập',
                  cancelButtonText: 'Thoát',
                },function(abc){
                  if (abc) {
                    location.href = "{{route('getLogin')}}";
                  }
                });
              }
              if(data == 1){
                swal({
                  title: "Thành công!",
                  text: "Cảm ơn bạn đã đóng góp ý kiến!", 
                  confirmButtonText: 'OK',
                  imageUrl: 'http://i.imgur.com/4NZ6uLY.jpg',
                },function(reload){
                  if (reload) {
                    location.reload();
                  }
                });
              }
            }
          })
      }
    });
  }

  function warningComment(e) {
    swal({
      title: "Cảnh báo!",
      text: "Bạn cần phải đăng nhập để Comment!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Đăng nhập',
      cancelButtonText: 'Thoát',
    },function(abc){
      if (abc) {
        location.href = "{{route('getLogin')}}";
      }
    });
  }
</script>
<script>
  $(document).ready(function(){
//<--------TabComment--------->
    function activeTab(obj)
    {
        // Xóa class active tất cả các tab
        $('.media-body ul li').removeClass('active');

        // Thêm class active vòa tab đang click
        $(obj).addClass('active');

        // Lấy href của tab để show content tương ứng
        var id = $(obj).find('a').attr('href');
        console.log(id);
        // Ẩn hết nội dung các tab đang hiển thị
        $('.mediaRep').hide();

        // Hiển thị nội dung của tab hiện tại
        $(id) .show();
    }

    // Sự kiện click đổi tab
    $('.Rep li').click(function(){
        activeTab(this);
        return false;
    });

    // Active tab đầu tiên khi trang web được chạy
    activeTab($('.media-body li:first-child'));
//----------Comment------->
    $('#btnSend').click(function(){
      var name1 = document.forms["myForm"]["txtName"].value;
      var email1 = document.forms["myForm"]["txtEmail"].value;
      var comment1 = document.forms["myForm"]["txtComment"].value;
      if (name1 == "") {
        // alert("Name must be filled out");
        // return false;
     
        // document.getElementById("spanA").innerHTML = text;
        $("#Toast").toast('show');
        document.getElementById("txtName").focus();
      } else if(email1 == ""){
        $("#Toast").toast('show');
        document.getElementById("txtEmail").focus();
        return false;
      }else if(comment1 == ""){
        $("#Toast").toast('show');
        document.getElementById("txtComment").focus();
        return false;
      }

      if (name1 !="" && email1 !="" && comment1 !="") {
        var name = $('#txtName').val();
          var comment = $('#contentComment').val();
          var productID = $('#productID').val();
          // var formData = $("#frmAddCmt").serializeArray();
          // swal("Good job!", "You clicked the button!", "success");
        
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });

          $.ajax({
              url: "{{route('postComment')}}",
              method: "POST",
              data: {
                comment: comment,
                productID: productID,
              },
              success:function(data){
                if(data == 0){
                  swal({
                    title: "Cảnh báo!",
                    text: "Bạn cần phải đăng nhập để Comment!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Đăng nhập',
                    cancelButtonText: 'Thoát',
                  },function(abc){
                    if (abc) {
                      location.href = "{{route('getLogin')}}";
                    }
                  });
                }
                if(data == 1){
                  swal({
                    title: "Thành công!",
                    text: "Cảm ơn bạn đã đóng góp ý kiến!", 
                    type: "success",
                    confirmButtonText: 'OK',
                  },function(reload){
                    if (reload) {
                      location.reload();
                    }
                  });
                }
              }
            })
      }
    });
  });

//   document.getElementById('btnSend').onclick = function(){
//     swal({
//         title: "Sweet!",
//         text: "Here's a custom image.",
//         imageUrl: 'https://i.imgur.com/4NZ6uLY.jpg'
//     });
//};
</script>
<!--/listing-->
@endsection