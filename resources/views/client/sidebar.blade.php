            <div class="col-xl-3 col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">My Dashboard</h3>
                    </div>
                    <div class="card-body text-center item-user border-bottom">
                        <div class="profile-pic">
                            <div class="profile-pic-img">
                                @if($data['img'] == null)
                                <img src="user.png" style="border: 2px solid #d9d5d5" class="brround" alt="user">
                                @else
                                <img src="../../public/img_user/{{$data['img']}}" style="border: 2px solid #d9d5d5" class="brround" alt="user">
                                @endif
                            </div>
                            <a href="userprofile.html" class="text-dark"> <span class="bg-success dots"
                                    data-toggle="tooltip" data-placement="top" title=""
                                    data-original-title="online"></span>
                                <h4 class="mt-3 mb-0 font-weight-semibold">{{$data['name']}}
                                </h4>
                            </a>
                        </div>
                    </div>
                    <div class="item1-links  mb-0"> <a class="active d-flex border-bottom">
                            <span class="icon1 mr-3"><i class="icon icon-user"></i></span> Chỉnh sửa hồ sơ </a> 

                            <a href="{{route('getMyAds',['id'=>Auth::User()->id])}}" class=" d-flex  border-bottom"><span class="icon1 mr-3"><i class="icon icon-diamond"></i></span> quảng cao của tôi </a> 
                            
                            <a href="{{route('getFavorite',['id'=>Auth::User()->id])}}"class=" d-flex border-bottom"> <span class="icon1 mr-3"><i
                                    class="icon icon-heart"></i></span> Bảng điều khiển của tôi </a>

                            <!-- <a href="managed.html"class="d-flex  border-bottom"> <span class="icon1 mr-3"><i
                                    class="icon icon-folder-alt"></i></span> Managed Ads </a>  -->
<!--                             <a href="payments.html"
                            class=" d-flex  border-bottom"> <span class="icon1 mr-3"><i
                                    class="icon icon-credit-card"></i></span> Payments </a>  -->
                            <a href="{{route('getOrders')}}" class="d-flex  border-bottom"> 
                                <span class="icon1 mr-3"><i class="icon icon-basket"></i></span> Đơn Hàng </a> 
                                    <!-- <a href="tips.html"
                            class="d-flex border-bottom"> <span class="icon1 mr-3"><i
                                    class="icon icon-game-controller"></i></span> Safety Tips </a> --> <!-- <a
                            href="settings.html" class="d-flex border-bottom"> <span class="icon1 mr-3"><i
                                    class="icon icon-settings"></i></span> Settings </a> -->


                        <a href="#" class="d-flex">
                            <span class="icon1 mr-3"><i class="icon icon-power"></i></span>
                            Đăng Xuất </a> </div>
                </div>
                <div class="card my-select">
                    <div class="card-header">
                        <h3 class="card-title">Quảng cáo tìm kiếm</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group search-cars1">
                            <form method="post" action="{{route('searchPost')}}">
                                @csrf
                                <input type="text" name="car_search" class="form-control" placeholder="Nhập tên xe">
                                <select name="cate_search" class="form-control">
                                    @foreach($brand_car as $value_brand)
                                    <option value="{{$value_brand->id}}">{{$value_brand->name}}</option>
                                    @endforeach
                                </select>
                                <input type="submit" name="search" value="Search">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card  mb-5 mb-xl-0">
                    <div class="card-header">
                        <h3 class="card-title">Mẹo an toàn cho người mua</h3>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled widget-spec  mb-0">
                            <li class=""> <i class="fa fa-check text-success" aria-hidden="true"></i> 
                                Gặp Người bán tại Địa điểm công cộng </li>
                            <li class=""> <i class="fa fa-check text-success" aria-hidden="true"></i> 
                                Kiểm tra mặt hàng trước khi bạn mua </li>
                            <li class=""> <i class="fa fa-check text-success" aria-hidden="true"></i> 
                                Chỉ trả tiền sau khi thu thập vật phẩm </li>
                            <li class="ml-5 mb-0"> <a href="tips.html"> Xem thêm..</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
