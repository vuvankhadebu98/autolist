@extends('client.master')
@section('content')
</div> <!-- Topbar -->

<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg"
         style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">Favorite</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Dashboard</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">Favorite</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->
<!--Section-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <!-- sidebar -->
        @include('client.sidebar',['data'=>$data])
        <!-- end sidebar -->
            <div class="col-xl-9 col-lg-12 col-md-12">
                <div class="card mb-0">
                    <div class="card-header">
                        <h3 class="card-title" style="margin:20px">ShowRoom List</h3>

                        <a href="{{route('getshowroom')}}" class="btn btn-azure">Add</a>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive border-top">
                            <table class="table table-bordered table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th> ID</th>
                                    <th>Name</th>
                                    <th>Ảnh</th>
                                    <th >Nội dung</th>
                                    <th>Price</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($list as $shroom)
                                    <tr>

                                        <td scope="row">{{$shroom->id}}</td>
                                        <td>{{$shroom->name}}</td>
                                        <td><img width="200px" height="auto" src="../upload/{{$shroom->img_detail}}" class="image"> </td>
                                        <td>{!! $shroom->content !!}</td>
                                        <td>{{number_format($shroom->price).' '.'VNĐ'}}</td>
                                        <td>
                                            <a class="btn btn-bitbucket" href="{{URL::to('/edit-showr/'.$shroom->id)}}">Edit</a>

                                            <a class="btn btn-danger" href="{{URL::to('/delete-showr/'.$shroom->id)}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</section>
<!--/Section-->
@endsection
