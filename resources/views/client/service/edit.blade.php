@extends('client.master')
@section('content')
    <section class="sptb">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
            @include('client.sidebar',['data'=>$data])
            <!-- end sidebar -->
                <div class="col-lg-9 col-md-12 col-md-12">

                    <form action="{{URL::to('/update-showr/'.$servicee->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">

                                <label>Tên Dịch Vụ</label><br>
                                <input class="form-control" name="name"  value="{{$servicee->name}}" type="text" placeholder="Nhập dịch vụ" />
                            </div>

                            <div class="form-group">
                                <label>Image </label><br>
                                <input multiple="multiple" type="file" name="img_detail" class="form-control">
                            </div>
                            <img src="../upload/{{$servicee->img_detail}}">
                            <div class="form-group">
                                <label>Nội dung</label><br>
                                <textarea  name="content" id="content">{{$servicee->content}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Giá</label><br>
                                <input class="form-control" name="price" type="number" value="{{$servicee->price}}" />

                            </div>
                            <div class="form-group"><br>
                                <button type="submit" class="btn btn-azure">Submit</button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>

    </section>


@section('js')
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
@endsection
