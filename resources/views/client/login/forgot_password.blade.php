@extends('client.master')
@section('content')
</div>
<!--/Topbar-->
<!--Section-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner1.jpg"
    style="background: url(&quot;../assets/images/banners/banner1.jpg&quot;) center center;">
    <div class="header-text mb-0">
        <div class="container">
            <div class="text-center text-white ">
                <h1 class="">Forgot Password</h1>
                <ol class="breadcrumb text-center">
                    <li class="breadcrumb-item"><a href="{{route('getHome')}}">Home</a></li>
                    <li class="breadcrumb-item active text-white" aria-current="page"><a
                        href="{{route('forgotPassword')}}">Forgot password</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Section-->
<!--Forgot password-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-xl-4 col-md-6 d-block mx-auto">
                <div class="single-page w-100 p-0">
                    <div class="wrapper wrapper2">
                        <form id="forgotpsd" class="card-body" method="post">
                            @csrf
                            <h3 class="pb-2">Forgot password</h3>
                            @if(Session::has('messages'))
                                <strong style="color: green">{{Session::get('messages')}}</strong>
                            @endif
                            <div class="mail"> <input type="email" name="email"> <label>Mail or Username</label>
                            </div>
                            <input type="submit" name="send" value="Send" id="btn-send">
                            <div id="back" class="text-center text-dark mb-0"> Forget it, <a href="{{route('getLogin')}}">send me
                            back</a> to the sign in. </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Forgot password-->
@endsection