@extends('client.master')
@section('content')
</div>
<!--/Topbar-->
<!--Section-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg"
        style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">Login</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="{{route('getHome')}}">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page"><a
                                href="{{route('getLogin')}}">Register</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Section-->
<!--Login-Section-->
<section class="sptb">
    <div class="container customerpage">
        <div class="row">
            <div class="single-page">
                <div class="col-lg-5 col-xl-4 col-md-6 d-block mx-auto">
                    <div class="wrapper wrapper2">
                        <form id="login" class="card-body" tabindex="500" method="post">
                            @csrf
                            <h3>Login</h3>
                            @if(Session::has('messages'))
                            <strong class="messages_sussesses">{{Session::get('messages')}}</strong>
                            @endif
                            <div class="mail"> <input type="text" name="username" value="{{old('username')}}">
                                <label>Username</label>
                            </div>
                            @if($errors->has('username'))<p class="errors_validation">
                                <strong>{{$errors->first('username')}}</strong></p>@endif

                            <div class="passwd"> <input type="password" name="password"> <label>Password</label>
                            </div>
                            @if($errors->has('password'))<p class="errors_validation">
                                <strong>{{$errors->first('password')}}</strong></p>@endif
                            <div class="remember_me">
                                <input type="checkbox" id="remember_me" name="remember_me"><span
                                    id="remember_me">Remember me</span>
                            </div>
                            <input type="submit" id="btn-login" name="login" value="Login" style="">
                            <p class="mb-2"><a href="{{route('forgotPassword')}}">Forgot Password</a></p>
                            <p class="text-dark mb-0">Don't have account?<a href="register.html"
                                    class="text-primary ml-1">Sign UP</a></p>
                        </form>
                        <hr class="divider">
                        <div class="card-body social-images">
                            <p class="text-body text-left">Sign In to Social Accounts</p>
                            <div class="container">
                            <div class="row">
                                <!-- <div class="col-6"> <a href="https://www.facebook.com/"
                                        class="btn btn-white mr-2 border px-2 btn-lg btn-block text-left"> <img
                                            src="../../public/client/face_.png" class="h-5 w-5" alt=""><span
                                            class="ml-3 d-inline-block font-weight-bold">
                                            Facebook</span> </a> </div> -->
                                            <a href="{{route('redirect')}}" class="col-5" style="box-shadow: 2px 2px #f2f2f2;font-weight: 600;height: 50px;line-height: 50px;background-color: #2962ff;color: white;font-size: 15px">
                                                <i class="fab fa-facebook-f" style="background-color: #2962ff"></i>
                                                <span style="margin-left: 10px">Facebook</span>
                                            </a>
                                            <div class="col-2"></div>
                                            <a href="#" class="col-5" style="box-shadow: 2px 2px #f2f2f2;font-weight: 600;height: 50px;line-height: 50px;background-color: #ff382b;color: white;font-size: 15px">
                                                <i class="fab fa-facebook-f"></i>
                                                <span style="margin-left: 10px">Google</span>
                                            </a>
                                <!-- <div class="col-6"> <a href="https://www.google.com/gmail/"
                                        class="btn btn-white mr-2 px-2 border btn-lg btn-block text-left"> <img
                                            src="../assets/images/svgs/svg/search.svg" class="h-5 w-5" alt=""><span
                                            class="ml-3 d-inline-block font-weight-bold">
                                            Google</span> </a> </div> -->
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Login-Section-->
<!-- Newsletter-->
<section class="sptb2 bg-white border-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-xl-6 col-md-12">
                <div class="sub-newsletter">
                    <h3 class="mb-2"><i class="fa fa-paper-plane-o mr-2"></i> Subscribe To Our Newsletter</h3>
                    <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                        tempor</p>
                </div>
            </div>
            <div class="col-lg-5 col-xl-6 col-md-12">
                <div class="input-group sub-input mt-1"> <input type="text" class="form-control input-lg "
                        placeholder="Enter your Email">
                    <div class="input-group-append "> <button type="button"
                            class="btn btn-primary btn-lg br-tr-3  br-br-3"> Subscribe </button> </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Newsletter-->
<!-- Recent Post-->
<section class="sptb2 border-top">
    <div class="container">
        <h6 class="fs-18 mb-4">Latest Posts</h6>
        <hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
        <div class="row">
            <div class="col-md-12 col-lg-4">
                <div class="d-flex mt-0 mb-5 mb-lg-0 border bg-white p-4 box-shadow2"> <img class="w-8 h-8 mr-4"
                        src="../assets/images/products/6.png" alt="img">
                    <div class="media-body">
                        <h4 class="mt-0 mb-1 fs-16"><a class="text-body" href="#">Buy a CrusaderRecusandae</a>
                        </h4> <span class="fs-12 text-muted"><i class="fa fa-calendar"></i> 13th May 2019</span>
                        <div class="h6 mb-0 mt-1 font-weight-normal"><span class="font-weight-semibold">Price:</span>
                            $128 <del>$218</del></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-4">
                <div class="d-flex mt-0 mb-5 mb-lg-0 border bg-white p-4 box-shadow2"> <img class="w-8 h-8 mr-4"
                        src="../assets/images/products/4.png" alt="img">
                    <div class="media-body">
                        <h4 class="mt-0 mb-1 fs-16"><a class="text-body" href="#">Best New Car</a></h4> <span
                            class="fs-12 text-muted"><i class="fa fa-calendar"></i> 20th Jun 2019</span>
                        <div class="h6 mb-0 mt-1 font-weight-normal"><span class="font-weight-semibold">Price:</span>
                            $245 <del>$354</del></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-4">
                <div class="d-flex mt-0 mb-0 border bg-white p-4 box-shadow2"> <img class="w-8 h-8 mr-4"
                        src="../assets/images/products/2.png" alt="img">
                    <div class="media-body">
                        <h4 class="mt-0 mb-1 fs-16"><a class="text-body" href="#">Fuel Effeciency Car</a></h4>
                        <span class="fs-12 text-muted"><i class="fa fa-calendar"></i> 14th Aug 2019</span>
                        <div class="h6 mb-0 mt-1 font-weight-normal"><span class="font-weight-semibold">Price:</span>
                            $214 <del>$562</del></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> <!-- Recent Post-->
<!--Footer Section-->
<section>
    <footer class="bg-dark-purple text-white">
        <div class="footer-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-12">
                        <h6>Resources</h6>
                        <hr class="deep-purple text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
                        <ul class="list-unstyled mb-0">
                            <li><a href="javascript:;">Our Team</a></li>
                            <li><a href="javascript:;">Contact US</a></li>
                            <li><a href="javascript:;">About</a></li>
                            <li><a href="javascript:;">CrusaderRecusandae</a></li>
                            <li><a href="javascript:;">Blog</a></li>
                            <li><a href="javascript:;">Terms and Conditions</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <h6>Contact</h6>
                        <hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
                        <ul class="list-unstyled mb-0">
                            <li> <a href="#"><i class="fa fa-car mr-3 text-primary"></i> New York, NY 10012,
                                    US</a> </li>
                            <li> <a href="#"><i class="fa fa-envelope mr-3 text-primary"></i>
                                    info12323@example.com</a></li>
                            <li> <a href="#"><i class="fa fa-phone mr-3 text-primary"></i> + 01 234 567 88</a>
                            </li>
                            <li> <a href="#"><i class="fa fa-print mr-3 text-primary"></i> + 01 234 567 89</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <h6>Download App</h6>
                        <hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
                        <div class=""> <a class="btn bg-store fs-16" href=""><i class="fa fa-apple mr-2"></i>
                                App Store</a> <a class="btn bg-store fs-16" href=""><i class="fa fa-android mr-2"></i>
                                Google Pay</a> </div>
                        <h6 class="mb-0 mt-5">Payments</h6>
                        <hr class="deep-purple  text-primary accent-2 mb-2 mt-3 d-inline-block mx-auto">
                        <div class="clearfix"></div>
                        <ul class="footer-payments">
                            <li class="pl-0"><a href="javascript:;"><i class="fa fa-cc-amex" aria-hidden="true"></i></a>
                            </li>
                            <li><a href="javascript:;"><i class="fa fa-cc-visa" aria-hidden="true"></i></a></li>
                            <li><a href="javascript:;"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></a></li>
                            <li><a href="javascript:;"><i class="fa fa-cc-mastercard" aria-hidden="true"></i></a></li>
                            <li><a href="javascript:;"><i class="fa fa-cc-paypal" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <h6>Subscribe</h6>
                        <hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
                        <div class="clearfix"></div>
                        <div class="input-group w-100"> <input type="text" class="form-control br-tl-3  br-bl-3 "
                                placeholder="Email">
                            <div class="input-group-append "> <button type="button"
                                    class="btn btn-primary br-tr-3  br-br-3"> Subscribe </button> </div>
                        </div>
                        <h6 class="mt-5 mb-3">Follow Us</h6>
                        <hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"> <a
                                    class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
                                    <i class="fa fa-facebook bg-facebook"></i> </a> </li>
                            <li class="list-inline-item"> <a
                                    class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
                                    <i class="fa fa-twitter bg-info"></i> </a> </li>
                            <li class="list-inline-item"> <a
                                    class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
                                    <i class="fa fa-google-plus bg-danger"></i> </a> </li>
                            <li class="list-inline-item"> <a
                                    class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
                                    <i class="fa fa-linkedin bg-linkedin"></i> </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-dark-purple text-white p-0">
            <div class="container">
                <div class="row d-flex">
                    <div class="col-lg-12 col-sm-12 mt-3 mb-3 text-center "> Copyright © 2019 <a href="#"
                            class="fs-14 text-primary">Autolist</a>. Designed by <a href="#"
                            class="fs-14 text-primary">Spruko</a> All rights reserved. </div>
                </div>
            </div>
        </div>
    </footer>
</section>
<!--Footer Section-->
<!-- Back to top --> <a href="#top" id="back-to-top"><i class="fa fa-rocket"></i></a> <!-- JQuery js-->
<noscript>
    <p>To display this page you need a browser that supports JavaScript.</p>
</noscript>

</div>
</body>

</html>
@endsection
