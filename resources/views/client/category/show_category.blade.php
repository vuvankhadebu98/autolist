@extends('client.master')
@section('content')
<head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" type="text/css" media="all" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
  </script>
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <style type="text/css">
    .ui-slider-range {
      background: #3FE331;

    }
    .item-card9 p{
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
    }
    /*.tab-content>.tab-pane {
      display: block !important;
    }*/
  </style>
</head>

<div>
    <div class="cover-image sptb-1 bg-background" data-image-src="assets/images/banners/banner1.jpg"
        style="background: url(&quot;assets/images/banners/banner1.jpg&quot;) center center;">
        <div class="header-text1 mb-0">
            <div class="container">
                <!-------------------------------------phần search -->
                @include('client.select_search')
                <!----------------------------------------------------->
            </div>
        </div><!-- /header-text -->
    </div>
</div>
<div class="bg-white border-bottom">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title">Danh sách ô tô</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="#">Các trang</a></li>
                <li class="breadcrumb-item active" aria-current="page">Danh sách ô tô</li>
            </ol>
        </div>
    </div>
</div>
<section class="sptb">
   <div class="container">
      <div class="row">
         <div class="col-xl-9 col-lg-9 col-md-12">
            <!--Lists--> 
            <div class=" mb-0">
               <div class="">
                  <div class="item2-gl ">
                     <div class=" mb-0">
                        <div class="">
                           <div class="bg-white p-5 item2-gl-nav d-flex">
                              <h6 class="mb-0 mt-3 text-left">
                                <font style="vertical-align: inherit;">
                                  <font style="vertical-align: inherit;">Hiển thị 1 đến 10 trong số 30 mục nhập</font>
                                </font>
                              </h6>
                              <ul class="nav item2-gl-menu ml-auto mt-1">
                                 <li class="">
                                   <a href="#tab-11" class="active show" data-toggle="tab" title="Phong cách danh sách">
                                    <i class="fa fa-list"></i>
                                  </a>
                                </li>
                                 <li>
                                  <a href="#tab-12" data-toggle="tab" class="" title="Lưới">
                                    <i class="fa fa-th"></i>
                                  </a>
                                </li>
                              </ul>
                              <div class="d-sm-flex">
                                 <label class="mr-2 mt-2 mb-sm-1">
                                   <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Sắp xếp theo:</font>
                                  </font>
                                </label> 
                                 <div class="selectgroup"> 
                                  <label class="selectgroup-item mb-md-0"> 
                                    <input type="radio" name="value" value="Price" class="selectgroup-input" checked=""> 
                                    <span class="selectgroup-button">
                                      <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Giá bán </font>
                                      </font>
                                      <i class="fa fa-sort ml-1"></i>
                                    </span> 
                                  </label> 
                                  <label class="selectgroup-item mb-md-0"> 
                                    <input type="radio" name="value" value="Popularity" class="selectgroup-input"> 
                                    <span class="selectgroup-button">
                                      <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Phổ biến</font>
                                      </font>
                                    </span> 
                                  </label> 
                                  <label class="selectgroup-item mb-0"> 
                                    <input type="radio" name="value" value="Latest" class="selectgroup-input"> 
                                    <span class="selectgroup-button">
                                      <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Muộn nhất</font>
                                      </font>
                                    </span> 
                                  </label> 
                                  <label class="selectgroup-item mb-0"> 
                                    <input type="radio" name="value" value="Rating" class="selectgroup-input"> 
                                    <span class="selectgroup-button">
                                      <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Xếp hạng</font>
                                      </font>
                                    </span> 
                                  </label> 
                                </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-content">
                        <div class="tab-pane active" id="tab-11">
                          @foreach($all_product_cate as $cate_home)
                           <div class="card overflow-hidden">
                              <div class="d-md-flex">
                                 <div class="item-card9-img">
                                  <?php
                                    if($cate_home->check_new == 3){
                                  ?> 
                                    <div class="arrow-ribbon bg-primary">
                                      <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Giảm giá</font>
                                      </font>
                                    </div>
                                  <?php
                                    }else{
                                  ?>
                                  <?php
                                    }
                                  ?>
                                    <div class="item-card9-imgs"> 
                                      <a class="link" href="cars.html"></a> 
                                      <img src="../img_product/{{$cate_home->image_product}}" alt="img" class="cover-image" style="height: 200px"> 
                                    </div>
                                    
                                    <div class="item-card9-icons"> 
                                      <a href="#" class="item-card9-icons1 wishlist"> 
                                        <i class="fa fa fa-heart-o"></i>
                                      </a> 
                                    </div>
                                    <div class="item-overly-trans">
                                       <div class="rating-stars">
                                          <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> 
                                          <div class="rating-stars-container">
                                             <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                             <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                             <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                             <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                             <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                          </div>
                                       </div>
                                       <span>
                                        <a href="cars.html" class="bg-gray">
                                          <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">Đã sử dụng</font>
                                          </font>
                                        </a>
                                      </span> 
                                    </div>
                                 </div>
                                 <div class="card border-0 mb-0">
                                    <div class="card-body ">
                                       <div class="item-card9">
                                          <a href="cars.html" class="text-dark">
                                             <h4 class="font-weight-semibold mt-1">
                                              <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;"> Dignissimos</font>
                                              </font>
                                            </h4>
                                          </a>
                                          <div class="item-card9-desc mb-2"> 
                                            <a href="#" class="mr-4">
                                              <span class="">
                                                <i class="fa fa-map-marker text-muted mr-1"></i>
                                                <font style="vertical-align: inherit;">
                                                  <font style="vertical-align: inherit;"> Hoa Kỳ</font>
                                                </font>
                                              </span>
                                            </a> 
                                            <a href="#" class="mr-4">
                                              <span class="">
                                                <i class="fa fa-calendar-o text-muted mr-1"></i>
                                                <font style="vertical-align: inherit;">
                                                  <font style="vertical-align: inherit;"> 2 ngày trước</font>
                                                </font>
                                              </span>
                                            </a> 
                                          </div>
                                          <p class="mb-0 leading-tight">
                                            <font style="vertical-align: inherit;">
                                              <font style="vertical-align: inherit;">Lorem Ipsum có sẵn, nhưng phần lớn đã bị thay đổi dưới một số hình thức</font>
                                            </font>
                                          </p>
                                       </div>
                                    </div>
                                    <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                       <div class="item-card9-footer d-sm-flex">
                                          <div class="item-card9-cost">
                                             <h4 class="text-dark font-weight-bold mb-0 mt-0">
                                              <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">$ 748,00</font>
                                              </font>
                                            </h4>
                                          </div>
                                          <div class="ml-auto"> 
                                            <a href="#" class="mr-2" title="Loại xe ô tô">
                                              <i class="fa fa-car  mr-1 text-muted"></i>
                                              <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;"> Tự động</font>
                                              </font>
                                            </a> 
                                            <a href="#" class="mr-2" title="Kilometrs">
                                              <i class="fa fa-road text-muted mr-1 "></i>
                                              <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">4000Kms</font>
                                              </font>
                                            </a> 
                                            <a href="#" class="" title="FuealType">
                                              <i class="fa fa-tachometer text-muted mr-1"></i>
                                              <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">Điện</font>
                                              </font>
                                            </a> 
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @endforeach
                        </div>
                        <div class="tab-pane" id="tab-12">
                           <div class="row">
                              <div class="col-lg-6 col-md-12 col-xl-4">
                                 <div class="card overflow-hidden">
                                    <div class="item-card9-img">
                                       <div class="arrow-ribbon bg-primary"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 748,00</font></font></div>
                                       <div class="item-card9-imgs"> <a class="link" href="cars.html"></a> <img src="../assets/images/products/h4.png" alt="img" class="cover-image"> </div>
                                       <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a> </div>
                                       <div class="item-overly-trans">
                                          <div class="rating-stars">
                                             <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> 
                                             <div class="rating-stars-container">
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                             </div>
                                          </div>
                                          <span><a href="cars.html" class="bg-gray"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đã sử dụng</font></font></a></span> 
                                       </div>
                                    </div>
                                    <div class="card border-0 mb-0">
                                       <div class="card-body ">
                                          <div class="item-card9">
                                             <a href="cars.html" class="text-dark">
                                                <h4 class="font-weight-semibold mt-1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Dignissimos</font></font></h4>
                                             </a>
                                             <div class="item-card9-desc mb-2"> <a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Hoa Kỳ</font></font></span></a> <a href="#" class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 2 ngày trước</font></font></span></a> </div>
                                             <p class="mb-0 leading-tight"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Lorem Ipsum có sẵn, nhưng phần lớn đã bị thay đổi dưới một số hình thức</font></font></p>
                                          </div>
                                       </div>
                                       <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                          <div class="item-card9-footer d-sm-flex">
                                             <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Car type"><i class="fa fa-car  mr-1 text-muted"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Tự động</font></font></a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Kilometrs"><i class="fa fa-road text-muted mr-1 "></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4000Kms</font></font></a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Điện</font></font></a> </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-12 col-xl-4">
                                 <div class="card overflow-hidden sold-out">
                                    <div class="ribbon ribbon-top-left text-danger"><span class="bg-danger"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">bán hết</font></font></span></div>
                                    <div class="item-card9-img">
                                       <div class="arrow-ribbon bg-primary"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 862,00</font></font></div>
                                       <div class="item-card9-imgs"> <a class="link" href="cars.html"></a> <img src="../assets/images/products/j2.png" alt="img" class="cover-image"> </div>
                                       <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a> </div>
                                       <div class="item-overly-trans">
                                          <div class="rating-stars">
                                             <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4"> 
                                             <div class="rating-stars-container">
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                             </div>
                                          </div>
                                          <span><a href="cars.html" class="bg-success mr-2"><font style="vertical-align: inherit;"></font></a><font style="vertical-align: inherit;"><a href="cars.html" class="bg-danger"><font style="vertical-align: inherit;">Thuê </font></a><a href="cars.html" class="bg-success mr-2"><font style="vertical-align: inherit;">mới</font></a></font><a href="cars.html" class="bg-danger"><font style="vertical-align: inherit;"></font></a></span> 
                                       </div>
                                    </div>
                                    <div class="card border-0 mb-0">
                                       <div class="card-body ">
                                          <div class="item-card9">
                                             <div class="rating-stars"> <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> </div>
                                             <a href="cars.html" class="text-dark">
                                                <h4 class="font-weight-semibold mt-1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bài tập</font></font></h4>
                                             </a>
                                             <div class="item-card9-desc mb-2"> <a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Hoa Kỳ</font></font></span></a> <a href="#" class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 5 ngày trước</font></font></span></a> </div>
                                             <p class=" leading-tight"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Lorem Ipsum có sẵn, nhưng phần lớn đã bị thay đổi dưới một số hình thức</font></font></p>
                                          </div>
                                       </div>
                                       <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                          <div class="item-card9-footer d-sm-flex">
                                             <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Car type"><i class="fa fa-car text-muted mr-1"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Thủ công</font></font></a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Kilometrs"><i class="fa fa-road text-muted mr-1"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 5000Kms</font></font></a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Disel</font></font></a> </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-12 col-xl-4">
                                 <div class="card overflow-hidden">
                                    <div class="item-card9-img">
                                       <div class="arrow-ribbon bg-primary"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 635,00</font></font></div>
                                       <div class="item-card9-imgs"> <a class="link" href="cars.html"></a> <img src="../assets/images/products/j3.png" alt="img" class="cover-image"> </div>
                                       <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a> </div>
                                       <div class="item-overly-trans"> <span><a href="cars.html" class="bg-success"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mới</font></font></a></span> </div>
                                    </div>
                                    <div class="card border-0 mb-0">
                                       <div class="card-body ">
                                          <div class="item-card9">
                                             <div class="rating-stars"> <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> </div>
                                             <a href="cars.html" class="text-dark">
                                                <h4 class="font-weight-semibold mt-1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cheyenne</font></font></h4>
                                             </a>
                                             <div class="item-card9-desc mb-2"> <a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Hoa Kỳ</font></font></span></a> <a href="#" class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 5 ngày trước</font></font></span></a> </div>
                                             <p class=" leading-tight"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Lorem Ipsum có sẵn, nhưng phần lớn đã bị thay đổi dưới một số hình thức</font></font></p>
                                          </div>
                                       </div>
                                       <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                          <div class="item-card9-footer d-sm-flex">
                                             <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Car type"><i class="fa fa-car text-muted mr-1"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Thủ công</font></font></a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Kilometrs"><i class="fa fa-road text-muted mr-1"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 5000Kms</font></font></a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Xăng dầu</font></font></a> </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-12 col-xl-4">
                                 <div class="card overflow-hidden">
                                    <div class="item-card9-img">
                                       <div class="arrow-ribbon bg-primary"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 526,00</font></font></div>
                                       <div class="item-card9-imgs"> <a class="link" href="cars.html"></a> <img src="../assets/images/products/h3.png" alt="img" class="cover-image"> </div>
                                       <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a> </div>
                                       <div class="item-overly-trans">
                                          <div class="rating-stars">
                                             <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4"> 
                                             <div class="rating-stars-container">
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                             </div>
                                          </div>
                                          <span> <a href="cars.html" class="bg-success"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mới</font></font></a> </span> 
                                       </div>
                                    </div>
                                    <div class="card border-0 mb-0">
                                       <div class="card-body ">
                                          <div class="item-card9">
                                             <div class="rating-stars"> <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> </div>
                                             <a href="cars.html" class="text-dark">
                                                <h4 class="font-weight-semibold mt-1">Meridian</h4>
                                             </a>
                                             <div class="item-card9-desc mb-2"> <a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a> <a href="#" class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 5 days ago</span></a> </div>
                                             <p class=" leading-tight">Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                                          </div>
                                       </div>
                                       <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                          <div class="item-card9-footer d-sm-flex">
                                             <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Car type"><i class="fa fa-car text-muted mr-1"></i> Automatic</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Kilometrs"><i class="fa fa-road text-muted mr-1"></i> 4000Kms</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i> Hybrid</a> </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-12 col-xl-4">
                                 <div class="card overflow-hidden">
                                    <div class="item-card9-img">
                                       <div class="arrow-ribbon bg-primary">$256.00</div>
                                       <div class="item-card9-imgs"> <a class="link" href="cars.html"></a> <img src="../assets/images/products/v1.png" alt="img" class="cover-image"> </div>
                                       <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a> </div>
                                       <div class="item-overly-trans">
                                          <div class="rating-stars">
                                             <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4"> 
                                             <div class="rating-stars-container">
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                             </div>
                                          </div>
                                          <span> <a href="cars.html" class="bg-success">New</a> <a href="cars.html" class="bg-danger">Rent</a> </span> 
                                       </div>
                                    </div>
                                    <div class="card border-0 mb-0">
                                       <div class="card-body ">
                                          <div class="item-card9">
                                             <div class="rating-stars"> <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> </div>
                                             <a href="cars.html" class="text-dark">
                                                <h4 class="font-weight-semibold mt-1">Specter</h4>
                                             </a>
                                             <div class="item-card9-desc mb-2"> <a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a> <a href="#" class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 5 days ago</span></a> </div>
                                             <p class=" leading-tight">Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                                          </div>
                                       </div>
                                       <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                          <div class="item-card9-footer d-sm-flex">
                                             <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Car type"><i class="fa fa-car text-muted mr-1"></i> Manual</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Kilometrs"><i class="fa fa-road text-muted mr-1"></i> 5000Kms</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i> Petrol + CNG</a> </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-12 col-xl-4">
                                 <div class="card overflow-hidden">
                                    <div class="item-card9-img">
                                       <div class="arrow-ribbon bg-primary">$987.00</div>
                                       <div class="item-card9-imgs"> <a class="link" href="cars.html"></a> <img src="../assets/images/products/h1.png" alt="img" class="cover-image"> </div>
                                       <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a> </div>
                                       <div class="item-overly-trans">
                                          <div class="rating-stars">
                                             <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4"> 
                                             <div class="rating-stars-container">
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                             </div>
                                          </div>
                                          <span><a href="cars.html" class="bg-gray">Used</a></span> 
                                       </div>
                                    </div>
                                    <div class="card border-0 mb-0">
                                       <div class="card-body ">
                                          <div class="item-card9">
                                             <div class="rating-stars"> <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> </div>
                                             <a href="cars.html" class="text-dark">
                                                <h4 class="font-weight-semibold mt-1">Roamer</h4>
                                             </a>
                                             <div class="item-card9-desc mb-2"> <a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a> <a href="#" class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 5 days ago</span></a> </div>
                                             <p class=" leading-tight">Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                                          </div>
                                       </div>
                                       <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                          <div class="item-card9-footer d-sm-flex">
                                             <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Car type"><i class="fa fa-car text-muted mr-1"></i> Manual</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Kilometrs"><i class="fa fa-road text-muted mr-1"></i> 5000Kms</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i> Petrol + LPG</a> </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-12 col-xl-4">
                                 <div class="card overflow-hidden">
                                    <div class="item-card9-img">
                                       <div class="arrow-ribbon bg-primary">$746.00</div>
                                       <div class="item-card9-imgs"> <a class="link" href="cars.html"></a> <img src="../assets/images/products/v2.png" alt="img" class="cover-image"> </div>
                                       <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a> </div>
                                       <div class="item-overly-trans">
                                          <div class="rating-stars">
                                             <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4"> 
                                             <div class="rating-stars-container">
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                             </div>
                                          </div>
                                          <span><a href="cars.html" class="bg-gray">Used</a></span> 
                                       </div>
                                    </div>
                                    <div class="card border-0 mb-0">
                                       <div class="card-body ">
                                          <div class="item-card9">
                                             <div class="rating-stars"> <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> </div>
                                             <a href="cars.html" class="text-dark">
                                                <h4 class="font-weight-semibold mt-1">Dynamo</h4>
                                             </a>
                                             <div class="item-card9-desc mb-2"> <a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a> <a href="#" class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 5 days ago</span></a> </div>
                                             <p class=" leading-tight">Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                                          </div>
                                       </div>
                                       <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                          <div class="item-card9-footer d-sm-flex">
                                             <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Car type"><i class="fa fa-car text-muted mr-1"></i> Manual</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Kilometrs"><i class="fa fa-road text-muted mr-1"></i> 5000Kms</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i> Disel</a> </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-12 col-xl-4">
                                 <div class="card overflow-hidden">
                                    <div class="item-card9-img">
                                       <div class="arrow-ribbon bg-primary">$869.00</div>
                                       <div class="item-card9-imgs"> <a class="link" href="cars.html"></a> <img src="../assets/images/products/h4.png" alt="img" class="cover-image"> </div>
                                       <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a> </div>
                                       <div class="item-overly-trans">
                                          <div class="rating-stars">
                                             <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> 
                                             <div class="rating-stars-container">
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                             </div>
                                          </div>
                                          <span><a href="cars.html" class="bg-gray">Used</a></span> 
                                       </div>
                                    </div>
                                    <div class="card border-0 mb-0">
                                       <div class="card-body ">
                                          <div class="item-card9">
                                             <a href="cars.html" class="text-dark">
                                                <h4 class="font-weight-semibold mt-1">Nebula</h4>
                                             </a>
                                             <div class="item-card9-desc mb-2"> <a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a> <a href="#" class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 2 days ago</span></a> </div>
                                             <p class="mb-0 leading-tight">Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                                          </div>
                                       </div>
                                       <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                          <div class="item-card9-footer d-sm-flex">
                                             <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Car type"><i class="fa fa-car  mr-1 text-muted"></i> Automatic</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Kilometrs"><i class="fa fa-road text-muted mr-1 "></i>4000Kms</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i>Petrol</a> </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-12 col-xl-4">
                                 <div class="card overflow-hidden">
                                    <div class="item-card9-img">
                                       <div class="arrow-ribbon bg-primary">$987.00</div>
                                       <div class="item-card9-imgs"> <a class="link" href="cars.html"></a> <img src="../assets/images/products/j2.png" alt="img" class="cover-image"> </div>
                                       <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a> </div>
                                       <div class="item-overly-trans">
                                          <div class="rating-stars">
                                             <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4"> 
                                             <div class="rating-stars-container">
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                             </div>
                                          </div>
                                          <span><a href="cars.html" class="bg-success">New</a></span> 
                                       </div>
                                    </div>
                                    <div class="card border-0 mb-0">
                                       <div class="card-body ">
                                          <div class="item-card9">
                                             <div class="rating-stars"> <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> </div>
                                             <a href="cars.html" class="text-dark">
                                                <h4 class="font-weight-semibold mt-1">Essence Meridian</h4>
                                             </a>
                                             <div class="item-card9-desc mb-2"> <a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a> <a href="#" class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 5 days ago</span></a> </div>
                                             <p class=" leading-tight">Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                                          </div>
                                       </div>
                                       <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                          <div class="item-card9-footer d-sm-flex">
                                             <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Car type"><i class="fa fa-car text-muted mr-1"></i> Manual</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Kilometrs"><i class="fa fa-road text-muted mr-1"></i> 5000Kms</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i> Disel</a> </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-12 col-xl-4">
                                 <div class="card overflow-hidden">
                                    <div class="item-card9-img">
                                       <div class="arrow-ribbon bg-primary">$745.00</div>
                                       <div id="carouselExampleSlidesOnly1" class="carousel car-slide item-card9-imgs" data-ride="carousel">
                                          <a class="link" href="cars.html"></a> 
                                          <div class="carousel-inner">
                                             <div class="carousel-item active"> <img src="../assets/images/products/j3.png" alt="img" class="cover-image"> </div>
                                             <div class="carousel-item"> <img src="../assets/images/products/j1.png" alt="img" class="cover-image"> </div>
                                             <div class="carousel-item"> <img src="../assets/images/products/j2.png" alt="img" class="cover-image"> </div>
                                          </div>
                                          <a class="carousel-control-prev" href="#carouselExampleSlidesOnly1" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carouselExampleSlidesOnly1" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> 
                                       </div>
                                       <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a> </div>
                                       <div class="item-overly-trans"> <span><a href="cars.html" class="bg-success">New</a></span> </div>
                                    </div>
                                    <div class="card border-0 mb-0">
                                       <div class="card-body ">
                                          <div class="item-card9">
                                             <div class="rating-stars"> <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> </div>
                                             <a href="cars.html" class="text-dark">
                                                <h4 class="font-weight-semibold mt-1">Tarragon</h4>
                                             </a>
                                             <div class="item-card9-desc mb-2"> <a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a> <a href="#" class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 5 days ago</span></a> </div>
                                             <p class=" leading-tight">Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                                          </div>
                                       </div>
                                       <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                          <div class="item-card9-footer d-sm-flex">
                                             <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Car type"><i class="fa fa-car text-muted mr-1"></i> Manual</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Kilometrs"><i class="fa fa-road text-muted mr-1"></i> 5000Kms</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i> Hybrid</a> </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-12 col-xl-4">
                                 <div class="card overflow-hidden">
                                    <div class="item-card9-img">
                                       <div class="arrow-ribbon bg-primary">$748.00</div>
                                       <div class="item-card9-imgs"> <a class="link" href="cars.html"></a> <img src="../assets/images/products/h4.png" alt="img" class="cover-image"> </div>
                                       <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist"> <i class="fa fa fa-heart-o"></i></a> </div>
                                       <div class="item-overly-trans">
                                          <div class="rating-stars">
                                             <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> 
                                             <div class="rating-stars-container">
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                             </div>
                                          </div>
                                          <span><a href="cars.html" class="bg-gray">Used</a></span> 
                                       </div>
                                    </div>
                                    <div class="card border-0 mb-0">
                                       <div class="card-body ">
                                          <div class="item-card9">
                                             <a href="cars.html" class="text-dark">
                                                <h4 class="font-weight-semibold mt-1"> Dignissimos</h4>
                                             </a>
                                             <div class="item-card9-desc mb-2"> <a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a> <a href="#" class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 2 days ago</span></a> </div>
                                             <p class="mb-0 leading-tight">Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                                          </div>
                                       </div>
                                       <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                          <div class="item-card9-footer d-sm-flex">
                                             <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Car type"><i class="fa fa-car  mr-1 text-muted"></i> Automatic</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Kilometrs"><i class="fa fa-road text-muted mr-1 "></i>4000Kms</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i>Electric</a> </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-12 col-xl-4">
                                 <div class="card overflow-hidden">
                                    <div class="item-card9-img">
                                       <div class="arrow-ribbon bg-primary">$862.00</div>
                                       <div class="item-card9-imgs"> <a class="link" href="cars.html"></a> <img src="../assets/images/products/j2.png" alt="img" class="cover-image"> </div>
                                       <div class="item-card9-icons"> <a href="#" class="item-card9-icons1 wishlist active"> <i class="fa fa fa-heart-o"></i></a> </div>
                                       <div class="item-overly-trans">
                                          <div class="rating-stars">
                                             <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4"> 
                                             <div class="rating-stars-container">
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm is--active"> <i class="fa fa-star"></i> </div>
                                                <div class="rating-star sm"> <i class="fa fa-star"></i> </div>
                                             </div>
                                          </div>
                                          <span><a href="cars.html" class="bg-success mr-2">New</a><a href="cars.html" class="bg-danger">Rent</a></span> 
                                       </div>
                                    </div>
                                    <div class="card border-0 mb-0">
                                       <div class="card-body ">
                                          <div class="item-card9">
                                             <div class="rating-stars"> <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="3"> </div>
                                             <a href="cars.html" class="text-dark">
                                                <h4 class="font-weight-semibold mt-1">Exercitationem</h4>
                                             </a>
                                             <div class="item-card9-desc mb-2"> <a href="#" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i> USA</span></a> <a href="#" class="mr-4"><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i> 5 days ago</span></a> </div>
                                             <p class=" leading-tight">Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                                          </div>
                                       </div>
                                       <div class="card-footer pr-4 pl-4 pt-4 pb-4">
                                          <div class="item-card9-footer d-sm-flex">
                                             <div class=""> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Car type"><i class="fa fa-car text-muted mr-1"></i> Manual</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="Kilometrs"><i class="fa fa-road text-muted mr-1"></i> 5000Kms</a> <a href="#" class="w-50 mt-1 mb-1 float-left" title="FuealType"><i class="fa fa-tachometer text-muted mr-1"></i> Disel</a> </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="center-block text-center">
                     <ul class="pagination mb-3">
                        <li class="page-item page-prev disabled"> <a class="page-link" href="#" tabindex="-1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Trước đó</font></font></a> </li>
                        <li class="page-item active"><a class="page-link" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1</font></font></a></li>
                        <li class="page-item"><a class="page-link" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2</font></font></a></li>
                        <li class="page-item"><a class="page-link" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3</font></font></a></li>
                        <li class="page-item page-next"> <a class="page-link" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Kế tiếp</font></font></a> </li>
                     </ul>
                  </div>
               </div>
            </div>
            <!--/Lists--> 
         </div>
         <!--Right Side Content--> 
         <div class="col-xl-3 col-lg-3 col-md-12">
            <div class="card">
               <div class="card-body">
                  <div class="input-group">
                     <input type="text" class="form-control br-tl-3  br-bl-3" placeholder="Tìm kiếm"> 
                     <div class="input-group-append "> <button type="button" class="btn btn-primary br-tr-3  br-br-3"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Tìm kiếm </font></font></button> </div>
                  </div>
               </div>
            </div>
            <div class="card overflow-hidden">
               <div class="px-4 py-3 border-bottom">
                  <h4 class="mb-0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Thể loại</font></font></h4>
               </div>
               <div class="card-body">
                  <div class="closed" id="container" style="height: 250px; overflow: hidden;">
                     <div class="filter-product-checkboxs"> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox1" value="option1"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Lamborghini</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">14</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Giống rau thơm</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">22</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox3" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bóng ma</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">78</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox4" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chinnka Digli</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">35</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox5" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bài tập</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">23</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox6" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Subaru</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">14</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox7" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Eiusmod</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">45</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox7" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Quaerat</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">34</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox7" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Liên tiểu bang</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">12</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox7" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Alfa Romeo</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">18</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox7" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Harani</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">02</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox7" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Quan điểm</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">15</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox7" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Slea</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">32</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox7" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Blanditiis</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">23</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox7" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Co lại </font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">19</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox7" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Volkswagen</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">12</font></font></span></a> </span> </label> <label class="custom-control custom-checkbox mb-3"> <input type="checkbox" class="custom-control-input" name="checkbox7" value="option3"> <span class="custom-control-label"> <a href="#" class="text-dark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Quaerat</font></font><span class="label label-secondary float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">05</font></font></span></a> </span> </label> </div>
                  </div>
                  <div class="showmore-button">
                     <div class="showmore-button-inner more"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cho xem nhiều hơn</font></font></div>
                  </div>
               </div>
               <div class="px-4 py-3 border-bottom border-top">
                  <h4 class="mb-0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phạm vi giá</font></font></h4>
               </div>
               <div class="card-body">
                  <div class="h6"> <input type="text" id="price"> </div>
                  <div id="mySlider" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                     <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 19.2113%; width: 30.3337%;"></div>
                     <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 19.2113%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 49.545%;"></span>
                  </div>
               </div>
               <div class="px-4 py-3 border-bottom">
                  <h4 class="mb-0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tình trạng</font></font></h4>
               </div>
               <div class="card-body">
                  <div class="filter-product-checkboxs"> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2"> <span class="custom-control-label"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Tất cả </font></font></span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox1" value="option1"> <span class="custom-control-label"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Mới </font></font></span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2"> <span class="custom-control-label"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Đã sử dụng </font></font></span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox3" value="option2"> <span class="custom-control-label"> Certified Pre-Owned </span> </label> </div>
               </div>
               <div class="px-4 py-3 border-bottom border-top">
                  <h4 class="mb-0">Year</h4>
               </div>
               <div class="card-body">
                  <div class="row">
                     <div class="form-group col-md-6 mb-0">
                        <label for="inputState1" class="col-form-label">Min</label> 
                        <select id="inputState1" class="form-control select2 select2-hidden-accessible" data-select2-id="inputState1" tabindex="-1" aria-hidden="true">
                           <option data-select2-id="2">1995</option>
                           <option>1996</option>
                           <option>1997</option>
                           <option>1998</option>
                           <option>1999</option>
                           <option>2000</option>
                           <option>2001</option>
                           <option>2002</option>
                           <option>2003</option>
                           <option>2004</option>
                           <option>2005</option>
                           <option>2006</option>
                           <option>2007</option>
                           <option>2008</option>
                           <option>2009</option>
                           <option>2010</option>
                           <option>2011</option>
                           <option>2012</option>
                           <option>2013</option>
                           <option>2014</option>
                           <option>2015</option>
                           <option>2016</option>
                           <option>2017</option>
                           <option>2019</option>
                           <option>2019</option>
                           <option>2020</option>
                        </select>
                        <span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="1" style="width: 101px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-inputState1-container"><span class="select2-selection__rendered" id="select2-inputState1-container" role="textbox" aria-readonly="true" title="1995">1995</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span> 
                     </div>
                     <div class="form-group col-md-6 mb-0">
                        <label for="inputState2" class="col-form-label">Max</label> 
                        <select id="inputState2" class="form-control select2 select2-hidden-accessible" data-select2-id="inputState2" tabindex="-1" aria-hidden="true">
                           <option data-select2-id="4">2020</option>
                           <option>2019</option>
                           <option>2019</option>
                           <option>2017</option>
                           <option>2016</option>
                           <option>2015</option>
                           <option>2014</option>
                           <option>2013</option>
                           <option>2012</option>
                           <option>2011</option>
                           <option>2010</option>
                           <option>2009</option>
                           <option>2008</option>
                           <option>2007</option>
                           <option>2006</option>
                           <option>2005</option>
                           <option>2004</option>
                           <option>2003</option>
                           <option>2002</option>
                           <option>2001</option>
                           <option>2000</option>
                           <option>1999</option>
                           <option>1998</option>
                           <option>1997</option>
                           <option>1996</option>
                           <option>1995</option>
                        </select>
                        <span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="3" style="width: 101px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-inputState2-container"><span class="select2-selection__rendered" id="select2-inputState2-container" role="textbox" aria-readonly="true" title="2020">2020</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span> 
                     </div>
                  </div>
               </div>
               <div class="px-4 py-3 border-bottom border-top">
                  <h4 class="mb-0">Posted By</h4>
               </div>
               <div class="card-body">
                  <div class="filter-product-checkboxs"> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox1" value="option1"> <span class="custom-control-label"> Dealer </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2"> <span class="custom-control-label"> Individual </span> </label> <label class="custom-control custom-checkbox mb-0"> <input type="checkbox" class="custom-control-input" name="checkbox2" value="option2"> <span class="custom-control-label"> Reseller </span> </label> </div>
               </div>
               <div class="px-4 py-3 border-bottom border-top">
                  <h4 class="mb-0">Fuel Type</h4>
               </div>
               <div class="card-body">
                  <div class="filter-product-checkboxs"> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox11" value="option1"> <span class="custom-control-label"> Electric </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox11" value="option2"> <span class="custom-control-label"> Diesel </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox11" value="option2"> <span class="custom-control-label"> Petrol </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox11" value="option2"> <span class="custom-control-label"> Hybrid </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox11" value="option2"> <span class="custom-control-label"> Petrol+CNG </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox11" value="option2"> <span class="custom-control-label"> Petrol+LPG </span> </label> </div>
               </div>
               <div class="px-4 py-3 border-bottom border-top">
                  <h4 class="mb-0">Body Type</h4>
               </div>
               <div class="card-body">
                  <div class="filter-product-checkboxs"> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox12" value="option1"> <span class="custom-control-label"> Convertable </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox12" value="option2"> <span class="custom-control-label"> Coupe </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox12" value="option2"> <span class="custom-control-label"> Crossover </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox12" value="option2"> <span class="custom-control-label"> Hatchback </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox12" value="option2"> <span class="custom-control-label"> Muv </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox12" value="option2"> <span class="custom-control-label"> Quadricycle </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox12" value="option2"> <span class="custom-control-label"> Ringer Ace </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox12" value="option2"> <span class="custom-control-label"> SUV </span> </label> </div>
               </div>
               <div class="px-4 py-3 border-bottom border-top">
                  <h4 class="mb-0">Transmission</h4>
               </div>
               <div class="card-body">
                  <div class="filter-product-checkboxs"> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox13" value="option1"> <span class="custom-control-label"> AMT </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox13" value="option2"> <span class="custom-control-label"> Automatic </span> </label> <label class="custom-control custom-checkbox mb-2"> <input type="checkbox" class="custom-control-input" name="checkbox13" value="option2"> <span class="custom-control-label"> Manual </span> </label> </div>
               </div>
               <div class="card-footer"> <a href="#" class="btn btn-secondary btn-block">Apply Filter</a> </div>
            </div>
            <div class="card mb-0">
               <div class="card-header">
                  <h3 class="card-title">Shares</h3>
               </div>
               <div class="card-body product-filter-desc">
                  <div class="product-filter-icons text-center"> <a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a> <a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a> <a href="#" class="google-bg"><i class="fa fa-google"></i></a> <a href="#" class="dribbble-bg"><i class="fa fa-dribbble"></i></a> <a href="#" class="pinterest-bg"><i class="fa fa-pinterest"></i></a> </div>
               </div>
            </div>
         </div>
         <!--/Right Side Content--> 
      </div>
   </div>
</section>

@endsection