@extends('client.master')
@section('content')
</div> <!-- Topbar -->
<!--Breadcrumb-->
<!-- <div id="display_gray">
</div>
<div id="details_orders">
    <div id="title">
        Đơn hàng <span class="title" id="id_orders">#123456</span>
        <p id="cancel"><i style="line-height: 50px" class="fas fa-times"></i></p>
    </div>
    <div id="div_details_orders">
        <div id="content_order">
            <th>Order ID</th>
                                        <th>Category</th>
                                        <th>Date</th>
                                        <th>Price</th>
                                        <th>Status</th>
            <ul>
                <li>ID đơn hàng : #123456</li>
                <li>Tên mặt hàng : Tua bin khí</li>
                <li>Người đặt : Đặng Quang Nam</li>
                <li></li>
                <li>ID đơn hàng : #123456</li>
                <li>ID đơn hàng : #123456</li>
                <li>ID đơn hàng : #123456</li>
            </ul>
        </div>
    </div>
</div> -->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg"
         style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">Favorite</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Dashboard</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">Favorite</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->
<!--Section-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <!-- sidebar -->
        @include('client.sidebar',['data'=>$data])
        <!-- end sidebar -->
            <div class="col-xl-9 col-lg-12 col-md-12">
                <div class="card mb-0">
                    <div class="card-header">
                        <h3 class="card-title">Favorie List</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive border-top">
                            <table class="table table-bordered table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th> ID</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th >Descript</th>
                                    <th>Status</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($farr as $key)
                                <tr>

                                    <td class="text-primary"><span class="id_phu_tung">{{$key->id}}</span></td>
                                    <td>
                                        <img src="" style="height: 50px; width: 50px">
                                    </td>
                                    <td>{{$key->name}}</td>
                                    <td>{{$key->price}}</td>
                                    <td class="font-weight-semibold fs-16" style="width: 20px" >{{$key->description}}</td>
                                    <td> <a href="#" class="badge badge-danger">Pending</a> </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $farr->links() }}
                        </div>

{{--                        <ul class="pagination">--}}
{{--                            <li class="page-item page-prev disabled"> <a class="page-link" href="#"--}}
{{--                                                                         tabindex="-1">Prev</a> </li>--}}
{{--                            <li class="page-item active"><a class="page-link" href="#">1</a></li>--}}
{{--                            <li class="page-item"><a class="page-link" href="#">2</a></li>--}}
{{--                            <li class="page-item"><a class="page-link" href="#">3</a></li>--}}
{{--                            <li class="page-item page-next"> <a class="page-link" href="#">Next</a> </li>--}}
{{--                        </ul>--}}
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>
<!--/Section-->
@endsection

