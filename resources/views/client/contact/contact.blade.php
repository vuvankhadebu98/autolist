@extends('client.master')
@section('content')
<div>
	<div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg" style="background: url(&quot;../assets/images/banners/banner2.jpg&quot;) center center;">
		<div class="header-text mb-0">
			<div class="container">
				<div class="text-center text-white ">
					<h1 class="">Liên Hệ Chúng Tôi</h1>
					<ol class="breadcrumb text-center">
						<li class="breadcrumb-item"><a href="#">Trang Chủ</a></li>
						<li class="breadcrumb-item active text-white" aria-current="page">Tiếp Tục</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="sptb bg-white mb-0">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-xl-4 col-md-12">
				<div class="contact-description">
					<h2>Nói Xin Chào</h2>
					<p class="mt-5">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt</p>
					<div class="mb-5">
						<small class="text-muted">Cần Giúp Đỡ?</small> 
						<p class="mb-0 fs-16 font-weight-bold">me@Autolist.com</p>
					</div>
					<div class="mb-5">
						<small class="text-muted">Bạn cần Thắc mặc cái gì?</small> 
						<p class="mb-0 fs-16 font-weight-bold">+09 856 965 85</p>
					</div>
					<small class="text-muted">Socail Share</small> 
					<ul class="list-unstyled list-inline mt-3 mb-5">
						<li class="list-inline-item"> <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light"> <i class="fa fa-facebook bg-facebook"></i> </a> </li>
						<li class="list-inline-item"> <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light"> <i class="fa fa-twitter bg-info"></i> </a> </li>
						<li class="list-inline-item"> <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light"> <i class="fa fa-google-plus bg-danger"></i> </a> </li>
						<li class="list-inline-item"> <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light"> <i class="fa fa-linkedin bg-linkedin"></i> </a> </li>
					</ul>
				</div>
			</div>
			
			<div class="col-lg-7 col-xl-8 col-md-12">
				<div class="single-page">
					<div class="col-lg-12 col-md-12 mx-auto d-block">
						@if(session('thongbao'))
							<div class="alert btn-success">
								{{session('thongbao')}}	
							</div>
						@endif
						<div class="wrapper wrapper2">
							<div class="card box-shadow-0 mb-0">
								<div class="card-body">
									<form action="{{URL::to('save-contact')}}" method="GET" enctype="multipart/form-data">
										@csrf
										{{-- tên của bạn --}}
										<div class="form-group"> 
											<input type="text" class="form-control" id="name" name="name" 
											placeholder="Tên của bạn" required > 
										</div>
										{{-- địa chỉ email --}}
										<div class="form-group"> 
											<input type="email" class="form-control" id="email" name="email" placeholder="Địa chỉ Email" required> 
										</div>
										{{-- Nội dung --}}
										<div class="form-group"> 
											<textarea class="form-control" rows="8" id="editor1" 
												placeholder="nhập nội dung.." name="content" type="text" value=""></textarea>
										</div>
										{{-- gửi tin nhắn --}}
										<button type="submit" class="btn btn-primary">Gửi Tin Nhắn</button><br>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection