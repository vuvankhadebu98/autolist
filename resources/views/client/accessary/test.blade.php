@extends('client.master')
@section('content')
</div>
<!--/Topbar-->
<!--Section-->
<script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
<form id="form-send" method="post" action="{{route('sendContact')}}">
    @csrf
    <label id="label-form">
        <p class="label-form">Send message</p>
        <p class="label-form" id="label-cancel">X</p>
    </label>
    <hr>
    <div>
        <input type="text" class="form-control form-form-control" name="name_contact" placeholder="Your Name">
        <p></p>
        <input type="text" placeholder="Email Address" class="form-control form-form-control" name="email_contact">
        <p></p>
        <textarea id="form-form-control" name="content_contact" class="form-control form-form-control"
            placeholder="Messages"></textarea>
    </div>
    <hr>
    <div id="button-send">
        <input type="submit" id="btn-cancel" value="Cancel">
        <input type="submit" id="btn-send" value="Send" name="Send">
    </div>
</form>

<form id="form-report" method="post">
    <label id="label-form">
        <p class="label-form">Report Abuse</p>
        <p class="label-form" id="label-cancel">X</p>
    </label>
    <hr>
    <div>
        <input type="text" class="form-control form-form-control" name="" placeholder="Enter url">
        <p></p>

        <input type="text" class="form-control form-form-control" name="" placeholder="Title">
        <p></p> <input type="text" placeholder="Email Address" class="form-control form-form-control" name="">
        <p></p>
        <textarea id="form-form-control" class="form-control form-form-control" placeholder="Messages"></textarea>
    </div>
    <hr>
    <div id="button-send">
        <input type="submit" id="btn-cancel" value="Cancel">
        <input type="submit" id="btn-send" value="Send" name="Send">
    </div>

</form>


<!--Section-->
<div>
    <div class=" cover-image sptb-1 bg-background" data-image-src="../assets/images/banners/banner1.jpg"
        style="background: url(&quot;../assets/images/banners/banner1.jpg&quot;) center center;">
        <div class="header-text1 mb-0">
            <div class="container">
        <!-------------------------------------phần search -->
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white ">
                    <h1 class="mb-1">Find The Best Cars Buy and sell Near By You</h1>
                    <p>It is a long established fact that a reader will be distracted by the when looking at its
                        layout.</p>
                </div>
                <div class="row">
                    <div class="col-xl-10 col-lg-12 col-md-12 d-block mx-auto">
                        <div class="item-search-tabs">
                            <div class="item-search-menu">
                                <ul class="nav">
                                    <li class=""><a id="search_new" href="#tab1" class="active" data-toggle="tab">New Cars</a></li>
                                    <li><a id="search_use" href="#tab2" data-toggle="tab">Used Cars</a></li>
                                </ul>
                            </div>
                            @if(!isset($id_brand) && !isset($price) && !isset($seats) && !isset($city))
                                @include('client.select_search')
                            @else
                                @include('client.select_search',['id_brand'=>$id_brand,'price'=>$price,'seats'=>$seats,'city'=>$city])
                            @endif    
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /header-text -->
        <!----------------------------------------------------->
            </div>
        </div><!-- /header-text -->
    </div>
</div>
<!--/Section-->
<!--/Section-->
<!--Breadcrumb-->
<div class="bg-white border-bottom">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title">Ad Details</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Pages</a></li>
                <li class="breadcrumb-item active" aria-current="page">Chi tiết phụ tùng</li>
            </ol>
        </div>
    </div>
</div>
<!--/Breadcrumb-->
<!--listing-->
<section class="sptb">
    <div class="container">
        @foreach($accessary as $value_accessary)
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="single-productslide">
                        <div class="row no-gutter">
                            <div class="col-lg-6 pr-lg-0">
                                <div class="product-slider px-4 py-5 border-right">
                                    <div id="carousel" class="carousel slide" data-ride="carousel">
                                        <div class="arrow-ribbon2 bg-primary">{{$value_accessary->price}}</div>
                                        <div class="carousel-inner">
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item active"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                            <div class="carousel-item"> <img
                                                    src="../../public/client/xe.png" alt="img">
                                            </div>
                                        </div> <a class="carousel-control-prev" href="#carousel" role="button"
                                            data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a>
                                        <a class="carousel-control-next" href="#carousel" role="button"
                                            data-slide="next"> <i class="fa fa-angle-right" aria-hidden="true"></i> </a>
                                    </div>
                                    <div class="clearfix">
                                        <div id="thumbcarousel" class="carousel thumbcarousel slide"
                                            data-interval="false">
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <div data-target="#carousel" data-slide-to="0" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="1" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="2" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="3" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="4" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                </div>
                                                <div class="carousel-item ">
                                                    <div data-target="#carousel" data-slide-to="5" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="6" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="7" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="8" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="9" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                </div>
                                                <div class="carousel-item ">
                                                    <div data-target="#carousel" data-slide-to="10" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="11" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="12" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="13" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="14" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                </div>
                                                <div class="carousel-item ">
                                                    <div data-target="#carousel" data-slide-to="15" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="16" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="17" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="18" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                    <div data-target="#carousel" data-slide-to="19" class="thumb"><img
                                                            src="../../public/client/xe.png" alt="img">
                                                    </div>
                                                </div>
                                            </div> <a class="carousel-control-prev" href="#thumbcarousel" role="button"
                                                data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i>
                                            </a> <a class="carousel-control-next" href="#thumbcarousel" role="button"
                                                data-slide="next"> <i class="fa fa-angle-right" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 pl-lg-0">
                                <div class="product-gallery-data mb-0 px-4 py-5">
                                    <div class="item-det mb-4"> <a href="#" class="text-dark">
                                            <h3>{{$value_accessary->name}}</h3>
                                        </a>
                                        <div class=" d-xl-flex">
                                            <ul class="d-flex mb-0">
                                                <li class="mr-5"><a href="#" class="icons"><i
                                                            class="ti-location-pin text-muted mr-1"></i> USA</a>
                                                </li>
                                                <li class="mr-5"><a href="#" class="icons"><i
                                                            class="ti-calendar text-muted mr-1"></i>
                                                        {{$value_accessary->created_at}}</a></li>
                                                <li class="mr-5"><a href="#" class="icons"><i
                                                            class="ti-eye text-muted mr-1 fs-15"></i>
                                                        {{$value_accessary->view}}</a>
                                                </li>
                                            </ul>
                                            <div class="rating-stars d-flex mr-5"> <input type="number"
                                                    readonly="readonly" class="rating-value star"
                                                    name="rating-stars-value" id="rating-stars-value" value="4">
                                                <div class="rating-stars-container mr-2">
                                                    <div class="rating-star sm is--active"> <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-star sm is--active"> <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-star sm is--active"> <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-star sm is--active"> <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-star sm"> <i class="fa fa-star"></i>
                                                    </div>
                                                </div> 4.0
                                            </div>
                                            <div class="rating-stars d-flex">
                                                <div class="rating-stars-container mr-2">
                                                    <div class="rating-star sm"> <i class="fa fa-heart"></i>
                                                    </div>
                                                </div>{{$value_accessary->like}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-4">
                                        <p>{{$value_accessary->description}}</p>
                                    </div>
                                    <a class="btn btn-primary btn-lg text-white">Book Text Drive</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="icons"> <a href="#" class="btn btn-light border icons mt-1 mb-1 mr-1"><i
                                class="fa fa-share-alt mr-1"></i> Share Ad</a> <a href="#"
                            class="btn btn-light border icons mt-1 mb-1 mr-1"><i class="fa fa-heart  mr-1"></i> 678</a>
                        <a class="btn btn-light border mt-1 mb-1 icons mr-1" href="#"><i class="fa fa-print  mr-1"></i>
                            print</a> <a class="btn btn-light border mt-1 mb-1 icons mr-1" href="#"><i
                                class="fa fa-tag  mr-1"></i> Make an Offer</a> <a
                            class="btn btn-light border mt-1 mb-1 icons mr-1" href="#"><i
                                class="fa fa-calendar mr-1"></i> Book for Test Drive</a> <a
                            class="btn btn-light border mt-1 mb-1 icons mr-1" href="#"><i
                                class="fa fa-envelope-o mr-1"></i> Email a freind</a> <a
                            class="btn btn-light border mt-1 mb-1 icons mr-1" href="#"><i
                                class="fa fa-exchange mr-1"></i> Compare Cars</a> </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="col-xl-12 col-lg-12 col-md-12">
            <h3 class="mb-5 mt-4">Related Posts</h3>
            <!--Related Posts-->
            <div id="myCarousel5" class="owl-carousel owl-carousel-icons owl-loaded owl-drag">
                <!-- Wrapper for carousel items -->
                <!-- Wrapper for carousel items -->
                <div class="owl-stage-outer">
                    <div class="owl-stage"
                        style="transform: translate3d(-2802px, 0px, 0px); transition: all 0.25s ease 0s; width: 4404px;">
                        <div class="owl-item cloned" style="width: 375.333px; margin-right: 25px;">
                            <div class="item">
                                <div class="card">
                                    <div class="item-card2-img"> <a class="link" href="cars.html"></a> <img
                                            src="../assets/images/products/cars/b1.jpg" alt="img" class="cover-image">
                                    </div>
                                    <div class="item-card2-icons"> <a href="#" class="item-card2-icons-l bg-primary"> <i
                                                class="fa fa-share-alt"></i></a> <a href="#"
                                            class="item-card2-icons-r wishlist"><i class="fa fa fa-heart-o"></i></a>
                                    </div>
                                    <div class="card-body pb-0">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text"> <a href="cars.html" class="text-dark">
                                                        <h4 class="mb-0">Marquette Polo</h4>
                                                    </a> </div>
                                                <div class="d-flex pb-0 pt-0"> <a href="">
                                                        <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                                class="fa fa-map-marker text-danger mr-2"></i>Florida,
                                                            Uk</p>
                                                    </a> <span
                                                        class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">$200.00</span>
                                                </div>
                                                <p class="">Lorem Ipsum available, quis int nostrum
                                                    exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer"> <a href="#" class="mr-4" data-toggle="tooltip"
                                            data-placement="bottom" data-original-title="Automatic"><i
                                                class="fa fa-car text-muted"></i> <span
                                                class="text-default">Auto</span></a> <a href="#" class="mr-4"
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i>
                                            <span class="text-default">4000</span></a> <a href="#" class=""
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="FuelType"><i class="fa fa-tachometer text-muted"></i>
                                            <span class="text-default">Petrol</span></a> </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item cloned" style="width: 375.333px; margin-right: 25px;">
                            <div class="item">
                                <div class="card">
                                    <div class="item-card2-img"> <a class="link" href="cars.html"></a> <img
                                            src="../assets/images/products/cars/v1.jpg" alt="img" class="cover-image">
                                    </div>
                                    <div class="item-card2-icons"> <a href="#" class="item-card2-icons-l bg-primary"> <i
                                                class="fa fa-share-alt"></i></a> <a href="#"
                                            class="item-card2-icons-r wishlist"><i class="fa fa fa-heart-o"></i></a>
                                    </div>
                                    <div class="card-body pb-0">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text"> <a href="cars.html" class="text-dark">
                                                        <h4 class="mb-0">Instigator</h4>
                                                    </a> </div>
                                                <div class="d-flex pb-0 pt-0"> <a href="">
                                                        <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                                class="fa fa-map-marker text-danger mr-2"></i>Florida,
                                                            Uk</p>
                                                    </a> <span
                                                        class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">$200.00</span>
                                                </div>
                                                <p class="">Lorem Ipsum available, quis int nostrum
                                                    exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer"> <a href="#" class="mr-4" data-toggle="tooltip"
                                            data-placement="bottom" data-original-title="Manual"><i
                                                class="fa fa-car text-muted"></i> <span
                                                class="text-default">Manual</span></a> <a href="#" class="mr-4"
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i>
                                            <span class="text-default">2000</span></a> <a href="#" class=""
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="FuelType"><i class="fa fa-tachometer text-muted"></i>
                                            <span class="text-default">Petrol</span></a> </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item cloned" style="width: 375.333px; margin-right: 25px;">
                            <div class="item">
                                <div class="card">
                                    <div class="power-ribbon power-ribbon-top-left text-warning"><span
                                            class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                                    <div class="item-card2-img"> <a class="link" href="cars.html"></a> <img
                                            src="../assets/images/products/cars/f3.jpg" alt="img" class="cover-image">
                                    </div>
                                    <div class="item-card2-icons"> <a href="#" class="item-card2-icons-l bg-primary"> <i
                                                class="fa fa-share-alt"></i></a> <a href="#"
                                            class="item-card2-icons-r wishlist active"><i
                                                class="fa fa fa-heart"></i></a> </div>
                                    <div class="card-body pb-0">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text"> <a href="cars.html" class="text-dark">
                                                        <h4 class="mb-0">Herkime</h4>
                                                    </a> </div>
                                                <div class="d-flex pb-0 pt-0"> <a href="">
                                                        <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                                class="fa fa-map-marker text-danger mr-2"></i>Florida,
                                                            Uk</p>
                                                    </a> <span
                                                        class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">$200.00</span>
                                                </div>
                                                <p class="">Lorem Ipsum available, quis int nostrum
                                                    exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer"> <a href="#" class="mr-4" data-toggle="tooltip"
                                            data-placement="bottom" data-original-title="Manual"><i
                                                class="fa fa-car text-muted"></i> <span
                                                class="text-default">Manual</span></a> <a href="#" class="mr-4"
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i>
                                            <span class="text-default">2000</span></a> <a href="#" class=""
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="FuelType"><i class="fa fa-tachometer text-muted"></i>
                                            <span class="text-default">Petrol</span></a> </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item" style="width: 375.333px; margin-right: 25px;">
                            <div class="item">
                                <div class="card mb-0">
                                    <div class="power-ribbon power-ribbon-top-left text-warning"><span
                                            class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                                    <div class="item-card2-img"> <a class="link" href="cars.html"></a> <img
                                            src="../assets/images/products/cars/v5.jpg" alt="img" class="cover-image">
                                    </div>
                                    <div class="item-card2-icons"> <a href="#" class="item-card2-icons-l bg-primary"> <i
                                                class="fa fa-share-alt"></i></a> <a href="#"
                                            class="item-card2-icons-r wishlist active"><i
                                                class="fa fa fa-heart"></i></a> </div>
                                    <div class="card-body pb-0">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text"> <a href="cars.html" class="text-dark">
                                                        <h4 class="mb-0">CrusaderRecusandae</h4>
                                                    </a> </div>
                                                <div class="d-flex"> <a href="">
                                                        <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                                class="fa fa-map-marker text-danger mr-2"></i>Florida,
                                                            USA</p>
                                                    </a> <span
                                                        class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">$200.00</span>
                                                </div>
                                                <p class="">Lorem Ipsum available, quis int nostrum
                                                    exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer"> <a href="#" class="mr-4" data-toggle="tooltip"
                                            data-placement="bottom" data-original-title="Automatic"><i
                                                class="fa fa-car text-muted"></i> <span
                                                class="text-default">Auto</span></a> <a href="#" class="mr-4"
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i>
                                            <span class="text-default">2300</span></a> <a href="#" class=""
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="FuelType"><i class="fa fa-tachometer text-muted"></i>
                                            <span class="text-default">Petrol</span></a> </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item" style="width: 375.333px; margin-right: 25px;">
                            <div class="item">
                                <div class="card mb-0">
                                    <div class="power-ribbon power-ribbon-top-left text-warning"><span
                                            class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                                    <div class="item-card2-img"> <a class="link" href="cars.html"></a> <img
                                            src="../assets/images/products/cars/dummy.jpg" alt="img"
                                            class="cover-image"> </div>
                                    <div class="item-card2-icons"> <a href="#" class="item-card2-icons-l bg-primary"> <i
                                                class="fa fa-share-alt"></i></a> <a href="#"
                                            class="item-card2-icons-r wishlist active"><i
                                                class="fa fa fa-heart"></i></a> </div>
                                    <div class="card-body pb-0">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text"> <a href="cars.html" class="text-dark">
                                                        <h4 class="mb-0">Voluptates Scorpio</h4>
                                                    </a> </div>
                                                <div class="d-flex pb-0 pt-0"> <a href="">
                                                        <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                                class="fa fa-map-marker text-danger mr-2"></i>Florida,
                                                            Uk</p>
                                                    </a> <span
                                                        class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">$200.00</span>
                                                </div>
                                                <p class="">Lorem Ipsum available, quis int nostrum
                                                    exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer"> <a href="#" class="mr-4" data-toggle="tooltip"
                                            data-placement="bottom" data-original-title="Manual"><i
                                                class="fa fa-car text-muted"></i> <span
                                                class="text-default">Manual</span></a> <a href="#" class="mr-4"
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i>
                                            <span class="text-default">3000</span></a> <a href="#" class=""
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="FuelType"><i class="fa fa-tachometer text-muted"></i>
                                            <span class="text-default">Petrol</span></a> </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item" style="width: 375.333px; margin-right: 25px;">
                            <div class="item">
                                <div class="card">
                                    <div class="item-card2-img"> <a class="link" href="cars.html"></a> <img
                                            src="../assets/images/products/cars/b1.jpg" alt="img" class="cover-image">
                                    </div>
                                    <div class="item-card2-icons"> <a href="#" class="item-card2-icons-l bg-primary"> <i
                                                class="fa fa-share-alt"></i></a> <a href="#"
                                            class="item-card2-icons-r wishlist"><i class="fa fa fa-heart-o"></i></a>
                                    </div>
                                    <div class="card-body pb-0">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text"> <a href="cars.html" class="text-dark">
                                                        <h4 class="mb-0">Marquette Polo</h4>
                                                    </a> </div>
                                                <div class="d-flex pb-0 pt-0"> <a href="">
                                                        <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                                class="fa fa-map-marker text-danger mr-2"></i>Florida,
                                                            Uk</p>
                                                    </a> <span
                                                        class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">$200.00</span>
                                                </div>
                                                <p class="">Lorem Ipsum available, quis int nostrum
                                                    exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer"> <a href="#" class="mr-4" data-toggle="tooltip"
                                            data-placement="bottom" data-original-title="Automatic"><i
                                                class="fa fa-car text-muted"></i> <span
                                                class="text-default">Auto</span></a> <a href="#" class="mr-4"
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i>
                                            <span class="text-default">4000</span></a> <a href="#" class=""
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="FuelType"><i class="fa fa-tachometer text-muted"></i>
                                            <span class="text-default">Petrol</span></a> </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item" style="width: 375.333px; margin-right: 25px;">
                            <div class="item">
                                <div class="card">
                                    <div class="item-card2-img"> <a class="link" href="cars.html"></a> <img
                                            src="../assets/images/products/cars/v1.jpg" alt="img" class="cover-image">
                                    </div>
                                    <div class="item-card2-icons"> <a href="#" class="item-card2-icons-l bg-primary"> <i
                                                class="fa fa-share-alt"></i></a> <a href="#"
                                            class="item-card2-icons-r wishlist"><i class="fa fa fa-heart-o"></i></a>
                                    </div>
                                    <div class="card-body pb-0">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text"> <a href="cars.html" class="text-dark">
                                                        <h4 class="mb-0">Instigator</h4>
                                                    </a> </div>
                                                <div class="d-flex pb-0 pt-0"> <a href="">
                                                        <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                                class="fa fa-map-marker text-danger mr-2"></i>Florida,
                                                            Uk</p>
                                                    </a> <span
                                                        class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">$200.00</span>
                                                </div>
                                                <p class="">Lorem Ipsum available, quis int nostrum
                                                    exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer"> <a href="#" class="mr-4" data-toggle="tooltip"
                                            data-placement="bottom" data-original-title="Manual"><i
                                                class="fa fa-car text-muted"></i> <span
                                                class="text-default">Manual</span></a> <a href="#" class="mr-4"
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i>
                                            <span class="text-default">2000</span></a> <a href="#" class=""
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="FuelType"><i class="fa fa-tachometer text-muted"></i>
                                            <span class="text-default">Petrol</span></a> </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item active" style="width: 375.333px; margin-right: 25px;">
                            <div class="item">
                                <div class="card">
                                    <div class="power-ribbon power-ribbon-top-left text-warning"><span
                                            class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                                    <div class="item-card2-img"> <a class="link" href="cars.html"></a> <img
                                            src="../assets/images/products/cars/f3.jpg" alt="img" class="cover-image">
                                    </div>
                                    <div class="item-card2-icons"> <a href="#" class="item-card2-icons-l bg-primary"> <i
                                                class="fa fa-share-alt"></i></a> <a href="#"
                                            class="item-card2-icons-r wishlist active"><i
                                                class="fa fa fa-heart"></i></a> </div>
                                    <div class="card-body pb-0">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text"> <a href="cars.html" class="text-dark">
                                                        <h4 class="mb-0">Herkime</h4>
                                                    </a> </div>
                                                <div class="d-flex pb-0 pt-0"> <a href="">
                                                        <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                                class="fa fa-map-marker text-danger mr-2"></i>Florida,
                                                            Uk</p>
                                                    </a> <span
                                                        class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">$200.00</span>
                                                </div>
                                                <p class="">Lorem Ipsum available, quis int nostrum
                                                    exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer"> <a href="#" class="mr-4" data-toggle="tooltip"
                                            data-placement="bottom" data-original-title="Manual"><i
                                                class="fa fa-car text-muted"></i> <span
                                                class="text-default">Manual</span></a> <a href="#" class="mr-4"
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i>
                                            <span class="text-default">2000</span></a> <a href="#" class=""
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="FuelType"><i class="fa fa-tachometer text-muted"></i>
                                            <span class="text-default">Petrol</span></a> </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item cloned active" style="width: 375.333px; margin-right: 25px;">
                            <div class="item">
                                <div class="card mb-0">
                                    <div class="power-ribbon power-ribbon-top-left text-warning"><span
                                            class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                                    <div class="item-card2-img"> <a class="link" href="cars.html"></a> <img
                                            src="../assets/images/products/cars/v5.jpg" alt="img" class="cover-image">
                                    </div>
                                    <div class="item-card2-icons"> <a href="#" class="item-card2-icons-l bg-primary"> <i
                                                class="fa fa-share-alt"></i></a> <a href="#"
                                            class="item-card2-icons-r wishlist active"><i
                                                class="fa fa fa-heart"></i></a> </div>
                                    <div class="card-body pb-0">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text"> <a href="cars.html" class="text-dark">
                                                        <h4 class="mb-0">CrusaderRecusandae</h4>
                                                    </a> </div>
                                                <div class="d-flex"> <a href="">
                                                        <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                                class="fa fa-map-marker text-danger mr-2"></i>Florida,
                                                            USA</p>
                                                    </a> <span
                                                        class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">$200.00</span>
                                                </div>
                                                <p class="">Lorem Ipsum available, quis int nostrum
                                                    exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer"> <a href="#" class="mr-4" data-toggle="tooltip"
                                            data-placement="bottom" data-original-title="Automatic"><i
                                                class="fa fa-car text-muted"></i> <span
                                                class="text-default">Auto</span></a> <a href="#" class="mr-4"
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i>
                                            <span class="text-default">2300</span></a> <a href="#" class=""
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="FuelType"><i class="fa fa-tachometer text-muted"></i>
                                            <span class="text-default">Petrol</span></a> </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item cloned active" style="width: 375.333px; margin-right: 25px;">
                            <div class="item">
                                <div class="card mb-0">
                                    <div class="power-ribbon power-ribbon-top-left text-warning"><span
                                            class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                                    <div class="item-card2-img"> <a class="link" href="cars.html"></a> <img
                                            src="../assets/images/products/cars/dummy.jpg" alt="img"
                                            class="cover-image"> </div>
                                    <div class="item-card2-icons"> <a href="#" class="item-card2-icons-l bg-primary"> <i
                                                class="fa fa-share-alt"></i></a> <a href="#"
                                            class="item-card2-icons-r wishlist active"><i
                                                class="fa fa fa-heart"></i></a> </div>
                                    <div class="card-body pb-0">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text"> <a href="cars.html" class="text-dark">
                                                        <h4 class="mb-0">Voluptates Scorpio</h4>
                                                    </a> </div>
                                                <div class="d-flex pb-0 pt-0"> <a href="">
                                                        <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                                class="fa fa-map-marker text-danger mr-2"></i>Florida,
                                                            Uk</p>
                                                    </a> <span
                                                        class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">$200.00</span>
                                                </div>
                                                <p class="">Lorem Ipsum available, quis int nostrum
                                                    exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer"> <a href="#" class="mr-4" data-toggle="tooltip"
                                            data-placement="bottom" data-original-title="Manual"><i
                                                class="fa fa-car text-muted"></i> <span
                                                class="text-default">Manual</span></a> <a href="#" class="mr-4"
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i>
                                            <span class="text-default">3000</span></a> <a href="#" class=""
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="FuelType"><i class="fa fa-tachometer text-muted"></i>
                                            <span class="text-default">Petrol</span></a> </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item cloned" style="width: 375.333px; margin-right: 25px;">
                            <div class="item">
                                <div class="card">
                                    <div class="item-card2-img"> <a class="link" href="cars.html"></a> <img
                                            src="../assets/images/products/cars/b1.jpg" alt="img" class="cover-image">
                                    </div>
                                    <div class="item-card2-icons"> <a href="#" class="item-card2-icons-l bg-primary"> <i
                                                class="fa fa-share-alt"></i></a> <a href="#"
                                            class="item-card2-icons-r wishlist"><i class="fa fa fa-heart-o"></i></a>
                                    </div>
                                    <div class="card-body pb-0">
                                        <div class="item-card2">
                                            <div class="item-card2-desc">
                                                <div class="item-card2-text"> <a href="cars.html" class="text-dark">
                                                        <h4 class="mb-0">Marquette Polo</h4>
                                                    </a> </div>
                                                <div class="d-flex pb-0 pt-0"> <a href="">
                                                        <p class="pb-0 pt-0 mb-2 mt-2"><i
                                                                class="fa fa-map-marker text-danger mr-2"></i>Florida,
                                                            Uk</p>
                                                    </a> <span
                                                        class="ml-3 pb-0 pt-0 mb-2 mt-2 font-weight-bold">$200.00</span>
                                                </div>
                                                <p class="">Lorem Ipsum available, quis int nostrum
                                                    exercitationem </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer"> <a href="#" class="mr-4" data-toggle="tooltip"
                                            data-placement="bottom" data-original-title="Automatic"><i
                                                class="fa fa-car text-muted"></i> <span
                                                class="text-default">Auto</span></a> <a href="#" class="mr-4"
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="2300 Kilometrs"><i class="fa fa-road text-muted"></i>
                                            <span class="text-default">4000</span></a> <a href="#" class=""
                                            data-toggle="tooltip" data-placement="bottom"
                                            data-original-title="FuelType"><i class="fa fa-tachometer text-muted"></i>
                                            <span class="text-default">Petrol</span></a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="owl-nav"><button type="button" role="presentation" class="owl-prev"><span
                            aria-label="Previous">‹</span></button><button type="button" role="presentation"
                        class="owl-next"><span aria-label="Next">›</span></button></div>
                <div class="owl-dots disabled"></div>
            </div>
            <!--/Related Posts-->
            <!--Comments-->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Rating And Reviews</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-4">
                                <p class="mb-2"> <span class="fs-14 ml-2"><i
                                            class="fa fa-star text-yellow mr-2"></i>5</span> </p>
                                <div class="progress progress-md mb-4 h-4">
                                    <div class="progress-bar bg-success w-100">9,232</div>
                                </div>
                            </div>
                            <div class="mb-4">
                                <p class="mb-2"> <span class="fs-14 ml-2"><i
                                            class="fa fa-star text-yellow mr-2"></i>4</span> </p>
                                <div class="progress progress-md mb-4 h-4">
                                    <div class="progress-bar bg-info w-80">8,125</div>
                                </div>
                            </div>
                            <div class="mb-4">
                                <p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i>
                                        3</span> </p>
                                <div class="progress progress-md mb-4 h-4">
                                    <div class="progress-bar bg-primary w-60">6,263</div>
                                </div>
                            </div>
                            <div class="mb-4">
                                <p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i>
                                        2</span> </p>
                                <div class="progress progress-md mb-4 h-4">
                                    <div class="progress-bar bg-secondary w-30">3,463</div>
                                </div>
                            </div>
                            <div class="mb-5">
                                <p class="mb-2"> <span class="fs-14 ml-2"><i class="fa fa-star text-yellow mr-2"></i>
                                        1</span> </p>
                                <div class="progress progress-md mb-4 h-4">
                                    <div class="progress-bar bg-orange w-20">1,456</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="media mt-0 p-5">
                        <div class="d-flex mr-3"> <a href="#"><img class="media-object brround" alt="64x64"
                                    src="../assets/images/faces/male/1.jpg"> </a> </div>
                        <div class="media-body">
                            <h5 class="mt-0 mb-1 font-weight-semibold">Joanne Scott <span class="fs-14 ml-0"
                                    data-toggle="tooltip" data-placement="top" title=""
                                    data-original-title="verified"><i
                                        class="fa fa-check-circle-o text-success"></i></span> <span class="fs-14 ml-2">
                                    4.5 <i class="fa fa-star text-yellow"></i></span>
                            </h5> <small class="text-muted"><i class="fa fa-calendar"></i> Dec 21st <i
                                    class=" ml-3 fa fa-clock-o"></i> 13.00 <i class=" ml-3 fa fa-map-marker"></i>
                                Brezil</small>
                            <p class="font-13  mb-2 mt-2"> Lorem Ipsum available, quis Neque porro quisquam
                                est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed
                                quia non numquam eius modi tempora incidunt ut labore et nostrud
                                exercitation ullamco laboris commodo consequat. </p><a href="#" class="mr-2"><span
                                    class="badge badge-primary">Helpful</span></a> <a href="" class="mr-2"
                                data-toggle="modal" data-target="#Comment"><span>Comment</span></a> <a href=""
                                class="mr-2" data-toggle="modal" data-target="#report"><span>Report</span></a>
                            <div class="media mt-5">
                                <div class="d-flex mr-3"> <a href="#"> <img class="media-object brround" alt="64x64"
                                            src="../assets/images/faces/female/2.jpg"> </a>
                                </div>
                                <div class="media-body">
                                    <h5 class="mt-0 mb-1 font-weight-semibold">Rose Slater <span class="fs-14 ml-0"
                                            data-toggle="tooltip" data-placement="top" title=""
                                            data-original-title="verified"><i
                                                class="fa fa-check-circle-o text-success"></i></span></h5>
                                    <small class="text-muted"><i class="fa fa-calendar"></i> Dec 22st <i
                                            class=" ml-3 fa fa-clock-o"></i> 6.00 <i class=" ml-3 fa fa-map-marker"></i>
                                        Brezil</small>
                                    <p class="font-13  mb-2 mt-2"> Lorem Ipsum available, quis nostrud
                                        exercitation ullamco laboris commodo Lorem Ipsum available, but the
                                        majority have suffered alteration in some form laboriosam, nisi ut
                                        aliquid ex ea commodi consequatur consequat. </p><a href="" data-toggle="modal"
                                        data-target="#Comment"><span class="badge badge-default">Comment</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="media p-5 border-top mt-0">
                        <div class="d-flex mr-3"> <a href="#"> <img class="media-object brround" alt="64x64"
                                    src="../assets/images/faces/male/3.jpg"> </a> </div>
                        <div class="media-body">
                            <h5 class="mt-0 mb-1 font-weight-semibold">Edward <span class="fs-14 ml-0"
                                    data-toggle="tooltip" data-placement="top" title=""
                                    data-original-title="verified"><i
                                        class="fa fa-check-circle-o text-success"></i></span> <span class="fs-14 ml-2">
                                    4 <i class="fa fa-star text-yellow"></i></span>
                            </h5> <small class="text-muted"><i class="fa fa-calendar"></i> Dec 21st <i
                                    class=" ml-3 fa fa-clock-o"></i> 16.35 <i class=" ml-3 fa fa-map-marker"></i>
                                UK</small>
                            <p class="font-13  mb-2 mt-2"> Lorem Ipsum available, quis Neque porro quisquam
                                est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed
                                quia non numquam eius modi tempora incidunt ut labore et nostrud
                                exercitation ullamco laboris commodo consequat. </p><a href="#" class="mr-2"><span
                                    class="badge badge-primary">Helpful</span></a> <a href="" class="mr-2"
                                data-toggle="modal" data-target="#Comment"><span>Comment</span></a> <a href=""
                                class="mr-2" data-toggle="modal" data-target="#report"><span>Report</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Comments-->
            <div class="card mb-lg-0">
                <div class="card-header">
                    <h3 class="card-title">Leave a reply</h3>
                </div>
                <div class="card-body">
                    <div>
                        <div class="form-group"> <input type="text" class="form-control" id="name1"
                                placeholder="Your Name"> </div>
                        <div class="form-group"> <input type="email" class="form-control" id="email"
                                placeholder="Email Address"> </div>
                        <div class="form-group"> <textarea class="form-control" name="example-textarea-input" rows="6"
                                placeholder="Comment"></textarea>
                        </div> <a href="#" class="btn btn-primary">Send Reply</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<!--/Add listing-->
@endsection
