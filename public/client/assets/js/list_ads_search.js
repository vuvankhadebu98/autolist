var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
    // Xử lí nút "Chọn kiểu hiển thị sách quảng cáo"
    $("#danh_sach_ngang").click(function(){ 
        $("input#type_show").val(0);
    })
    $("#danh_sach_doc").click(function(){
        $("input#type_show").val(1);
    })
	// Xử lí xắp xếp quảng cáo theo tiền, lượt quan tâm và sao
	$("label.selectgroup-item").click(function(e){
        e.preventDefault();
		var type_order_by = $(this).attr('id').substring(9,10);
        $("input#order_by").val(type_order_by); 
        var url_order_by = $("input#url_order_by").val();
        var id_brand = $("input#val_search_brand").val();
        var seats = $("input#val_search_seats").val();
        var price = $("input#val_search_price").val();
        var city = $("input#val_search_city").val();
        var order_by = $("input#order_by").val(); 
        var type_show =  $("input#type_show").val();
            $.ajax({
                url: url_order_by,
                data:{
                    _token:CSRF_TOKEN,
                    id_brand:id_brand,
                    seats:seats,
                    price:price,
                    city:city,
                    order_by:order_by,
                    type_show:type_show
                },
                type: 'get',
                success: function (data) {
                    $("div#search_list_poster").html(data);              
                }
            })
        
	});
})
