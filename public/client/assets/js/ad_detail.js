var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
var url = $('input#route').val();
var id_poster = $('input#id_poster').attr('value');
var id_user = $('input#id_user').attr('value');
$(document).ready(function () {
    // $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    //     });
    //     $.ajax({
    //         url: url,
    //         type: "POST",
    //         cache: false,
    //         data: {
    //             _token: CSRF_TOKEN,
    //             id_poster: id_poster,
    //             id_user: id_user
    //         },
    //         success: function (data) {
    //             $("span#qty_like").text(data);
    //         },
    //         error: function (error) {
    //             alert("Thêm thất bại");
    //         }
    //     });
    
    // Thích quảng cáo
    $("a#a_like").click(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: "POST",
            cache: false,
            data: {
                _token: CSRF_TOKEN,
                id_poster: id_poster,
                id_user: id_user
            },
            success: function (data) {
                $("span#qty_like").text(data);
            },
            error: function (error) {
                alert("Thêm thất bại");
            }
        });
    })
    // ---------- end thích quảng cáo
    // Hover rate
  //   if( $("div#rate").hasClass('rate') != true ){
  //   	$("i.fa-star-rate").hover(function(){
	 //    	// Khi hover vào
		//     $("i.fa-star-rate").css('color','#cac5c5');
	 //    	var id = $(this).attr('id');
	 //    	id = id.substring(13, 14)
	 //    	for(var i = 0; i<= id;i++){
	 //    		$("i#fa-star-rate_"+i).css('color','#ffa22b');
	 //    	}
		// }, function(){
		// 	// Khi hover ra chỗ khác
		// 	if( $("div#rate").hasClass('rate') != true ){
		//     	$("i.fa-star-rate").css('color','#cac5c5');
		//     }
		// });
  //   }

    // Rate
	$("i.fa-star-rate").click(function(){
        $("i.fa-star-rate").css("color",'#cac5c5');
    	var id = $(this).attr('id');
    	id = id.substring(13, 14);
        $("#__sum_rate").attr("value",id);
    	for(var i = 0; i<= id;i++){
    		$("i#fa-star-rate_"+i).css('color','ffa22b');
    		$("div#rate").addClass('rate');
    	}
	});
    $("i#fa-star-rate_0").click(function(){
        $("i.fa-star-rate").css("color",'#cac5c5');
        var id = $(this).attr('id');
        id = id.substring(13, 14);
        $("#__sum_rate").attr("value",id);
    })
    // $('form#rate').submit(function (e) {
    //         var rate = $("#__sum_rate").val();
    //         var url_rate = $("#url_rate").val();
    //         e.preventDefault();
    //         $.ajaxSetup({
    //             headers: {
    //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //             }
    //         });
    //         $.ajax({
    //             url: url_rate,
    //             type: 'POST',
    //             data: {
    //                 _token: CSRF_TOKEN,
    //                 rate:rate,
    //                 id_user:id_user,
    //                 id_poster:id_poster
    //             },
    //             success: function (data) {
    //                 $("div#alert_rate").empty();
    //                 $("div#alert_rate").append("<p>Cảm ơn bạn đã đánh giá sản phẩm</p>");             
    //             }
    //         })
    //     }) 
    // ---------- end rate

    $("a#contact-me").click(function () {
        $("form#form-send").css('display', 'block');
        $("div#background-gray").css('display', 'block');
    })

    $("p#label-cancel").click(function () {
        $("form#form-send").css('display', 'none');
        $("div#background-gray").css('display', 'none');
        $("form#form-report").css('display', 'none');
    })

    $("div#background-gray").click(function () {
        $("form#form-send").css('display', 'none');
        $("div#background-gray").css('display', 'none');
        $("form#form-report").css('display', 'none');
    })

    $("a#report-abuse").click(function () {
        $("form#form-report").css('display', 'block');
        $("div#background-gray").css('display', 'block');
    })

    // Lấy thông tin người rep và hiển thị ô để comment
    $("a.reply").click(function(){
        $("form.reply_comment").css("display","none");
        $("div.img_user").empty();
        $("span.div_2").empty();
        var comment_id = $(this).attr('id');
        var url_get_info = $("#url_get_info").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url_get_info,
            type: 'POST',
            data: {
                _token: CSRF_TOKEN,
                id_user:id_user
            },
            success: function (data) {          
                var string_img = `<a href="#">`
                        if(data['img']!= null){
                            string_img += `<img class="media-object brround" alt="64x64" src="../assets/images/faces/male/3.jpg">`;
                        }else{
                            string_img += `<img class="media-object brround" alt="64x64" src="../../public/client/user.png">`;
                        }                       
                        string_img +=   `</a>`
                $("div.img_user").append(string_img); 

                var string_info = `<h5 class="mt-0 mb-1 font-weight-semibold"> ${data['name']}
                            <span class="fs-14 ml-0" data-toggle="tooltip" data-placement="top" title="" data-original-title="verified">
                                <i class="fa fa-check-circle-o text-success"></i>
                            </span>
                        </h5> `;
                $("span.div_2").append(string_info);
                $("form#reply_comment_"+comment_id).css('display',"inline-block");
                        
            }
        })
        // Trả lời bình luận
        $('form#reply_comment_'+comment_id).submit(function (e) {
            var url_rep_comment = $("#url_rep_comment").val();
            var reply_comment = $('textarea#rep_comment_'+comment_id).val();
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url_rep_comment,
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    comment_id:comment_id,
                    rep_comment:reply_comment,   
                    id_poster:id_poster,
                    id_user:id_user
                },
                success: function (data) {
                    $("p.content_rep").empty();
                    var string_content = 
                    `<p class="font-13"> ${data['content']}</p>
                    <a class="reply mr-2" id="${data['id']}">
                        <span class="badge badge-primary">Reply</span>
                    </a>`;
                    $("p.content_rep").append(string_content);               
                },
                error: function (error) {
                    if (error['responseJSON']['errors']['rep_comment'] != null) {
                        $("#error_rep_comment_"+comment_id).text(error['responseJSON']['errors']['rep_comment']);
                        $('html, body').animate({
                            scrollTop: $("h5.font-weight-semibold").offset().top
                        }, '500');
                    }
                }
            })
        }) 
    })
})