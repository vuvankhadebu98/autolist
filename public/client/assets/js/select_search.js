$(document).ready(function () {
            // form.search_poster_and_product
            $("input#search_name_car").keyup(function () {
                $("input#val_search_name").val($(this).val());
            })
            $("select#search_brand_car").change(function () {
                $("input#val_search_brand").val($(this).val());
            })
            $("select#search_seats_car").change(function () {
                $("input#val_search_seats").val($(this).val());
            })
            $("select#search_price_car").change(function () {
                $("input#val_search_price").val($(this).val());
            })
            $("select#search_city_car").change(function () {
                $("input#val_search_city").val($(this).val());
            })
            // form.search_phu_tung
            $("input#search_name_phu_tung").keyup(function () {
                $("input#val_search_name").val($(this).val());
            })
            $("select#search_brand_phu_tung").change(function () {
                $("input#val_search_brand").val($(this).val());
            })
            $("select#search_price_phu_tung").change(function () {
                $("input#val_search_price").val($(this).val());
            })
            $("select#search_city_phu_tung").change(function () {
                $("input#val_search_city").val($(this).val());
            })
            // Sử lý nút tìm kiếm xe,phụ tùng và dịch vụ
            $("a#search_poster_and_product").click(function(){
                $("input#search_type").val(0);
                $("div.search_poster_and_product").css("display","block");
                $("div.search_phu_tung").css("display","none");
                $("div.search_dich_vu").css("display","none");
            })
            $("a#search_phu_tung").click(function(){
                $("input#search_type").val(1);
                $("div.search_poster_and_product").css("display","none");
                $("div.search_phu_tung").css("display","block");
                $("div.search_dich_vu").css("display","none");
            })
            $("a#search_dich_vu").click(function(){
                $("input#search_type").val(2);
                $("div.search_dich_vu").css("display","block");
                $("div.search_poster_and_product").css("display","none");
                $("div.search_phu_tung").css("display","none");
            })
        })