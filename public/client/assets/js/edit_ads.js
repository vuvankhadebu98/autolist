var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function () {
    //===========================================================
    // B1: LẤY FILE TRONG Ô INPUT FILE LƯU VÀO THƯ MỤC img_poster
    //    -   Khi ô input file thay đổi, ta sẽ dùng ajax để lưu file vào thư mục trước. Nếu muốn xóa một ảnh trong
    //    dãy các ảnh, ta sẽ lấy tên ảnh và dùng ajax tìm kiếm trong thư mục và xóa
    //===========================================================
        /*
        B1_1 : Tạo mảng img_list
        B2_2 : Khi người dùng thay đổi giá trị ô input, ta sẽ lấy chiều dài file trong thẻ input[files]
        B3_3 : Tạo một vòng lặp sau đó tạo một mảng form_data, lưu file đó vào mảng vào dùng ajax gửi vào controller để lưu file
        */
    // B1_1
    var img_list = new Array();
    if (window.File && window.FileList && window.FileReader) {
        $("#image_list").on("change", function (e) {
            //B1_2
            var files = e.target.files,
            filesLength = files.length;
            var url = $("input#url_up").val();
                for(var i =0;i<filesLength;i++){
                    //Lấy ra files
                    var f = files[i];
                        //khởi tạo đối tượng form data
                        var form_data = new FormData();
                        //thêm files vào trong form data
                        form_data.append('files', f);

                        //B1_3
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: url,
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (data) {}
                        });
                }      
    //===========================================================
    // B2: HIỂN THỊ ẢNH RA MÀN HÌNH
    //===========================================================      
            /*
            B2_1 : Lấy số lượng ảnh ở thẻ input[hidden--#qty_img_detail--value=0]
            B2_2 : Dùng vòng lặp for lần lượt đọc dữ liệu file bằng sự kiện readAsDataURL()
            B2_3 : Lưu file vào trong mảng img_list
            B2_4 : Khi hiển thị ảnh ra client tạo một thẻ    
                $("div#" + id_div + "").append("<input type='hidden' class='_img_detail' id='img_" + id_div + "' name='img_" + id_div + "' value='" + img_list[id_div]['name'] + "'>");
                trong đó : 
                    + id_div = i + qty_img
                    + img_list[id_div]['name'] : Lấy tên của file đã lưu vào trong mảng
            B2_5 : Tăng qty_img lên 1
            */
            // B2_1
            var qty_img = $("input#qty_img_detail").val();
            //  B2_2
            for (let i = 0; i < filesLength; i++) {
                // B2_3
                var f = files[i];
                img_list.push(f);
                // B2_4
                var fileReader = new FileReader();
                fileReader.onload = (function (e) {
                    var file = e.target;
                    $(` <div id="${i + Number(qty_img)}" class='list_detail_photo col-md-2'>
                            <input type="text" class="custom-file-input" id="${i + Number(qty_img)}" name="files_compare[]" value="${e.target.result}" readonly style="display: none;"/>
                            <span class="pip">
                                <img class="imageThumb" style="width:100px ; height:100px" src="${e.target.result}" title="file.name"/>
                                <br/>
                                <span class="remove" id="${i + Number(qty_img)}" onclick="document.getElementById('${i + Number(qty_img)}').value = '' " >Xóa</span>
                            </span>
                        </div>`).insertAfter("#image_list");
                    var id_div = Number(i) + Number(qty_img);
                    $("div#" + id_div + "").append("<input type='hidden' class='_img_detail' id='img_" + id_div + "' name='img_" + id_div + "' value='" + img_list[id_div]['name'] + "'>");
                    //===========================================================
                    // B3: XÓA ẢNH
                    //===========================================================   
                        /* 
                        B3_1 : Khi ấn vào nút Xóa sẽ lấy được id của ô input lưu tên của ảnh đó. Khi đó ta sẽ lấy value của ô input
                            và đó cũng chính là tên ảnh cần xóa
                        B3_2 : Ajax để vào thư mục tìm ảnh trùng tên và xóa
                        */
                        $(".remove").click(function () {
                            // B3_1
                            var url_del = $("input#url_del").val();
                            var id = $(this).attr('id');
                            var name_img_del = $("input#img_"+id+"").val(); 
                            // B3_2                         
                            $.ajaxSetup({
                                headers : {
                                    'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            $.ajax({
                                url : url_del,
                                type : "POST",
                                cache: false,
                                data : {_token:CSRF_TOKEN,name_img_del:name_img_del},
                                success:function(data){ 
                                },error:function(error){
                                    alert("Thêm thất bại");
                                }
                            });
                            $("div#" + id).remove();
                        });       
                    // B2_5
                    $("input#qty_img_detail").val(Number(qty_img) + Number(filesLength));
                });
                fileReader.readAsDataURL(f);
            }
        });
    }
    $(".remove").click(function () {
        var url_del = $("input#url_del").val();
        var id = $(this).attr('id');
        var name_img_del = $("input#img_"+id+"").val(); 
                       
        $.ajaxSetup({
            headers : {
                'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
        }
        });
        $.ajax({
            url : url_del,
            type : "POST",
            cache: false,
            data : {_token:CSRF_TOKEN,name_img_del:name_img_del},
            success:function(data){ 
            },error:function(error){
                alert("Thêm thất bại");
            }
        });
        $("div#" + id).remove();
    });       

});

// THUẬT TOÁN
// Khi người dùng thêm ảnh thì tạo ra một khối chứa ảnh cùng với 1 thẻ input hidden lấy tên ảnh cùng id vs khối chứa. sau đó lưu ảnh luôn vào thư mục
// Khi người dùng ấn xóa thì lấy id khối chứa ảnh cũng là id của ô input chứa tên ảnh sau đó dùng ajax để truyền tên ảnh vào controller để tìm và xóa

    