

<?php
/*
|--------------------------------------------------------------------------*+

| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// nam
Route::get('/facebook/redirect', 'SocialAuthController@redirect')->name('redirect');
Route::get('/facebook/callback', 'SocialAuthController@callback');
Route::group(['namespace'=>'client'],function(){
    // Home
    Route::group(['namespace'=>'Home'],function(){
        Route::get('/','HomeController@index')->name('getHome');
        Route::post('tim-kiem-trang-chu','HomeController@search')->name('searchHome');
        Route::post('tim-kiem-trang-chu-2','HomeController@search')->name('searchHome_1');
        Route::post('tim-kiem-trang-chu-3','HomeController@search')->name('searchHome_2');
        Route::get('/about','HomeController@about')->name('getAbout');
        Route::post('search','HomeController@search')->name('searchHome');
        Route::get('list-ads','HomeController@list_ads')->name('list_ads');
        Route::get('order_by','HomeController@order_by')->name('order_by');
        // Route::get('search_poster','HomeController@search_poster')->name('search_poster');
        Route::get('poster_pagination','HomeController@poster_pagination')->name('poster_pagination');

        // chưa làm
        Route::get('filter','HomeController@filter')->name('filter');
    });
    // Advertisement
    Route::group(['namespace'=>'Ads'],function(){
        Route::get('advertisement','AdsController@index')->name('getAdvertisement');
        Route::get('edit-advertisement/{id}','AdsController@edit')->name('getEditAdvertisement');
        Route::post('up-img','AdsController@upImg')->name('upImg');
        Route::post('del-img','AdsController@delImg')->name('delImg');
        Route::post('edit-advertisement','AdsController@postEdit')->name('postEditAdvertisement');
        Route::get('delete-ads/{id}','AdsController@deleteAds')->name('deleteAds');
        Route::get('advertisement-detail/{id}','AdsController@getDetail')->name('getDetail');
        Route::post('like','AdsController@like')->name('like');
        Route::post('send-reply','AdsController@sendReply')->name('sendReply');
        Route::post('list-ads','AdsController@searchPost')->name('searchPost');
        Route::get('them-quang-cao','AdsController@index')->name('addAds');
        Route::post('add-ads','AdsController@postaddAds')->name('postaddAds');
        Route::post('rep_comment','AdsController@rep_comment')->name('rep_comment');
        // Chưa làm
        Route::post('rate','AdsController@rate')->name('rate');
        Route::post('send-mail-contact','MailController@sendContact')->name('sendContact');
    });
    // favorite
    Route::group(['namespace'=>'favorite'],function(){
        Route::get('my_favorite/{id}','FavoriteController@index')->name('getFavorite');
        Route::get('favoritee','FavoriteController@getfavorite')->name('favoritee');
        Route::get('delete-favorite/{id}','FavoriteController@delete')->name('deleteFavorite');
    });
    //service
    Route::group(['namespace'=>'service'],function(){
        Route::get('showroomm','ServiceController@showroomm')->name('getshowroom');
        Route::post('showroomm','ServiceController@postshowroom')->name('posttshowroom');
        Route::get('list-showr','ServiceController@index')->name('listsr');
        Route::get('delete-showr/{id}','ServiceController@delete')->name('delete-showr');
        Route::get('edit-showr/{id}','ServiceController@editservice')->name('edit-showr');
        Route::post('update-showr/{id}','ServiceController@posteditservice');
    });
    //Registerkj
    Route::group(['namespace'=>'Register'],function(){
        Route::get('register','RegisterController@index')->name('getRegister');
        Route::post('register','RegisterController@register')->name('postRegister');
        Route::get('confirm-email/{token}/{id}','RegisterController@confirm_email')->name('getConfirm_email');
    });
    // Login
    Route::group(['namespace'=>'Login'],function(){
        Route::get('login','LoginController@index')->name('getLogin');
        Route::post('login','LoginController@postLogin')->name('postLogin');
        Route::get('forgot-password','LoginController@forgotPassword')->name('forgotPassword');
        Route::post('forgot-password','LoginController@postforgotPassword')->name('postforgotPassword');
        Route::get('edit-password','LoginController@erroredit_password')->name('erroredit_password');
        Route::get('logout','LoginController@logout')->name('logout');
        Route::post('edit-password','LoginController@edit_password')->name('edit_password');
        Route::post('edit-password-data','LoginController@postedit_password')->name('postedit_password');
    });
    // My Dashboard
    Route::group(['namespace'=>'Dashboard'],function(){
        // Profile
        Route::get('my-profile/{id}','ProfileController@index')->name('getProfile');
        Route::post('my-profile','ProfileController@update')->name('updateProfile');
        // My ads
        Route::get('my-ads/{id}','MyAdsController@index')->name('getMyAds');
        Route::get('get_poster_info_by_click','MyAdsController@get_poster_info_by_click')->name('get_poster_info_by_click');
        Route::get('get_poster_info_pagination','MyAdsController@get_poster_info_pagination')->name('get_poster_info_pagination');
    });
    // Personal
    Route::group(['namespace'=>'Personal'],function(){
        Route::get('personal-blog/{id}','PersonalController@index')->name('getPersonal');
        Route::post('check-info','PersonalController@ b')->name('checkInfo');
        Route::post('AjaxGetInfo','PersonalController@AjaxGetInfo')->name('AjaxGetInfo');
    });
    //Blog
    Route::group(['namespace'=>'Blog'],function(){
        Route::get('list-blog','BlogController@listBlog')->name('listBlog');
        Route::get('list-blog-id/{id}','BlogController@listBlogID')->name('listBlogID');
        Route::get('detail-blog/{id}','BlogController@detailBlog')->name('detailBlog');
        // Route::get('comment-blog/{id}','BlogController@commentBlog')->name('commentBlog');
        Route::get('tag-blog','BlogController@tagBlog')->name('tagBlog');
        Route::get('tag-blog/{id}','BlogController@tagBlogID')->name('tagBlogID');
        Route::post('comment/{id}', '

        @postComment')->name('postComment');
    });
    //Contact
    Route::group(['namespace'=>'Contact'],function(){
        Route::get('contact','ContactController@contact')->name('contact');
        Route::get('save-contact','ContactController@save_contact');      
    });
    //  Route::group(['namespace'=>'Comment'],function(){
    //    Route::post('/comment/{id}', 'CommentController@postComment')->name('postComment');

    // });
    //Accessary
    Route::group(['namespace'=>'Accessary'],function(){
        Route::get('chi-tiet-phu-tung/{id}','AccessaryController@ChiTietPhuTung')->name('ChiTietPhuTung');
        Route::get('index','AccessaryController@getindex')->name('index');

    });
    //Orders
    Route::group(['namespace'=>'Orders'],function(){
        Route::get('don-hang','OrdersController@index')->name('getOrders');
    });
    // làm chi tiết quảng cáo..................................................................
    Route::group(['namespace'=>'Detail'],function(){
        Route::get('show-detail-poster/{id}','DetailController@show_detail_poster');
    });
});


// Của ai thì ghi tên xong viết riêng nhá.

//Luyen
Route::group(['namespace' => 'client'], function(){
    Route::group(['namespace' => 'Detail'],function(){
        Route::get('Car-Detail-01/{id}','DetailController@index')
                ->name('Car-Detail-01');
        Route::post('comment','DetailController@comment')->name('postComment');
        Route::post('Repcomment','DetailController@Repcomment')->name('RepComment');
        Route::get('like-product/{id}','DetailController@like_product')
                ->name('like-product');
        Route::get('unlike-product/{id}','DetailController@unlike_product')
                ->name('unlike-product');
    });

    Route::group(['namespace' => 'Detail'],function(){
        Route::get('Car_List','ListController@ListCar')->name('Car_List');
        // danh mục sản phẩm
        Route::get('show-category/{id}','ListController@show_category');

        Route::get('ajax', 'ListController@ajaxPriceDecrease')->name('ajax');
        Route::get('desc', 'ListController@PriceDecrease')->name('PriceDesc');
        Route::get('Ajax', 'ListController@ajaxPriceAscending')->name('Ajax');
        Route::post('search','ListController@search')->name('search');
        Route::get('filter','ListController@filter')->name('filter');
    });
});








// --------------------------------------------------------------------------------------------------------------
// Route Admin
Route::group(['namespace' => 'admin', 'prefix' => 'admin'], function () {
    //Route Home Admin
    Route::group(['namespace' => 'home'], function () {
        Route::get('/user-index', 'HomeController@index')->name('admin.home')->middleware('adminLogin');
    });
});

// Route Admin
Route::group(['namespace' => 'admin', 'prefix' => 'admin'], function () {
    //Route Home Admin
    Route::group(['namespace' => 'home'], function () {
        Route::get('/user-index', 'HomeController@index')->name('admin.home')->middleware('adminLogin');
    });
});

//Admin.............................................................................................

//------------------------------------------------------------------------------------------------------------------
//.......................................................Admin.......................................................

// Của ai thì ghi tên xong viết riêng nhá.

Route::group(['namespace' => 'admin'], function () {
     //Route Sản phẩm và thuộc tính sản phẩm
    Route::group(['namespace' => 'product','prefix' => 'product'], function () {

        //Danh sách sản phẩm
        Route::get('danh-sach-san-pham','ProductController@get_list_product')->name('get-list-pro');

        //load trang ajax
        Route::get('load-listpro','ProductController@get_load_list_product')->name('change.load.listpro');
        Route::get('load-listactive','ProductController@get_list_active_product')->name('load.listactive');
        Route::get('load-list-cate-product','ProductController@load_list_cate_product')->name('change.cate.listpro');
        Route::get('load-list-brand-product','ProductController@load_list_brand_product')->name('change.brand.listpro');

        //Thêm sản phẩm
        Route::get('them-san-pham','ProductController@get_create_product')->name('get-add-pro');
        Route::post('post-product','ProductController@post_create_product')->name('post-add-pro');
         //Cập nhật sản phẩm
        Route::get('cap-nhat-san-pham/{id}','ProductController@get_edit_product')->name('get-edit');
        Route::post('update-produtc/{id}','ProductController@post_edit_product')->name('post-edit');
        Route::get('xoa-san-pham/{id}','ProductController@delete_product')->name('delete-product');
                //Trạng thái
        Route::get('active-product/{id}','ProductController@active_pro_status')->name('active-pro');

        Route::get('unactive-product/{id}','ProductController@unactive_pro_status')->name('unactive-pro');

            //Tìm kiếm
        Route::post('tim-kiem-san-pham','ProductController@search_product')->name('search.product');

        //Thêm thuộc tính sản phẩm
        Route::get('them-thuoc-tinh','PropertiesController@get_create_properties')->name('get-add-properties');
        Route::post('post-properties','PropertiesController@post_create_properties')->name('post-add-properties');
        //Danh sách thuộc tính
        Route::get('danh-sach-thuoc-tinh','PropertiesController@get_list')->name('get-list-properties');
        //Sửa thuộc tính
        Route::get('sua-thuoc-tinh/{id}','PropertiesController@get_edit')->name('get-edit-property');
        Route::post('update-property/{id}','PropertiesController@post_edit')->name('post-edit-property');
        //Xóa thuộc tính
        Route::get('xoa-thuoc-tinh/{id}','PropertiesController@delete_property')->name('delete-property');
    });//Kết thúc route sản phẩm

    //Route phần đơn hàng
    Route::group(['namespace' => 'order','prefix' => 'order'], function () {
        Route::get('danh-sach-don-hang','OrderController@get_view_order')->name('get.view.order');
        //ajax
        Route::get('load-list-order','OrderController@get_load_list_order')->name('change.load.list.order');
        Route::get('load-listactive-order','OrderController@load_list_active_order')->name('load.listactive.order');
        Route::get('load-import-order','OrderController@load_list_import_order')->name('load.import.order');
        Route::get('load-sale-order','OrderController@load_list_sale_order')->name('load.sale.order');
        //Trạng thái
        Route::get('active-order/{id}','OrderController@active_order_status')->name('active-order');

        Route::post('search-order','OrderController@search_order')->name('search.order');
    });
    //Kết thúc route đơn hàng

    // Admin/blog....................................................................................
    Route::group(['namespace' => 'blog'], function () {
        // tìm kiếm danh mục blog theo ajax ............................
        Route::get('load-list-cate-blog','BlogController@load_list_cate_blog')->name('change.cate.blog');
        // thêm,sửa,xóa,hiển thị blog...........................................
        Route::get('add-blog','BlogController@add_blog');
        Route::get('edit-blog/{id}','BlogController@edit_blog');
        Route::get('delete-blog/{id}','BlogController@delete_blog');
        Route::get('all-blog','BlogController@all_blog');
        // lưu, cập nhật blog.........
        Route::post('save-blog','BlogController@save_blog');
        Route::post('update-blog/{id}','BlogController@update_blog');
        //hiển thị, không hiển thị blog.........
        Route::get('active-blog/{id}','BlogController@active_blog');
        Route::get('unactive-blog/{id}','BlogController@unactive_blog');
        // chức năng tìm kiếm....................
        Route::post('search-blog','BlogController@search_blog')->name('search-blog');
    });

    // Admin/category_blog.............................................................
    Route::group(['namespace' => 'category_blog'], function () {

        // thêm,sửa,xóa,hiển thị category_blog...........................................
        Route::get('add-category-blog','CategoryBlogController@add_category_blog');
        Route::get('edit-category-blog/{id}','CategoryBlogController@edit_category_blog');
        Route::get('delete-category-blog/{id}','CategoryBlogController@delete_category_blog');
        Route::get('all-category-blog','CategoryBlogController@all_category_blog');
        // lưu, cập nhật category_blog

        Route::post('save-category-blog','CategoryBlogController@save_category_blog');
        Route::post('update-category-blog/{id}','CategoryBlogController@update_category_blog');
        //hiển thị, không hiển thị category_blog.
        Route::get('active-category-blog/{id}','CategoryBlogController@active_category_blog');
        Route::get('unactive-category-blog/{id}','CategoryBlogController@unactive_category_blog');
        // tìm kiếm category_blog...........................................
        Route::post('search-category-blog','CategoryBlogController@search_category_blog')->name('search-category-blog');
    });

    // Admin/tag.............................................................................................
    Route::group(['namespace' => 'tag'], function () {
        // tìm kiếm theo tag của các loại bằng ajax
        Route::get('load-product','TagController@select_active_product')->name('load.product');
        Route::get('load-blog','TagController@select_active_blog')->name('load.blog');
        Route::get('load-poster','TagController@select_active_poster')->name('load.poster');
        Route::get('load-accessaries','TagController@select_active_accessaries')->name('load.accessaries');

        // thêm,sửa,xóa,hiển thị tag...........................................
        Route::get('add-tag','TagController@add_tag');
        Route::get('edit-tag/{id}','TagController@edit_tag');
        Route::get('delete-tag/{id}','TagController@delete_tag');
        Route::get('all-tag','TagController@all_tag');
        // lưu, cập nhật tag
        Route::post('save-tag','TagController@save_tag');
        Route::post('update-tag/{id}','TagController@update_tag');
        //hiển thị, không hiển thị tag.
        Route::get('active-tag/{id}','TagController@active_tag');

        Route::get('unactive-tag/{id}','TagController@unactive_tag');
        // tìm kiếm tag....................................................
        Route::post('search-tag','TagController@search_tag')->name('search-tag');
    });

    // Admin/brand.............................................................................................
    Route::group(['namespace' => 'brand'], function () {
        // thêm,sửa,xóa,hiển thị brand...........................................
        Route::get('add-brand','BrandController@add_brand');
        Route::get('edit-brand/{id}','BrandController@edit_brand');
        Route::get('delete-brand/{id}','BrandController@delete_brand');
        Route::get('all-brand','BrandController@all_brand');
        // lưu, cập nhật brand
        Route::post('save-brand','BrandController@save_brand');
        Route::post('update-brand/{id}','BrandController@update_brand');
        //hiển thị, không hiển thị brand.
        Route::get('active-brand/{id}','BrandController@active_brand');
        Route::get('unactive-brand/{id}','BrandController@unactive_brand');
        // tìm kiếm brand....................................................
        Route::post('search-brand','BrandController@search_brand')->name('search-brand');
    });

    // Admin/category.............................................................................................
    Route::group(['namespace' => 'category'], function () {
        // tìm kiếm danh mục theo ajax...........................................
        Route::get('load-active','CategoryController@select_active_category')->name('load.active');
        Route::get('load-active-1','CategoryController@select_active_category_1')->name('load.active.1');
        // thêm,sửa,xóa,hiển thị category...........................................
        Route::get('add-category','CategoryController@add_category');
        Route::get('edit-category/{id}','CategoryController@edit_category');
        Route::get('delete-category/{id}','CategoryController@delete_category');
        Route::get('all-category','CategoryController@all_category');
        // lưu, cập nhật category
        Route::post('save-category','CategoryController@save_category');
        Route::post('update-category/{id}','CategoryController@update_category');
        //hiển thị, không hiển thị category.
        Route::get('active-category/{id}','CategoryController@active_category');
        Route::get('unactive-category/{id}','CategoryController@unactive_category');
        // tìm kiếm category....................................................
        Route::post('search-category','CategoryController@search_category')->name('search-category');
    });
    //Admim/contact
    Route::group(['namespace'=>'contact'],function(){
        // tất cả các liên hệ
        Route::get('load-contact','ContactController@select_load_contact')->name('load.contact');
        //  tìm kiếm theo tag của các loại bằng ajax
        Route::get('load-contact-active','ContactController@select_active_contact')->name('load.contact.active');

        Route::get('load-contact-unactive','ContactController@select_unactive_contact')->name('load.contact.unactive');
        // list-contact
        Route::get('list-contact','ContactController@list_Contact');
        Route::get('delete-contact/{id}','ContactController@delete_contact');
        //hiển thị, không hiển thị contact.
        Route::get('active-contact/{id}','ContactController@active_contact');
        Route::get('unactive-contact/{id}','ContactController@unactive_contact');
        // tìm kiếm theo theo liên hệ
        Route::post('search-contact','ContactController@search_contact')->name('search-contact');
    });
    Route::group(['namespace'=>'inbox'],function(){
        Route::get('inbox','InboxController@getInbox')->name('getInbox');
    });
    // tìm kiếm tag....................................................
    Route::post('search-tag','TagController@search_tag')->name('search-tag');
    // Admin/poster.............................................................................................

    Route::group(['namespace' => 'poster'], function () {
        // tìm kiếm danh mục và thương hiệu sản phẩm theo ajax ............................
        Route::get('load-list-cate-poster','PosterController@load_list_cate_poster')->name('change.cate.poster');
        Route::get('load-list-brand-poster','PosterController@load_list_brand_poster')->name('change.brand.poster');
        // thêm,sửa,xóa,hiển thị poster...........................................
        Route::get('add-poster','PosterController@add_poster');
        Route::get('edit-poster/{id}','PosterController@edit_poster');
        Route::get('delete-poster/{id}','PosterController@delete_poster');
        Route::get('all-poster','PosterController@all_poster');
        // lưu, cập nhật poster
        Route::post('save-poster','PosterController@save_poster');
        Route::post('update-poster/{id}','PosterController@update_poster');
        //hiển thị, không hiển thị poster.
        Route::get('active-poster/{id}','PosterController@active_poster');
        Route::get('unactive-poster/{id}','PosterController@unactive_poster');
        // tìm kiếm poster....................................................
        Route::post('search-poster','PosterController@search_poster')->name('search-poster');
    });

    // Admin/information.............................................................................................
    Route::group(['namespace' => 'information'], function () {
        // thêm,sửa,xóa,hiển thị information...........................................
        Route::get('add-information','InformationController@add_information');
        Route::get('edit-information/{id}','InformationController@edit_information');
        Route::get('delete-information/{id}','InformationController@delete_information');
        Route::get('all-information','InformationController@all_information');
        // lưu, cập nhật information
        Route::post('save-information','InformationController@save_information');
        Route::post('update-information/{id}','InformationController@update_information');
        //hiển thị, không hiển thị information.
        Route::get('active-information/{id}','InformationController@active_information');
        Route::get('unactive-information/{id}','InformationController@unactive_information');
        // tìm kiếm information....................................................
        Route::post('search-information','InformationController@search_information')->name('search-information');
    });
    // Admin/accessaries.............................................................................................  
    Route::group(['namespace' => 'accessary'], function () {
        // lọc theo phụ tùng bán bằng ajax......................................
        Route::get('load-accessary-0','AccessaryController@select_accessary_0')->name('load.accessary.0');
        Route::get('load-accessary-1','AccessaryController@select_accessary_1')->name('load.accessary.1');

        // thêm,sửa,xóa,hiển thị information...........................................
        Route::get('add-accessary','AccessaryController@add_accessary');
        Route::get('edit-accessary/{id}','AccessaryController@edit_accessary');
        Route::get('delete-accessary/{id}','AccessaryController@delete_accessary');
        Route::get('all-accessary','AccessaryController@all_accessary');
        // lưu, cập nhật accessary
        Route::post('save-accessary','AccessaryController@save_accessary');
        Route::post('update-accessary/{id}','AccessaryController@update_accessary');
        //hiển thị, không hiển thị accessary.
        Route::get('active-accessary/{id}','AccessaryController@active_accessary');
        Route::get('unactive-accessary/{id}','AccessaryController@unactive_accessary'); 
        // tìm kiếm accessary....................................................
        Route::post('search-accessary','AccessaryController@search_accessary')->name('search-accessary');
    });
});

// Route Admin
Route::group(['namespace' => 'admin', 'prefix' => 'admin'], function () {

    //Route Role Admin
    Route::group(['namespace' => 'role'], function () {
        Route::resource('role', 'RoleController');
    });

    //Route login admin
    Route::group(['namespace' => 'user','prefix' => 'users'], function () {
        Route::get('login', 'UserController@login')->name('admin.user.login');
        Route::get('logout', 'UserController@logOut')->name('admin.user.logout');
    });
    //Route Addmin
    Route::group(['namespace' => 'user'], function () {
        Route::resource('user', 'UserController');
        Route::group(['prefix' => 'users'], function () {
            Route::get('add-admin', 'UserController@add_admin')->name('admin.user.add_admin');
            Route::get('lock-admin', 'UserController@lock_Admin')->name('admin.user.lock');
            Route::get('unlock-admin', 'UserController@unlock_Admin')->name('admin.user.unlock');
            Route::get('edit-admin/{id}', 'UserController@get_edit_admin')->name('admin.user.get_edit_admin');
            Route::post('edit-pass-admin', 'UserController@edit_pass')->name('admin.user.edit_pass');
            Route::post('update-infomation-admin', 'UserController@update_user')->name('admin.user.update_user');
            Route::post('search-user', 'UserController@search_user')->name('search-user');
        });
    });
    //Route  User Admin
    Route::group(['namespace' => 'user', 'prefix' => 'admin'], function () {

       Route::resource('user', 'UserController');
        Route::group(['prefix' => 'users'], function () {
            Route::get('login', 'UserController@login')->name('admin.user.login');
            Route::get('logout', 'UserController@logOut')->name('admin.user.logout');
            Route::get('add-admin', 'UserController@add_admin')->name('admin.user.add_admin');
            Route::get('lock-admin', 'UserController@lock_Admin')->name('admin.user.lock');
            Route::get('unlock-admin', 'UserController@unlock_Admin')->name('admin.user.unlock');
            Route::get('edit-admin/{id}', 'UserController@get_edit_admin')->name('admin.user.get_edit_admin');
            Route::post('edit-infomation-admin', 'UserController@thong_tin')->name('admin.user.thong_tin');
            Route::post('update-infomation-admin', 'UserController@update_user')->name('admin.user.update_user');
        });
    });
});
