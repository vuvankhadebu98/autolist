<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        // Check: Kiểm tra
        // -   0:quảng cáo phụ tùng hay 
        // -   1:đăng bán phụ tùng
        // status: Kiểm tra xem quảng cáo đã đc duyệt chưa, và ở trạng thái đang bán hay đã bán
        Schema::create('accessaries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id'); // Người tạo 
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->nullable()
                ->onDelete('cascade');
            $table->integer('code');
            $table->string('name');
            $table->string('img')->nullable();
            $table->integer('price');
            $table->integer('rate')->default(0);
            $table->integer('view')->default(0);
            $table->text('description'); //mô tả phụ tùng......
            $table->text('content');//nội dung ............... thêm mới
            $table->integer('status')->default(0);
            $table->integer('check')->default(0); //check xem quảng cáo phụ tùng,sản phẩm bán.
            $table->integer('package')->nullable();//gói quảng cáo
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accessaries');
    }
}
