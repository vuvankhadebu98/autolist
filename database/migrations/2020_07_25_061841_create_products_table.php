<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('category_id'); //Là phần kiểu dáng trong view
            $table->integer('brand_id');
            $table->string('price');
            $table->integer('promotion')->default(0);
            $table->integer('seats');
            $table->string('image_product');
            $table->integer('like')->default(0);
            $table->integer('rate')->default(0);
            $table->integer('view')->default(0);
            $table->integer('buy_count')->default(0);
            $table->string('video')->default('https://www.youtube.com/embed/yCes3BRbGjY');
            $table->integer('user_id');
            $table->longText('description'); //mô tả
            $table->text('content');//nội dung !!!!!thêm mới........................
            $table->string('engine');
            $table->string('enegine_displacement');
            $table->string('fuel_type');
            $table->integer('fuel_tank_capacity');
            $table->integer('status')->default(0);
            $table->string('break');
            $table->string('bootspace');
            $table->string('arai_milage');
            $table->string('max_torque');
            $table->string('max_power');
            $table->integer('transmission_type');
            $table->string('tags')->nullable();
            $table->integer('check_sp')->default(0);//check xem còn hàng hay hêt hàng mới.thêm mới.....................
            $table->integer('check_new')->default(0);//check xem hàng mới hay hàng cũ hay cho thuê. thêm mới.......................
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
