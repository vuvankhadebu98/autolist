<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id'); //Id   
            $table->string('firstname')->nullable(); // Tên 
            $table->string('lastname');// Họ
            $table->string('img')->nullable();// Ảnh
            $table->string('email')->unique(); // Email
            $table->timestamp('email_verified_at')->nullable(); // Mặc định laravel
            $table->string('phone')->nullable(); // Số điện thoại
            $table->string('username')->nullable(); // Tên tài khoản
            $table->string('password'); // Mật khẩu
            $table->integer('sex')->nullable(); //Giới tính 
            $table->string('country')->nullable(); // Đất Nước
            $table->string('city')->nullable(); // Tỉnh
            $table->string('address')->nullable(); // Tỉnh
            $table->string('url_face')->nullable(); // Địa chỉ face
            $table->string('url_google')->nullable(); // Địa chỉ Goolge
            $table->string('url_twitter')->nullable(); // Địa chỉ Twitter
            $table->string('url_print')->nullable(); // ???
            $table->text('detail_aboutme')->nullable();
            $table->integer('role_id'); //Mã chức danh
            $table->string('status')->default('0');
            $table->rememberToken(); // Mã hóa láy lại mật khẩu
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
