<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePoster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poster', function (Blueprint $table) {
            // Check : Kiểm tra xem muốn bán xe hay mua xe --- 0: bán, 1: mua
            // Check_new : Kiểm tra xem xe cũ hay xe đã sử dụng  ---- 0: mới, 1: đã sử dụng
            // Status : Quảng cáo đc duyệt ( 0: Chưa duyệt, 1:đã duyệt , 2: đang bán, 3:đã bán)
            // Package : Gói quảng cáo người dùng chọn 
            //  0:30day free
            //  1:60day: 20$
            //  2:6month: 50$
            //  3:1year: 80$.
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_user'); // Người tạo 
            $table->foreign('id_user')
                ->references('id')
                ->on('users')
                ->nullable()
                ->onDelete('cascade');
            // Thương hiệu
            $table->unsignedBigInteger('brand_id');
            $table->foreign('brand_id')
                ->references('id')
                ->on('brand')
                ->onDelete('cascade')
                ->nullable()
                ->after('id_user');
            //Kiểu dáng trong view
            $table->unsignedBigInteger('category_id'); 
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade')
                ->nullable()
                ->after('brand_id');
            $table->string('img')->nullable(); // Các ảnh chi tiết
            $table->string('title');
            $table->integer('price');
            $table->integer('seats')->nullable(); // ghế
            $table->integer('km_went')->nullable(); // km đã đi
            $table->string('promotion')->default(0);
            $table->integer('fuel_type')->nullable(); // Kiểu nhiên liệu
            $table->integer('transmission_type')->nullable(); // Kiểu hộp số
            $table->text('description')->nullable(); //mô tả
            $table->text('content')->nullable();//thêm mới nội dung.................................
            $table->string('engine')->nullable();// động cơ
            $table->string('enegine_displacement')->nullable();//hệ thống động cơ
            $table->integer('fuel_tank_capacity')->nullable();//dung tích bình xăng
            $table->string('break')->nullable();//phá vỡ
            $table->string('bootspace')->nullable();//không gian khởi động
            $table->string('arai_milage')->nullable();//
            $table->string('max_torque')->nullable();//momen xoắn cực đại
            $table->string('max_power')->nullable();//cộng xuất tối đa
            $table->integer('like')->default(0);//yêu thích
            $table->integer('rate')->default(0);//tỉ lệ
            $table->integer('view')->default(0);//lượt xem
            $table->string('video')->nullable();//video
            $table->integer('package');//chọn gói
            $table->integer('check'); //kiểm tra còn hàng hay hết hàng
            $table->integer('check_new');//kiểm chứng
            $table->integer('status')->default(0);//trạng thái
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poster');
    }
}
