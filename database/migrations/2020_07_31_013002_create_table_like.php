<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableLike extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like', function (Blueprint $table) {
            $table->bigIncrements('id');
            // status : Kiểm tra xem yêu thích gì 
            // 0 : Quảng cáo;
            // 1 : xe
            // 2 : phụ tùng   
            $table->integer('status');
            $table->integer('product_id')->nullable();
            $table->integer('blog_id')->nullable();
            $table->integer('poster_id')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like');
    }
}
