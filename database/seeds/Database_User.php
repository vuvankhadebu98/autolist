<?php

use Illuminate\Database\Seeder;

class Database_User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // 1: Khách hàng
        // 2: Quảng trị viên
        $data = [
        	[
        		'firstname'=>'khách',
        		'lastname'=>'Mua Hàng',
        		'email'=>'quangnamds76@gmail.com',
                'username'=>'khach.hang',
        		'password'=>bcrypt('123456'),
        		'remember_token'=>'$2y$10$SCLeX2KOFbk4gBJLhWEMBu0k3CLVjZ7ZfgLv6ZCFyuBGyayOJ33sq',
                'created_at'=>'2020-07-28 21:09:40',
                'role_id'=>1
            ],
            [
        		'firstname'=>'Quản',
        		'lastname'=>'Trị viên',
                'email'=>'quangnam12d10@gmail.com',
        		'username'=>'quan.tri',
        		'password'=>bcrypt('123456'),
        		'remember_token'=>'$2y$10$SCLeX2KOFbk4gBJLhWEMBu0k3CLVjZ7ZfgLv6ZCFyuBGyayOJ33sq',
                'created_at'=>'2020-07-28 21:09:40',
                'role_id'=>2
            ],
            [
        		'firstname'=>'Cộng',
        		'lastname'=>'Tác viên Sản Phẩm',
        		'email'=>'quangnam12d11@gmail.com',
        		'username'=>'ctv.sanpham',
        		'password'=>bcrypt('123456'),
        		'remember_token'=>'$2y$10$SCLeX2KOFbk4gBJLhWEMBu0k3CLVjZ7ZfgLv6ZCFyuBGyayOJ33sq',
                'created_at'=>'2020-07-28 21:09:40',
                'role_id'=>3
            ],
            [
        		'firstname'=>'Cộng tác viên',
        		'lastname'=>'Bài viết',
        		'email'=>'quangnam12d12@gmail.com',
        		'username'=>'ctv.blog',
        		'password'=>bcrypt('123456'),
        		'remember_token'=>'$2y$10$SCLeX2KOFbk4gBJLhWEMBu0k3CLVjZ7ZfgLv6ZCFyuBGyayOJ33sq',
                'created_at'=>'2020-07-28 21:09:40',
                'role_id'=>4
            ],
            [
        		'firstname'=>'Cộng tác viên',
        		'lastname'=>'Sale',
        		'email'=>'quangnam12d13@gmail.com',
        		'username'=>'sale',
        		'password'=>bcrypt('123456'),
        		'remember_token'=>'$2y$10$SCLeX2KOFbk4gBJLhWEMBu0k3CLVjZ7ZfgLv6ZCFyuBGyayOJ33sq',
                'created_at'=>'2020-07-28 21:09:40',
                'role_id'=>5
            ],
            [
        		
        		'firstname'=>'Cộng tác viên',
        		'lastname'=>'Kiểm kho',
        		'email'=>'quangnam12d15@gmail.com',
        		'username'=>'kiem.kho',
        		'password'=>bcrypt('123456'),
        		'remember_token'=>'$2y$10$SCLeX2KOFbk4gBJLhWEMBu0k3CLVjZ7ZfgLv6ZCFyuBGyayOJ33sq',
                'created_at'=>'2020-07-28 21:09:40',
                'role_id'=>6
            ],
            [
        		'firstname'=>'Cộng tác viên',
        		'lastname'=>'Kế toán',
        		'email'=>'quangnam12d14@gmail.com',
        		'username'=>'ke.toan',
        		'password'=>bcrypt('123456'),
        		'remember_token'=>'$2y$10$SCLeX2KOFbk4gBJLhWEMBu0k3CLVjZ7ZfgLv6ZCFyuBGyayOJ33sq',
                'created_at'=>'2020-07-28 21:09:40',
                'role_id'=>7
            ],
            [
        		'firstname'=>'Cộng tác viên',
        		'lastname'=>'Editor',
        		'email'=>'quangnam12d127@gmail.com',
        		'username'=>'editor',
        		'password'=>bcrypt('123456'),
        		'remember_token'=>'$2y$10$SCLeX2KOFbk4gBJLhWEMBu0k3CLVjZ7ZfgLv6ZCFyuBGyayOJ33sq',
                'created_at'=>'2020-07-28 21:09:40',
                'role_id'=>8
            ],
            [
        		
        		'firstname'=>'Cộng tác viên',
        		'lastname'=>'Chăm sóc khách hàng',
        		'email'=>'quangnam12d161@gmail.com',
        		'username'=>'cham.soc',
        		'password'=>bcrypt('123456'),
        		'remember_token'=>'$2y$10$SCLeX2KOFbk4gBJLhWEMBu0k3CLVjZ7ZfgLv6ZCFyuBGyayOJ33sq',
                'created_at'=>'2020-07-28 21:09:40',
                'role_id'=>9
            ],
            [
                
                'firstname'=>'Tác giả bài viết',
                'lastname'=>'Tác giả bài viết',
                'email'=>'quangnam12d163@gmail.com',
                'username'=>'tacgia',
                'password'=>bcrypt('123456'),
                'remember_token'=>'$2y$10$SCLeX2KOFbk4gBJLhWEMBu0k3CLVjZ7ZfgLv6ZCFyuBGyayOJ33sq',
                'created_at'=>'2020-07-28 21:09:40',
                'role_id'=>10
            ],
            
            
        ];
        DB::table('users')->insert($data);
    }
}
