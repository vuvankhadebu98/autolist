<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
class DatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
      $this->call(CreateUser::class);
    }
   
}

class CreateUser extends Seeder
{
   public function run()
    {
      //    $user = DB::table('users')->insert([
      //     'firstname' => 'Nguyễn Phú',
      //     'lastname' =>'Đạt', 
      //     'email' => 'admin@gmail.com',
      //     'phone' => '0357142288',
      //     'username' => 'nguyenphudat',
      //     'password' => bcrypt('123456'),
      //     'sex' => '1',
      //     'role' => '0',
      //     'status' => '0',
      //     'created_at' => new Datetime(), 
      //  ]);
       
        $permission = [
          'create_user', 
          'lock_user',
          'create_blog',
          'edit_blog',
          'create_product',
          'edit_product',
          'create_brand',
          'edit_brand',
          'create_category',
          'edit_category',
          'check',
          'sale',
          'report',
          'support'
        ];
        foreach ($permission as $per){
            Permission::create(['name' => $per]);
        }
        $role =  Role::create(['name' => 'admin']); 
        $role->syncPermissions($permission);
        $user = User::find(1);
        $user->assignRole('admin');

        /////
        $role2 =  Role::create(['name' => 'quan_tri']);
        $role2->syncPermissions($permission);
        $user2 = User::find(2);
        $user2->assignRole('quan_tri');
        $user2->givePermissionTo('create_user');

        $role3 =  Role::create(['name' => 'ctv_product']);
        $role3->syncPermissions('create_product');
        $user3 = User::find(3);
        $user3->assignRole('ctv_product');
        $user3->givePermissionTo('create_product');

        $role4 =  Role::create(['name' => 'ctv_blog']);
        $role4->syncPermissions('create_blog');
        $user4 = User::find(4);
        $user4->assignRole('ctv_blog');
        $user4->givePermissionTo('create_blog');

        $role5 =  Role::create(['name' => 'ctv_category']);
        $role5->syncPermissions('create_category');
        
      
        
        $role6 =  Role::create(['name' => 'ctv_brand']);
        $role6->syncPermissions('create_brand');
        
        $role7 =  Role::create(['name' => 'sale']);
        $role7->syncPermissions('sale');
        $user7 = User::find(5);
        $user7->assignRole('sale');
        $user7->givePermissionTo('sale');

        $role8 =  Role::create(['name' => 'check']);
        $role8->syncPermissions('check');
        $user8 = User::find(6);
        $user8->assignRole('check');
        $user8->givePermissionTo('check');

        $role9 =  Role::create(['name' => 'report']);
        $role9->syncPermissions('report');
        $user9 = User::find(7);
        $user9->assignRole('report');
        $user9->givePermissionTo('report');

        $role1 =  Role::create(['name' => 'editor']);
        $role1->syncPermissions('edit_product');
        $user = User::find(8);
        $user->assignRole('editor');
       
        

        $role1 =  Role::create(['name' => 'support']);
        $role1->syncPermissions('support');
        $user = User::find(9);
        $user->assignRole('sale');

        
        

    }
}

