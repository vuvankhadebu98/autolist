<?php

use Illuminate\Database\Seeder;
// use Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id'=>1,
                'user_id'=>2,
                'category_id'=>1,
                'brand_id'=>1,
                'name'=>'Linsra Recusandae',
                'price'=>300,
                'promotion'=>10,
                'seats'=>2,
                'image_product'=>'FC6HZO_0WMLvw_j3.jpg,ObRxjg_1p0gnx_porshe.jpg,9mbPmf_39QITG_porshe.jpg,McSJSw_bF4TSd_b3.png,jsLapr_dguwxn_hupQif_bF4TSd_b3.png',
                'like'=>0,
                'rate'=>0,
                'view'=>0,
                'buy_count'=>0,
                'description'=>'<p>Cuộc đời con sẽ gặp nhiều người phụ nữ, c&oacute; người sẽ muốn b&ecirc;n con trọn đời nhưng sẽ c&oacute; kh&ocirc;ng &iacute;t người chỉ muốn nh&igrave;n v&agrave;o v&iacute; tiền của con. Người muốn gắn b&oacute; với con cả đời c&ocirc; ấy sẽ chăm s&oacute;c cuộc sống của con, c&ograve;n người suốt ng&agrave;y lo t&igrave;nh cảm của c&ocirc; ấy th&igrave; chỉ nh&igrave;n v&agrave;o v&iacute; tiền m&agrave; th&ocirc;i!</p',
                'engine'=>'F8D',
                'enegine_displacement'=>'179',
                'fuel_type'=>'1',
                'fuel_tank_capacity'=>35,
                'status'=>0,
                'break'=>'ABS',
                'bootspace'=>'177',
                'arai_milage'=>'25',
                'max_torque'=>'69Nm@3500rpm',
                'max_power'=>'47.3bhp@6000rpm',
                'transmission_type'=>1,
                'tags'=>'1',
                'created_at'=>'2020-09-08 03:11:26',
                'video'=>'https://www.youtube.com/embed/Rxq_My5KKaM'
            ],
        ];
        DB::table('products')->insert($data);
    }    
}
