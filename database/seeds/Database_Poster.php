<?php

use Illuminate\Database\Seeder;

class Database_Poster extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
                'id'=>1,
        		'id_user'=>1,
	            'brand_id'=>1,
                'category_id'=>1,
	            'title'=>"adsasd",
                'price'=>180,
                'seats'=>4,
                'km_went'=>3333,
                'fuel_type'=>1,
                'transmission_type'=>1,
	            'description'=>"Ngày 28/7, tại Trung tâm quản lý, điều hành trực tuyến hỗ trợ chuyên môn chẩn đoán, điều trị Covid-19, Phó Thủ tướng Vũ Đức Đam, Trưởng Ban Chỉ đạo quốc gia phòng chống dịch bệnh Covid-19 đã có cuộc họp trực tuyến với các bệnh viện về công tác điều trị.",
                'engine'=>'V8',
                'enegine_displacement'=>'A30_2020',
                'fuel_tank_capacity'=>20,
                'break'=>"Chống đạn",
                'bootspace'=>'A12',
                'arai_milage'=>"B12",
                'max_torque'=>100,
                'max_power'=>1000,
                'like'=>0,
                'package'=>0,
                'check'=>0,
                'view'=>180,
                'rate'=>4,
                'check_new'=>0,
                'video'=>'videoplayback.mp4',
                'created_at'=>'2020-09-10 09:40:54'
        	],
        	[
                'id'=>2,
                'id_user'=>1,
                'brand_id'=>1,
                'category_id'=>1,
                'title'=>"Audi-a4-2017",
                'price'=>90,
                'seats'=>2,
                'km_went'=>3333,
                'fuel_type'=>1,
                'transmission_type'=>1,
                'description'=>"Ngày 28/7, tại Trung tâm quản lý, điều hành trực tuyến hỗ trợ chuyên môn chẩn đoán, điều trị Covid-19, Phó Thủ tướng Vũ Đức Đam, Trưởng Ban Chỉ đạo quốc gia phòng chống dịch bệnh Covid-19 đã có cuộc họp trực tuyến với các bệnh viện về công tác điều trị.",
                'engine'=>'V8',
                'enegine_displacement'=>'A30_2020',
                'fuel_tank_capacity'=>20,
                'break'=>"Chống đạn",
                'bootspace'=>'A12',
                'arai_milage'=>"B12",
                'max_torque'=>100,
                'max_power'=>1000,
                'like'=>10,
                'package'=>0,
                'check'=>0,
                'view'=>90,
                'rate'=>5,
                'check_new'=>0,
                'video'=>'videoplayback.mp4',
                'created_at'=>'2020-09-10 09:40:54'
        	],
            [
                'id'=>3,
                'id_user'=>1,
                'brand_id'=>1,
                'category_id'=>2,
                'title'=>"Audi-A8-3",
                'price'=>50,
                'seats'=>2,
                'km_went'=>3333,
                'fuel_type'=>1,
                'transmission_type'=>1,
                'description'=>"Ngày 28/7, tại Trung tâm quản lý, điều hành trực tuyến hỗ trợ chuyên môn chẩn đoán, điều trị Covid-19, Phó Thủ tướng Vũ Đức Đam, Trưởng Ban Chỉ đạo quốc gia phòng chống dịch bệnh Covid-19 đã có cuộc họp trực tuyến với các bệnh viện về công tác điều trị.",
                'engine'=>'V8',
                'enegine_displacement'=>'A30_2020',
                'fuel_tank_capacity'=>20,
                'break'=>"Chống đạn",
                'bootspace'=>'A12',
                'arai_milage'=>"B12",
                'max_torque'=>100,
                'max_power'=>1000,
                'like'=>100,
                'package'=>3,
                'check'=>0,
                'view'=>77,
                'rate'=>3,
                'check_new'=>0,
                'video'=>'videoplayback.mp4',
                'created_at'=>'2020-09-10 09:40:54'
            ],
            [   
                'id'=>4,
                'id_user'=>1,
                'brand_id'=>2,
                'category_id'=>2,
                'title'=>"Audi-A8-3",
                'price'=>299,
                'seats'=>8,
                'km_went'=>3333,
                'fuel_type'=>1,
                'transmission_type'=>2,
                'description'=>"Ngày 28/7, tại Trung tâm quản lý, điều hành trực tuyến hỗ trợ chuyên môn chẩn đoán, điều trị Covid-19, Phó Thủ tướng Vũ Đức Đam, Trưởng Ban Chỉ đạo quốc gia phòng chống dịch bệnh Covid-19 đã có cuộc họp trực tuyến với các bệnh viện về công tác điều trị.",
                'engine'=>'V8',
                'enegine_displacement'=>'A30_2020',
                'fuel_tank_capacity'=>20,
                'break'=>"Chống đạn",
                'bootspace'=>'A12',
                'arai_milage'=>"B12",
                'max_torque'=>100,
                'max_power'=>1000,
                'like'=>44,
                'package'=>3,
                'check'=>0,
                'view'=>1238,
                'rate'=>4,
                'check_new'=>0,
                'video'=>'videoplayback.mp4',
                'created_at'=>'2020-09-10 09:40:54'
            ],
            [               
                'id'=>5,
                'id_user'=>1,
                'brand_id'=>2,
                'category_id'=>2,
                'title'=>"Audi-A8-3",
                'price'=>299,
                'seats'=>8,
                'km_went'=>3333,
                'fuel_type'=>1,
                'transmission_type'=>4,
                'description'=>"Ngày 28/7, tại Trung tâm quản lý, điều hành trực tuyến hỗ trợ chuyên môn chẩn đoán, điều trị Covid-19, Phó Thủ tướng Vũ Đức Đam, Trưởng Ban Chỉ đạo quốc gia phòng chống dịch bệnh Covid-19 đã có cuộc họp trực tuyến với các bệnh viện về công tác điều trị.",
                'engine'=>'V8',
                'enegine_displacement'=>'A30_2020',
                'fuel_tank_capacity'=>20,
                'break'=>"Chống đạn",
                'bootspace'=>'A12',
                'arai_milage'=>"B12",
                'max_torque'=>100,
                'max_power'=>1000,
                'like'=>44,
                'package'=>3,
                'check'=>0,
                'view'=>1238,
                'rate'=>4,
                'check_new'=>0,
                'video'=>'videoplayback.mp4',
                'created_at'=>'2020-09-10 09:40:54'
            ],
            [
                'id'=>6,
                'id_user'=>1,
                'brand_id'=>4,
                'category_id'=>3,
                'title'=>"Audi-A8-3",
                'price'=>1200,
                'seats'=>2,
                'km_went'=>3333,
                'fuel_type'=>1,
                'transmission_type'=>2,
                'description'=>"Ngày 28/7, tại Trung tâm quản lý, điều hành trực tuyến hỗ trợ chuyên môn chẩn đoán, điều trị Covid-19, Phó Thủ tướng Vũ Đức Đam, Trưởng Ban Chỉ đạo quốc gia phòng chống dịch bệnh Covid-19 đã có cuộc họp trực tuyến với các bệnh viện về công tác điều trị.",
                'engine'=>'V8',
                'enegine_displacement'=>'A30_2020',
                'fuel_tank_capacity'=>20,
                'break'=>"Chống đạn",
                'bootspace'=>'A12',
                'arai_milage'=>"B12",
                'max_torque'=>100,
                'max_power'=>1000,
                'like'=>354,
                'package'=>4,
                'check'=>0,
                'view'=>13,
                'rate'=>5,
                'check_new'=>0,
                'video'=>'videoplayback.mp4',
                'created_at'=>'2020-09-10 09:40:54'
            ], 
        ];
        DB::table('poster')->insert($data);
    }
}
