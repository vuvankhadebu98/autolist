<?php

use Illuminate\Database\Seeder;

class Database_Category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        // Thể loại xe và thể loại phụ tùng
        // Check : kiểm tra xem là xe hay là phụ tùng 0:xe, 1:phụ tùng
        $data = [
        	[
                'id'=>1,
        		'name'=>"Thể loại 1",
                'check'=>0,
                'img'=>'xe.png',
                'created_at'=>'2020-08-04 15:44:24'

        	],
        	[
                'id'=>2,
        		'name'=>"Thể loại 2",
                'check'=>0,
                'img'=>'xe.png',
                'created_at'=>'2020-08-04 15:44:24'

        	],
        	[
                'id'=>3,
        		'name'=>"Thể loại 3",
                'check'=>0,
                'img'=>'xe.png',
                'created_at'=>'2020-08-04 15:44:24'

        	],
            [
                'id'=>4,
                'name'=>"Thể loại 4",
                'check'=>0,
                'img'=>'xe.png',
                'created_at'=>'2020-08-04 15:44:24'

            ],
            [
                'id'=>5,
                'name'=>"Thể loại 5",
                'check'=>0,
                'img'=>'xe.png',
                'created_at'=>'2020-08-04 15:44:24'

            ],
            [
                'id'=>6,
                'name'=>"Thể loại 6",
                'check'=>0,
                'img'=>'xe.png',
                'created_at'=>'2020-08-04 15:44:24'

            ],           
        ];

        DB::table('categories')->insert($data);
    }
}
