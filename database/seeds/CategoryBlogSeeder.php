<?php

use Illuminate\Database\Seeder;

class CategoryBlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_blog')->insert([
        	['id'=> 1, 'name'=>'product'],
        	['id'=> 2, 'name'=>'services'],
        	['id'=> 3, 'name'=>'brand']
        ]);
    }
}
