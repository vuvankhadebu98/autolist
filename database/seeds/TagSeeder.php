<?php

use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->delete();
        DB::table('tags')->insert([
        	['id'=>'1','name'=>'Thuong hieu','product_id'=>'1'],
        	['id'=>'2','name'=>'dich vu','product_id'=>'2'],
        	['id'=>'3','name'=>'product','product_id'=>3],
        	['id'=>'4','name'=>'dich vu','product_id'=>4]

        ]);
    }
}
