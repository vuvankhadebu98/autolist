<?php

use Illuminate\Database\Seeder;

class Data_Accessaries extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
        	[
                'id'=>1,
                'user_id'=>1,
        		'name'=>"Tua bin khí 1",
        		'img'=>"khai_thac.png",
        		'price'=>'5555',
        		'description'=>"(Dân trí) - “Càng có đại dịch càng phải khắc phục các khiếm khuyết đã bộc lộ. Nếu làm được thì sẽ có sức bật mạnh sau khủng hoảng. Một chữ V đang chờ đợi chúng ta nếu biết tổ chức công việc” - Thủ tướng nói.",
                'check'=>0
        	],
        	[
                'id'=>2,
                'user_id'=>1,
        		'name'=>"Tua bin khí 2",
        		'img'=>"khai_thac.png",
        		'price'=>'5555',
        		'description'=>"(Dân trí) - “Càng có đại dịch càng phải khắc phục các khiếm khuyết đã bộc lộ. Nếu làm được thì sẽ có sức bật mạnh sau khủng hoảng. Một chữ V đang chờ đợi chúng ta nếu biết tổ chức công việc” - Thủ tướng nói.",
                'check'=>1
        	],
        	[
                'id'=>3,
                'user_id'=>1,
        		'name'=>"Tua bin khí 3",
        		'img'=>"khai_thac.png",
        		'price'=>'5555',
        		'description'=>"(Dân trí) - “Càng có đại dịch càng phải khắc phục các khiếm khuyết đã bộc lộ. Nếu làm được thì sẽ có sức bật mạnh sau khủng hoảng. Một chữ V đang chờ đợi chúng ta nếu biết tổ chức công việc” - Thủ tướng nói.",
                'check'=>0
        	],
        	[
                'id'=>4,
                'user_id'=>1,
        		'name'=>"Tua bin khí 4",
        		'img'=>"khai_thac.png",
        		'price'=>'5555',
        		'description'=>"(Dân trí) - “Càng có đại dịch càng phải khắc phục các khiếm khuyết đã bộc lộ. Nếu làm được thì sẽ có sức bật mạnh sau khủng hoảng. Một chữ V đang chờ đợi chúng ta nếu biết tổ chức công việc” - Thủ tướng nói.",
                'check'=>0
        	],
        	[
                'id'=>5,
                'user_id'=>1,
        		'name'=>"Tua bin khí 5",
        		'img'=>"khai_thac.png",
        		'price'=>'5555',
        		'description'=>"(Dân trí) - “Càng có đại dịch càng phải khắc phục các khiếm khuyết đã bộc lộ. Nếu làm được thì sẽ có sức bật mạnh sau khủng hoảng. Một chữ V đang chờ đợi chúng ta nếu biết tổ chức công việc” - Thủ tướng nói.",
                'check'=>0
        	],
        	[
                'id'=>6,
                'user_id'=>1,
        		'name'=>"Tua bin khí 6",
        		'img'=>"khai_thac.png",
        		'price'=>'5555',
        		'description'=>"(Dân trí) - “Càng có đại dịch càng phải khắc phục các khiếm khuyết đã bộc lộ. Nếu làm được thì sẽ có sức bật mạnh sau khủng hoảng. Một chữ V đang chờ đợi chúng ta nếu biết tổ chức công việc” - Thủ tướng nói.",
                'check'=>1
        	],
        	[
                'id'=>7,
                'user_id'=>1,
        		'name'=>"Tua bin khí 7",
        		'img'=>"khai_thac.png",
        		'price'=>'5555',
        		'description'=>"(Dân trí) - “Càng có đại dịch càng phải khắc phục các khiếm khuyết đã bộc lộ. Nếu làm được thì sẽ có sức bật mạnh sau khủng hoảng. Một chữ V đang chờ đợi chúng ta nếu biết tổ chức công việc” - Thủ tướng nói.",
                'check'=>0
        	],
        ];
        DB::table('accessaries')->insert($data);
    }
}
