<?php

use Illuminate\Database\Seeder;

class Database_Brand extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // THương hiệu xe và thương hiệu phụ tùng
        $data = [
        	[
                'id'=>1,
        		'name'=>"Audi",
	            'img'=>"audi.jfif",
	            'check'=>0,
        	],
        	[
                'id'=>2,
        		'name'=>"Mercedes",
	            'img'=>"mercedes.jfif",
	            'check'=>0,
        	],
        	[
                'id'=>3,
        		'name'=>"HD Auto",
	            'img'=>"hd auto.png",
	            'check'=>1,
        	],
            [
                'id'=>4,
                'name'=>"Lamborghini",
                'img'=>"lamborghini.png",
                'check'=>0,
            ],
            [
                'id'=>5,
                'name'=>"Toyota",
                'img'=>"toyota.png",
                'check'=>0,
            ],
            [
                'id'=>6,
                'name'=>"Tesla",
                'img'=>"tesla.png",
                'check'=>0,
            ],
            [
                'id'=>7,
                'name'=>"Vinfast",
                'img'=>"vinfast.jfif",
                'check'=>0,
            ],
            [
                'id'=>8,
                'name'=>"Ferrari ",
                'img'=>"ferrari.jfif",
                'check'=>0,
            ],
        ];
        DB::table('brand')->insert($data);
    }
}
