<?php

use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('blogs')->delete();
        $data = [
        	[
        		'name'=>"Audi",
	            'img'=>"audi.jfif",
	            'description'=>"Of course,  .",
	            'status'=>1,
	            'user_id'=>1,
	            'category_blog_id'=>1
        	],
        	[
        		'name'=>"Mercedes",
	            'img'=>"mercedes.jfif",
	            'description'=>"Of course, manually sp.",
	            'status'=>1,
	            'user_id'=>2,
	            'category_blog_id'=>2

        	],
        	[
        		'name'=>"HD Auto",
	            'img'=>"hd auto.png",
	            'description'=>"Of course, manually specifying the attributes for each model seed is cumbersome. Instead, you can use model factories to conveniently generate.",
	            'status'=>1,
	            'user_id'=>3,
	            'category_blog_id'=>3

        	],
        	[
        		'name'=>"Audi",
	            'img'=>"audi.jfif",
	            'description'=>"Of course, manually specifying the attributes for each model seed is cumbersome. Instead, you can use model factories to conveniently generate.",
	            'status'=>1,
	            'user_id'=>4,
	            'category_blog_id'=>4
        	],
        	[
        		'name'=>"Mercedes",
	            'img'=>"mercedes.jfif",
	           'description'=>"Of course, manually specifying the attributes for each model seed is cumbersome. Instead, you can use model factories to conveniently generate",
	            'status'=>1,
	            'user_id'=>5,
	            'category_blog_id'=>5
        	],
        	[
        		'name'=>"HD Auto",
	            'img'=>"hd auto.png",
	            'description'=>"Of course, manually specifying the attributes for each model seed is cumbersome. Instead, you can use model factories to conveniently generat.",
	            'status'=>1,
	            'user_id'=>6,
	            'category_blog_id'=>6
        	],
        
        ];
        DB::table('blogs')->insert($data);
    }
}
