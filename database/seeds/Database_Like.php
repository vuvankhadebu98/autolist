<?php

use Illuminate\Database\Seeder;

class Database_Like extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
        	[
        		'user_id'=>1,
	            'poster_id'=>3,
	            'status'=>0,
        	],
        	[
        		'user_id'=>"1",
	            'poster_id'=>4,
	            'status'=>0,
        	],
        ];
        DB::table('like')->insert($data);
    }
}
