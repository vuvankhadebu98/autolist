<?php

use Illuminate\Database\Seeder;

class Database_Role extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
                'name' => 'Khách Hàng',
                'guard_name' => 'a'
            ],
            [
                'name' => 'Quản trị viên',
                'guard_name' => 'a'
            ],
            [
                'name' => 'Cộng tác viên đăng sản phẩm',
                'guard_name' => 'a'
            ],
            [
                'name' => 'Cộng tác viên đăng blog',
                'guard_name' => 'a'
            ],
            [
                'name' => 'Sale',
                'guard_name' => 'a'
            ],
            [
                'name' => 'Kiểm kho',
                'guard_name' => 'a'
            ],
            [
                'name' => 'Kế toán',
                'guard_name' => 'a'
            ],
            [
                'name' => 'Editor',
                'guard_name' => 'a'
            ],
            [
                'name' => 'Chăm sóc Khách Hàng',
                'guard_name' => 'a'
            ],
            [
                'guard_name'=>'Chưa hiểu',
                'name' => 'Quản trị viên'
            ],
            [
                'guard_name'=>'Chưa hiểu',
                'name' => 'Cộng tác viên đăng sản phẩm'
            ],
            [
                'guard_name'=>'Chưa hiểu',
                'name' => 'Cộng tác viên đăng blog'
            ],
            [
                'guard_name'=>'Chưa hiểu',
                'name' => 'Sale'
            ],
            [
                'guard_name'=>'Chưa hiểu',
                'name' => 'Kiểm kho'
            ],
            [
                'guard_name'=>'Chưa hiểu',
                'name' => 'Kế toán'
            ],
            [
                'guard_name'=>'Chưa hiểu',
                'name' => 'Editor'
            ],
          
        ];
        DB::table('roles')->insert($data);

    }
}
