<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comment')->delete();
        DB::table('comment')->insert([
        	['id'=>1,'content'=>'dep','product_id'=>1,'blog_id'=>1,'user_id'=>1],
        	['id'=>2,'content'=>'dep','product_id'=>2,'blog_id'=>2,'user_id'=>2],
        	['id'=>3,'content'=>'dep','product_id'=>3,'blog_id'=>3,'user_id'=>3],
        	['id'=>4,'content'=>'dep','product_id'=>4,'blog_id'=>4,'user_id'=>4]
        ]);
    }
}
