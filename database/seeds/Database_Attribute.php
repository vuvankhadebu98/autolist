<?php

use Illuminate\Database\Seeder;

class Database_Attribute extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Các chức năng đi kèm xe (VD : điều hòa 2 chiều, lái tự động)
        $data = [
        	[
                'id'=>1,
        		'name'=>"Thuộc tính 1",
        	],
        	[
                'id'=>2,
        		'name'=>"Thuộc tính 2",
        	],
        	[
                'id'=>3,
        		'name'=>"Thuộc tính 3",
        	],
            [
                'id'=>4,
                'name'=>"Thuộc tính 4",
            ],
            [
                'id'=>5,
                'name'=>"Thuộc tính 5",
            ],
            [
                'id'=>6,
                'name'=>"Thuộc tính 6",
            ],
            
        ];
        DB::table('attributes')->insert($data);
    }
}
