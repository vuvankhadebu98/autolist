<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Model\Category;
use App\Model\Brand;
use App\Model\Order;
use App\Model\Product;
Use App\Model\Poster;
use App\User;
use App\Model\Province;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $data['brand_car'] = Brand::where('check',0)->get();
        $data['categories'] = Category::all();
        $data['provinces'] = Province::all();
        $data['poster_home'] = Poster::where('status',1)->get();
        
        $data['user_home'] = User::all();
        $data['product_home'] = Product::where('status',1)->get();
        view()->share($data);

        view()->composer('admin/master', function($view){

            //Đơn hàng
            $order = Order::where('status',0)->count();
            $order_time = Order::where('status',0)->orderBy('id','desc')->first();
            //Sản phẩm
            $product = Product::where('status', 0)->count();
            $product_time = Product::where('status',0)->orderBy('id','desc')->first();

            $category_product = Category::orderBy('id','desc')->get();
            // dd($product_time);
            // dd($order_time);
            $view->with(['order'=>$order,'order_time'=>$order_time,'product' => $product,'product_time' => $product_time,'category_product' => $category_product]);
        });

        view()->composer('admin/product/list_product',function($view){
            $brand_product = Brand::orderBy('id','desc')->get();
            $category_product = Category::orderBy('id','desc')->get();
            $view->with(['category_product' => $category_product,'brand_product' => $brand_product]);
        });
          view()->composer('admin/product/search_product',function($view){
            $brand_product = Brand::orderBy('id','desc')->get();
            $category_product = Category::orderBy('id','desc')->get();
            $view->with(['category_product' => $category_product,'brand_product' => $brand_product]);
        });
    }
}
