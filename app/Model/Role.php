<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "roles";
    protected $privateKeys ="id";    
    protected $fillable = ['id','name','guard_name'];//Phải định nghĩa fillable cho phép các trường được phép sử dụng
    public function use()
    {   
        return $this->hasMany('App\User','role', 'id');
    }   
}