<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = 'ratings';

    public function Product()
    {
    	return $this->hasmany('App\Model\Product','id','product_id');
    }
    public function User()
    {
    	return $this->hasmany('App\User','id','user_id');
    }
}

