<?php

namespace App\Model;
use App\ProductValue;
use App\Attribute;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    public static function checkAtt($product_id){
    	  $attrOfProduct = ProductValue::where('attribute_id',$product_id)
                                        ->select('attribute_id')->pluck('attribute_id')
                                        ->toArray();
    	  	$stringAtt = implode(',',$attrOfProduct);
    	  	$stringA = explode(',', $stringAtt);
    	  	$string = (int)$stringA;
    	   $attr = Attribute::select('id')->pluck('id')->toArray();
           
           if(in_array($string,$attr)){
    		echo '<i class="icon icon-check text-success">';
    	 	}
    	  else{
    	  		echo '<i class="icon icon-check text-danger">';
    	  }
    }
    protected $table = 'products';
    protected $primaryKey = 'id';

    protected $fillable = ['name','category_id','brand_id','price','promotion','seats','image_product','like','rate','view','buy_count','video','description','engine','engine_displacement','fuel_type','fuel_tank_capacity','break','bootspace','arai_milage','max_torque','max_power','transmission_type','created_at'];
    
    public $timestamps = false;

    public function category(){
        return $this->hasOne('App\Model\Category','id','category_id');
    }
    public function brand(){
        return $this->hasOne('App\Model\Brand','id','brand_id');
    }
    public function attribute(){
        return $this->belongsToMany('App\Attribute','product_values','product_id','attribute_id');
    }

    //<------Luyện------
    public function categoryId()
    {
        return $this->belongsTo('App\Model\Category','id','category_id');
    }
    //----------->
     public function Rating()
    {
        return $this->hasOne('App\Model\Rating','product_id','id');
    }
     public function User(){
        return $this->hasOne('App\User','id','user_id');
    }
}
