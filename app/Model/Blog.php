<?php

namespace App\Model;
use App\Model\Blog;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

   
    protected  $table = 'blogs';

    protected $fillable = [
        'firstname', 'email', 'password',
    ];
      
    public function user()
    {
    	return $this->hasOne('App\User','id','user_id');
    }
    public function category_blog()
    {
    	return $this->hasOne('App\Model\Category_blog','id','category_blog_id');
    }
    public function tag()
    {
        return $this->belongsToMany(Tag::class,'blog_tag');
    } 
     // đếm có bao nhiêu cái blog theo loại
    public static function blogCount($cate_blog_id){
    	$blogCount = Blog::where(['category_blog_id'=>$cate_blog_id])->count();
    	return $blogCount;
    } 
     public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    } 
}
