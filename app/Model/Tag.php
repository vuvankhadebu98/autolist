<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    protected  $table = 'tags';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'product_id', 'blog_id',
    ];
      
    public function blog()
    {
    	return $this->belongsToMany(Blog::class,'blog_tag');
    }
    public function product()
    {
        return $this->belongsToMany(Product::class,'product_tag');
    } 
}
