<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Poster;

class Poster extends Model
{
    //
    protected $table = "poster";

    public static function posterCount($cate_id){
    	$postCount = Poster::where(['category_id'=>$cate_id])->count();
    	return $postCount;
    } 
    public function user()
    {
    	return $this->hasOne('App\User','id','id_user');
    } 
    public function category()
    {
    	return $this->hasOne('App\Model\Category','id','category_id');
    }
    public function brand()
    {
        return $this->hasOne('App\Model\Brand','id','brand_id');
    }
}
