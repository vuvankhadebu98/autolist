<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    protected $primaryKey = 'id';
    protected $fillable = ['customer_id','status','check','created_at','updated_at'];
    public $timestamps = false;
    public function customer(){
    	return $this->hasOne('App\User','id','customer_id');
    }
    public function order_detail(){
    	return $this->hasMany('App\Model\Order_Detail','order_id','id');
    }
    public function product(){
        return $this->hasMany('App\Model\Product','product_id','id');
    }
}
