<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

  protected $table = "categories";
    protected $primaryKey = "id";
    protected $fillable = [
        'id',
        'name',
        'img',
        'check',
      ]; 

      public function category()
    {
        return $this->belongsTo(Category::class, 'id');
    }
    // public function post()
    // {
    //     return $this->hasMany(Post::class,'cate_id','id');
    // }

    //<------Luyện------>
    function productId(){
        return $this->hasMany('App\Model\Product','category_id','id');
    }
    public function post()
    {
        return $this->hasMany(Post::class,'cate_id','id');
    }
}
