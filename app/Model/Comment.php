<?php

namespace App\Model;
use App\Model\Comment;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $timestamps = false;
    protected $fillable = [
    	'content', 'product_id', 'blog_id','user_id'
    ];
    protected $primaryKey = 'id';
    protected $table = 'comment';

    // đếm xem có bao nhiêu cái comment theo loại
	public static function commentCount($blog_id){
		$commentCount = Comment::where(['blog_id'=>$blog_id])->count();
		return $commentCount;
    }
    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }
    public function Blog()
    {
        return $this->hasOne('App\Model\Blog','id','blog_id');
    }

}

