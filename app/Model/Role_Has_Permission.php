<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Role_Has_Permission extends Model
{
    protected $table ="role_has_permissions";
    protected $fillable = ['permission_id','role_id'];
}
