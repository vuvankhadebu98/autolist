<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order_Detail extends Model
{
    // 
    protected $table = 'order_detail';
    protected $primaryKey = 'id';
    protected $fillable = ['order_id','product_id','qty','price','created_at','updated_at'];
    public $timestamps = false;
    public function order(){
    	return $this->hasOne('App\Model\Order','id','order_id');
    }
    public function product(){
    	return $this->belongsTo('App\Model\Product','product_id','id');
    }
}
