<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;

use Spatie\Permission\Models\Permission;


class User extends Authenticatable
{
    use Notifiable, HasRoles;
    protected $table = 'users';
    Protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','firstname','lastname','role_id','img'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function citys() {
        return $this->belongsTo('App\Model\Province','city','id');
    }
    // public function roles() {
    //     return $this->belongsTo(Role::class,'role_id','id');
    // }
     public function Rating()
    {
        return $this->hasOne('App\Model\Rating','user_id','id');
    }
    public function Product(){
        return $this->hasOne('App\Model\Product','user_id','id');
    }
     public function Accessary(){
        return $this->hasMany('App\Accessary','user_id','id');
    }
     public function Blog(){
        return $this->hasMany('App\Model\Blog','id','user_id');
    }


}
