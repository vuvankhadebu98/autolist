<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
	public $timestamps = false;

    protected $table = 'attributes';
    protected $fillable = ['attribute_name'];
    
    protected $primaryKey = 'id';

    public function product(){
    	return $this->belongsToMany('App\Model\Product');
    }
    
}
