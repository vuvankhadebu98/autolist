<?php
    if (! function_exists('connect')) {
        function connect(){
            $servername = "localhost";
            $username = "root";
            $password = "";
                // Create connection
            $conn = new mysqli($servername, $username, $password);
                // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }else{
                return $conn;
            }
        }
    }

    // Lấy nhiều bản ghi
    if (! function_exists('select_multy')) {
        function select_multy($sql){
            $data = [];
            $i = 0;
            $conn = connect();
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    $city = select_by_id($row['id_user']);
                    $city = $city['city'];
                    $data[$i]['id'] = $row['id'];
                    $data[$i]['img'] = $row['img'];
                    $data[$i]['view'] = $row['view'];
                    $data[$i]['title'] = $row['title'];
                    $data[$i]['address'] = $city;
                    $data[$i]['created_at'] = $row['created_at'];
                    $data[$i]['description'] = $row['description'];
                    $data[$i]['price'] = $row['price'];
                    $data[$i]['rate'] = $row['rate'];
                    $data[$i]['km'] = $row['km_went'];
                    $data[$i]['transmission_type'] = $row['transmission_type'];
                    $data[$i]['fuel_type'] = $row['fuel_type'];
                    $data[$i]['status'] = $row['status'];
                    if($i==0){
                        $data[$i]['sql'] = $sql;
                    }
                    $i++;
                }
            }
            return $data;
            $conn->close();
        }
    }

    // Lấy bản ghi duy nhất
    if (! function_exists('select_by_id')) {
        function select_by_id($id){
            $data = [];
            $i = 0;
            $conn = connect();
            $sql = "SELECT * FROM autolist.users WHERE id='".$id."'";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    if($row["city"] != null){

                        $sql = "SELECT * FROM auto.provinces WHERE id='".$row["city"]."'";

                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
                            while($_row = $result->fetch_assoc()) {
                                $city = $_row['_name'];
                                $id_city = $_row['id'];
                            }
                        }
                    }else{
                        $id_city = "";
                        $city = "";
                    }
                    $data = [
                        'id' => $row["id"],
                        'name' => $row["firstname"]." ".$row["lastname"],
                        'email' => $row["email"],
                        'img' => $row["img"],
                        'phone' => $row["phone"],
                        'address' => $row["address"],
                        'id_city' => $id_city,
                        'face' => $row["url_face"],
                        'city' => $city,
                        'google' => $row["url_google"],
                        'twitter' => $row["url_twitter"],
                        'print' => $row["url_print"],
                    ];
                }
            }
            return $data;
            $conn->close();
        }
    }
    // Bộ lọc
    if (! function_exists('filter')) {
        function filter($a,$b,$c,$d,$e,$f){
            if($b!=null){
                if($a==null){
                    $b = $b;
                }else{
                    $b = "AND ".$b;
                }
            }
            if($c!=null){
                if( ($a==null) && ($b==null) ){
                    $c = $c;
                }else{
                    $c = "AND ".$c;
                }
            }
            if($d!=null){
                if( ($a==null) && ($b==null) && ($c==null)){
                    $d = $d;
                }else{
                    $d = "AND ".$d;
                }
            }
            if($e!=null){
                if( ($a==null) && ($b==null) && ($c==null) && ($d==null)){
                    $e = $e;
                }else{
                    $e = "AND ".$e;
                }
            }
            if($f!=null){
                if( ($a==null) && ($b==null) && ($c==null) && ($d==null) && (ed==null)){
                    $f = $f;
                }else{
                    $f = "AND ".$f;
                }
            }
            $sql = "SELECT * FROM auto_list1.poster WHERE ".$a.$b.$c.$d.$e.$f;
            $data = select_multy($sql);
            return $data;
            $conn->close();
        }
    }
?>
