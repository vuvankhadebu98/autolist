<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccessaryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:255',
            'code' => 'required|numeric',
            'price' => 'required|numeric',
            'img'=>'required',
            'description'=>'required',
            'status' => 'required'            
        ];
    }
    public function messages()
    {
        return[
            'required'=> ':attribute không được để trống !',
            'numeric'=> ':attribute phải nhập giá trị số !',
            'name.max'=>'Độ dài không được quá 255 kí tự'
        ];
    }
}
