<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:255',
            // 'qty' => 'required',
            'img' => 'required|image'
            
        ];
    }
    public function messages()
    {
        return[
            'required'=> ':attribute không được để trống !',
            'name.unique'=>'Tên danh mục đã tồn tại',
            'title.max'=>'Độ dài không được quá 255 kí tự',
            'image.mimes'=>'Sai định dạng ảnh',
            'image.max'=>'Kích thước ảnh tối đa 10000Kb',
            'img.image'=>'Ảnh phải có đuôi là file(jpeg, png, bmp, gif, or svg)'
        ];
    }
    
    public function attributes()
    {
        return[
        'name'=>'Tên',
        'qty'=>'Số lượng',
        'img'=>'Hình ảnh'
        ];
    }
}
