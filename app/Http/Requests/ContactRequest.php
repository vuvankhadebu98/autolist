<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'name'=>'required|max:255',
            'email' => 'required|min:5|max:50',
            'content' => 'required'
        ];
    }
    public function messages()
    {
        return[
            'required'=> ':attribute không được để trống !',
            'email.min'=> ':attribute ít nhất có 5 kí tự !',
            'email.max'=> ':attribute không được dài quá 50 kí tự !',
            'name.max'=>'Độ dài không được quá 255 kí tự'
        ];
    }
    
    // // public function attributes()
    // // {
    // //     return[
    // //     'name'=>'Tên',
    // //     'qty'=>'Số lượng',
    // //     'img'=>'Hình ảnh'
    // //     ];
    // }
    // }
}
