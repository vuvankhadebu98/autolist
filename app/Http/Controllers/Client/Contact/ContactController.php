<?php

namespace App\Http\Controllers\Client\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use Carbon\Carbon;
use App\Model\Contact;
use Mail;

class ContactController extends Controller
{
    // trang liên hệ
    public function contact(){
    	return view('Client.contact.contact');
    }
    // liên hệ
    public function save_contact(Request $request){
        $data = $request->except('_token');
        // $data['created_at'] = $data['updated_at'] = Carbon::now();
        Contact::insert($data);
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'content'=>$request->content,
        ];
        Mail::send('client.contact.contact_mail',$data,function($message) use ($data){
            $message->from($data['email'],'Liên hệ');
            $message->to('vuvankhadebu98@gmail.com','MR:vankha');
            $message->subject('Liên hệ Từ Khách Hàng');
        });
        return redirect()->back()->with('thongbao','Gửi liên hệ thành công');    
    }
}
