<?php

namespace App\Http\Controllers\Client\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Country;
use App\Model\Category;
use App\Model\Poster;
use Auth;
class MyAdsController extends Controller
{

    // Hiển thị trang quảng cáo của tôi
    public function index(Request $Request,$id){
    	$data = select_by_id(Auth::User()->id);
        $user = User::where('id',Auth::User()->id)->get();
        $category = Category::all();
        return view('client.dashboard.myads',['data'=>$data,'user'=>$user,'category'=>$category]);  	
    }
    // Hiển thị trang quảng cáo của tôi
    public function get_poster_info_by_click(Request $Request){
    	if($Request->ajax()) {
    		$category = Category::all();
	        $poster = $this->getData($Request['id_brand']);
	        $view = view('client.dashboard.test',compact('poster','category'))->render();
	        return response($view);
	    }	   	
    }
     
    public function get_poster_info_pagination(Request $Request){
    	if($Request->ajax()) {
    		$category = Category::all();
	        $poster = $this->getData($Request['id_brand']);
	        return view('client.dashboard.test',compact('poster','category'))->render();
	    }	   	
    }

    public function getData($id_brand){
    	return Poster::where('id_user',Auth::User()->id)->where('brand_id',$id_brand)->paginate(1);
    }
}
