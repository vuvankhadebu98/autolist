<?php

namespace App\Http\Controllers\Client\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; 
use App\User;
use App\Model\Province;

class ProfileController extends Controller
{
    // Hiển thị trang cá nhân
    public function index($id){
    	$user = User::where('id',$id)->get();
        $data = select_by_id($id);
        $Province = Province::all();
    	return view('client.dashboard.profile',['data'=>$data,'user'=>$user,'Province'=>$Province]);
    }

    // Xử lí cập nhật trang cá nhân
    public function update(Request $Request){
    	// $validate = $Request->validate([
    	// 	'firstname' => 'required',
    	// 	'lastname' => 'required',
     //        'email' => 'required|unique:users',
     //        'phone' => 'number|min:10|max:10',
     //        'address' => 'required',
     //        'city' => 'required',
     //        'country' => 'required',
     //        'url_face' => 'required',
     //        'url_google' => 'required',
     //        'url_twitter' => 'required',
     //        'url_print' => 'required',
     //        'aboutme'=>'required',
     //        'img'=>'required',

     //    ],[
     //    	'firstname.required'=>"Firstname is not empty",
     //    	'lastname.required'=>"Lastname is not empty",
     //    	'email.required'=>"Email is not empty",
     //    	'phone.required'=>"Phone is not empty",
     //    	'address.required'=>"Address is not empty",
     //        'city.required'=>"City is not empty",
     //        'country.required'=>"Country is not empty",
     //        'url_face.required'=>"Address facebook is not empty",
     //        'url_google.required'=>"Address google is not empty",
     //        'url_twitter.required'=>"Address twitter is not empty",
     //        'url_print.required'=>"Address pinterest is not empty",
     //        'img.required'=>"Image is not empty",
     //    ]);
        if($Request->hasFile('img')){
            $user = User::where('id',$Request->id)->get();
            foreach ($user as  $value) {
                $url = 'img_user/';
                File::delete($url.$value->img);
            }
            $file_name = $Request->file('img')->getClientOriginalName();
            $Request->file('img')->move('img_user/',$file_name);
            $user = User::where('id',$Request->id)->update([
                'img'=>$file_name
            ]);
        }
        $user = User::where('id',$Request->id)->update([
            'firstname' => $Request->firstname,
            'lastname' => $Request->lastname,
            'email' => $Request->email,
            'phone' => $Request->phone,
            'address' => $Request->address,
            'city' => $Request->city,
            'country' => "Việt Nam",
            'url_face' => $Request->url_face,
            'url_google' => $Request->url_google,
            'url_twitter' => $Request->url_twitter,
            'url_print' => $Request->url_print,
            'detail_aboutme'=>$Request->aboutme,
        ]);
    	return back()->with('messages',"Cập nhật thành công");
    }
}
