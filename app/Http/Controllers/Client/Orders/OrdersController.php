<?php

namespace App\Http\Controllers\Client\Orders;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
class OrdersController extends Controller
{
    //
    public function index(Request $request){
    	$data = select_by_id(Auth::User()->id);
    	return view('client.orders.index',['data'=>$data]);
    }
}
