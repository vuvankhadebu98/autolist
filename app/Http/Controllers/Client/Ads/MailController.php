<?php

namespace App\Http\Controllers\Client\Ads;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Ads_detail;
class MailController extends Controller
{
	// Gửi mail liên hệ.
	public function sendContact(Request $Request) {
		$user_ads = select_by_id($Request->id_user_ads);
		$to_email = $user_ads['email'];
		$objDemo = new \stdClass();
		$objDemo->name = $Request->name_contact;
		$objDemo->messages = $Request->content_contact;
		Mail::to($to_email)->send(new Ads_detail($objDemo));
		return back();
	}
}
