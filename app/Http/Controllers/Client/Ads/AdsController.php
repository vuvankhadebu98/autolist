<?php

namespace App\Http\Controllers\Client\Ads;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File; 
use Illuminate\Http\Request;
use App\Model\Poster;
use App\Model\Category;
use App\Model\Province;
use App\Model\Like;
use App\Model\Comment;
use App\User;
use Str;
use DB;
use Auth;
use Carbon\Carbon;
use App\Model\accessary;
class AdsController extends Controller
{
    // Hiển thị trang thêm quảng cáo
    public function index(){
        $id_user = Auth::User()->id;
        $data = select_by_id($id_user);
        $category = Category::where('check',1)->get();
        $Province = Province::all();
        $user = select_by_id($id_user);
        $list_comment['name'] = $user['name'];
        $list_comment['email'] = $user['email'];
        $list_comment['city'] = $user['city'];
        $list_comment['img'] = $user['img'];
        $list_comment['phone'] = $user['phone'];
        $list_comment['address'] = $user['address'];
        $list_comment['city'] = $user['city'];
        $list_comment['id_city'] = $user['id_city'];
        $list_comment['face'] = $user['face'];
        $list_comment['google'] = $user['google'];
        $list_comment['twitter'] = $user['twitter'];
        $list_comment['print'] = $user['print'];
        return view('client.ads._add_ads',['Province'=>$Province,'list_comment'=>$list_comment,'data'=>$data,'category'=>$category]);
    }
    // Thêm quảng cáo
    public function postaddAds(Request $Request){
        if($Request->check_ == 1){
            // Thêm dữ liêu quảng cáo phụ tùng
            $id = DB::table('accessaries')->insertGetId([
                'user_id'=>Auth::User()->id,
                'name'=>$Request->title,
                'description'=>$Request->message,
                'price'=>$Request->price,
                'created_at'=>Carbon::now(),
                'package'=>$Request->radios1,
                'check'=>0, 
            ]);
            $img_detail = [];
            for($i=0;$i<$Request->qty_img_detail; $i++){
                $name = "img_".$i;
                array_push($img_detail,$Request->$name);
            }
            accessary::where('id',$id)->update([
                'img'=>$img_detail,
            ]);
        }
        else{
            //Thêm dữ liệu quảng cáo xe
            if($Request->hasFile('video_poster')){
                $video = $Request->file('video_poster');
                $name_video = $video->getClientOriginalName();
                $video->move('video_poster/',$name_video);
            }else{
                $name_video = "";
            }
            $id = DB::table('poster')->insertGetId([
                'id_user'=>Auth::User()->id,
                'title'=>$Request->title,
                'description'=>$Request->message,
                'price'=>$Request->price,
                'check_new'=>$Request->radios2,
                'created_at'=>Carbon::now(),
                'package'=>$Request->radios1,
                'check'=>$Request->check_, 
                'brand_id'=>$Request->brand,
                'category_id'=>$Request->category,
                'seats'=>$Request->seats,
                'km_went'=>$Request->km,
                'video'=>$name_video,
                'fuel_type'=>$Request->fuel,
                'transmission_type'=>$Request->transmission_type,
                'engine'=>$Request->engine,
                'enegine_displacement'=>$Request->enegine_displacement,
                'fuel_tank_capacity'=>$Request->fuel_tank_capacity,
                'break'=>$Request->break,
                'bootspace'=>$Request->bootspace,
                'arai_milage'=>$Request->arai_milage,
                'max_torque'=>$Request->max_torque,
                'max_power'=>$Request->max_power, 
            ]);
            $img_detail = [];
            for($i=0;$i<$Request->qty_img_detail; $i++){
                $name = "img_".$i;
                array_push($img_detail,$Request->$name);
            }
            Poster::where('id',$id)->update([
                'img'=>$img_detail,
            ]);
        }
        return redirect()->route('addAds')->with('messages',"Thêm thành công quảng cáo");
    }
    // --------- end thêm quảng cáo
    // Hiển thị trang chỉnh sửa quảng cáo
    public function edit($id){
        $data = [];
    	$poster = Poster::where('id',$id)->get();
        foreach ($poster as $value) {
            // Lấy thông tin người đăng
            $user = $value->id_user;
            $user = User::where('id',$user)->get();
            foreach ($user as $value_user) {
                $data['name'] = $value_user->firstname." ".$value_user->lastname;
                $data['email'] = $value_user->email;
                $data['img'] = $value_user->img;
                $data['phone'] = $value_user->phone;
                $data['street'] = $value_user->street;
                $data['city'] = $value_user->city;
                $data['address'] = $value_user->address;
            }
        }
        $Province = Province::all();
    	return view('client.ads.edit_ads',['poster'=>$poster,'data'=>$data,'Province'=>$Province]);
    }
    // Ajax save ảnh
    public function upImg(Request $Request){
        // Kiểm tra xem ảnh có tồn tại trong thư mục
            // if (File::exists($url.$Request->$name))
            // {
            //     if(isset($Request->$name)){
            //         array_push($img_detail,$Request->$name);
            //     }
        // }
        $filename = $Request->file('files');
        $name = $filename->getClientOriginalName();
        $filename->move('img_poster/',$name);
    }
    // Ajax xóa ảnh
    public function delImg(Request $Request){
        $filename = $Request->get('name_img_del');
        $url = 'C:\xampp\htdocs\autolist_2\public\img_poster\\';
        File::delete($url.$filename);
    }
    // Xử lí sửa thông tin quảng cáo
    public function postEdit(Request $Request){
        Poster::where('id',$Request->id_poster)->update([
            'title'=>$Request->title,
            'description'=>$Request->message,
            'price'=>$Request->price,
            'check_new'=>$Request->radios2,
            'updated_at'=>Carbon::now(),
            'package'=>$Request->radios1,
        ]);
        //Chèn ảnh quảng cáo
        $poster = new Poster;      
        $img_detail = [];
        if($Request->hasFile('files')){
            $Poster = new Poster;
            for($i=0;$i<$Request->qty_img_detail; $i++){
                $name = "img_".$i;
                array_push($img_detail,$Request->$name);
            }
            $poster->img = $img_detail;
            $poster->save();
        }
        if($poster->check_ == 0){
            // Chèn video quảng cáo
            if($Request->hasFile('video_poster')){
                $video = $Request->file('video_poster');
                $name_video = $video->getClientOriginalName();
                $video->move('video_poster/',$name_video);
                Poster::where('id',$Request->id_poster)->update([
                    'video'=>$name_video,
                ]);
            }
            // Chèn dữ liệu quảng cáo
            Poster::where('id',$Request->id_poster)->update([
                'brand_id'=>$Request->brand,
                'category_id'=>$Request->category,
                'seats'=>$Request->seats,
                'km_went'=>$Request->km,
                'fuel_type'=>$Request->fuel,
                'transmission_type'=>$Request->transmission_type,
                'engine'=>$Request->engine,
                'enegine_displacement'=>$Request->enegine_displacement,
                'fuel_tank_capacity'=>$Request->fuel_tank_capacity,
                'break'=>$Request->break,
                'bootspace'=>$Request->bootspace,
                'arai_milage'=>$Request->arai_milage,
                   'max_torque'=>$Request->max_torque,
                'max_power'=>$Request->max_power,         
            ]);      
        }
        return redirect()->route('getEditAdvertisement',['id'=>$Request->id_poster])->with('messages',"Sửa thành công quảng cáo");
    }
    // Xóa quảng cáo
    public function deleteAds($id){
        $Poster = Poster::where('id',$id)->get();
        foreach ($Poster as $value) {
            $url = 'C:\xampp\htdocs\autolist_2\public\img_poster\\';
            File::delete($url.$value->img);       
        }     
        $Poster = Poster::where('id',$id)->delete();       
        return back()->with('messages','Xóa thành công'); 
    }
    // HIển thị danh sách quảng cáo
    public function searchPost(Request $Request){
        $Poster = Poster::where("title",'LIKE','%'.$Request->car_search.'%')->where('category_id',$Request->cate_search)->get();
        return view('client.ads.list_ads',['Poster'=>$Poster]);  
    }

    // like quảng cáo
    public function like(Request $Request){
        $like = Like::where('poster_id',$Request->get('id_poster'))->where('user_id',$Request->get('id_user'))->where('status',0)->get();
        if(count($like) == 0 ){
            DB::table('like')->insert([
                'status' => 0,
                'poster_id'=>$Request->get('id_poster'),
                'user_id'=>$Request->get('id_user'),
                'created_at'=>Carbon::now()
            ]);
        }else{
            Like::where('poster_id',$Request->get('id_poster'))->where('user_id',$Request->get('id_user'))->where('status',0)->delete();
        }
        $count = Like::where('poster_id',$Request->get('id_poster'))->where('status',0)->get();
        echo $count = count($count);
    }
    // ------------ end like quảng cáo
    // Xử lí rate
    public function rate(Request $Request){
        $rate = $Request->get('rate') + 1;
        DB::table('comment')->insert([
            'poster_id'=>$Request->get('id_poster'),
            'rate'=>$rate,
            'user_id'=>$Request->get('id_user'),
            'created_at'=>Carbon::now()
        ]);
        $rate = Comment::where('poster_id',$id_poster)->where('content',null)->get();
            $avg_rate = count($rate);
            if($avg_rate > 0){
                $avg = 0;
                foreach ($rate as $value_rate) {
                    $avg += $value_rate->rate;
                }
                $avg /= $avg_rate;
            }else{
                $avg = 0;
            }
        $all_poster_by_brand = Poster::where('brand_id',$value->brand_id)->get();
    }
    // ---------- end xử lí rate
    // Xử lí binh luận
    public function sendReply(Request $Request){
        $validate = $Request->validate([
            'your_name_reply' => 'required',            
            'email_reply' => 'required|email',
            'textarea_reply'=>'required'
        ],[         
            'your_name_reply.required'=>"Name không được để trống",       
            'email_reply.required'=>"Email hông được để trống",
            'textarea_reply.required'=>"Bình luận không được để trống",
            'email_reply.email'=>"Điah chỉ không đúng",
        ]);
        DB::table('comment')->insert([
            'content'=>$Request->textarea_reply,
            'poster_id'=>$Request->poster_id,
            'user_id'=>Auth::User()->id,
            'created_at'=>Carbon::now()
        ]);
        return back()->with('messages','Đã gửi bình luận'); ;
    }
    // ------------ end xử lí bình luận
    // Xử lí rep comment
    public function rep_comment(Request $Request){
        $data = [];
        $Request->validate([
            'rep_comment' => 'required',            
        ],[         
            'rep_comment.required'=>"Xin mời điền câu trả lời",       
        ]);
        $id_comment = DB::table('comment')->insertGetId([
            'content'=>$Request->get('rep_comment'),
            'poster_id'=>$Request->get('id_poster'),
            'user_id'=>$Request->get('id_user'),
            'comment_id'=>$Request->get('comment_id'),
            'created_at'=>Carbon::now(),
        ]); 
        $data['id'] = $id_comment;
        $data['content'] = $Request->get('rep_comment');
        return $data; 
    }
    // ------------ end xử lí rep comemnt
    // Hiển thị chi tiết quảng cáo
    public function getDetail($id){
        # Lấy thông tin quảng cáo
        $poster = Poster::where('id',$id)->get();
        foreach ($poster as $value) {
            $id_poster = $value->id;
            // Lấy thông tin người đăng
            $data_info_user = select_by_id($value->id_user); 
            $user = User::where('id',$value->id_user)->get();
            // ------------- end lấy thông tin người đăng 
            // Kiểm tra đánh giá sản phẩm
            $check = count(Comment::where('user_id',$data_info_user['id'])->where('rate',"!=",null)->where('poster_id',$id_poster)->get());
            if($check > 0){
                $check = 1;
            }else{
                $check = 0;
            }
            // ------------- end kiểm tra đánh giá sản phẩm 
            // Lấy số lượt like
            $like = count(Like::where('poster_id',$id)->where('status',0)->get());
            //-------------- end lấy like
            // Lấy các comment
            $comment = Comment::where('poster_id',$id_poster)->where('content','!=',null)->get();
            $list_comment = [];
            $i = 0;
            foreach ($comment as $value_comment) {
                $list_comment[$i]['id'] = $value_comment->id;
                $list_comment[$i]['content'] = $value_comment->content;
                $list_comment[$i]['rate'] = $value_comment->rate;
                $list_comment[$i]['comment_id'] = $value_comment->comment_id;
                $list_comment[$i]['created_at'] = $value_comment->created_at;
                $_user = select_by_id($value_comment->user_id);
                $list_comment[$i]['id_user'] = $_user['id'];
                $list_comment[$i]['name'] = $_user['name'];
                $list_comment[$i]['email'] = $_user['email'];
                $list_comment[$i]['city'] = $_user['city'];
                $list_comment[$i]['img'] = $_user['img'];
                $list_comment[$i]['phone'] = $_user['phone'];
                $list_comment[$i]['address'] = $_user['address'];
                $list_comment[$i]['face'] = $_user['face'];
                $list_comment[$i]['google'] = $_user['google'];
                $list_comment[$i]['twitter'] = $_user['twitter'];
                $list_comment[$i]['print'] = $_user['print'];
                $i++;
            }
            //-------------- end lấy comment 
            // Lấy trung bình rate
            $rate = Comment::where('poster_id',$id_poster)->where('content',null)->get();
            $avg_rate = count($rate);
            if($avg_rate > 0){
                $avg = 0;
                foreach ($rate as $value_rate) {
                    $avg += $value_rate->rate;
                }
                $avg /= $avg_rate;
            }else{
                $avg = 0;
            }
            //-------------- end lấy trung bình rate
            $all_poster_by_brand = Poster::where('brand_id',$value->brand_id)->get();

        };
        return view('client.ads.ad_details',['poster'=>$poster,'like'=>$like,'check'=>$check,'data_info_user'=>$data_info_user,'list_comment'=>$list_comment,'avg'=>$avg,'all_poster_by_brand'=>$all_poster_by_brand]);
    }
    // ------------- end hiển thị chi tiết quảng cáo 
    
}