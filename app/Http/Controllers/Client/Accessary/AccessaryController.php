<?php

namespace App\Http\Controllers\Client\Accessary;

use App\Http\Controllers\Controller;
use App\Model\Like;
use Illuminate\Http\Request;
use App\Model\accessary;
use Illuminate\Support\Facades\Auth;

class AccessaryController extends Controller
{
    //
    public function getindex($id){
        if($user = Auth::user()){
            $favorite = Like::where('user_id',$user->id)->where('accessary_id',$id)->where('status', 1)->first();
            if($favorite){
                $accessary = accessary::where('id',$id)->get();
                return view('client.accessary.test',['accessary'=>$accessary, 'favorite'=>$favorite]);
            }
            $accessary = accessary::where('id',$id)->get();
            return view('client.accessary.test',['accessary'=>$accessary]);
        }

        $accessary = accessary::where('id',$id)->get();
        return view('client.accessary.test',['accessary'=>$accessary]);
    }
}
