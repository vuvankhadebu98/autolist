<?php

namespace App\Http\Controllers\Client\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Blog;
use App\Model\Comment;
use App\Model\Tag;
use App\Model\Category_blog;
use App\Model\Blog_tag;
use App\User;
use Auth;

class BlogController extends Controller
{
    public function listBlog(Request $req){
        $data['list_blog'] = Blog::orderBy('id','desc')->Paginate(4);
        $data['category_blog'] = Category_blog::all();
        $data['tag_blog_id'] = Blog_tag::join('blogs','blogs.id','=', 'blog_tag.blog_id')->get();

        $data['author_blog'] = User::where('role_id','=',10)->limit(5)->get();


    	return view('client.blog.list_blog',$data);
    }
    public function listBlogID(Request $req,$id){
        $data['list_blog1'] = Blog::join('category_blog','category_blog.id','=','blogs.category_blog_id')
                    ->where('category_blog_id',$req->id)
                    ->select('blogs.*')
                    ->get();
        $data['category_blog'] = Category_blog::all();        
        $data['tag_blog_id'] = Blog_tag::join('blogs','blogs.id','=', 'blog_tag.blog_id')->get();
        $data['author_blog'] = User::where('role_id','=',10)->limit(5)->get();

       //  $blog_id = $id;
       // $blog =Blog::find($id); 
       // $comment = new Comment;
       // $comment->blog_id = $blog_id;
       // $comment->user_id = Auth::user()->id;
       // $comment->content = $request->content;

       // $comment->save();

        return view('client.blog.list_blog_id',$data);
    }
     public function detailBlog(Request $req){
    	$data['detail_blog'] = Blog::where('id',$req->id)->first();
        $data['category_blog'] = Category_blog::all();
        $data['tag_blog_id'] = Blog_tag::join('blogs','blogs.id','=', 'blog_tag.blog_id')->get();
        $data['comment_blog'] = Comment::join('blogs','comment.blog_id','=','blogs.id')
                            ->where('blog_id',$req->id)
                            ->get();
        $data['author_blog'] = User::where('role_id','=',10)->limit(5)->get();
        $data['id']=$req->id;
        // echo $data['id'];

    	return view('client.blog.detail_blog',$data);
    }
    public function postComment(Request $request,$id)
    {
       $blog_id = $id;
       $blog =Blog::find($id); 
       $comment = new Comment;
       $comment->blog_id = $blog_id;
       $comment->user_id = Auth::user()->id;
       $comment->content = $request->content;
       $comment->save();
       return redirect()->back()->with('thongbao','Viết bình luận thành công');

    }
}
