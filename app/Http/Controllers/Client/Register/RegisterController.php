<?php

namespace App\Http\Controllers\Client\Register;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use DB;
use App\User;
use Carbon\Carbon;
class RegisterController extends Controller
{
    //
    public function index(){
    	return view('Client.Register.register_');
    }
    public function register(Request $Request){
    	$validate = $Request->validate([
    		'firstname' => 'required',
    		'lastname' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required|min:6',
            'confirm_password'=>'required|min:6|same:password',
        ],[
        	'firstname.required'=>"Firstname is not empty",
        	'lastname.required'=>"Lastname is not empty",
        	'username.required'=>"Username is not empty",
        	'email.required'=>"Email is not empty",
        	'password.required'=>"Password is not empty",
        	'confirm_password.required'=>"Confim_password is not empty",
            "username.unique"=>"Username already exists",
            "password.min" => "Password is at least 6 characters",
            "confirm_password.min" => "Password is at least 6 characters",
            "confirm_password.same"=>"Password incorrect"
        ]);
        $token = bcrypt(mt_rand(0, 1000));
        $id = DB::table('users')->insertGetId([
            'firstname'=>$Request->firstname,
            'lastname'=>$Request->lastname,
            'username'=>$Request->username,
            'email'=>$Request->email,
            'role_id'=>$Request->role_id,
            'password'=>bcrypt($Request->password),
            'created_at'=>Carbon::now(),
            'remember_token'=>$token,

            'status'=>0
        ]);
        $to_email = $Request->email;
        $objDemo = new \stdClass();
        $objDemo->name = $Request->firstname." ".$Request->lastname;
        $objDemo->username = $Request->username;
        $objDemo->email = $Request->email;
        $objDemo->password = $Request->password;
        $objDemo->url = route('getConfirm_email',['token'=>$token,'id'=>$id]);
        Mail::to($to_email)->send(new SendMail($objDemo));
        return back();
    }

    public function confirm_email($token,$id){
        $user = User::where('id',$id)->get();
        foreach($user as $value_user){
            if($value_user->status == 0){
                if($value_user->remember_token == $token){
                    DB::table('users')->update([
                        'status'=>1
                    ]);
                    return redirect()->route('getRegister')->with('messages','Xác thực thành công');
                }else{
                    return redirect()->route('getRegister')->with('messages','Không thể xác thực');
                }
            }else{
                return redirect()->route('getRegister')->with('messages','Tài khoản đã được xác thực');;
            }
        }
    }
}
