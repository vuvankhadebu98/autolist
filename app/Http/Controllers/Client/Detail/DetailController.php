<?php

namespace App\Http\Controllers\client\Detail;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Product;
use App\ProductValue;
use App\Attribute;
use App\Model\Category;
use App\Model\Poster;
use DB;
use App\Model\Comment;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Str;


class DetailController extends Controller
{
    public function index(Request $request,$id)
    {
    	$product = Product::find($id);
        $productt = Product::all();
        foreach ($productt as $value) {
            $description = $value->name;
            $keyword = $value->price;
            $url_canonical = $request->url();
            $title = "Car-Detail-01";
        }
        $productId = DB::table('products')->pluck('id');
    	$product_val = ProductValue::all();
    	$attri = Attribute::all();

    	$category = Category::all();
        $comment = Comment::all();
        $comment2 = Comment::all();
        $users = User::all();

        $attrOfProduct = ProductValue::where('product_id',$id)->select('attribute_id')->pluck('attribute_id')->toArray();
        $stringAtt = implode(',',$attrOfProduct);
        
        $attr = Attribute::select('id')->pluck('id')->toArray();
        $stringa = implode(',', $attr);
        $stringA = explode(',', $stringa);

        $c = crc32($stringa);
        
        $in = array_values($attrOfProduct);
        $a = Attribute::select('id')->get();
        $idUser = Auth::id();

    	return view('client.detail.Car_detail_01',
    		compact('product','product_val','attri','category','productt',
                'comment','users','productId','attrOfProduct','attr','stringAtt','stringa','stringA','description','keyword','url_canonical','title','comment2','idUser'));
    }

    public function comment(Request $req)
    {
        $id = Auth::id();
        $datetime = Carbon::now();
        $content = $req->get('comment');
        $productId = $req->get('productID');
        $idComment = $req->get('idComment');
        $commentRep = $req->get('commentRep');
        // dd($c, $b);
        if(!Auth::check()){
            return 0;
        }else{
           $comment = Comment::insert([
            'content' => $content,
            'user_id' =>  $id, 
            'product_id' => $productId,
            'created_at' => $datetime,
            // 'poster_id' => , 
                
            ]);
            return 1;
        }
    }
    // detail_quản cáo...........................................................................
    public function show_detail_poster(Request $request,$id){
        // $user_home = DB::table('users')->where('id',$id)->get();
        $detail_poster = Poster::where('id',$id)->where('status','1')->get();
        return view('client.detail.detail_poster.show_detail_poster',compact('detail_poster'));    
    }
    // 
    public function Repcomment(Request $req)
    {
        $id = Auth::id();
        $productId = $req->get('productID');
        $idComment = $req->get('idComment');
        $commentRep = $req->get('commentRep');
        $datetime = Carbon::now();
        // dd($c, $b);
        if(!Auth::check()){
            return 0;
        }else{
           $comment = Comment::insert([
            'content' => $commentRep,
            'user_id' =>  $id, 
            'product_id' => $productId,
            'comment_id' => $idComment,
            'created_at' => $datetime,
            // 'poster_id' => , 
                
            ]);
            return 1;
        }
    }

    public function like_product($id_product)
    {
        Product::where('id',$id_product)->update(['like'=>1]);
        return redirect()->route('Car-Detail-01',$id_product);
    }

    public function unlike_product($id_product)
    {
        Product::where('id',$id_product)->update(['like'=>0]);
        return redirect()->route('Car-Detail-01',$id_product);
    }
}
