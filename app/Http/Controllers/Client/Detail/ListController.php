<?php

namespace App\Http\Controllers\client\Detail;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\Category;
use App\Model\Brand;
use DB;
use Paginate;
use Carbon\Carbon;

class ListController extends Controller
{
	//<------------List snar phẩm-------->
    public function ListCar(Request $request)
    {
    	$product = Product::paginate(2);
    	$pro = Product::all();
        foreach ($pro as $value) {
            $description = $value->name;
            $keyword = $value->price;
            $url_canonical = $request->url();
            $title = "Car-List";
        }
    	$cons = Category::withCount('productId')->get();
    	$brand = Brand::all();
        $test= Carbon::now('Asia/Ho_Chi_Minh')->toDateString();
    	// if ($product->promotion > 0) {
    	// 	$tien = ($product->price)- ($product->price * $product->promotion/100);
    	// }
    	// dd($tien);
        return view('client.detail.Car_List',compact('product','cons', 'brand','pro','description','keyword','url_canonical','title','test'));
    }

    public function show_category($id){
        $all_product_cate = Product::where('category_id',$id)->get();
        return view('client.category.show_category',compact('all_product_cate'));
    }

//<------------sắp xếp sản phẩm theo giá giảm dần-------->
    public function ajaxPriceDecrease()
    {
    	// $dat = Product::orderBy('price','desc')->get();
    	// $data = $dat->paginate(2);
    	$data = Product::orderBy('price','desc')->get();
    	return view('client.detail.Sort_price',compact('data'));
    }

//<------------sắp xếp sản phẩm theo giá tăng dần-------->
    public function ajaxPriceAscending()
    {
    	$data = Product::orderBy('price','asc')->get();
    	return view('client.detail.Sort_price',compact('data'));
    }

//<------------Tìm kiếm sản phẩm-------->
    public function search(Request $request)
    {
    	if ($request->get('query')) {
    		$query = $request->get('query');
    		$data = DB::table('products')
    					->where('name', 'LIKE', '%'.$query.'%')
    					->orWhere('price', 'LIKE', '%'.$query.'%')
    					->get();
    		return view('client.detail.Live_Search',compact('data'));
    	}
    }

//<------------Lọc sản phẩm-------->
    public function filter(Request $req)
    {
    	//<----tạo biến để hứng dữ liệu từ Car_List.blade.php ----->
    	$categores = $req->get('categores');
        $brand = $req->get('brand');
    	$fuel_type = $req->get('fuel_type');
    	$transmission_type = $req->get('transmission_type');
    	$min_price = $req->get('min_price');
    	$max_price = $req->get('max_price');
    	//<----join bảng để lọc theo danh mục----->
    	$join = DB::table('products')
    				->join('categories','products.category_id','categories.id')
                    ->join('brand','products.brand_id','brand.id')
    				->select('products.*');
    	if(!empty($categores)){
           $join = $join->whereIn('products.category_id', $req->get('categores'));
        }
        if(!empty($brand)){
           $join = $join->whereIn('products.brand_id', $req->get('brand'));
        }
        //<------Lọc theo kiểu nhiên liệu------>
        if (!empty($fuel_type)) {
        	 $join  = $join->whereIn('products.fuel_type', $req->get('fuel_type'));
        }
        //<------Lọc theo kiểu hộp số------->
        if (!empty($transmission_type)) {
        	$join = $join->whereIn('products.transmission_type', $req->get('transmission_type'));
        }
        //<------Lọc theo khoảng giá-------->
        if (!empty($min_price) and !empty($max_price)) {
        	$join = $join->whereBetween('products.price',[$req->get('min_price'),$req->get('max_price')]);
        }
        $data = $join->orderBy('products.id','desc')->get();
        if($data->count() > 0){
            return view('client.detail.Live_Search', compact('data'));
        }
        return '<h3 style="text-align: center;color: #d6301a;">
        			không có sản phẩm nào!
        		</h3>';
    }
}
