<?php

namespace App\Http\Controllers\Client\Favorite;

use App\Http\Controllers\Controller;
use App\Model\Accessary;
use Illuminate\Http\Request;
use App\Model\Like;
use App\User;
use App\Model\Poster;
use App\Model\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class FavoriteController extends Controller
{
    // Hiển thị my favorite
    public function index($id)
    {
        if ($user = Auth::user()) {
            $like = Like::where('user_id',$user->id)->where('accessary_id',$id)->first();
            if ($like) {
                $message =  "Bạn đã thích sản phẩm này";
                return response()->json([
                    'success' => false,
                    'message' => $message
                ]);
            } else {
                $likes = new Like;
                $likes->user_id = $user->id;
                $likes->accessary_id = $id;
                $success = $likes->save();
                if ($success) {
                    $like_acc = Like::where('accessary_id', $id)->get();
                    $count_accessary = count($like_acc);
                    $accessary = Accessary::find($id);
                    $accessary->favorite = $count_accessary;
                    $accessary->save();

                    return response()->json([
                        'success' => true,
                        'message' => 'success'
                    ]);
                } else {
                    $message =  "Thêm vào thất bại";
                    return response()->json([
                        'succes'=>false,
                        'message'=>$message
                    ]);
                }
            }
        }
        else{
            $message =  "Bạn chưa đăng nhập";
            return response()->json([
                'succes'=>false,
                'message'=>$message
            ]);
        }


        $data = select_by_id($id);
        dd($data);
//    	$poster = Poster::all();
//        $category = Category::all();
//    	// $favorite = Like::where('user_id',$id)->get()->paginate(10);
//        $favorite = DB::table('Like')->where('user_id',$id)->paginate(2);
//    	return view('client.dashboard.my_favorite',['data'=>$data,'favorite'=>$favorite,'category'=>$category,'poster'=>$poster]);
//
    }

    // Xóa yêu thích
    public function delete($id)
    {
        Like::where('id', $id)->delete();
        return back()->with("messages", "Xóa thành công.");
    }


    public function getfavorite(){
        $farr = DB::table('accessaries')->paginate(4);
        $data = select_by_id(Auth::User()->id);
        return view('client.favorite.index',['farr'=>$farr,'data'=>$data]);
    }
}
