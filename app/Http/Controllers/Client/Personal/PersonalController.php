<?php

namespace App\Http\Controllers\Client\Personal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Province;
use App\Model\Poster;
class PersonalController extends Controller
{
    // Hiển thị thông tin user
    public function index($id){
    	$User = User::where("id",$id)->get();
    	$Poster = Poster::where("id_user",$id)->get();
    	foreach ($User as $value) {
    		$Province = Province::where('id',$value->city)->get();
    		foreach ($Province as $value_city) {
    			$Province = $value_city->_name;
    		}
    	}
    	return view('client.personal.personal',['User'=>$User,'Province'=>$Province,'Poster'=>$Poster]);
    }
    // Lấy thông tin user (Hàm khi trang có ajax lấy thông tin người dùng)
    public function AjaxGetInfo(Request $Request){
        $data = select_by_id($Request->get('id_user'));
        return $data;
    }
}
