<?php

namespace App\Http\Controllers\Client\Service;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Service;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\User;

class ServiceController extends Controller
{
    public function showroomm(){
        $data = select_by_id(Auth::User()->id);
//        dd($data);

        return view('client.service.add',compact('data'));
    }
    public function postshowroom(Request $request){
        $data = $request->all();
        unset($data['_token']);
//        $data = array();
////        dd($request);
//        $data['id'] =  $request->id;
//        $data['name'] =  $request->name;
//        $data['price'] =  $request->price;
//        $data['content'] =  $request->content;
//        $imagee=$request->img_detail;
        $get_image = $request-> file('img_detail');
        if($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.', $get_name_image));
            $new_image = $name_image .  '.' . $get_image->getClientOriginalExtension();
            $get_image->move('upload/', $new_image);
            $data['img_detail'] = $new_image;
        }
        DB::table('services')->insert($data);
        return Redirect()->route('listsr')->with('messages','Thêm thành công');
    }
        public function index(){
        $list= Service::all();
        $data = select_by_id(Auth::User()->id);
//        dd($list);
        return view('client.service.index',compact('list','data'));
        }

        public function delete($id){
            $service=Service::where('id',$id)->delete();
            return Redirect()->route('listsr',compact('service'))->with('messages','Xóa thành công');
        }

        public function editservice($id){
        $servicee= Service::where('id',$id)->find($id);
            $data = select_by_id(Auth::User()->id);
           return view('client.service.edit',compact('servicee','data'));
        }
        public function posteditservice(Request $request, $id){
            $data=$request->all();
            unset($data['_token']);
            $get_image = $request-> file('img_detail');
            if($get_image) {
                $get_name_image = $get_image->getClientOriginalName();
                $name_image = current(explode('.', $get_name_image));
                $new_image = $name_image  . '.' . $get_image->getClientOriginalExtension();
                $get_image->move('upload/', $new_image);
                $data['img_detail'] = $new_image;
            }
//            dd($data);
            DB::table('services')->where('id',$id)->update($data);
            return Redirect()->route('listsr')->with('messages','Sửa thành công');
        }




}
