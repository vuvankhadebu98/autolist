<?php

namespace App\Http\Controllers\Client\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Auth;
use App\User;
use DB;
class LoginController extends Controller
{
    // Hiển thị trang đăng nhập
	public function index(){
		return view('client.login.login');
	}
	
	// Xử lí đăng nhập
	public function postLogin(Request $Request){
		$validate = $Request->validate([
			'username' => 'required',            
			'password' => 'required|min:6',
		],[        	
			'username.required'=>"Username is not empty",     	
			'password.required'=>"Password is not empty",
			"password.min" => "Password is at least 6 characters",
		]);
		if(Auth::attempt(['username'=>$Request->username,'password'=>$Request->password])){
			return redirect()->route('getHome')->with('messages','Đăng nhập thành công!');
		}
		else
			return back()->withInput()->with('alert','Không đúng tài khoản hoặc mật khẩu!');
	}

	// Hiển thị trang gửi mail lấy mật khẩu
	public function forgotPassword(){
		return view('client.login.forgot_password');
	}

	// Xử lí lấy mật khẩu
	public function postforgotPassword(Request $Request){
		$validate = $Request->validate([
			'email' => 'required',
		],[			
			'email.required'=>"Email is not empty",
		]);
		$token = bcrypt(mt_rand(0, 1000));
		$user = User::where('email',$Request->email)->get();
		foreach ($user as $value) {
			$id = $value->id;
			DB::table('users')->where('id',$id)->update([	          
	            'remember_token'=>$token,
	        ]);
		}
		$user = User::where('email',$Request->email)->get();
		foreach ($user as $value) {
			$to_email = $Request->email;
			$objDemo = new \stdClass();
			$objDemo->name = $value->firstname." ".$value->lastname;
			$objDemo->token = $value->remember_token;
			$objDemo->id = $value->id;
			$objDemo->url = route('edit_password');
			Mail::to($to_email)->send(new SendMail($objDemo));
		}
		return back()->with('messages','Đã gửi email xác nhận');
	}

	// Hiển thị trang chỉnh sửa mật khẩu
	public function edit_password(Request $Request){
		$user = User::where('id',$Request->id)->get();
		foreach ($user as $value) {
			if($Request->token == $value->remember_token){
				return view('client.login.edit_password',['username'=>$value->username,'id'=>$Request->id]);
			}else{
				echo "Không đúng token";
			}
		}	
	}

	// Xử lí thay đổi password
	public function postedit_password(Request $Request){
		$validate = $Request->validate([         
			'password' => 'required|min:6',
            'confirm_password'=>'required|min:6|same:password',
		],[        	
        	'password.required'=>"Password is not empty",
        	'confirm_password.required'=>"Confim_password is not empty",
            "password.min" => "Password is at least 6 characters",
            "confirm_password.min" => "Password is at least 6 characters",
            "confirm_password.same"=>"Password incorrect"
		]);
		$token = bcrypt(mt_rand(0, 1000));
		DB::table('users')->where('id',$Request->id)->update([            
            'password'=>bcrypt($Request->password), 
            'remember_token'=>$token,          
        ]);
        return redirect()->route('getLogin')->with('messages','Đổi mật khẩu thành công');
	}

	// Cố tình vào trang đổi mật khẩu
	public function erroredit_password(){
		return redirect()->route('forgotPassword');
	}

	// Đăng xuất
	public function logout(){
		Auth::logout();
		return redirect()->route('getHome');
	}
}
