<?php

namespace App\Http\Controllers\Client\Home;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Model\Product;
use App\User;
use App\Model\Brand;
use App\Model\Like;
use App\Model\Blog;
use Carbon\Carbon;
use App\Model\Poster;
use App\Model\Category;
use App\Model\Province;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Model\accessary;
use App\Model\Service;
class HomeController extends Controller
{
    //
    public function index(Request $req){
    	$data['category'] = category::all();
        $data['accessary'] = accessary::all();
    	$data['product'] = Product::where('transmission_type','>',0)->get();
        $data['products'] = Product::where('transmission_type','>',0)->orderBy('id','desc')->get();
        $data['brand'] = Brand::where('id','>',0)->limit(6)->get();
        //tabs
        $data['productTrendAll'] = Product::where('status',1)->orderBy('id','desc')->get();
    	$data['productTrendID1'] = Product::where('brand_id','=',1)->where('status',1)->orderBy('id','desc')->get();
        $data['productTrendID2'] = Product::where('brand_id','=',2)->where('status',1)->orderBy('id','desc')->get();
        $data['productTrendID3'] = Product::where('brand_id','=',3)->where('status',1)->orderBy('id','desc')->get();
        $data['productTrendID4'] = Product::where('brand_id','=',4)->where('status',1)->orderBy('id','desc')->get();
        $data['productTrendID5'] = Product::where('brand_id','=',5)->where('status',1)->orderBy('id','desc')->get();
        $data['post'] = Blog::where('status','=',1)->limit(5)->orderBy('id','desc')->get();
    	//đếm
    	$data['countCus'] = User::where('role_id',1)->count();
    	$data['carsSale'] = Product::join('order_detail','products.id','=','order_detail.product_id')
                            ->join('order','order_detail.id','=','order.id')
                            // ->where('status',)
                            ->where('check',1)
                            ->count();
    	$data['rentedCars'] =Product::join('order_detail','products.id','=','order_detail.product_id')
                            ->join('order','order_detail.id','=','order.id')
                            // ->where('status',)
                            ->where('check',2)
                            ->count();
    	$data['happy'] = User::where('role_id',10)->count();
        //recent news
        $data['recent'] = Blog::where('status',1)->orderBy('id','desc')->limit(10)->get();

        //============================================================================================
        // show cateogry ở sản phẩm..........................
        $data['category_home'] = Category::where('check',0)->get();

        // show chi tiết phụ tùng 
        $data['phu_tung_b'] = accessary::where('check',0)->get();
        $data['phu_tung_qc'] = accessary::where('check',1)->get();
         //   của nam
        //============================================================================================
        $data['ads_pt'] = Poster::where('status',0)->get();
        $data['ads_car'] = Poster::where('status',1)->get();
        $data['all_poster'] = Poster::all();

        return view('client.home.index',$data);
    }
    public function about(){
        $data['countCus'] = User::where('role_id',1)->count();
        $data['carsSale'] = Product::join('order_detail','products.id','=','order_detail.product_id')
                            ->join('order','order_detail.id','=','order.id')
                            // ->where('status',)
                            ->where('check',1)
                            ->count();
        $data['rentedCars'] =Product::join('order_detail','products.id','=','order_detail.product_id')
                            ->join('order','order_detail.id','=','order.id')
                            // ->where('status',)
                            ->where('check',2)
                            ->count();
        $data['happy'] = User::where('role_id',10)->count();
        return view('client.about.about',$data);
    }
    // // Tìm kiếm list_ads_search
    // public function search_poster(Request $Request){
    // 	if($Request->ajax()) {
    // 		$seats = $Request->get('seats');
    //         dd($seats);
    // 		$id_brand = $Request->get('id_brand');
    //         $price = $Request->get('price');
    //         $city = $Request->get('city');  
    //         $order_by = $Request->get('order_by');          
	   //      $poster = $this->getData($id_brand,$seats,$price,$city,$order_by);
    //         $type_show = $Request->get('type_show');
	   //      $view = view('client.home.search',compact('poster','type_show','order_by','poster_qt'))->render();
	   //      return response($view);
	   //  }	   	
    // }
    // Xử lí phân trang
    public function poster_pagination(Request $Request){
    	if($Request->ajax()) {
    		$seats = $Request->get('seats');
    		$id_brand = $Request->get('id_brand');
            $price = $Request->get('price');
	        $city = $Request->get('city');
            $order_by = $Request->get('order_by');
            $poster = $this->getData($id_brand,$seats,$price,$city,$order_by);
            $type_show = $Request->get('type_show');
	        return view('client.home.search',compact('poster','type_show','order_by'))->render();
	    }
    }
    // Xử lí tìm kiếm
    public function getData($name,$id_brand,$seats,$price,$city,$order_by,$search_type){
        if($search_type == 0){
        	$poster = Poster::query();
            $poster = Poster::join('users','users.id','=','poster.id_user');
            if($name != ""){
                $poster->where('title', 'LIKE', "%".$name."%");
            }
        	if($id_brand != ""){
        		$poster->where('brand_id',$id_brand);
        	}
        	if($seats != ""){
        		$poster->where('seats',$seats);
        	}
        	if($price != ""){
        		if($price == 1){
        			$a = 0;
        			$b = 10;
        		}
        		if($price == 2){
        			$a = 10;
        			$b = 20;
        		}
        		if($price == 3){
        			$a = 20;
        			$b = 30;
        		}
        		if($price == 4){
        			$a = 30;
        			$b = 40;
        		}
        		if($price == 5){
        			$a = 40;
        			$b = 50;
        		}
        		if($price == 6){
        			$a = 50;
        			$b = 60;
        		}
        		if($price == 7){
        			$a = 60;
        			$b = 70;
        		}
        		if($price == 8){
        			$a = 70;
        			$b = 80;
        		}
        		if($price == 9){
        			$a = 80;
        			$b = 999999999999999;
        		}
        		$poster->where('price','>=',$a)->where('price','<=',$b);
        	}
            if($city != ""){
                $poster->where('city',$city);
            }
            if($order_by==0){
                return $poster->where('check',0)->paginate(5);
            }
            if($order_by==1){
                return $poster->where('check',0)->orderBy('price','desc')->paginate(5);
            }
            if($order_by==2){
                return $poster->where('check',0)->orderBy('view','desc')->paginate(5);
            }
            if($order_by==3){
                return $poster->where('check',0)->orderBy('rate','desc')->paginate(5);
            }
            if($order_by==4){
                return $poster->where('check',0)->orderBy('created_at','asc')->paginate(5);
            }
        }
        if($search_type == 1){
            $accessary = accessary::query();
            $accessary = accessary::join('users','users.id','=','accessaries.user_id');
            if($name != ""){
                $accessary->where('title', 'LIKE', "%".$name."%");
            }
            if($id_brand != ""){
                $poster->where('brand_id',$id_brand);
            }
            if($price != ""){
                if($price == 1){
                    $a = 0;
                    $b = 10;
                }
                if($price == 2){
                    $a = 10;
                    $b = 20;
                }
                if($price == 3){
                    $a = 20;
                    $b = 30;
                }
                if($price == 4){
                    $a = 30;
                    $b = 40;
                }
                if($price == 5){
                    $a = 40;
                    $b = 50;
                }
                if($price == 6){
                    $a = 50;
                    $b = 60;
                }
                if($price == 7){
                    $a = 60;
                    $b = 70;
                }
                if($price == 8){
                    $a = 70;
                    $b = 80;
                }
                if($price == 9){
                    $a = 80;
                    $b = 999999999999999;
                }
                $accessary->where('price','>=',$a)->where('price','<=',$b);
            }
            if($city != ""){
                $accessary->where('city',$city);
            }
            if($order_by==0){
                return $accessary->where('check',0)->paginate(5);
            }
            if($order_by==1){
                return $accessary->where('check',0)->orderBy('price','desc')->paginate(5);
            }
            if($order_by==2){
                return $accessary->where('check',0)->orderBy('view','desc')->paginate(5);
            }
            if($order_by==3){
                return $accessary->where('check',0)->orderBy('rate','desc')->paginate(5);
            }
            if($order_by==4){
                return $accessary->where('check',0)->orderBy('created_at','asc')->paginate(5);
            }
        }
        if($search_type == 2){
            $service = Service::query();
            if($name != ""){
                $service->where('title', 'LIKE', "%".$name."%");
            }
          
            if($price != ""){
                if($price == 1){
                    $a = 0;
                    $b = 10;
                }
                if($price == 2){
                    $a = 10;
                    $b = 20;
                }
                if($price == 3){
                    $a = 20;
                    $b = 30;
                }
                if($price == 4){
                    $a = 30;
                    $b = 40;
                }
                if($price == 5){
                    $a = 40;
                    $b = 50;
                }
                if($price == 6){
                    $a = 50;
                    $b = 60;
                }
                if($price == 7){
                    $a = 60;
                    $b = 70;
                }
                if($price == 8){
                    $a = 70;
                    $b = 80;
                }
                if($price == 9){
                    $a = 80;
                    $b = 999999999999999;
                }
                $service->where('price','>=',$a)->where('price','<=',$b);
            }
            if($city != ""){
                $service->where('city',$city);
            }
            if($order_by==0){
                return $service->where('check',0)->paginate(5);
            }
            if($order_by==4){
                return $service->where('check',0)->orderBy('created_at','asc')->paginate(5);
            }
        }
    }
    // Tìm kiếm trang home
    public function search(Request $Request){
        $search_type = $Request->search_type; // Kiểu search
        $id_brand = $Request->search_brand_car;
        $seats = $Request->search_seat_car;
        $price = $Request->search_price_car;
        $city = $Request->search_city_car;
        if($search_type == null && $id_brand == null && $seats == null && $price == null && $city == null)
        {
            return Redirect::to('/');
        }
        
        if($search_type == 0 ){
            echo $name = $Request->search_name_car;
        }
        if($search_type == 1){
            echo $name = $Request->search_name_phu_tung;
        }
        if($search_type == 2){
            echo $name = $Request->search_name_dich_vi;
        }
        $order_by = $Request->get('order_by'); //Kiểu sắp xếp
        $type_show = $Request->get('type_show'); // Kiểu hiển thị (dọc,ngang)  
        
        $poster = $this->getData($name,$id_brand,$seats,$price,$city,$order_by,$type_show,$search_type);
    	return view('client.ads.list_ads_search',['poster'=>$poster,'id_brand'=>$id_brand,'seats'=>$seats,'price'=>$price,'city'=>$city,'order_by'=>$order_by,'type_show'=>$type_show,'name'=>$name,'search_type'=>$search_type]);   
    }
    // Xử lí xắp xếp quảng cáo theo tiền, lượt quan tâm và sao
    public function order_by(Request $Request){
    	$seats = $Request->get('seats');
        $id_brand = $Request->get('id_brand');
        $price = $Request->get('price');
        $city = $Request->get('city');
        $order_by = $Request->get('order_by');
        $poster = $this->getData($id_brand,$seats,$price,$city,$order_by);
        $type_show = $Request->get('type_show');
        return view('client.home.search',['poster'=>$poster,'id_brand'=>$id_brand,'seats'=>$seats,'price'=>$price,'city'=>$city,'type_show'=>$type_show,'order_by'=>$order_by]);
    }
    // // Xử lí bộ lọc
    public function filter(Request $Request){
    	if( ($Request->checkbox1 == null) && ($Request->price_min == null) && ($Request->price_max == null) && ($Request->radio22 == null) && ($Request->checkbox11 == null) && ($Request->checkbox13 == null)){
    		return back()->with('alert',"Hãy điền thông tin cho bộ lọc");
    	}else{
	    	//Thương hiệu
	    	$string_brand = "";
	    	if($Request->checkbox1 != null){
	    		$string_brand = "brand_id = '";
	    		for($i = 0;$i<count($Request->checkbox1);$i++){
	    			if($i!=(count($Request->checkbox1)-1)){
	    				$string_brand .= $Request->checkbox1[$i]."' or brand_id = '";	
	    			}else{
	    				$string_brand .= $Request->checkbox1[$i]."' ";
	    			}
	    		}
	    	}
	    	// Khoảng giá
	    	$string_price = "";
	    	if(($Request->price_min == null) && ($Request->price_max != null)){
	    		$string_price = "price <= '".$Request->price_max."'";
	    	}
	    	if(($Request->price_min != null) && ($Request->price_max == null)){
	    		$string_price = "price >= '".$Request->price_min."'";
	    	}
	    	if(($Request->price_min != null) && ($Request->price_max != null)){
	    		$string_price = "price BETWEEN ".$Request->price_min." AND ".$Request->price_max." ";
	    	}
	    	// status
	    	$string_status = "";
	    	if($Request->radio22 == 1){
	    		$string_status = "check_new = '0' ";
	    	}
	    	if($Request->radio22 == 2){
	    		$string_status = "check_new = '1' ";
	    	}
	    	if($Request->radio22 == 3){
	    		$string_status = "check_new = '1' or check_new = '0' ";
	    	}
	    	// Kiểu nhiên liệu
	    	$string_feul = "";
	    	if($Request->checkbox11 != null){
	    		$string_feul = "feul_type = '";
	    		for($i = 0;$i<count($Request->checkbox11);$i++){
	    			if($i!=(count($Request->checkbox11)-1)){
	    				$string_feul .= $Request->checkbox11[$i]."' or feul_type = ' ";	
	    			}else{
	    				$string_feul .= $Request->checkbox11[$i]."' ";
	    			}
	    		}
	    	}
	    	// Kiểu body
	    	$string_body = "";
	    	if($Request->checkbox12 != null){
	    		$string_body = "category_id = '";
	    		for($i = 0;$i<count($Request->checkbox12);$i++){
	    			if($i!=(count($Request->checkbox12)-1)){
	    				$string_body .= $Request->checkbox12[$i]."' or category_id = ' ";	
	    			}else{
	    				$string_body .= $Request->checkbox12[$i]."' ";
	    			}
	    		}
	    	}
	    	// Kiểu hộp số
	    	$string_tranmission= "";
	    	if($Request->checkbox13 != null){
	    		$string_tranmission = "transmission_type = '";
	    		for($i = 0;$i<count($Request->checkbox13);$i++){
	    			if($i!=(count($Request->checkbox13)-1)){
	    				$string_tranmission .= $Request->checkbox13[$i]."' or transmission_type = '";	
	    			}else{
	    				$string_tranmission .= $Request->checkbox13[$i]."'";
	    			}
	    		}
	    	}
	    	$data = filter($string_brand,$string_price,$string_status,$string_feul,$string_body,$string_tranmission);
	    	$Request->session()->put('data', $data);
	    	return redirect()->route('list_ads');
	    }
    }
}
