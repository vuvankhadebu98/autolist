<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Comment;
use App\Model\Blog;
use Auth;

class CommentController extends Controller
{
   	public function postComment(Request $request,$id)
    {
       $blog_id = $id;
       $blog =Blog::find($id); 
       $comment = new Comment;
       $comment->blog_id = $blog_id;
       $comment->user_id = Auth::user()->id;
       $comment->content = $request->noidung;
       $comment->save();
       return redirect(route('detailBlog'.$id))->with('thongbao','Viết bình luận thành công');

    }
}
