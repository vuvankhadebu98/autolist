<?php

namespace App\Http\Controllers\admin\home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    public function index(){
        $user_admin = User::where('role_id','>','2')->get();
        $user_customer = User::where('role_id',1)->get();
        $user_role = User::where('id',Auth::user()->id)->leftjoin('roles','users.role_id','=','roles.id')->select('users.id','roles.name as role_name');
        return view('admin.index',['user_admin'=>$user_admin,'user_customer'=>$user_customer,'user_role' =>$user_role]);
    }
}



