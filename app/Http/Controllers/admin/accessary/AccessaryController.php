<?php

namespace App\Http\Controllers\admin\accessary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use validator;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\accessary;
use Illuminate\Support\Facades\Auth;

class AccessaryController extends Controller
{
	 // tìm kiếm....................................
    public function search_accessary(Request $request){
        $all_accessary = accessary::where('name','like','%'.$request->key.'%')
        ->get();
        return view('admin.accessary.search_accessary',compact('all_accessary'));
    }

    // lọc theo phụ tùng bán bằng ajax
    public function select_accessary_0(){
        $all_accessary = accessary::where('check',0)->get();
        return view('admin.accessary.ajax_accessary.ajax_accessary_0',compact('all_accessary'));
    }
    // lọc theo phụ tùng quảng cáo bằng ajax
    public function select_accessary_1(){
        $all_accessary = accessary::where('check',1)->get();
        return view('admin.accessary.ajax_accessary.ajax_accessary_1',compact('all_accessary'));
    }

    // add_accessary........................................................
    public function add_accessary(){
    	$user = DB::table('users')->orderby('id','desc')->get();
        return view('admin.accessary.add_accessary')->with('user',$user);
	}

    //all_accessary.........................................................
    public function all_accessary(){
        $all_accessary  = accessary::paginate(5);
        $manager_accessary = view('admin.accessary.all_accessary')->with('all_accessary', $all_accessary);
        return view('admin.master')->with('admin.accessary.all_accessary',$manager_accessary);
    }

    //edit_accessary.......................................................
    public function edit_accessary($id){
        $user = DB::table('users')->orderby('id','desc')->get();
        $edit_accessary = DB::table('accessaries')->where('id',$id)->get();

        $manager_accessary = view('admin.accessary.edit_accessary')
        ->with('edit_accessary', $edit_accessary)->with('user',$user);
        return view('admin.master')->with('admin.accessary.edit_accessary',$manager_accessary);      
    }

    //unactive_accessary-------------------------------------------------------
    public function unactive_accessary($id){
        DB::table('accessaries')->where('id', $id)->update(['status'=>1]);
        Session::put('message','kích hoạt phụ tùng thành công !');
        return Redirect::to('all-accessary');
    }
    //active_accessary..........................................................
    public function active_accessary($id){
        DB::table('accessaries')->where('id', $id)->update(['status'=>0]);
        Session::put('message','không kích hoạt phụ tùng thành công !');
        return Redirect::to('all-accessary');
    }
    
    // save_accessary...........................................................
    public function save_accessary(Request $request){
         $this->validate($request, [
         	'code'=>'required|max:10',
            'name'=>'required|min:3',
            'img'=>'required',
            'price'=>'required|max:20',
            'rate'=>'required',
            'view'=>'required',
            'description'=>'required|max:150',
            'content'=>'required',

        ], [
        	'code.required'=>'Bạn chưa nhập mã phụ tùng',
        	'code.max'=>'Bạn chỉ được nhập tối thiểu là 10 ký tự',
            'name.required'=>'Bạn chưa nhập tên phụ tùng',
            'name.min'=>'Tên phải lớn hơn 3 kí tự',
            'img.required'=>'Bạn chưa chọn hình ảnh',
            'price.required'=>'Bạn chưa nhập giá của phụ tùng',
            'rate.required'=>'Bạn chưa nhập đánh giá của phùng',
            'view.required'=>'Bạn chưa nhập lượt xem của phụ tùng',
            'description.required'=>'Bạn chưa nhập mô tả phụ tùng',
            'description.max'=>'Bạn chỉ được nhập tối thiểu 150 ký tự',
            'content.required'=>'Bạn chưa nhập nội dung phụ tùng',
            ]
        );
        $accessary = new accessary;
        $accessary->user_id   = $request->user_admin;
        $accessary->code = $request->code;
        $accessary->name   = $request->name;
        $accessary->price = $request->price;
        $accessary->rate = $request->rate;
        $accessary->view = $request->view;
        $accessary->description  = $request->description;
        $accessary->content  = $request->content;
        $accessary->package = $request->package;
        $accessary->check =$request->check;
        $accessary->status   = $request->status;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('img_accessary',$new_image);
            $accessary->img = $new_image;
        }else{
            $accessary->img = '';
        }
        $accessary->save();
        Session::put('message','thêm phụ tùng thành công');
        return Redirect::to('add-accessary');
    }

    // update_prudct..........................................................
    public function update_accessary(Request $request,$id){
         $this->validate($request, [
            'code'=>'required|max:10',
            'name'=>'required|min:3',
            'price'=>'required|max:20',
            'rate'=>'required',
            'view'=>'required',
            'description'=>'required|max:150',
            'content'=>'required',
        ], [
            'code.required'=>'Bạn chưa nhập mã phụ tùng',
            'code.max'=>'Bạn chỉ được nhập tối thiểu là 10 ký tự',
            'name.required'=>'Bạn chưa nhập tên phụ tùng',
            'name.min'=>'Tên phải lớn hơn 3 kí tự',
            'price.required'=>'Bạn chưa nhập giá của phụ tùng',
            'rate.required'=>'Bạn chưa nhập đánh giá của phùng',
            'view.required'=>'Bạn chưa nhập lượt xem của phụ tùng',
            'description.required'=>'Bạn chưa nhập mô tả phụ tùng',
            'description.max'=>'Bạn chỉ được nhập tối thiểu 150 ký tự',
            'content.required'=>'Bạn chưa nhập nội dung phụ tùng',
            ]
        );
        $accessary = accessary::find($id);
        $accessary->user_id   = $request->user_admin;
        $accessary->code = $request->code;
        $accessary->name   = $request->name;
        $accessary->price = $request->price;
        $accessary->rate = $request->rate;
        $accessary->view = $request->view;
        $accessary->description  = $request->description;
        $accessary->content  = $request->content;
        $accessary->package = $request->package;
        $accessary->check =$request->check;
        $accessary->status   = $request->status;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('img_accessary',$new_image);
            $accessary->img = $new_image;
        }
        $accessary->update();
        Session::put('message','cập nhật phụ tùng thành công');
        return Redirect::to('all-accessary');
    }
     // delete_accessary.........................................................
    public function delete_accessary($id){
        DB::table('accessaries')->where('id',$id)->delete();
        Session::put('message','xóa tùng thành công !');
        return Redirect::to('all-accessary');
    }   
}
