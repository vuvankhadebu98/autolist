<?php

namespace App\Http\Controllers\admin\category_blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Category_blog;
use Illuminate\Support\Facades\Auth;

class CategoryBlogController extends Controller
{
    // tìm kiếm danh mục category-blog
    public function search_category_blog(Request $request){
        $all_category_blog = Category_blog::where('name','like','%'.$request->key.'%')->get();
        return view('admin.category_blog.search_category_blog',compact('all_category_blog'));
    }
    // thêm mới danh mục category-blog
    public function add_category_blog(){
        return view('admin.category_blog.add_category_blog');
	}

    //hiển thị danh mục category-blog.........................................................
    public function all_category_blog(){
        $all_category_blog  = Category_blog::paginate(5);
        $manager_category_blog = view('admin.category_blog.all_category_blog')
        ->with('all_category_blog', $all_category_blog);
    	return view('admin.master')->with('admin.category_blog.all_category_blog',$manager_category_blog);
    }

    // //edit_category_blog.......................................................
    public function edit_category_blog($id){

        $edit_category_blog = DB::table('category_blog')->where('id',$id)->get();
        $manager_category_blog = view('admin.category_blog.edit_category_blog')
        ->with('edit_category_blog', $edit_category_blog);
        return view('admin.master')->with('admin.category_blog.edit_category_blog',$manager_category_blog);      
    }

    // save_category_blog...........................................................
    public function save_category_blog(Request $request){
        $this->validate($request, [
            'name'=>'required',
        ], [
             'name.required'=>'Bạn chưa nhập tên danh mục blog',
             'name.min'=>'Mật khẩu phải lớn hơn 3 kí tự'   
            ]
        );
        $data = array();
        $data['name']   = $request->name;

        DB::table('category_blog')->insert($data);
        Session::put('message','thêm danh mục blog thành công');
        return Redirect::to('add-category-blog');
    }

    // update_prudct..........................................................
    public function update_category_blog(Request $request,$id ){
        $data = array();
        $data['name']   = $request->name;
        DB::table('category_blog')->where('id',$id)->update($data);
        Session::put('message','cập nhật danh mục blog thành công');
        return Redirect::to('all-category-blog');
    }
     // delete_blog.........................................................
    public function delete_category_blog($id){
        DB::table('category_blog')->where('id',$id)->delete();
        Session::put('message','xóa danh mục blog thành công !');
        return Redirect::to('all-category-blog');
    }

}
