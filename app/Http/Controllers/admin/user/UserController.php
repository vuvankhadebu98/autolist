<?php

namespace App\Http\Controllers\admin\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Province;
use Illuminate\Support\Facades\Auth;
use Hash;
use DB;
use App\Model\Role_Has_Permission;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index()
    {
        $user =  User::all();
        $user_customer = User::where('role_id',1)->get();
        $user_role = Role::where('id',">",2)->get();
        $user_admin = DB::table('users')->where('role_id',">",2)->leftjoin('roles','users.role_id','=','roles.id')->select('users.id','users.firstname','users.lastname','users.img','roles.name as role_name','users.email','users.status','users.created_at','users.updated_at')->get();
        return view('admin.user.list_user',['user' =>$user,'user_admin' =>$user_admin,'user_customer'=>$user_customer,'user_role'=>$user_role]);
    }
    // tìm kiếm user.................................................................
    public function search_user(Request $request){

    $list_user = User::where('lastname','like','%'.$request->key.'%')
                ->orWhere('firstname','like','%'.$request->key.'%')
                ->orwhere('id',$request->key)->get();
    return view('admin.user.search_user',compact('list_user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'username'=>'required',
            'password'=>'required|min:3|max:32',
        ], [
             'username.required'=>'Bạn chưa nhập tài khoản',
             'password.required'=>'Bạn chưa nhập mật khẩu',
             'password.min'=>'Mật khẩu phải lớn hơn 3 kí tự',
             'password.max'=>'Mật khẩu phải nhỏ hơn 32 kí tự'   
            ]
        );
        $username = $request->username;
        $password = $request->password;

        if(Auth::attempt(['username' => $username, 'password' => $password])){
            return redirect('/admin/user-index');
        }else{
            return back()->with('thongbao','Đăng nhập thất bại');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Hiện thị chi tiết user
    public function show($id)
    {
        $detail_User = User::where('id', $id)->get();
        $user_role = User::where('id',Auth::user()->id)->leftjoin('roles','users.role_id','=','roles.id')->select('users.id','roles.name as role_name');
        return view('admin.user.detail_User',['detail_User'=>$detail_User,'user_role'=>$user_role]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Kiểm tra đăng nhập
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'username'=>'required',
            'password'=>'required|min:3|max:32',
            'email'=>'required',
            'phone'=>'required',
            'firstname'=>'required',
            'lastname'=>'required'
        ], [
             'username.required'=>'Bạn chưa nhập tài khoản',
             'password.required'=>'Bạn chưa nhập mật khẩu',
             'password.min'=>'Mật khẩu phải lớn hơn 3 kí tự',
             'password.max'=>'Mật khẩu phải nhỏ hơn 32 kí tự',   
             'email.required'=>'Bạn chưa nhập email',
             'phone.required'=>'Bạn chưa nhập số điện thoại',
             'firstname.required' =>'Bạn chưa nhập tên',
             'lastname.required'=>'Bạn chưa nhập họ'
            ]
        );
        if($request->password!=$request->repassword){
            return back()->with('error_pass','Xác nhận mật khẩu không khớp');
        }else{
            $user_id= User::insertGetId([
                'username'=>$request->username,
                'email'=>$request->email,
                'img'=>'default.jpg',
                'phone' =>$request->phone,
                'firstname' =>$request->firstname,
                'lastname' =>$request->lastname,
                'sex' =>$request->sex,
                'password' =>bcrypt($request->password),
                'role_id' =>$request->role,
                'status' =>0,
            ]);    
            $user = User::find($user_id);
            $role = Role_Has_Permission::where('role_id',$request->role)->rightjoin('permissions','role_has_permissions.permission_id','=','permissions.id')->select('permissions.name as permission_name')->get();
        
            foreach ($role as $r){
                $user->givePermissionTo($r->permission_name);
            }
            return back()->with('thongbao','Tạo tài khoản thành công');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function logOut(){
        Auth::logout();
        return view('admin.user.login');
    }
    public function add_Admin(){
        $role = Role::all();
        return view('admin.user.add_admin',['role' => $role]);
    }
    public function lock_Admin(Request $request){
        $id = $request->id;
        foreach($id as $i){
            User::where('id',$i)->update([
                'status' =>'Bị Khóa',
            ]);
        }
        return $request->get('id');
    }

    public function unlock_Admin(Request $request){
        $id = $request->id;
        foreach($id as $i){
            User::where('id',$i)->update([
                'status' =>'Đang Hoạt Động',
            ]);
        }
        return $request->get('id');
    }
    public function login(){
        return view('admin.user.login');
    }
    public function get_edit_admin($id){
        $province = Province::all();
        $user_detail = User::where('id',$id)->get();
        
        return view('admin.user.edit_User',['user_detail'=>$user_detail,'province'=>$province]);
    }
    
    public function edit_pass(Request $request){  
        $this->validate($request, [
            'password'=>'required',
            'new_password'=>'required',
        ], [
             'password.required'=>'Bạn chưa nhập mật khẩu cũ',
             'new_password.required'=>'Bạn chưa nhập mật khẩu mới',
            ]
        );
           if(Hash::check($request->password,Auth::user()->password)){
                User::where('id',Auth::user()->id)->update([
                    'password'=>bcrypt($request->new_password)
                ]);
                return back()->with('success','Đổi mật khẩu thành công');
            }else{
                return back()->with('thatbai','Mật khẩu không đúng');
            }
    }

    public function update_user(Request $request){
        $this->validate($request, [
            'firstname'=>'required',
            'lastname'=>'required',
            'phone'=>'required|numeric',
            'sex'=>'required',
            'url_face'=>'required',
            'city'=>'required',
            'address'=>'required',
            'detail_aboutme'=>'required',
        ], [
             'firstname.required'=>'Bạn chưa nhập tên',
             'lastname.required'=>'Bạn chưa họ',
             'phone.required'=>'Bạn chưa nhập số điện thoại',
             'phone.numeric'=>'Số điện thoại phải là chữ số',
             'sex.required'=>'Bạn chưa chọn giới tính',   
             'url_face.required'=>'Bạn chưa nhập địa chỉ FaceBook',
             'city.required'=>'Bạn chưa chọn thành phố',
             'address.required' =>'Bạn chưa nhập địa chỉ',
             'detail_aboutme.required'=>'Bạn chưa nhập thông tin chi tiết'
            ]
        );
        User::where('id',Auth::user()->id)->update([
            'firstname'=>$request->firstname,
            'lastname'=>$request->lastname,
            'phone'=>$request->phone,
            'sex' =>$request->sex,
            'url_face'=>$request->url_face,
            'address' =>$request->address,
            'city' =>$request->city,
            'detail_aboutme'=>$request->detail_aboutme,
            
        ]);
        return back()->with('success','Đổi thông tin thành công');
    }
}