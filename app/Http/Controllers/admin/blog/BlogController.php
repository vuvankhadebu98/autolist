<?php

namespace App\Http\Controllers\admin\blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use validator;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Blog;
use App\Model\Blog_tag;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{   
    // tìm kiếm....................................
    public function search_blog(Request $request){
        $all_blog = Blog::where('name','like','%'.$request->key.'%')->get();
        return view('admin.blog.search_blog',compact('all_blog'));
    }

    // tìm kiếm theo category bằng ajax
    public function load_list_cate_blog(Request $request){
        $all_blog = Blog::where('category_blog_id',$request->get('value'))->get();
        return view('admin.blog.ajax_blog.ajax_blog_category',compact('all_blog'));
    }

    // add_blog........................................................
    public function add_blog(){
    	$user = DB::table('users')->orderby('id','desc')->get();
        $category_blog = DB::table('category_blog')->orderby('id','desc')->get();
        $tag = DB::table('tags')->orderby('id','desc')->get();  
        return view('admin.blog.add_blog')->with('user',$user)
        ->with('category_blog',$category_blog)->with('tag',$tag);
	}

    //all_blog.........................................................
    public function all_blog(){
        $all_blog  = Blog::paginate(5);
        $category_blog = DB::table('category_blog')->orderby('id','desc')->get();
        $tag = DB::table('tags')->orderby('id','desc')->get();
        $manager_blog = view('admin.blog.all_blog')->with('all_blog', $all_blog)
        ->with('category_blog',$category_blog)->with('tag',$tag);

        return view('admin.master')->with('admin.blog.all_blog',$manager_blog);
    }

    //edit_blog.......................................................
    public function edit_blog($id){
        $user = DB::table('users')->orderby('id','desc')->get();
        $category_blog = DB::table('category_blog')->orderby('id','desc')->get();
        $tag = DB::table('tags')->orderby('id','desc')->get();
        $edit_blog = DB::table('blogs')->where('id',$id)->get();

        $manager_blog = view('admin.blog.edit_blog')->with('edit_blog', $edit_blog)
        ->with('user',$user)->with('category_blog',$category_blog)->with('tag',$tag);
        return view('admin.master')->with('admin.blog.edit_blog',$manager_blog);      
    }

    //unactive_blog-------------------------------------------------------
    public function unactive_blog($id){
        DB::table('blogs')->where('id', $id)->update(['status'=>1]);
        Session::put('message','kích hoạt blog thành công !');
        return Redirect::to('all-blog');
    }
    //active_blog..........................................................
    public function active_blog($id){
        DB::table('blogs')->where('id', $id)->update(['status'=>0]);
        Session::put('message','không kích hoạt blog thành công !');
        return Redirect::to('all-blog');
    }
    
    // save_blog...........................................................
    public function save_blog(Request $request){
        $this->validate($request, [
            'name'=>'required|min:3',
            'img'=>'required',
            'description'=>'required|max:150',
            'content'=>'required',

        ], [
             'name.required'=>'Bạn chưa nhập tên blog',
             'img.required'=>'Bạn chưa chọn hình ảnh',
             'description.required'=>'Bạn chưa nhập mô tả blog',
             'description.max'=>'Bạn chỉ được nhập tối thiểu 150 ký tự',
             'content.required'=>'Bạn chưa nhập nội dung blog',
             'name.min'=>'Tên phải lớn hơn 3 kí tự' ,  
            ]
        );
        $blog = new blog;
        $blog->name   = $request->name;
        $blog->user_id   = $request->user_admin;
        $blog->category_blog_id = $request->category_bl;
        $blog->description  = $request->description;
        $blog->content  = $request->content;
        $blog->status   = $request->status;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('img_blog',$new_image);
            $blog->img = $new_image;
        }else{
            $blog->img = '';
        }
        $blog->save();
        $blog_tag = new blog_tag;
        $blog_tag->blog_id = $blog->id;
        $blog_tag->tag_id  = $request->tag_id;
        $blog_tag->save();
        Session::put('message','thêm blog thành công');
        return Redirect::to('add-blog');
    }

    // update_prudct..........................................................
    public function update_blog(Request $request,$id){
        $this->validate($request, [
            'name'=>'required|min:3',
            'description'=>'required|max:150',
            'content'=>'required',
        ], [
             'name.required'=>'Bạn chưa nhập tên blog',
             'description.required'=>'Bạn chưa nhập mô tả blog',
             'description.max'=>'Bạn chỉ được nhập tối thiểu 150 ký tự',
             'content.required'=>'Bạn chưa nhập nội dung blog',
             'name.min'=>'Tên phải lớn hơn 3 kí tự' ,    
            ]
        );
        $blog = Blog::find($id);
        $blog->name   = $request->name;
        $blog->user_id   = $request->user_admin;
        $blog->category_blog_id = $request->category_bl;
        $blog->description  = $request->description;
        $blog->content = $request->content;
        $blog->status   = $request->status;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('img_blog',$new_image);
            $blog->img = $new_image;   
        }
        $blog->update();
        $blog_tag = Blog_tag::where('blog_id',$id)->first();
        $blog_tag->blog_id = $blog->id;
        $blog_tag->tag_id  = $request->tag_id;
        $blog_tag->update();
        Session::put('message','cập nhật blog thành công');
        return Redirect::to('all-blog');
    }
     // delete_blog.........................................................
    public function delete_blog($id){
        $blog_tag = Blog_tag::where('blog_id',$id)->first();
        $blog_tag->delete();
        DB::table('blogs')->where('id',$id)->delete();
        Session::put('message','xóa Blog thành công !');
        return Redirect::to('all-blog');
    }
}
