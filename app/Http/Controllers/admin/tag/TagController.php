<?php

namespace App\Http\Controllers\admin\tag;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Tag;
use App\Model\Blog_tag;
use App\Model\Product_tag;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    // search_tag.................................................
     public function search_tag(Request $request){
        $all_tag = Tag::where('name','like','%'.$request->key.'%')->get();
        return view('admin.tag.search_tag',compact('all_tag'));
    }
    // tìm kiếm bằng ajax với tag
    public function select_active_product(){
        $all_tag = Tag::where('check',1)->get();
        return view('admin.tag.ajax_tag.ajax_tag_product',compact('all_tag'));
    }
    // tìm kiếm bằng ajax với tag
    public function select_active_blog(){

        $all_tag = Tag::where('check',2)->get();
        return view('admin.tag.ajax_tag.ajax_tag_blog',compact('all_tag'));
    }
    // tìm kiếm bằng ajax với tag
    public function select_active_poster(){

        $all_tag = Tag::where('check',3)->get();
        return view('admin.tag.ajax_tag.ajax_tag_poster',compact('all_tag'));
    }
    // tìm kiếm bằng ajax với tag
    public function select_active_accessaries(){

        $all_tag = Tag::where('check',4)->get();
        return view('admin.tag.ajax_tag.ajax_tag_accessaries',compact('all_tag'));
    }
	// add_tag....................................................
    public function add_tag(){  
        return view('admin.tag.add_tag');
	}

    //all_tag.........................................................
    public function all_tag(){
        $all_tag  = Tag::paginate(5); 
        $manager_tag = view('admin.tag.all_tag')->with('all_tag', $all_tag);
        return view('admin.master')->with('admin.tag.all_tag',$manager_tag);
    }

    //edit_tag.......................................................
    public function edit_tag($id){
        $edit_tag = DB::table('tags')->where('id',$id)->get();
        $manager_tag = view('admin.tag.edit_tag')->with('edit_tag', $edit_tag);
        return view('admin.master')->with('admin.tag.edit_tag',$manager_tag);      
    }

    // save_tag...........................................................
    public function save_tag(Request $request){
        $this->validate($request, [
            'name'=>'required',
        ], [
             'name.required'=>'Bạn chưa nhập tên tag',
             'name.min'=>'Mật khẩu phải lớn hơn 3 kí tự'   
            ]
        );
        $tag = new tag;
        $tag->name   = $request->name;
        $tag->check  = $request->check;
        $tag->save();
        Session::put('message','thêm tag thành công');
        return Redirect::to('add-tag');
    }

    // update_tag..........................................................
    public function update_tag(Request $request,$id){
        $tag = Tag::find($id);
        $tag->name   = $request->name;
        $tag->check  = $request->check;
        $tag->update();
        Session::put('message','cập nhật tag thành công');
        return Redirect::to('all-tag');
    }
     // delete_tag.........................................................
    public function delete_tag($id){
        DB::table('tags')->where('id',$id)->delete();
        Session::put('message','xóa Tag thành công !');
        return Redirect::to('all-tag');
    }

}
