<?php

namespace App\Http\Controllers\admin\poster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Poster;
use Illuminate\Support\Facades\Auth;

class PosterController extends Controller
{
	// tìm kiếm poster
    public function search_poster(Request $request){
        $all_poster = Poster::where('title','like','%'.$request->key.'%')
        ->get();
        return view('admin.poster.search_poster',compact('all_poster'));
    }
    // tìm kiếm theo category bằng ajax
    public function load_list_cate_poster(Request $request){
        $all_poster = Poster::where('category_id',$request->get('value'))->get();
        return view('admin.poster.ajax_poster.ajax_poster_category',compact('all_poster'));
    }
    // tìm kiếm theo brand bằng ajax
    public function load_list_brand_poster(Request $request){
        $all_poster = Poster::where('brand_id',$request->get('value'))->get();
        return view('admin.poster.ajax_poster.ajax_poster_brand',compact('all_poster'));
    }
    // add_poster........................................................
    public function add_poster(){
    	$user = DB::table('users')->orderby('id','desc')->get();
        $category = DB::table('categories')->where('check',0)->orderby('id','desc')->get();
        $brand = DB::table('brand')->orderby('id','desc')->get();
        return view('admin.poster.add_poster')->with('user',$user)
        ->with('category',$category)->with('brand',$brand);
	}
	//all_poster..............................................................
	public function all_poster(){
        $all_poster  = Poster::paginate(5);
        $user = DB::table('users')->orderby('id','desc')->get();
        $category = DB::table('categories')->orderby('id','desc')->get();
        $brand = DB::table('brand')->orderby('id','desc')->get();
        $manager_poster = view('admin.poster.all_poster')->with('all_poster',$all_poster)
        ->with('user',$user)->with('category',$category)->with('brand',$brand);

        return view('admin.master')->with('admin.poster.all_poster',$manager_poster);
    } 
    // edit_poster.................................................................
    public function edit_poster($id){
        $user = DB::table('users')->orderby('id','desc')->get();
        $category = DB::table('categories')->orderby('id','desc')->get();
        $brand = DB::table('brand')->orderby('id','desc')->get();
        $edit_poster = DB::table('poster')->where('id',$id)->get();

        $manager_poster = view('admin.poster.edit_poster')->with('edit_poster', $edit_poster)
        ->with('user',$user)->with('category',$category)->with('brand',$brand);
        return view('admin.master')->with('admin.poster.edit_poster',$manager_poster);      
    }

    //unactive_poster-------------------------------------------------------
    public function unactive_poster($id){
        DB::table('poster')->where('id', $id)->update(['status'=>1]);
        Session::put('message','kích hoạt quảng cáo thành công !');
        return Redirect::to('all-poster');
    }
    //active_poster..........................................................
    public function active_poster($id){
        DB::table('poster')->where('id', $id)->update(['status'=>0]);
        Session::put('message','không kích hoạt quảng cáo thành công !');
        return Redirect::to('all-poster');
    }
    
    // save_blog...........................................................
    public function save_poster(Request $request){
        $this->validate($request, [
            'img'=>'required',
            'title'=>'required|min:3',
            'price'=>'required|min:3|max:20',
            'seats'=>'required|max:3',
            'km_went'=>'required',
            'promotion'=>'required',
            'description'=>'required|min:5|max:150',
            'content'=>'required|min:5',
            'engine'=>'required',
            'enegine_displacement'=>'required',
            'fuel_tank_capacity'=>'required|max:5',
            'break'=>'required',
            'bootspace'=>'required',
            'arai_milage'=>'required',
            'max_torque'=>'required',
            'max_power'=>'required',
            'like'=>'required',
            'rate'=>'required',
            'view'=>'required',
            'video'=>'required',
        ], [
             'title.required'=>'Bạn chưa nhập tên quảng cáo',
             'title.min'=>'Bạn Phải nhập ít nhất là 3 ký tự trở lên',
             'img.required'=>'Bạn chưa chọn hình ảnh',
             'price.required'=>'Bạn chưa nhập giá quảng cáo',
             'price.min'=>'Bạn phải nhập ít nhất 3 ký t',
             'price.max'=>'Bạn chỉ được nhập tối thiểu là 20 ký tự',
             'seats.required'=>'Bạn chưa nhập số ghế cho quảng cáo',
             'seats.max'=>'Bạn chỉ được nhập tối thiểu là 3 ký tự',
             'km_went.required'=>'Bạn chưa nhập km đã đi được',
             'promotion.required'=>'Bạn chưa nhập vẫn tốc tối đa',
             'description.required'=>'Bạn chưa nhập mô tả quảng cáo',
             'description.min'=>'Bạn phải nhập ít nhất 3 ký tự trở lên',
             'description.max'=>'Bạn phải nhập tối thiểu 150 ký tự',
             'content.required'=>'Bạn chưa nhập nội dung quảng cáo',
             'content.min'=>'Bạn phải nhập ít nhất 3 ký tự trở lên',
             'engine.required'=>'Bạn chưa nhập động cơ',
             'enegine_displacement.required'=>'Bạn chưa nhập hệ thống động cơ',
             'fuel_tank_capacity.required'=>'Bạn chưa nhập dung tích bình xăng',
             'fuel_tank_capacity.max'=>'Bạn chị được nhập tối thiểu 5 ký tự',

             'break.required'=>'Bạn chưa nhập break',
             'bootspace.required'=>'Bạn chưa nhập không gian khởi động',
             'arai_milage.required'=>'Tên chưa nhập ô arai_milage',
             'max_torque.required'=>'Bạn chưa nhập momen xoắn cực đại',
             'max_power.required'=>'Bạn chưa nhập công xuất tối đa',
             'like.required'=>'Bạn chưa nhập ô yêu thích',
             'rate.required'=>'Bạn chưa nhập ô tỉ lệ',
             'view.required'=>'Bạn chưa nhập lượt xem',
             'video.required'=>'Bạn chưa điền link video',
            ]
        );
        $poster = new poster;
        $poster->id_user   = $request->user_admin;
        $poster->brand_id = $request->brand_sp;
        $poster->category_id = $request->category_sp;
        $poster->title  = $request->title;
        $poster->price  = $request->price;
        $poster->seats  = $request->seats;
        $poster->km_went  = $request->km_went;
        $poster->promotion  = $request->promotion;
        $poster->fuel_type  = $request->fuel_type;
        $poster->transmission_type  = $request->transmission_type;
        $poster->description  = $request->description;
        $poster->content  = $request->content;
        $poster->engine  = $request->engine;
        $poster->enegine_displacement  = $request->enegine_displacement;
        $poster->fuel_tank_capacity  = $request->fuel_tank_capacity;
        $poster->break  = $request->break;
        $poster->bootspace = $request->bootspace;
        $poster->arai_milage = $request->arai_milage;
        $poster->max_torque = $request->max_torque;
        $poster->max_power = $request->max_power;
        $poster->like = $request->like;
        $poster->rate = $request->rate;
        $poster->view = $request->view;
        $poster->video = $request->video;
        $poster->package   = $request->package;
        $poster->check   = $request->check;
        $poster->check_new   = $request->check_new;
        $poster->status   = $request->status;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('img_poster',$new_image);
            $poster->img = $new_image;
        }else{
            $poster->img = '';
        }
        $poster->save();
        Session::put('message','thêm Quảng cáo thành công');
        return Redirect::to('add-poster');
    }

    // update_prudct..........................................................
    public function update_poster(Request $request,$id){
        $poster = Poster::find($id);
        $poster->id_user   = $request->user_admin;
        $poster->brand_id = $request->brand_sp;
        $poster->category_id = $request->category_sp;
        $poster->title  = $request->title;
        $poster->price  = $request->price;
        $poster->seats  = $request->seats;
        $poster->km_went  = $request->km_went;
        $poster->promotion  = $request->promotion;
        $poster->fuel_type  = $request->fuel_type;
        $poster->transmission_type  = $request->transmission_type;
        $poster->description  = $request->description;
        $poster->content  = $request->content;
        $poster->engine  = $request->engine;
        $poster->enegine_displacement  = $request->enegine_displacement;
        $poster->fuel_tank_capacity  = $request->fuel_tank_capacity;
        $poster->break  = $request->break;
        $poster->bootspace = $request->bootspace;
        $poster->arai_milage = $request->arai_milage;
        $poster->max_torque = $request->max_torque;
        $poster->max_power = $request->max_power;
        $poster->like = $request->like;
        $poster->rate = $request->rate;
        $poster->view = $request->view;
        $poster->video = $request->video;
        $poster->package   = $request->package;
        $poster->check   = $request->check;
        $poster->check_new   = $request->check_new;
        $poster->status   = $request->status;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('img_poster',$new_image);
            $poster->img = $new_image;
        }
        $poster->update();
        Session::put('message','Cập Nhật quảng cáo thành công');
        return Redirect::to('all-poster');
    }
     // delete_blog.........................................................
    public function delete_poster($id){
        DB::table('poster')->where('id',$id)->delete();
        Session::put('message','xóa quảng cáo thành công !');
        return Redirect::to('all-poster');
    }

}
