<?php

namespace App\Http\Controllers\admin\contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Model\Contact;

class ContactController extends Controller
{
    // tìm kiếm theo liên hệ
    public function search_contact(Request $request){
        $all_contact = Contact::where('name','like','%'.$request->key.'%')->get();
        return view('admin.contact.search_contact',compact('all_contact'));
    }
    // show tất cả các liên hệ
    public function select_load_contact(){
        $all_contact = Contact::all();   
        return view('admin.contact.ajax_contact.ajax_load_contact',compact('all_contact'));
    }
    // tìm kiếm bằng ajax với contact active
    public function select_active_contact(){
        $all_contact = Contact::where('status',1)->get();
        return view('admin.contact.ajax_contact.ajax_contact_active',compact('all_contact'));
    }
    // tìm kiếm bằng ajax với contact unactive
    public function select_unactive_contact(){
        $all_contact = Contact::where('status',0)->get();
        return view('admin.contact.ajax_contact.ajax_contact_unactive',compact('all_contact'));
    }
    // trang liên hệ
    public function list_Contact(){
    $all_contact = Contact::paginate(5);
    return view('admin.contact.list',compact('all_contact'));
    }
    //unactive_contact-------------------------------------------------------
    public function unactive_contact($id){
        DB::table('contacts')->where('id', $id)->update(['status'=>1]);
        Session::put('message','kích hoạt liên hệ thành công !');
        return Redirect::to('list-contact');
    }
    //active_blog..........................................................
    public function active_contact($id){
        DB::table('contacts')->where('id', $id)->update(['status'=>0]);
        Session::put('message','không kích hoạt liên hệ thành công !');
        return Redirect::to('list-contact');
    }
     // delete_contact.........................................................
    public function delete_contact($id){
        DB::table('contacts')->where('id',$id)->delete();
        Session::put('message','xóa liên hệ thành công !');
        return Redirect::to('list-contact');
    }
}
