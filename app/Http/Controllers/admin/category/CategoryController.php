<?php
namespace App\Http\Controllers\admin\category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use validator;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Category;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{

    // tìm kiếm....................................
    public function search_category(Request $request){
        $all_category = Category::where('name','like','%'.$request->key.'%')->get();
        return view('admin.category.search_category',compact('all_category'));
    }
    // tìm kiếm ajax_sản phẩm...........................................
      public function select_active_category(){

        $all_category = Category::where('check',0)->get();
        return view('admin.category.ajax_category.ajax_category_active',compact('all_category'));
    }
    // tìm kiếm theo ajax_phụ tùng
    public function select_active_category_1(){

        $all_category = Category::where('check',1)->get();
        return view('admin.category.ajax_category.ajax_category_active_1',compact('all_category'));
    }
    // add_category........................................................
    public function add_category(){
        return view('admin.category.add_category');
  }
    //all_category.........................................................
    public function all_category(){
        $all_category  = Category::paginate(5);
        $manager_category = view('admin.category.all_category')->with('all_category', $all_category);
        return view('admin.master')->with('admin.category.all_category',$manager_category);
    }

    //edit_category.......................................................
    public function edit_category($id){
        $edit_category = DB::table('categories')->where('id',$id)->get();
        $manager_category = view('admin.category.edit_category')->with('edit_category', $edit_category);
        return view('admin.master')->with('admin.category.edit_category',$manager_category);     
    }

    //unactive_category-------------------------------------------------------
    public function unactive_category($id){
        DB::table('categories')->where('id', $id)->update(['check'=>1]);
        Session::put('message','kích hoạt thương hiệu thành công !');
        return Redirect::to('all-category');
    }
    //active_category..........................................................
    public function active_category($id){
        DB::table('categories')->where('id', $id)->update(['check'=>0]);
        Session::put('message','không kích hoạt thương hiệu thành công !');
        return Redirect::to('all-category');
    }
    
    // save_category...........................................................
    public function save_category(Request $request){
         $this->validate($request, [
            'name'=>'required|min:3',
            'img'=>'required',
            'check'=>'required'
        ], [
             'name.required'=>'Bạn chưa nhập tên danh mục',
             'img.required'=>'Bạn chưa chọn hình ảnh',
             'check.required'=>'Bạn chưa chọn tình trạng danh mục',
             'name.min'=>'Tên phải lớn hơn 3 kí tự'   
            ]
        );
        $category = new Category;
        $category->name   = $request->name;
        $category->check   = $request->check;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('img_category',$new_image);
            $category->img = $new_image;
        }else{
            $category->img = '';
        }
        $category->save();
        Session::put('message','thêm danh mục thành công');
        return Redirect::to('add-category');
    }

    // update_prudct..........................................................
    public function update_category(Request $request,$id){
        $category = Category::find($id);
        $category->name   = $request->name;
        $category->check   = $request->check;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('img_category',$new_image);
            $category->img = $new_image;   
        }
        $category->update();
        Session::put('message','cập nhật danh mục thành công');
        return Redirect::to('all-category');
    }
     // delete_category.........................................................
    public function delete_category($id){
        DB::table('categories')->where('id',$id)->delete();
        Session::put('message','xóa thương hiệu thành công !');
        return Redirect::to('all-category');
    }
}
