<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Attribute;
use Illuminate\Support\Str;
use App\ProductValue;
use App\Model\Product;
use App\Model\Category;
use App\Model\Brand;
use Illuminate\Support\Facades\Auth;
use Validator;
use RealRashid\SweetAlert\Facades\Alert;
use App\Model\Tag;
use DateTime;

class ProductController extends Controller
{
    //Thêm sản phẩm
	public function get_create_product(){
		$tags = Tag::where('check',1)->get();
		$attribute = Attribute::all();
		$category = Category::where('check',0)->get();
		$brand = Brand::all();
		return view('admin.product.add_product',compact('category','attribute','brand','tags'));
	}
	public function post_create_product(Request $request){
		// dd($request->all());
		$this->validate($request, [
            'product_name'=>'required|min:3|max:100',
            'seats'=>'required|max:3',
            'boot_space'=>'required',
            'arai_milege'=>'required',
            'max_torque'=>'required',
            'price'=>'required|min:3|max:15',
            'promotion'=>'required|max:5',
            'egn_displace'=>'required',
            'max_power'=>'required|max:6',
            'engine'=>'required',
            'fuel_tank'=>'required|max:5',
            'break'=>'required',
            'video_url'=>'required',
            'product_description'=>'required|min:5|max:150',
            'content'=>'required',
            'checkbox'=>'required',
            'tags'=>'required',
            'images'=>'required',
        ], [
             'product_name.required'=>'Bạn chưa nhập tên Sản Phẩm',
             'product_name.min'=>'Bạn phải nhập ít nhất 3 ký tự',
             'product_name.max'=>'Bạn chỉ được nhập dưới 100 ký tự',
             'seats.required'=>'Bạn chưa nhập số ghế',
             'seats.max'=>'Bạn chỉ được nhập dưới 3 ký tự',
             'boot_space.required'=>'Bạn chưa nhập không gian khởi động',
             'arai_milege.required'=>'Bạn chưa nhập arai_milege',
             'max_torque.required'=>'Bạn chưa nhập arai_milege',
             'price.required'=>'Bạn chưa nhập giá sản phẩm',
             'price.min'=>'Bạn phải nhập ít nhất 3 ký tự',
             'price.max'=>'Bạn chỉ được nhập tối thiểu 15 ký tự',
             'promotion.required'=>'Bạn chưa nhập ưu đãi',
             'promotion.max'=>'Bạn chỉ được nhập tối thiểu 5 ký tự',
             'egn_displace.required'=>'Bạn chưa nhập egn_displace',

             'max_power.required'=>'Bạn chưa nhập công xuất tối đa',
             'max_power.max'=>'Bạn chỉ được nhập tối thiểu 6 ký tự',
             'engine.required'=>'Bạn chưa nhập động cơ',

             'fuel_tank.required'=>'Bạn chưa nhập dung tích bình xăng',
             'fuel_tank.max'=>'Bạn chỉ được nhập tối thiểu 5 ký tự',
             'break.required'=>'Bạn chưa nhập bộ hãm phanh',
             'video_url.required'=>'Bạn chưa điền link video',

             'product_description.required'=>'Bạn chưa nhập mô tả sản phẩm',
             'product_description.min'=>'Bạn phải nhập ít nhất 5 ký tự',
             'product_description.max'=>'Bạn chỉ được nhập tối thiểu 150 ký tự',
             'content.required'=>'Bạn chưa nhập nội dung sản phẩm',
             'checkbox.required'=>'Bạn chưa chọn tính năng',
             'tags.required'=>'Bạn chưa chọn tags',
             'images.required'=>'Bạn chưa chọn hình ảnh',
            ]
        );
		$urlVideo = str_replace('watch?v=', 'embed/', $request->video_url);

		if (!empty($request->tags)) {
    		$tags = implode(',',$request->tags);
    	}
		// dd($request->all());

		if($request->hasFile('images')){
			$array_image = [];
			foreach($request->file('images') as $file_name){
				$uniqueFileName = Str::random(6).'_'.$file_name->getClientOriginalName();
				array_push($array_image, $uniqueFileName);
				$file_name->move(public_path('img_product/'),$uniqueFileName);
			}
			if($array_image){
			
				$image = implode(',', $array_image);	
			}
			$product = Product::insertGetId([
			'name' 					=>  $request->product_name,
			'category_id' 			=>	$request->category_name,
			'brand_id' 				=>	$request->brand_name,
			'price' 				=>	$request->price,
			'promotion'				=> 	$request->promotion,
			'seats' 				=>	$request->seats,
			'image_product' 		=> 	$image,
			'user_id'				=> 	Auth::user()->id,
			'description' 			=> 	$request->product_description,
			'content'				=>	$request->content,
			'engine' 				=>	$request->engine,
			'video'					=>  $urlVideo,
			'bootspace' 			=>	$request->boot_space,
			'arai_milage' 			=>	$request->arai_milege,
			'enegine_displacement' 	=>	$request->egn_displace,
			'max_torque' 			=>	$request->max_torque,
			'max_power' 			=>	$request->max_power,
			'fuel_type'				=> 	$request->fuel_type,
			'fuel_tank_capacity' 	=>	$request->fuel_tank,
			'tags'					=>  $tags,
			'check_sp'				=>  $request->check_ps,
			'check_new'				=>  $request->check_new,
			'transmission_type' 	=>	$request->transmission,
			'break'					=>  $request->break,
			'created_at'			=>  new DateTime(),
			]);
		}
		return redirect()->back()->with('success','Thêm sản phẩm thành công');		
	}
    //Kết thúc thêm sản phẩm

    //Danh sách sản phẩm
    public function get_list_product(){
    	
    	$products = Product::orderBy('id','desc')->paginate(5);
    	$category_product = Category::orderBy('id','desc');
    	$brand_product = Brand::orderBy('id','desc');
    	return view('admin.product.list_product',compact('products','category_product','brand_product'));
    }
    public function get_list_active_product(){
    	$products = Product::where('status',1)->get();
    	return view('admin.product.ajax_load_list.ajax_active_product',compact('products'));
    }
    public function load_list_cate_product(Request $request){
    	$products = Product::where('category_id',$request->get('value'))->get();
    	return view('admin.product.ajax_load_list.ajax_cate_product',compact('products'));
    }

    public function load_list_brand_product(Request $request){
    	$products = Product::where('brand_id',$request->get('value'))->get();
    	return view('admin.product.ajax_load_list.ajax_brand_product',compact('products'));
    }

    //Cập nhật sản phẩm
    public function get_edit_product($id_product){
    	$product = Product::find($id_product);
    	$tags = Tag::where('check',1)->get();
    	$attribute = Attribute::all();
		$category = Category::all();
		$brand = Brand::all();
    	return view('admin.product.edit_product',compact('product','category','brand','attribute','tags'));
    }

    public function post_edit_product(Request $request, $id_product){
    	$this->validate($request, [
            'product_name'=>'required|min:3|max:100',
            'seats'=>'required|max:3',
            'boot_space'=>'required',
            'arai_milege'=>'required',
            'max_torque'=>'required',
            'price'=>'required|min:3|max:15',
            'promotion'=>'required|max:5',
            'egn_displace'=>'required',
            'max_power'=>'required|max:6',
            'engine'=>'required',
            'fuel_tank'=>'required|max:5',
            'break'=>'required',
            'video_url'=>'required',
            'product_description'=>'required|min:5|max:150',
            'content'=>'required',
        ], [
             'product_name.required'=>'Bạn chưa nhập tên Sản Phẩm',
             'product_name.min'=>'Bạn phải nhập ít nhất 3 ký tự',
             'product_name.max'=>'Bạn chỉ được nhập dưới 100 ký tự',
             'seats.required'=>'Bạn chưa nhập số ghế',
             'seats.max'=>'Bạn chỉ được nhập dưới 3 ký tự',
             'boot_space.required'=>'Bạn chưa nhập không gian khởi động',
             'arai_milege.required'=>'Bạn chưa nhập arai_milege',
             'max_torque.required'=>'Bạn chưa nhập arai_milege',
             'price.required'=>'Bạn chưa nhập giá sản phẩm',
             'price.min'=>'Bạn phải nhập ít nhất 3 ký tự',
             'price.max'=>'Bạn chỉ được nhập tối thiểu 15 ký tự',
             'promotion.required'=>'Bạn chưa nhập ưu đãi',
             'promotion.max'=>'Bạn chỉ được nhập tối thiểu 5 ký tự',
             'egn_displace.required'=>'Bạn chưa nhập egn_displace',

             'max_power.required'=>'Bạn chưa nhập công xuất tối đa',
             'max_power.max'=>'Bạn chỉ được nhập tối thiểu 6 ký tự',
             'engine.required'=>'Bạn chưa nhập động cơ',

             'fuel_tank.required'=>'Bạn chưa nhập dung tích bình xăng',
             'fuel_tank.max'=>'Bạn chỉ được nhập tối thiểu 5 ký tự',
             'break.required'=>'Bạn chưa nhập bộ hãm phanh',
             'video_url.required'=>'Bạn chưa điền link video',

             'product_description.required'=>'Bạn chưa nhập mô tả sản phẩm',
             'product_description.min'=>'Bạn phải nhập ít nhất 5 ký tự',
             'product_description.max'=>'Bạn chỉ được nhập tối thiểu 150 ký tự',
             'content.required'=>'Bạn chưa nhập nội dung sản phẩm',
            ]
        );

    	$urlVideo = str_replace('watch?v=', 'embed/', $request->video_url);
    	
    	$product = Product::find($id_product);
    	if (!empty($request->tags)) {
    		$tags = implode(',',$request->tags);
    		$product->tags 	=	$tags;
    	}

    	if($request->hasFile('images')){
			$array_image = [];
			foreach($request->file('images') as $file_name){
				$uniqueFileName = Str::random(6).'_'.$file_name->getClientOriginalName();
				array_push($array_image, $uniqueFileName);
				$file_name->move(public_path('img_product/'),$uniqueFileName);
			}
			if($array_image){
				$image = implode(',', $array_image);

				$product->image_product = 	$image;
			}
		}
		$product->name 					=  $request->product_name;
		$product->category_id 			=	$request->category_name;
		$product->brand_id 				=	$request->brand_name;
		$product->price 				=	$request->price;
		$product->promotion				= 	$request->promotion;
		$product->seats 				=	$request->seats;
		$product->description 			= 	$request->product_description;
		$product->content 				= 	$request->content;
		$product->engine 				=	$request->engine;
		$product->video 				=   $urlVideo;
		$product->bootspace 			=	$request->boot_space;
		$product->arai_milage 			=	$request->arai_milege;
		$product->enegine_displacement 	=	$request->egn_displace;
		$product->max_torque 			=	$request->max_torque;
		$product->max_power 			=	$request->max_power;
		$product->fuel_type				= 	$request->fuel_type;
		$product->fuel_tank_capacity 	=	$request->fuel_tank;
		$product->check_sp  				= 	$request->check_sp;
		$product->check_new 			= 	$request->check_new;
		$product->transmission_type 	=	$request->transmission;
		$product->break					=   $request->break;		
		$product->save();
		ProductValue::where('product_id',$id_product)->delete();

		return redirect()->route('get-list-pro')->with('success','Cập nhật thành công');
    }

    //Kết thúc cập nhật

    //Xóa sản phẩm

    public function delete_product($id_product){
    	 $product = Product::find($id_product);
    	 $imgs = explode(',',$product->image_product);
    	 foreach ($imgs as $key => $img) {
    	 	unlink(public_path('img_product/'.$img));
    	 }
    	 ProductValue::where('product_id',$id_product)->delete();
    	  
    	
    	$product->delete();
    	 
    	return redirect()->route('get-list-pro')->with('success','Xóa sản phẩm thành công');
    }
    //Duyệt && Khóa duyệt sản phẩm
    public function active_pro_status($id_product){
		Product::where('id',$id_product)->update(['status'=>1]);
		return redirect()->route('get-list-pro')->with('toast_success','Duyệt sản phẩm thành công');
	}
	public function unactive_pro_status($id_product){
		Product::where('id',$id_product)->update(['status'=>0]);
		return redirect()->route('get-list-pro')->with('toast_success','Bỏ duyệt sản phẩm thành công');
	}

	//Tìm kiếm

	public function search_product(Request $request){
		$key = $request->input('search');
		// dd($key);
		$products = Product::where('name','like','%'.$key.'%')->paginate(10);

		return view('admin.product.search_product',compact('products'));
	}


}
