<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Attribute;
use Illuminate\Support\Str;
use App\ProductValue;
use App\Model\Product;
use App\Model\Category;
use App\Model\Thuong_hieu;
use Illuminate\Support\Facades\Auth;
use Validator;

class PropertiesController extends Controller
{
    //Thêm thuộc tính sản phẩm

	public function get_create_properties(){
		return view('admin.product.properties.add_properties');
	} 

	public function post_create_properties(Request $request){

		$validate = Validator::make($request->all(),
			[
				'attribute_name' => 'required',
			]
			
		);

		if($validate->fails()){
			return redirect()->back()->with('error','Không được để trống tên thuộc tính');
		}
		else{

			$attribute = Attribute::insert([
				'attribute_name' => $request->attribute_name,  
			]);
			// $value = [];
			// foreach ($request->value_atrb as $key => $atrb) {
			// 	array_push($value,$atrb);
			// 	$val = explode(',',$atrb);
			// }

			// foreach ($val as $key => $values) {
			// 	AttributeValue::insert([
			// 		'attribute_id' => $attribute,
			// 		'value' => $values,
			// 	]);	
			// }
			return redirect()->back()->with('success','Thêm thuộc tính thành công');
		}
		
			
		
		// return redirect()->back();
	}
    //Kết thúc thêm thuộc tính sản phẩm

    //Danh sách thuộc tính
    public function get_list(){
    	$properties = Attribute::orderBy('id','desc')->paginate(10);
    	return view('admin.product.properties.list_properties',compact('properties'));
    }

    public function get_edit($attribute_id){
    	$property = Attribute::find($attribute_id);
    	return view('admin.product.properties.edit_property',compact('property'));
    }
    public function post_edit(Request $request ,$attribute_id){
    	$property = Attribute::find($attribute_id);
    	$validate = Validator::make($request->all(),
			[
				'attribute_name' => 'required',
			]
			
		);

		if($validate->fails()){
			return redirect()->back()->with('error','Không được để trống tên thuộc tính');
		}
		else{

			$property->attribute_name = $request->attribute_name;
			$property->save();
			return redirect()->route('get-list-properties')->with('success','Cập nhật thuộc tính thành công');
   		 }
	}
    public function delete_property($attribute_id){
    	Attribute::where('id',$attribute_id)->delete();
    	return redirect()->back()->with('success','Xóa thuộc tính thành công');
    }
}
