<?php

namespace App\Http\Controllers\admin\information;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Information;
use Illuminate\Support\Facades\Auth;


class InformationController extends Controller
{
	// tìm kiểm......................................................
	public function search_information(Request $request){
        $all_information = Information::where('name','like','%'.$request->key.'%')->get();
        return view('admin.information.search_information',compact('all_information'));
    }

	// add_information.............................................................
    public function add_information(){
    	return view('admin.information.add_information');
    }
    // all_information......................................................................
    public function all_information(){
    	$all_information = Information::paginate(5);
    	$manager_information = view('admin.information.all_information')->with('all_information',$all_information);
    	return view('admin.master')->with('admin.information.all_information',$manager_information);
    }
    // edit..............................................................................................
    public function edit_information($id){
    	$edit_information = DB::table('information')->where('id',$id)->get();
    	$manager_information = view('admin.information.edit_information')->with('edit_information',$edit_information);
    	return view('admin.master')->with('admin.information.edit_information',$manager_information);
    }
    // unactive_information.............................................................................
    public function unactive_information($id){
    	DB::table('information')->where('id',$id)->update(['status'=> 1]);
    	Session::put('messge','kích hoạt thông tin thành công !');
    	return Redirect::to('all-information');
    }
    // active_information.................................................
    public function active_information($id){
    	DB::table('information')->where('id',$id)->update(['status'=> 0]);
    	Session::put('messge','Không kích hoạt thông thin thành công !');
    	return Redirect::to('all-information');
    }
    // save_information.................................................
    public function save_information(Request $request){
    	$this->validate($request,[
    		'name'=>'required|min:3|max:50',
    		'url'=>'required',
    		'phone'=>'required|min:9|max:11',
    		'address'=>'required|min:10|max:200'
    	], [
    		'name.required'=>'bạn chưa nhập tên thông tin',
    		'url.required'=>'bạn chưa điền đường đẫn',
    		'phone.required'=>'bạn chưa điền số điện thoại',
    		'address.required'=>'bạn chưa điền địa chỉ',
    		'name.min'=>'Tên phải lớn hơn 3 ký tự',
    		'name.max'=>'Tên phải ít hơn 50 ký tự',
    		'phone.min'=>'số điện thoại phải ít nhất 9 ký tự',
    		'phone.max'=>'số điện thoại phải ít hơn 11 ký tự',
    		'address.min'=>'số điện thoại phải tít nhất 10 ký tự',
    		'address.max'=>'số điện thoại phải ít hơn 200 ký tự'
    	   ]
    	);
    	$information = new information;
    	$information->name = $request->name;
    	$information->url = $request->url;
    	$information->phone = $request->phone;
    	$information->address =$request->address;
    	$information->status = $request->status;
    	$information->save();
    	Session::put('message','Thêm Thông tin thành công');
    	return Redirect::to('add-information');
    }
    // update_information...................................................
    public function update_information(Request $request,$id){
    	$information = Information::find($id);
    	$information->name = $request->name;
    	$information->url = $request->url;
    	$information->phone = $request->phone;
    	$information->address =$request->address;
    	$information->status = $request->status;
    	$information->update();
    	Session::put('message','Cập nhật Thông tin thành công');
    	return Redirect::to('all-information');
    }
    public function delete_information($id){
    	DB::table('information')->where('id',$id)->delete();
    	Session::put('message','xóa thông tin thành công');
    	return Redirect::to('all-information');
    }
}
