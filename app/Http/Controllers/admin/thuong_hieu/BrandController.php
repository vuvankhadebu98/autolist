<?php

namespace App\Http\Controllers\admin\thuong_hieu;
use App\Model\Brand;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CreateRequest;

class BrandController extends Controller
{
     public function index_brand()
   {   
    $brand = Brand::orderBy('created_at','asc') ->get();
    return view('admin.thuong_hieu.index_brand',['brand'=> $brand, ]); 
   }
    public function create_brand()
    {
     return view('admin.thuong_hieu.create_brand');
     
    }
     public function store_brand(CreateRequest $request){
       $brand = new Brand();
       $brand['name']= $request->get('name');
      

       $file = $request->file('img');
       $file_info = new \stdClass();
       $file_info->name = $file->getClientOriginalName();
       $file_info->extension = $file->getClientOriginalExtension();
       $file_info->path = $file->getRealPath();
       $file_info->size = $file->getSize();
       $file_info->mime = $file->getMimeType();

       
       $destinationPath = 'img_thuong_hieu';
       $file->move($destinationPath,$file->getClientOriginalName());

       $file_info->link_img = ''.$file->getClientOriginalName();
       $brand['img']=$file_info->link_img;

       $brand->save();
     return redirect()->route('admin.thuong_hieu.index');

    }

    public function edit_brand($id)
    {
        $brand = Brand::find($id);
        return view('admin.thuong_hieu.edit_brand', ['brand' => $brand ]);
    }
 
    public function update_brand(CreateRequest $request,$id)
    {
      $brand = Brand::find($id);
      $params = [];
      $params['name'] = request()->get('name');
      
      $file = $request->file('img');
      if ($file){
        $file_info = new \stdClass();
        $file_info->name = $file->getClientOriginalName();
        $file_info->extension = $file->getClientOriginalExtension();
        $file_info->path = $file->getRealPath();
        $file_info->size = $file->getSize();
        $file_info->mime = $file->getMimeType();

        
        $destinationPath = 'img_thuong_hieu';
        $file->move($destinationPath,$file->getClientOriginalName());

        $file_info->link_img = ''.$file->getClientOriginalName();
      //  $file->customUploadFile($_FILES['img'],$thuong_hieus->img);
        $params['img']=$file_info->link_img;
      }
      else{
        $brand->img='';
      }

      $brand->update($params);
     return redirect()->route('admin.thuong_hieu.index');
    }
 
 
    public function destroy(Brand $brand)
    {
     $brand->delete();
     return redirect()->route('admin.thuong_hieu.index');
    }


 

}
