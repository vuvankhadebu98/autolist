<?php

namespace App\Http\Controllers\admin\brand;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use validator;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Brand;
use Illuminate\Support\Facades\Auth;

class BrandController extends Controller
{
     // tìm kiếm....................................
    public function search_brand(Request $request){
        $all_brand = Brand::where('name','like','%'.$request->key.'%')->get();
        return view('admin.brand.search_brand',compact('all_brand'));
    }

    // add_brand........................................................
    public function add_brand(){
        return view('admin.brand.add_brand');
	}
    //all_brand.........................................................
    public function all_brand(){
        $all_brand  = Brand::paginate(5);
        $manager_brand = view('admin.brand.all_brand')->with('all_brand', $all_brand);
        return view('admin.master')->with('admin.brand.all_brand',$manager_brand);
    }

    //edit_brand.......................................................
    public function edit_brand($id){
        $edit_brand = DB::table('brand')->where('id',$id)->get();
        $manager_brand = view('admin.brand.edit_brand')->with('edit_brand', $edit_brand);
        return view('admin.master')->with('admin.brand.edit_brand',$manager_brand);     
    }

    //unactive_brand-------------------------------------------------------
    public function unactive_brand($id){
        DB::table('brand')->where('id', $id)->update(['check'=>1]);
        Session::put('message','kích hoạt thương hiệu thành công !');
        return Redirect::to('all-brand');
    }
    //active_brand..........................................................
    public function active_brand($id){
        DB::table('brand')->where('id', $id)->update(['check'=>0]);
        Session::put('message','không kích hoạt thương hiệu thành công !');
        return Redirect::to('all-brand');
    }
    
    // save_brand...........................................................
    public function save_brand(Request $request){
         $this->validate($request, [
            'name'=>'required|min:3',
            'img'=>'required',
            'check'=>'required'
        ], [
             'name.required'=>'Bạn chưa nhập tên thương hiệu',
             'img.required'=>'Bạn chưa chọn hình ảnh',
             'check.required'=>'Bạn chưa chọn tình trạng thương hiệu',
             'name.min'=>'Tên phải lớn hơn 3 kí tự'   
            ]
        );
        $brand = new brand;
        $brand->name   = $request->name;
        $brand->check   = $request->check;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('img_brand',$new_image);
            $brand->img = $new_image;
        }else{
            $brand->img = '';
        }
        $brand->save();
        Session::put('message','thêm thương Hiệu thành công');
        return Redirect::to('add-brand');
    }

    // update_prudct..........................................................
    public function update_brand(Request $request,$id){
        $brand = Brand::find($id);
        $brand->name   = $request->name;
        $brand->check   = $request->check;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('img_brand',$new_image);
            $brand->img = $new_image;   
        }
        $brand->update();
        Session::put('message','cập nhật thương hiệu thành công');
        return Redirect::to('all-brand');
    }
     // delete_brand.........................................................
    public function delete_brand($id){
        DB::table('brand')->where('id',$id)->delete();
        Session::put('message','xóa thương hiệu thành công !');
        return Redirect::to('all-brand');
    }
}


