<?php

namespace App\Http\Controllers\admin\order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Order;
use App\Model\Order_Detail;
use App\Model\Product;
use App\User;
class OrderController extends Controller
{
    public function get_view_order(){

    	$order_view = Order::orderBy('id','desc')->paginate(10);
    	
    	return view('admin.order.index',compact('order_view'));
    }

    public function get_load_list_order(Request $request){
    	$order_view = Order::orderBy('id','desc')->paginate($request->get('value'));
    	return view('admin.order.ajax_order.list_apply_order',compact('order_view'));
    }
   
    public function active_order_status($id_order){
		Order::where('id',$id_order)->update(['status'=>1]);
		return redirect()->route('get.view.order')->with('toast_success','Duyệt đơn hàng thành công');
	}
	public function load_list_active_order(){
		$order_view = Order::where('status',1)->get();
    	
    	return view('admin.order.ajax_order.list_active',compact('order_view'));
	}

    public function load_list_import_order(){
        $order_view = Order::where('check',0)->get();
        return view('admin.order.ajax_order.import_order',compact('order_view'));
    }

    public function load_list_sale_order(){
        $order_view = Order::where('check',1)->get();
        return view('admin.order.ajax_order.sale_order',compact('order_view'));
    }

    public function search_order(Request $request){
        $key = $request->search;
        
        // $search = User::where('firstname','like','%'.$key.'%')->get();
        // dd($search);
        // foreach($search as $s){
        //     $order_view = Order::where('customer_id',$s->id)->get();
        //     dd($order_view);
        // }

        $order_view = Order::where('name','like','%'.$key.'%')->paginate(10);
        
        // dd($order_view);
        return view('admin.order.search',compact('order_view'));
    }
	
}
