<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductValue extends Model
{
    public $timestamps = false;
    protected $fillable = [
    	'product_id','attribute_id','value'
    ];
    protected $primaryKey = 'id';
    protected $table = 'product_values';
}
